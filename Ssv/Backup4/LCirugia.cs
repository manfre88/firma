﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LCirugia
    {

        public DataSet PuntualidadQR(string FechaIni, string FechaFin, string Tipo)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Tipo"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Tipo
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            if (Tipo == "CIR")
            {
                Tipo = "AND G.SER_OBJ_CODIGO IN('SA1     ','SA2     ','SA3     ','SA4     ','SA5     ','SA6     ','SA7     ') ";
            }
            else 
            {
                if (Tipo == "CAR")
                {
                    Tipo = "AND G.SER_OBJ_CODIGO IN('SA8     ','SA9     ','S91     ') ";
                }
                else 
                {
                    Tipo = "";
                }
            }

            string sql = "select identificacion, paciente, n_orden, ambito, tipsolicitud, fecha_orden, fecha_cirugia,SALA,hora_prog, horaingreso,round ((to_number(to_date(min(decode(hora_prog,'24:00','00:00',hora_prog)),'HH24:MI') -TO_DATE(horaingreso,'HH24:MI' ))*24*60),2 ) puntualidad from " +
                                "(select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) PACIENTE, " +
                                          "A.ORDNUMERO N_ORDEN, decode(a.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO, " +
                                          "DECODE(a.SOLQRF,'U','URGENTE','E','ELECTIVA','') TIPSOLICITUD, F.ORDFECHA FECHA_ORDEN, " +
                                          "decode(TRIM(G.SER_OBJ_CODIGO),'SA1','SALA 1','SA2','SALA 2','SA3','SALA 3','SA4','SALA 4','SA5','SALA 5','SA6','SALA 6','SA7','SALA 7','SA8','SALA 8','SA9','SALA 9','S91','SALA 10',TRIM(G.SER_OBJ_CODIGO) ) SALA, " +
                                          "G.FECQRF FECHA_CIRUGIA, " +
                                          "( select  min(HORQRF) from TabPrgQrfDet b where  G.pac_pac_numero = E.pac_pac_numero  and  b.OrdTipo = F.OrdTipo And  b.OrdNumero = F.OrdNumero) hora_prog, " +
                                          "A.HORAINI horaingreso " +
                                "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g " +
                                "where " +
                                   "a.ORDNUMERO=b.ORDNUMERO " +
                                   "and trunc(G.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO " +
                                   "and a.HORAFIN<>'00:00' " +
                                   "and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                                   "and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO " +
                                   "AND A.MTVCORRELATIVO = F.MTVCORRELATIVO " +
                                   "AND A.TABENCUENTRO = F.TABENCUENTRO " +
                                   "and A.ORDNUMERO = F.ORDNUMERO " +
                                   "and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO " +
                                   "and trim(e.PAC_PAC_RUT) not in ( '10','12') " +
                                   " " + Tipo + " " +
                                   "and A.ORDNUMERO = G.ORDNUMERO) " +
                        "group by identificacion, paciente, n_orden, ambito, tipsolicitud, fecha_orden, fecha_cirugia,hora_prog, horaingreso,SALA " +
                        "ORDER BY fecha_cirugia ";                       
                                                                               
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }

        public DataSet InfHorasQuirofano(string FechaI, string FechaF, string HoraI, string HoraF, string Sala)
        {
            string[] nomParam = 
            {
                "In_FechaI",
                "In_FechaF",
                "In_HoraI",
                "In_HoraF",
                "In_Sala"
            };

            object[] vlrParam = 
            {
                FechaI,
                FechaF,
                HoraI,
                HoraF,
                Sala
            };



            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if (HoraI.CompareTo(HoraF) != 1)
            {
                sql = "select distinct ID.TAB_TIPOIDENTGLOSA TIPO_ID, e.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) PACIENTE, " +
                                      "A.ORDNUMERO N_ORDEN,  F.ORDFECHA FECHA_ORDEN, " +
                                      "decode(TRIM(G.SER_OBJ_CODIGO),'SA1','SALA 1','SA2','SALA 2','SA3','SALA 3','SA4','SALA 4','SA5','SALA 5','SA6','SALA 6','SA7','SALA 7','SA8','SALA 8','SA9','SALA 9','S91','SALA 10',TRIM(G.SER_OBJ_CODIGO) ) SALA, " +
                                      "G.FECQRF FECHA_CIRUGIA,min(DET.HORQRF) INICIO_CIRUGIA,  g.HORQRF FIN_CIRUGIA, CONV.CON_CON_DESCRIPCIO CONVENIO " +
                       "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g,TAB_TIPOIDENT ID,TabPrgQrfdet det, CON_CONVENIO CONV " +
                       "where a.ORDNUMERO=b.ORDNUMERO " +
                               "and trunc(G.FECQRF) between to_date('" + FechaI + "','yyyy/mm/dd') and to_date('" + FechaF + "','yyyy/mm/dd') " +
                               "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO " +
                               "and a.HORAFIN<>'00:00' " +
                               "and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                               "AND E.PAC_PAC_TIPOIDENTCODIGO=ID.TAB_TIPOIDENTCODIGO " +
                               "and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO " +
                               "AND A.MTVCORRELATIVO = F.MTVCORRELATIVO " +
                               "AND A.TABENCUENTRO = F.TABENCUENTRO " +
                               "and A.ORDNUMERO = F.ORDNUMERO " +
                               "and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO " +
                               "and A.ORDNUMERO = G.ORDNUMERO " +
                               "and A.PAC_PAC_NUMERO = DET.PAC_PAC_NUMERO " +
                               "and A.ORDNUMERO = DET.ORDNUMERO " +
                               "and trim(e.PAC_PAC_RUT) not in ( '10','12') " +
                               "and A.CON_CON_CODIGO = CONV.CON_CON_CODIGO " +
                               "AND G.SER_OBJ_CODIGO IN('" + Sala + "') " +
                               "AND A.HORAINI between '" + HoraI + "' AND '" + HoraF + "' " +
                    "group by ID.TAB_TIPOIDENTGLOSA, e.PAC_PAC_RUT,E.PAC_PAC_NUMERO,A.ORDNUMERO,  F.ORDFECHA, G.SER_OBJ_CODIGO ,G.FECQRF, g.HORQRF,CONV.CON_CON_DESCRIPCIO " +
                    "ORDER BY 6,g.FECQRF, g.HORQRF ";
            }
            else
            {
                sql = "select distinct ID.TAB_TIPOIDENTGLOSA TIPO_ID, e.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) PACIENTE, " +
                                      "A.ORDNUMERO N_ORDEN,  F.ORDFECHA FECHA_ORDEN, " +
                                      "decode(TRIM(G.SER_OBJ_CODIGO),'SA1','SALA 1','SA2','SALA 2','SA3','SALA 3','SA4','SALA 4','SA5','SALA 5','SA6','SALA 6','SA7','SALA 7','SA8','SALA 8','SA9','SALA 9','S91','SALA 10',TRIM(G.SER_OBJ_CODIGO) ) SALA, " +
                                      "G.FECQRF FECHA_CIRUGIA,min(DET.HORQRF) INICIO_CIRUGIA,  g.HORQRF FIN_CIRUGIA, CONV.CON_CON_DESCRIPCIO CONVENIO " +
                       "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g,TAB_TIPOIDENT ID,TabPrgQrfdet det, CON_CONVENIO CONV " +
                       "where a.ORDNUMERO=b.ORDNUMERO " +
                               "and trunc(G.FECQRF) between to_date('" + FechaI + "','yyyy/mm/dd') and to_date('" + FechaF + "','yyyy/mm/dd') " +
                               "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO " +
                               "and a.HORAFIN<>'00:00' " +
                               "and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                               "AND E.PAC_PAC_TIPOIDENTCODIGO=ID.TAB_TIPOIDENTCODIGO " +
                               "and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO " +
                               "AND A.MTVCORRELATIVO = F.MTVCORRELATIVO " +
                               "AND A.TABENCUENTRO = F.TABENCUENTRO " +
                               "and A.ORDNUMERO = F.ORDNUMERO " +
                               "and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO " +
                               "and A.ORDNUMERO = G.ORDNUMERO " +
                               "and A.PAC_PAC_NUMERO = DET.PAC_PAC_NUMERO " +
                               "and A.ORDNUMERO = DET.ORDNUMERO " +
                               "and A.CON_CON_CODIGO = CONV.CON_CON_CODIGO " +
                               "and trim(e.PAC_PAC_RUT) not in ( '10','12') " +
                               "AND G.SER_OBJ_CODIGO IN('" + Sala + "') " +
                               "AND A.HORAINI between '" + HoraI + "' AND '24:00' " +
                        "group by ID.TAB_TIPOIDENTGLOSA, e.PAC_PAC_RUT,E.PAC_PAC_NUMERO,A.ORDNUMERO,  F.ORDFECHA, G.SER_OBJ_CODIGO ,G.FECQRF, g.HORQRF,CONV.CON_CON_DESCRIPCIO " +
                      "UNION ALL " +
                          "select distinct ID.TAB_TIPOIDENTGLOSA TIPO_ID, e.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) PACIENTE, " +
                                          "A.ORDNUMERO N_ORDEN,  F.ORDFECHA FECHA_ORDEN, " +
                                          "decode(TRIM(G.SER_OBJ_CODIGO),'SA1','SALA 1','SA2','SALA 2','SA3','SALA 3','SA4','SALA 4','SA5','SALA 5','SA6','SALA 6','SA7','SALA 7','SA8','SALA 8','SA9','SALA 9','S91','SALA 10',TRIM(G.SER_OBJ_CODIGO) ) SALA, " +
                                          "G.FECQRF FECHA_CIRUGIA,min(DET.HORQRF) INICIO_CIRUGIA,  g.HORQRF FIN_CIRUGIA, CONV.CON_CON_DESCRIPCIO CONVENIO " +
                           "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g,TAB_TIPOIDENT ID,TabPrgQrfdet det, CON_CONVENIO CONV " +
                           "where a.ORDNUMERO=b.ORDNUMERO " +
                                   "and trunc(G.FECQRF) between to_date('" + FechaI + "','yyyy/mm/dd') and to_date('" + FechaF + "','yyyy/mm/dd') " +
                                   "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO " +
                                   "and a.HORAFIN<>'00:00' " +
                                   "and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                                   "AND E.PAC_PAC_TIPOIDENTCODIGO=ID.TAB_TIPOIDENTCODIGO " +
                                   "and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO " +
                                   "AND A.MTVCORRELATIVO = F.MTVCORRELATIVO " +
                                   "AND A.TABENCUENTRO = F.TABENCUENTRO " +
                                   "and A.ORDNUMERO = F.ORDNUMERO " +
                                   "and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO " +
                                   "and A.ORDNUMERO = G.ORDNUMERO " +
                                   "and A.PAC_PAC_NUMERO = DET.PAC_PAC_NUMERO " +
                                   "and trim(e.PAC_PAC_RUT) not in ( '10','12') " +
                                   "and A.ORDNUMERO = DET.ORDNUMERO " +
                                   "and A.CON_CON_CODIGO = CONV.CON_CON_CODIGO " +
                                   "AND G.SER_OBJ_CODIGO IN('" + Sala + "') " +
                                   "AND A.HORAINI between '00:00' AND '" + HoraF + "' " +
                            "group by ID.TAB_TIPOIDENTGLOSA, e.PAC_PAC_RUT,E.PAC_PAC_NUMERO,A.ORDNUMERO,  F.ORDFECHA, G.SER_OBJ_CODIGO ,G.FECQRF, g.HORQRF,CONV.CON_CON_DESCRIPCIO " +
                      "ORDER BY 6,7,9 ";
            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }


        public DataSet InfTotalHorasQuir(string Sala)
        {
            string[] nomParam = 
            {
                "In_Sala"
            };

            object[] vlrParam = 
            {
                Sala
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select trim(sala) SALA, fecha_cirugia, sum(horas) horas from temp_ocuquiro group by sala, fecha_cirugia order by sala, fecha_cirugia";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public bool BorrarTemporal(string Temporal)
        {
            string[] nomParam = 
            { 
                "@Temporal"
            };

            object[] vlrParam = 
            { 
                Temporal 

            };

            string sql = "delete from " + Temporal + " ";

            return new DA.DA("Oracle").EjecutarSentencia(sql);
        }


        public bool PoblarTemporal(string FechaI, string FechaF, string HoraI, string HoraF, string Sala)
        {
            string[] nomParam = 
            {
                "In_FechaI",
                "In_FechaF",
                "In_HoraI",
                "In_HoraF",
                "In_Sala"
            };

            object[] vlrParam = 
            {
                FechaI,
                FechaF,
                HoraI,
                HoraF,
                Sala
            };


            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if (HoraI.CompareTo(HoraF) != 1)
            {
                sql = " insert into temp_ocuquiro ( SELECT decode(TRIM(OBJETO),'SA1','SALA 1','SA2','SALA 2','SA3','SALA 3','SA4','SALA 4','SA5','SALA 5','SA6','SALA 6','SA7','SALA 7','SA8','SALA 8','SA9','SALA 9','S91','SALA 10',TRIM(OBJETO) ) SALA, " +
                                                  "FECHA_CIRUGIA,COUNT(*)*15/60 HORAS  FROM " +
                                                       "(select distinct G.SER_OBJ_CODIGO OBJETO, G.FECQRF FECHA_CIRUGIA,(DET.HORQRF) HORAS " +
                                                       "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g,TAB_TIPOIDENT ID,TabPrgQrfdet det, CON_CONVENIO CONV " +
                                                       "where a.ORDNUMERO=b.ORDNUMERO and trunc(G.FECQRF) between to_date('" + FechaI + "','yyyy/mm/dd') and to_date('" + FechaF + "','yyyy/mm/dd') " +
                                                       "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO and a.HORAFIN<>'00:00' and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO AND E.PAC_PAC_TIPOIDENTCODIGO=ID.TAB_TIPOIDENTCODIGO " +
                                                       "and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO AND A.MTVCORRELATIVO = F.MTVCORRELATIVO AND A.TABENCUENTRO = F.TABENCUENTRO and A.ORDNUMERO = F.ORDNUMERO " +
                                                       "and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO and A.ORDNUMERO = G.ORDNUMERO and A.PAC_PAC_NUMERO = DET.PAC_PAC_NUMERO and A.ORDNUMERO = DET.ORDNUMERO " +
                                                       "and trim(e.PAC_PAC_RUT) not in ( '10','12') " +
                                                       "and A.CON_CON_CODIGO = CONV.CON_CON_CODIGO AND G.SER_OBJ_CODIGO IN('" + Sala + "') AND  G.HORQRF between '" + HoraI + "' AND '" + HoraF + "' " +
                                                       "group by G.FECQRF ,G.SER_OBJ_CODIGO,DET.HORQRF) " +
                                                   "GROUP BY OBJETO,FECHA_CIRUGIA); commit ";
            }
            else
            {
                sql = "insert into temp_ocuquiro ( SELECT decode(TRIM(OBJETO),'SA1','SALA 1','SA2','SALA 2','SA3','SALA 3','SA4','SALA 4','SA5','SALA 5','SA6','SALA 6','SA7','SALA 7','SA8','SALA 8','SA9','SALA 9','S91','SALA 10',TRIM(OBJETO) ) SALA, " +
                                                  "FECHA_CIRUGIA,COUNT(*)*15/60 HORAS  FROM " +
                                                       "(select distinct G.SER_OBJ_CODIGO OBJETO, G.FECQRF FECHA_CIRUGIA,(DET.HORQRF) HORAS " +
                                                       "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g,TAB_TIPOIDENT ID,TabPrgQrfdet det, CON_CONVENIO CONV " +
                                                       "where a.ORDNUMERO=b.ORDNUMERO and trunc(G.FECQRF) between to_date('" + FechaI + "','yyyy/mm/dd') and to_date('" + FechaF + "','yyyy/mm/dd') " +
                                                       "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO and a.HORAFIN<>'00:00' and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO AND E.PAC_PAC_TIPOIDENTCODIGO=ID.TAB_TIPOIDENTCODIGO " +
                                                       "and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO AND A.MTVCORRELATIVO = F.MTVCORRELATIVO AND A.TABENCUENTRO = F.TABENCUENTRO and A.ORDNUMERO = F.ORDNUMERO " +
                                                       "and trim(e.PAC_PAC_RUT) not in ( '10','12') " +
                                                       "and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO and A.ORDNUMERO = G.ORDNUMERO and A.PAC_PAC_NUMERO = DET.PAC_PAC_NUMERO and A.ORDNUMERO = DET.ORDNUMERO " +
                                                       "and A.CON_CON_CODIGO = CONV.CON_CON_CODIGO AND G.SER_OBJ_CODIGO IN('" + Sala + "') AND  G.HORQRF between '" + HoraI + "' AND '24:00' " +
                                                       "group by G.FECQRF ,G.SER_OBJ_CODIGO,DET.HORQRF) " +
                                                   "GROUP BY OBJETO,FECHA_CIRUGIA " +
                                                   "UNION ALL " +
                                                   "SELECT decode(TRIM(OBJETO),'SA1','SALA 1','SA2','SALA 2','SA3','SALA 3','SA4','SALA 4','SA5','SALA 5','SA6','SALA 6','SA7','SALA 7','SA8','SALA 8','SA9','SALA 9','S91','SALA 10',TRIM(OBJETO) ) SALA, " +
                                                   "FECHA_CIRUGIA,COUNT(*)*15/60 HORAS  FROM " +
                                                       "(select distinct G.SER_OBJ_CODIGO OBJETO, G.FECQRF FECHA_CIRUGIA,(DET.HORQRF) HORAS " +
                                                       "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g,TAB_TIPOIDENT ID,TabPrgQrfdet det, CON_CONVENIO CONV " +
                                                       "where a.ORDNUMERO=b.ORDNUMERO and trunc(G.FECQRF) between to_date('" + FechaI + "','yyyy/mm/dd') and to_date('" + FechaF + "','yyyy/mm/dd') " +
                                                       "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO and a.HORAFIN<>'00:00' and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO AND E.PAC_PAC_TIPOIDENTCODIGO=ID.TAB_TIPOIDENTCODIGO " +
                                                       "and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO AND A.MTVCORRELATIVO = F.MTVCORRELATIVO AND A.TABENCUENTRO = F.TABENCUENTRO and A.ORDNUMERO = F.ORDNUMERO " +
                                                       "and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO and A.ORDNUMERO = G.ORDNUMERO and A.PAC_PAC_NUMERO = DET.PAC_PAC_NUMERO and A.ORDNUMERO = DET.ORDNUMERO " +
                                                       "and trim(e.PAC_PAC_RUT) not in ( '10','12') " +
                                                       "and A.CON_CON_CODIGO = CONV.CON_CON_CODIGO AND G.SER_OBJ_CODIGO IN('" + Sala + "') AND  G.HORQRF between '00:00' AND '" + HoraF + "' " +
                                                       "group by G.FECQRF ,G.SER_OBJ_CODIGO,DET.HORQRF) " +
                                                   "GROUP BY OBJETO,FECHA_CIRUGIA); commit ";

            }

            return new DA.DA("Oracle").EjecutarSentencia(sql);
        }


        public DataSet OportunidadQXUrgente(string Especialidad, string FechaIni, string FechaFin, string Tipo)
        {
            string[] nomParam = 
            {
                "In_Especialidad",
                "In_FechaIni",
                "In_FechaFin",
                "In_Tipo"
            };

            object[] vlrParam = 
            {
                Especialidad,
                FechaIni,
                FechaFin,
                Tipo
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

             string sql = string.Empty;


             if (Tipo == "CIR")
             {
                 Tipo = "AND B.SER_OBJ_CODIGO IN('SA1     ','SA2     ','SA3     ','SA4     ','SA5     ','SA6     ','SA7     ') ";
             }
             else
             {
                 if (Tipo == "CAR")
                 {
                     Tipo = "AND B.SER_OBJ_CODIGO IN('SA8     ','SA9     ','S91     ') ";
                 }
                 else
                 {
                     Tipo = "";
                 }
             }



             if (Especialidad == "-1")
             {
                 sql = "SELECT C.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(C.PAC_PAC_NUMERO) PACIENTE,A.ORDFECHA FECHA_ORDEN,to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi') FECHA_CIRUGIA,round((to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi')-TRUNC(A.ORDFECHA))*24*60,2) DIAS_OPORTUNIDAD, " +
                         " decode(A.SER_SER_AMBITO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO, " +
                         " a.ORDNUMERO N_ORDEN, F.PRE_PRE_DESCRIPCIO PRESTACION, h.TIPOPROFENOMBRE ESPECIALIDAD " +
                         " FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d,tabprdrel e, pre_prestacion f, tab_tipoprofe h,ser_profesiona g  " +
                         " WHERE (A.SER_SER_AMBITO='03' or D.SOLQRF='U') " +
                             "and a.ORDNUMERO=d.ORDNUMERO " +
                             "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO " +
                             "AND A.ORDNUMERO=B.ORDNUMERO " +
                             "and A.ORDNUMERO = E.ORDNUMERO " +
                             "and E.PRE_PRE_CODIGO = F.PRE_PRE_CODIGO " +
                             "and E.SER_PRO_RUT=G.SER_PRO_RUT " +
                             "and G.SER_PRO_TIPO=h.TIPOPROFECODIGO " +
                             "and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                             "AND A.ORDTIPO='P' " +
                             "AND d.ORDCIRESTCOD IN ('V','X') " +
                             " " + Tipo + " " +
                             "and D.HORAFIN <> '00:00' " +
                             "and trim(C.PAC_PAC_RUT) not in ('10','12') " +
                         " GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,A.SER_SER_AMBITO, a.ORDNUMERO,F.PRE_PRE_DESCRIPCIO, h.TIPOPROFENOMBRE,B.HORQRF " +
                         " ORDER BY B.FECQRF,A.ORDNUMERO,A.SER_SER_AMBITO ";
             }
             else
             {
                 sql = "SELECT C.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(C.PAC_PAC_NUMERO) PACIENTE,A.ORDFECHA FECHA_ORDEN,to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi') FECHA_CIRUGIA,round((to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi')-TRUNC(A.ORDFECHA)),2) DIAS_OPORTUNIDAD, " +
                         " decode(A.SER_SER_AMBITO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO, " +
                         " a.ORDNUMERO N_ORDEN, F.PRE_PRE_DESCRIPCIO PRESTACION, h.TIPOPROFENOMBRE ESPECIALIDAD " +
                         " FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d,tabprdrel e, pre_prestacion f, tab_tipoprofe h,ser_profesiona g  " +
                         " WHERE (A.SER_SER_AMBITO='03' or D.SOLQRF='U') " +
                             "and a.ORDNUMERO=d.ORDNUMERO " +
                             "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO " +
                             "AND A.ORDNUMERO=B.ORDNUMERO " +
                             "and A.ORDNUMERO = E.ORDNUMERO " +
                             "and E.PRE_PRE_CODIGO = F.PRE_PRE_CODIGO " +
                             "and E.SER_PRO_RUT=G.SER_PRO_RUT " +
                             "and G.SER_PRO_TIPO=h.TIPOPROFECODIGO " +
                             "and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                             "AND A.ORDTIPO='P' " +
                             "and H.TIPOPROFECODIGO = '" + Especialidad + "' " +
                             "AND d.ORDCIRESTCOD IN ('V','X') " +
                             " " + Tipo + " " +
                             "and D.HORAFIN <> '00:00' " +
                             "and trim(C.PAC_PAC_RUT) not in ('10','12') " +
                         " GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,A.SER_SER_AMBITO, a.ORDNUMERO,F.PRE_PRE_DESCRIPCIO, h.TIPOPROFENOMBRE,B.HORQRF " +
                         " ORDER BY B.FECQRF,A.ORDNUMERO,A.SER_SER_AMBITO ";

             }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet OportunidadQXUrgEnc(string FechaIni, string FechaFin, string Tipo)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Tipo"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Tipo
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            if (Tipo == "CIR")
            {
                Tipo = "AND B.SER_OBJ_CODIGO IN('SA1     ','SA2     ','SA3     ','SA4     ','SA5     ','SA6     ','SA7     ') ";
            }
            else
            {
                if (Tipo == "CAR")
                {
                    Tipo = "AND B.SER_OBJ_CODIGO IN('SA8     ','SA9     ','S91     ') ";
                }
                else
                {
                    Tipo = "";
                }
            }
            
            string sql = "SELECT C.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(C.PAC_PAC_NUMERO) PACIENTE,A.ORDFECHA FECHA_ORDEN,to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi') FECHA_CIRUGIA,round((to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi')-TRUNC(A.ORDFECHA))*24*60,2) DIAS_OPORTUNIDAD, decode(A.SER_SER_AMBITO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO, a.ORDNUMERO N_ORDEN, " +
                         "(select W.SER_ESP_DESCRIPCIO from TABPRDREL d, Ser_Especiali w,Net_SerEspSerPro z where D.SER_PRO_RUT=Z.SER_PRO_RUT and Z.SER_ESP_CODIGO=W.SER_ESP_CODIGO and D.ORDNUMERO = a.ORDNUMERO and rownum=1) ESPECIALIDAD " +
                          "FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d " +
                         "WHERE (A.SER_SER_AMBITO='03' or D.SOLQRF='U') " +
                                "and a.ORDNUMERO=d.ORDNUMERO " +
                                "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO " +
                                "AND A.ORDNUMERO=B.ORDNUMERO " +
                                "and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                "AND A.ORDTIPO='P' " +
                                "AND d.ORDCIRESTCOD IN ('V','X') " +
                                " " + Tipo + " " +
                                "and D.HORAFIN <> '00:00' " +
                                "and trim(C.PAC_PAC_RUT) not in ('10','12') " +
                         "GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,A.SER_SER_AMBITO,a.ORDNUMERO,B.HORQRF " +
                         "ORDER BY B.FECQRF,A.SER_SER_AMBITO ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            
        }


        public DataSet OportunidadQXPrgEnc(string FechaIni, string FechaFin, string Tipo)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Tipo"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Tipo
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            if (Tipo == "CIR")
            {
                Tipo = "AND B.SER_OBJ_CODIGO IN('SA1     ','SA2     ','SA3     ','SA4     ','SA5     ','SA6     ','SA7     ') ";
            }
            else
            {
                if (Tipo == "CAR")
                {
                    Tipo = "AND B.SER_OBJ_CODIGO IN('SA8     ','SA9     ','S91     ') ";
                }
                else
                {
                    Tipo = "";
                }
            }
            
            string sql = "SELECT C.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(C.PAC_PAC_NUMERO) PACIENTE,A.ORDFECHA FECHA_ORDEN,to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi') FECHA_CIRUGIA,round((to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi')-TRUNC(A.ORDFECHA)),2) DIAS_OPORTUNIDAD, decode(A.SER_SER_AMBITO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO, a.ORDNUMERO N_ORDEN, " +
                         "(select W.SER_ESP_DESCRIPCIO from TABPRDREL d, Ser_Especiali w,Net_SerEspSerPro z where D.SER_PRO_RUT=Z.SER_PRO_RUT and Z.SER_ESP_CODIGO=W.SER_ESP_CODIGO and D.ORDNUMERO = a.ORDNUMERO and rownum=1) ESPECIALIDAD " +         
                              "FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d " +
                             "WHERE (A.SER_SER_AMBITO<>'03' and  D.SOLQRF<>'U') " +
                                "and a.ORDNUMERO=d.ORDNUMERO " +
                                "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO " +
                                "AND A.ORDNUMERO=B.ORDNUMERO " +
                                "and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                "AND A.ORDTIPO='P' " +
                                "AND d.ORDCIRESTCOD IN ('V','X') " +
                                " " + Tipo + " " +
                                "And D.HORAFIN <> '00:00' " +
                                "and trim(C.PAC_PAC_RUT) not in ('10','12') " +
                        "GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,A.SER_SER_AMBITO, a.ORDNUMERO,B.HORQRF " +
                        "ORDER BY B.FECQRF,A.SER_SER_AMBITO ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }

        
        public DataSet OportunidadQXPrg(string Especialidad, string FechaIni, string FechaFin, string Tipo)
        {
            string[] nomParam = 
            {
                "In_Especialidad",
                "In_FechaIni",
                "In_FechaFin",
                "In_Tipo"
            };

            object[] vlrParam = 
            {
                Especialidad,
                FechaIni,
                FechaFin,
                Tipo
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if (Tipo == "CIR")
            {
                Tipo = "AND B.SER_OBJ_CODIGO IN('SA1     ','SA2     ','SA3     ','SA4     ','SA5     ','SA6     ','SA7     ') ";
            }
            else
            {
                if (Tipo == "CAR")
                {
                    Tipo = "AND B.SER_OBJ_CODIGO IN('SA8     ','SA9     ','S91     ') ";
                }
                else
                {
                    Tipo = "";
                }
            }            
            
            
            if (Especialidad == "-1")
            {
                sql = "SELECT C.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(C.PAC_PAC_NUMERO) PACIENTE,A.ORDFECHA FECHA_ORDEN,to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi') FECHA_CIRUGIA,round((to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi')-TRUNC(A.ORDFECHA)),2) DIAS_OPORTUNIDAD, decode(A.SER_SER_AMBITO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,a.ORDNUMERO N_ORDEN,TRIM(H.PRE_PRE_DESCRIPCIO) PRESTACION ,F .TIPOPROFENOMBRE ESPECIALIDAD " +
                      "FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d,tabprdrel e,tab_tipoprofe f,ser_profesiona g, pre_prestacion H " +
                      "WHERE (A.SER_SER_AMBITO<>'03' and D.SOLQRF<>'U') " +
                            "and a.ORDNUMERO=d.ORDNUMERO " +
                            "and A.ORDNUMERO=E.ORDNUMERO " +
                            "and E.SER_PRO_RUT=G.SER_PRO_RUT " +
                            "and G.SER_PRO_TIPO=F.TIPOPROFECODIGO " +
                            "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO " +
                            "AND A.ORDNUMERO=B.ORDNUMERO " +
                            "AND E.PRE_PRE_CODIGO = H.PRE_PRE_CODIGO " +
                            "and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "AND A.ORDTIPO='P' " +
                            "AND d.ORDCIRESTCOD IN ('V','X') " +
                            " " + Tipo + " " +
                            "And D.HORAFIN <> '00:00' " +
                            "and trim(C.PAC_PAC_RUT) not in ('10','12') " +
                      "GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,A.SER_SER_AMBITO,F.TIPOPROFENOMBRE,a.ORDNUMERO, H.PRE_PRE_DESCRIPCIO,B.HORQRF " +
                      "ORDER BY B.FECQRF,a.ORDNUMERO,A.SER_SER_AMBITO ";
            }
            else
            {
                sql = "SELECT C.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(C.PAC_PAC_NUMERO) PACIENTE,A.ORDFECHA FECHA_ORDEN,to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi') FECHA_CIRUGIA,round((to_date((to_char(B.FECQRF,'yyyy/mm/dd') || ' ' ||B.HORQRF),'yyyy/mm/dd hh24:mi')-TRUNC(A.ORDFECHA)),2) DIAS_OPORTUNIDAD, decode(A.SER_SER_AMBITO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,a.ORDNUMERO N_ORDEN,TRIM(H.PRE_PRE_DESCRIPCIO) PRESTACION ,F .TIPOPROFENOMBRE ESPECIALIDAD " +
                      "FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d,tabprdrel e,tab_tipoprofe f,ser_profesiona g, pre_prestacion H " +
                      "WHERE  (A.SER_SER_AMBITO<>'03' and D.SOLQRF<>'U') " +
                            "and a.ORDNUMERO=d.ORDNUMERO " +
                            "and A.ORDNUMERO=E.ORDNUMERO " +
                            "and E.SER_PRO_RUT=G.SER_PRO_RUT " +
                            "and G.SER_PRO_TIPO=F.TIPOPROFECODIGO " +
                            "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO " +
                            "AND A.ORDNUMERO=B.ORDNUMERO " +
                            "AND E.PRE_PRE_CODIGO = H.PRE_PRE_CODIGO " +
                            "and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "AND A.ORDTIPO='P' " +
                            "and f.TIPOPROFECODIGO = '" + Especialidad + "' " +
                            "AND d.ORDCIRESTCOD IN ('V','X') " +
                            " " + Tipo + " " +
                            "And D.HORAFIN <> '00:00' " +
                            "and trim(C.PAC_PAC_RUT) not in ('10','12') " +
                      "GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,A.SER_SER_AMBITO,F.TIPOPROFENOMBRE,a.ORDNUMERO, H.PRE_PRE_DESCRIPCIO,B.HORQRF " +
                      "ORDER BY B.FECQRF,a.ORDNUMERO,A.SER_SER_AMBITO ";

            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet CirugiasCanceladas(string CausaCancelar, string FechaIni, string FechaFin, string Historia)
        {
            string[] nomParam = 
            {
                "In_CausaCancelar",
                "In_FechaIni",
                "In_FechaFin",
                "In_Historia"
            };

            object[] vlrParam = 
            {
                CausaCancelar,
                FechaIni,
                FechaFin,
                Historia
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;
            string paciente = "";

            if (Historia != "             ")
            {
                paciente = " and (a.PAC_PAC_RUT) = '" + Historia + "' ";
            }

            if (FechaIni != "" && FechaFin != "")
            { 
                FechaIni = " and trunc(C.FECQRF) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') " ;
            }
            else
            {
                FechaIni = FechaFin = "";
            }

            if (CausaCancelar == "-1")
            {
                sql = "select  a.PAC_PAC_RUT IDENTIFICACION,nomcompletopaciente(A.PAC_PAC_NUMERO) PACIENTE, b.ORDCAUCANNOM CAUSA_CANCELACION,TRIM(c.DESCCANCELACION) DESCRIPCION,c.FECCANCELACION FECHA_CANCELACION " +
                       ",Admsalud.Glbgetnomusr (c.USUCANCELACION)  USUARIO_QUE_CANCELO,I.TIPOPROFENOMBRE ESPECIALIDAD " +
                       "from pac_paciente a,tabordcircaucan b,taborddetcir c, taborddetalle d, ser_profesiona h,tab_tipoprofe i, TABMOTIVOCONS J " +
                           "where a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and c.ORDCAUCANCOD=b.ORDCAUCANCOD " +
                                "and trim(a.PAC_PAC_RUT) not in ( '10','12') " +
                                " " + FechaIni + " " +
                                "and C.PAC_PAC_NUMERO= D.PAC_PAC_NUMERO " +
                                "and C.MTVCORRELATIVO = D.MTVCORRELATIVO " +
                                "and C.TABENCUENTRO = D.TABENCUENTRO " +
                                "and C.ORDNUMERO = D.ORDNUMERO " +
                                "and C.ORDCIRESTCOD='C' " +
                                "AND J.SER_PRO_RUT=h.SER_PRO_RUT AND D.PAC_PAC_NUMERO=J.PAC_PAC_NUMERO AND D.MTVCORRELATIVO=J.MTVCORRELATIVO  and H.SER_PRO_TIPO=I.TIPOPROFECODIGO " +
                                " " + paciente + " " +
                            "group by B.ORDCAUCANCOD, a.PAC_PAC_RUT,A.PAC_PAC_NUMERO, b.ORDCAUCANNOM ,c.DESCCANCELACION,c.FECCANCELACION ,c.USUCANCELACION,I.TIPOPROFENOMBRE " +
                            "ORDER BY B.ORDCAUCANCOD ";
            }
            else
            {
                sql = "select  a.PAC_PAC_RUT IDENTIFICACION,nomcompletopaciente(A.PAC_PAC_NUMERO) PACIENTE, b.ORDCAUCANNOM CAUSA_CANCELACION,TRIM(c.DESCCANCELACION) DESCRIPCION,c.FECCANCELACION FECHA_CANCELACION " +
                        ",Admsalud.Glbgetnomusr (c.USUCANCELACION)  USUARIO_QUE_CANCELO,I.TIPOPROFENOMBRE ESPECIALIDAD " +
                      "from pac_paciente a,tabordcircaucan b,taborddetcir c, taborddetalle d, ser_profesiona h,tab_tipoprofe i, TABMOTIVOCONS J " +
                           "where a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and c.ORDCAUCANCOD=b.ORDCAUCANCOD " +
                                "and trim(a.PAC_PAC_RUT) not in ( '10','12') " +
                                " " + FechaIni + " " +
                                "and C.PAC_PAC_NUMERO= D.PAC_PAC_NUMERO " +
                                "and C.MTVCORRELATIVO = D.MTVCORRELATIVO " +
                                "and C.TABENCUENTRO = D.TABENCUENTRO " +
                                "and C.ORDNUMERO = D.ORDNUMERO " +
                                "and C.ORDCIRESTCOD='C' " +
                                "AND J.SER_PRO_RUT=h.SER_PRO_RUT AND D.PAC_PAC_NUMERO=J.PAC_PAC_NUMERO AND D.MTVCORRELATIVO=J.MTVCORRELATIVO  and H.SER_PRO_TIPO=I.TIPOPROFECODIGO " +
                                "and c.ORDCAUCANCOD='" + CausaCancelar + "' " +
                                " " + paciente + " " +
                            "group by B.ORDCAUCANCOD, a.PAC_PAC_RUT,A.PAC_PAC_NUMERO, b.ORDCAUCANNOM ,c.DESCCANCELACION,c.FECCANCELACION ,c.USUCANCELACION,I.TIPOPROFENOMBRE " +
                            "ORDER BY B.ORDCAUCANCOD ";

            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet NCirugiasxAmbito(string Ambito, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Ambito",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Ambito,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if (Ambito == "")
            {
                sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) PACIENTE, A.ORDNUMERO N_ORDEN, " +
                                 "decode(a.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,  G.FECQRF FECHA_CIRUGIA " +
                         "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g, TABPRDREL H ,ATE_PRESTACION I " +
                         "where a.ORDNUMERO=b.ORDNUMERO and trunc(G.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                           "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO and a.HORAFIN<>'00:00' and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO " +
                           "AND A.MTVCORRELATIVO = F.MTVCORRELATIVO AND A.TABENCUENTRO = F.TABENCUENTRO and A.ORDNUMERO = F.ORDNUMERO and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO " +
                           "AND B.PRE_PRE_CODIGO = H.PRE_PRE_CODIGO AND B.PAC_PAC_NUMERO=H.PAC_PAC_NUMERO AND B.ORDNUMERO=H.ORDNUMERO AND H.PAC_PAC_NUMERO = I.ATE_PRE_NUMERPACIE " +
                           "AND H.PRE_PRE_CODIGO=I.ATE_PRE_CODIGO AND H.RPA_FOR_NUMERFORMU=I.ATE_PRE_NUMERFORMU AND H.RPA_FOR_TIPOFORMU = I.ATE_PRE_TIPOFORMU " +
                           "and trim(e.PAC_PAC_RUT) not in ( '10','12') and A.ORDNUMERO = G.ORDNUMERO " +
                        "group by e.PAC_PAC_RUT, E.PAC_PAC_NUMERO, A.ORDNUMERO, a.AMBITOCODIGO, G.FECQRF " +
                        "ORDER BY G.FECQRF, AMBITO,PACIENTE ";
            }
            else
            {
                sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) PACIENTE, A.ORDNUMERO N_ORDEN, " +
                                 "decode(a.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,  G.FECQRF FECHA_CIRUGIA " +
                         "from taborddetcir a,tabprdreldet b,pre_prestacion c,pac_paciente e,TABORDENESSERV f, TABPRGQRF g, TABPRDREL H ,ATE_PRESTACION I " +
                         "where a.ORDNUMERO=b.ORDNUMERO and trunc(G.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                           "and b.PRE_PRE_CODIGO=c.PRE_PRE_CODIGO and a.HORAFIN<>'00:00' and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO and E.PAC_PAC_NUMERO = F.PAC_PAC_NUMERO " +
                           "AND A.MTVCORRELATIVO = F.MTVCORRELATIVO AND A.TABENCUENTRO = F.TABENCUENTRO and A.ORDNUMERO = F.ORDNUMERO and A.PAC_PAC_NUMERO = G.PAC_PAC_NUMERO " +
                           "AND B.PRE_PRE_CODIGO = H.PRE_PRE_CODIGO AND B.PAC_PAC_NUMERO=H.PAC_PAC_NUMERO AND B.ORDNUMERO=H.ORDNUMERO AND H.PAC_PAC_NUMERO = I.ATE_PRE_NUMERPACIE " +
                           "AND H.PRE_PRE_CODIGO=I.ATE_PRE_CODIGO AND H.RPA_FOR_NUMERFORMU=I.ATE_PRE_NUMERFORMU AND H.RPA_FOR_TIPOFORMU = I.ATE_PRE_TIPOFORMU " +
                           "and trim(e.PAC_PAC_RUT) not in ( '10','12') and A.ORDNUMERO = G.ORDNUMERO and A.AMBITOCODIGO='" + Ambito + "' " +
                        "group by e.PAC_PAC_RUT, E.PAC_PAC_NUMERO, A.ORDNUMERO, a.AMBITOCODIGO, G.FECQRF " +
                        "ORDER BY G.FECQRF, AMBITO,PACIENTE ";
            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }


        public DataSet CirugiasxEspecialidad(string Especialidad, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Especialidad",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Especialidad,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            if (Especialidad != "-1")
            {
                Especialidad = " and f.TIPOPROFECODIGO = '" + Especialidad + "' ";
            }
            else
            {
                Especialidad = "";
            }
            
            string sql = "SELECT C.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(C.PAC_PAC_NUMERO) PACIENTE,B.FECQRF FECHA_CIRUGIA,decode(D.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,a.ORDNUMERO N_ORDEN,TRIM(H.PRE_PRE_DESCRIPCIO) PRESTACION ,F.TIPOPROFENOMBRE ESPECIALIDAD " +
                      "FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d,tabprdrel e,tab_tipoprofe f,ser_profesiona g, pre_prestacion H, ATE_PRESTACION I  " +
                      "WHERE a.ORDNUMERO=d.ORDNUMERO " +
                            "and A.ORDNUMERO=E.ORDNUMERO " +
                            "and E.SER_PRO_RUT=G.SER_PRO_RUT " +
                            "and G.SER_PRO_TIPO=F.TIPOPROFECODIGO " +
                            "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO " +
                            "AND A.ORDNUMERO=B.ORDNUMERO " +
                            "AND E.PRE_PRE_CODIGO = H.PRE_PRE_CODIGO " +
                            "and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "AND A.ORDTIPO='P' AND E.PRE_PRE_CODIGO=I.ATE_PRE_CODIGO AND E.RPA_FOR_NUMERFORMU=I.ATE_PRE_NUMERFORMU AND E.RPA_FOR_TIPOFORMU = I.ATE_PRE_TIPOFORMU " +
                            "AND E.PAC_PAC_NUMERO = I.ATE_PRE_NUMERPACIE " + Especialidad + " " +
                            "AND d.ORDCIRESTCOD IN ('V','X') " +
                            "And D.HORAFIN <> '00:00' " +
                            "and trim(C.PAC_PAC_RUT) not in ('10','12') " +
                      "GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,D.AMBITOCODIGO,F.TIPOPROFENOMBRE,a.ORDNUMERO, H.PRE_PRE_DESCRIPCIO " +
                      "ORDER BY B.FECQRF,a.ORDNUMERO,D.AMBITOCODIGO ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataTable BuscarProfesionales(string Historia)
        {
            string[] nomParam = 
            {
                "In_Historia"
            };

            object[] vlrParam = 
            {
                Historia
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select A.ORDNUMERO || A.PRE_PRE_CODIGO ORDNUMERO ,to_char(C.FECQRF,'yyyy/mm/dd') fec_cirugia,C.HORQRF hora_cirugia, A.PRE_PRE_CODIGO CUP, A.SER_PRO_RUT cirujano, A.ANESID anestesiologo, A.AYUDID ayudantia, NOMCOMPLETOPACIENTE(A.PAC_PAC_NUMERO) PACIENTE  " +
                           "from tabprdrel a, pac_paciente b, TABPRGQRF c " +
                           "where A.PAC_PAC_NUMERO = B.PAC_PAC_NUMERO and A.ORDNUMERO = C.ORDNUMERO and A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO and trim(A.ORDNUMERO) = '" + Historia + "' " +
                           "order by 1 DESC, 2, 3";

            return new DA.DA("Oracle").getDataTable(sql, nomParam, vlrParam);
        }


        public DataSet ActualizarProfesional(string tinver, string porc, string fecha, string codigo)
        {
            string[] nomParam = 
            {
                "In_Cirujano",
                "In_Anestesiologo",
                "In_Ayudantia",
                "In_OrdNumero"
            };

            object[] vlrParam = 
            {
                tinver,
                porc,
                fecha,
                codigo
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "update tabprdrel a set  A.SER_PRO_RUT='" + tinver + "', A.ANESID='" + porc + "',  A.AYUDID='" + fecha + "' where A.ORDNUMERO || A.PRE_PRE_CODIGO='" + codigo + "' ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
