﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LPrioridades
    {
        public bool PrioridadGuardar(Prioridades prioridad)
        {
            string[] nomParam = { "@Codigo", "@Nombre", "@Usuario", "@Horas" };
            object[] vlrParam = { prioridad.Codigo, prioridad.Nombre, prioridad.Usuario, prioridad.Horas };

            return EjecutarSentencia("uspPrioridadActualizar", nomParam, vlrParam);
        }

        public bool PrioridadEstado(Prioridades app)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                app.Codigo,
                app.Estado
            };

            return EjecutarSentencia("uspPrioridadEstado", nomParam, vlrParam);
        }

        public DataTable PrioridadConsultar(Prioridades prioridad)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { prioridad.Codigo };

            return getDataTable("uspPrioridadConsultar", nomParam, vlrParam);
        }

        public bool PrioridadReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Prioridad", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspPrioridadReactivar", nomParam, valParam);
        }

        public bool PrioridadRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Prioridad", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspPrioridadRetirar", nomParam, valParam);
        }

        public DataTable PrioridadConsultar()
        {
            return getDataTable("uspPrioridadConsultarCodigos");
        }

        public bool PrioridadesRetirar(string xmlPrioridad, string Usuario)
        {
            string[] nomParam = { "@xmlPrioridad", "@Usuario" };
            object[] valParam = { xmlPrioridad, Usuario };

            return EjecutarSentencia("uspPrioridadRetirarXml", nomParam, valParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
