﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LMActividades
    {
        public bool ActividadGuardar(Actividades act)
        {
            string[] nomParam = { "@Codigo", "@Nombre", "@Usuario" };
            object[] vlrParam = { act.Codigo, act.Nombre, act.Usuario };

            return EjecutarSentencia("uspActividadesActualizar", nomParam, vlrParam);
        }

        public DataTable  ActividadesEstado(Actividades act)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                act.Codigo,
                act.Estado
            };

            return getDataTable("uspActividadesEstado", nomParam, vlrParam);
        }

        public DataTable ActividadesConsultar(Actividades act)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { act.Codigo };

            return getDataTable("uspActividadesConsultar", nomParam, vlrParam);
        }

        public bool ActividadesReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Act", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspActividadesReactivar", nomParam, valParam);
        }

        public bool ActividadRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Act", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspActividadesRetirar", nomParam, valParam);
        }

        public DataTable ActividadConsultar()
        {
            return getDataTable("uspActividadesConsultarCodigos");
        }

        public bool ActividadesRetirar(string xmlActividad, string Usuario)
        {
            string[] nomParam = { "@xmlActividad", "@Usuario" };
            object[] valParam = { xmlActividad, Usuario };

            return EjecutarSentencia("uspActividadesRetirarXml", nomParam, valParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
