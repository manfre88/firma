﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAlmacen
    {
        public DataSet ProductoxVCompraxVVenta(string FechaIni, string FechaFin, string Almacen)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Almacen"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Almacen
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT HIS.FLD_PRODUCTOCODIGO cod_producto,ABA.FLD_PRODUCTOGLOSA producto, " +
                                   "NVL(round(valor),0) v_salidas, " +
                                   "NVL(ROUND(valor1),0) v_compras " +
                            "FROM ABA_PRODUCTO ABA, OPA_STOCKHISTORICO HIS, " +
                                        "(SELECT DECODE(DEVOL,'',sum(a.OPA_VALORNETO),sum(a.OPA_VALORNETO)-DEVOL) VALOR, A.FLD_PRODUCTOCODIGO codigo " +
                                         "FROM opa_detmov a,opa_tipomov b, (SELECT sum(a.OPA_VALORNETO) DEVOL, A.FLD_PRODUCTOCODIGO codigo1 " +
                                                                            "FROM opa_detmov a,opa_tipomov b " +
                                                                            "WHERE b.OPA_TIPOMOVCLASE='S' " +
                                                                            "AND TRUNC(a.FECPRC) BETWEEN to_date('" + FechaIni + "','yyyy/mm/dd') AND to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                                                            "AND a.OPA_TIPOMOVCODIGO=b.OPA_TIPOMOVCODIGO " +
                                                                            "AND A.OPA_BODEGACODIGO='" + Almacen + "' " +
                                                                            "AND B.OPA_TIPOMOVCODIGO='12' " +
                                                                            "AND TRIM(B.OPA_TIPOMOVCODIGOUSO)  ='20' " +
                                                                            "group by A.FLD_PRODUCTOCODIGO) " +
                                        "WHERE b.OPA_TIPOMOVCLASE='S' " +
                                        "AND TRUNC(a.FECPRC) BETWEEN to_date('" + FechaIni + "','yyyy/mm/dd') AND to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                        "AND a.OPA_TIPOMOVCODIGO=b.OPA_TIPOMOVCODIGO " +
                                        "AND A.OPA_BODEGACODIGO='" + Almacen + "' " +
                                        "AND A.FLD_PRODUCTOCODIGO=CODIGO1(+) " +
                                        "group by A.FLD_PRODUCTOCODIGO,DEVOL), " +
                                        "(SELECT  sum(a.OPA_VALORNETO) valor1, A.FLD_PRODUCTOCODIGO codigo1 " +
                                        "FROM  opa_detmov a,opa_tipomov b " +
                                        "WHERE b.OPA_TIPOMOVCLASE='E' " +
                                        "AND TRUNC(a.FECPRC) BETWEEN to_date('" + FechaIni + "','yyyy/mm/dd') AND to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                        "AND a.OPA_TIPOMOVCODIGO=b.OPA_TIPOMOVCODIGO " +
                                        "AND a.OPA_BODEGACODIGO='" + Almacen + "' " +
                                        "group by A.FLD_PRODUCTOCODIGO) " +
                            "WHERE ABA.FLD_PRODUCTOCODIGO = HIS.FLD_PRODUCTOCODIGO " +
                            "AND HIS.OPA_BODEGACODIGO='" + Almacen + "' " +
                            "AND TRUNC(HIS.OPA_FECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "AND ABA.FLD_VIGENTE = 1 " +
                            "AND HIS.OPA_STOCKACTUAL > 0 " +
                            "and ABA.FLD_PRODUCTOCODIGO=codigo(+) " +
                            "and ABA.FLD_PRODUCTOCODIGO=codigo1(+) " +
                            "GROUP BY HIS.FLD_PRODUCTOCODIGO,ABA.FLD_PRODUCTOGLOSA,valor,valor1 order by 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }

        public DataSet Peddespachocompletosincantidad(string FechaIni, string FechaFin, string Almacen)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Almacen"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Almacen
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select a.OPA_NUMEROPED PEDIDO,c.EST_CEN_DESCRIPCIO QUIENSOLICITA,a.OPA_BODDESTCODIGO QUIENDESPACHA,B.OPA_ESTADOPEDGLOSA ESTADO,A.FECPRC FEC_PEDIDO,D.FLD_PRODUCTOCODIGO COD_PRODUCTO,D.OPA_CANTIDAD SOLICITADA,D.OPA_CANTIDADDESPACHADA DESPACHADA,D.OPA_CANTIDADAPROBADA APROBADA,A.USRCOD USUARIO " +
                           "from Opa_Pedido a,opa_pedestado b,est_cenresul c,opa_peddetalle d " +
                          "where trunc(a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')  and a.OPA_ESTADOPEDCODIGO=b.OPA_ESTADOPEDCODIGO and a.EST_CEN_CODIGO=c.EST_CEN_CODIGO " +
                            "and A.OPA_NUMEROPED=D.OPA_NUMEROPED AND A.OPA_NUMEROPED like 'PI%' and D.OPA_CANTIDADDESPACHADA=0 and B.OPA_ESTADOPEDCODIGO='08' and A.OPA_BODDESTCODIGO='" + Almacen + "' ORDER BY 5 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }



        public DataSet PedidosxPaciente(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select B.PAC_PAC_RUT identificacion, nomcompletopaciente(B.PAC_PAC_NUMERO) paciente, A.FRMCORRELATIVO orden, A.FECPRC fecha, decode(A.AMBITOCODIGO ,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO  " +
                           "from tabinssol a, pac_paciente b " +
                          "where trunc(A.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and  to_date('" + FechaFin + "','yyyy/mm/dd') and  (To_char(a.FECPRC, 'HH24') < 6 or  To_char(a.FECPRC, 'HH24') > 18) and A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO " +
                          "order by 3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            
        }



    }
}
