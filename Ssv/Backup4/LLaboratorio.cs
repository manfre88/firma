﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LLaboratorio
    {
        public DataSet PacientesconVDRL(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select distinct  i.PAC_PAC_RUT num_id,l.TIPIDAV tipo_id ,I.PAC_PAC_NOMBRE nombre ,(RTrim(i.PAC_PAC_ApellPater) || ' ' || RTrim(i.PAC_PAC_ApellMater)) apellido,Decode(I.PAC_PAC_SEXO,'M','masculino','F','femenino') genero,I.PAC_PAC_DIRECCIONGRALHABIT direccion, " +
                                "I.PAC_PAC_FONO telefono,M.DVP_PRO_GLOSA munresidencia ,'urbana' zona,'Clinica CES' ips,K.CON_CON_DESCRIPCIO eps ,decode(O.TIPOEMPRESACODIGO,'01','contributivo','02','subsidiado','sin dato') sgsalud,  I.PAC_PAC_FECHANACIM fechanacimiento , " +
                                "round((a.RPA_FOR_FECHARECEP- I.PAC_PAC_FECHANACIM)/365) edad, 'años' tipoedad,'otros' etnia,'otros' gpoblacional,'Clinica CES' laboratorio,D.LAB_OPC_DESCRIPCIO vdrlcualit , '' rprcualit,desc2 vdrl,'' rpr,'' pruebatreponemica,'' embarazo,'08/02/2011' fechareporte ,a.RPA_FOR_FECHARECEP fechatomamuestra " +
                        "from rpa_forlab a,cex_examsolic b,cex_resultados c,lab_opciones d,pac_paciente i,ate_prestacion j,con_convenio k ,tab_tipoident l,dvp_provincia m,con_empresa n,TAB_TipoEmpresa o, rpa_formulario p " +
                                ",(select h.LAB_OPC_DESCRIPCIO desc2,e.ATE_PRE_TIPOFORMU tfor ,e.ATE_PRE_NUMERFORMU nfor,g.CEX_EXS_CODIGPREST cpre from rpa_forlab e,cex_examsolic f,cex_resultados g,lab_opciones h where e.ATE_PRE_TIPOFORMU=f.ATE_PRE_TIPOFORMU " +
                                            "and e.ATE_PRE_NUMERFORMU=f.ATE_PRE_NUMERFORMU and f.CEX_EXS_CODIGPREST='906916' AND trunc(e.RPA_FOR_FECHARECEP) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD')  and g.ATE_PRE_NUMERFORMU=e.ATE_PRE_NUMERFORMU " +
                                            "and g.ATE_PRE_TIPOFORMU=e.ATE_PRE_TIPOFORMU and g.CEX_EXS_CODIGPREST=f.CEX_EXS_CODIGPREST and g.CEX_RES_SECUENCIA='83859' and E.ORDTIPO='L' and g.LAB_FOR_CODIGO='VDRL' and h.LAB_VAR_CODIGO='VDR1' AND TRIM( h.LAB_OPC_CODIGO)=TRIM(g.CEX_RES_TEXTO) ) " +
                        "where A.ATE_PRE_TIPOFORMU=B.ATE_PRE_TIPOFORMU and A.ATE_PRE_NUMERFORMU=B.ATE_PRE_NUMERFORMU and B.CEX_EXS_CODIGPREST='906916' AND trunc(A.RPA_FOR_FECHARECEP) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') and A.ATE_PRE_TIPOFORMU=tfor (+) " +
                            "and C.ATE_PRE_NUMERFORMU=A.ATE_PRE_NUMERFORMU and C.ATE_PRE_TIPOFORMU=A.ATE_PRE_TIPOFORMU and C.CEX_EXS_CODIGPREST=B.CEX_EXS_CODIGPREST and C.CEX_RES_SECUENCIA='83858' and A.ORDTIPO='L' and D.LAB_VAR_CODIGO=C.LAB_FOR_CODIGO " +
                            "and D.LAB_VAR_CODIGO='VDRL' AND TRIM( D.LAB_OPC_CODIGO)=TRIM(C.CEX_RES_TEXTO) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO=P.PAC_PAC_NUMERO and A.RPA_FOR_TIPOFORMU=P.RPA_FOR_TIPOFORMU0 and A.RPA_FOR_NUMERFORMU=P.RPA_FOR_NUMERFORMU0 " +
                            "and P.RPA_FOR_TIPOFORMU=J.ATE_PRE_TIPOFORMU and P.RPA_FOR_NUMERFORMU=J.ATE_PRE_NUMERFORMU and J.ATE_PRE_CODIGO=C.CEX_EXS_CODIGPREST and J.ATE_PRE_CODIGO=B.CEX_EXS_CODIGPREST and trim(J.CON_CON_CODIGO)=trim(K.CON_CON_CODIGO) " +
                            "and i.PAC_PAC_NUMERO = A.PAC_PAC_NUMERO and i.PAC_PAC_NUMERO not in ('1308','5024') and l.TAB_TIPOIDENTCODIGO = i.PAC_PAC_TIPOIDENTCODIGO and I.PAC_PAC_CIUDAHABIT =M.DVP_PRO_CODIGO and K.CON_EMP_RUT=N.CON_EMP_RUT and N.TIPOEMPRESACODIGO=O.TIPOEMPRESACODIGO and A.ATE_PRE_NUMERFORMU=nfor (+) " +
                        "UNION ALL " +
                        "select distinct  i.PAC_PAC_RUT num_id,l.TIPIDAV tipo_id ,I.PAC_PAC_NOMBRE nombre ,(RTrim(i.PAC_PAC_ApellPater) || ' ' || RTrim(i.PAC_PAC_ApellMater)) apellido,Decode(I.PAC_PAC_SEXO,'M','masculino','F','femenino') genero,I.PAC_PAC_DIRECCIONGRALHABIT direccion, " +
                            "I.PAC_PAC_FONO telefono,M.DVP_PRO_GLOSA munresidencia ,'urbana' zona,'Clinica CES' ips,'PARTICULAR' eps,'sin dato' sgsalud, I.PAC_PAC_FECHANACIM fechanacimiento ,round((a.RPA_FOR_FECHARECEP- I.PAC_PAC_FECHANACIM)/365) edad, 'años' tipoedad,'otros' etnia, 'otros' gppoblacional, " +
                            "'Clinica CES' laboratorio, D.LAB_OPC_DESCRIPCIO vdrlcualit ,'' rprcualit,desc2 vdrl,'' rpr,'' pruebatreponemica,'' embarazo,'08/02/2011' fechareporte,a.RPA_FOR_FECHARECEP fechatomamuestra " +
                          "from rpa_forlab a,cex_examsolic b,cex_resultados c,lab_opciones d,pac_paciente i,ate_prestacion j ,tab_tipoident l,dvp_provincia m ,(select h.LAB_OPC_DESCRIPCIO desc2,e.ATE_PRE_TIPOFORMU tfor ,e.ATE_PRE_NUMERFORMU nfor,g.CEX_EXS_CODIGPREST cpre from rpa_forlab e,cex_examsolic f, " +
                                  "cex_resultados g,lab_opciones h where e.ATE_PRE_TIPOFORMU=f.ATE_PRE_TIPOFORMU and e.ATE_PRE_NUMERFORMU=f.ATE_PRE_NUMERFORMU and f.CEX_EXS_CODIGPREST='906916' AND trunc(e.RPA_FOR_FECHARECEP) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                                  "and g.ATE_PRE_NUMERFORMU=e.ATE_PRE_NUMERFORMU and g.ATE_PRE_TIPOFORMU=e.ATE_PRE_TIPOFORMU and g.CEX_EXS_CODIGPREST=f.CEX_EXS_CODIGPREST and g.CEX_RES_SECUENCIA='83859' and E.ORDTIPO='L' and g.LAB_FOR_CODIGO='VDRL' " +
                                  "and h.LAB_VAR_CODIGO='VDR1' AND TRIM( h.LAB_OPC_CODIGO)=TRIM(g.CEX_RES_TEXTO) )  " +
                        "where A.ATE_PRE_TIPOFORMU=B.ATE_PRE_TIPOFORMU and A.ATE_PRE_NUMERFORMU=B.ATE_PRE_NUMERFORMU and B.CEX_EXS_CODIGPREST='906916' AND trunc(A.RPA_FOR_FECHARECEP) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                            "and C.ATE_PRE_NUMERFORMU=A.ATE_PRE_NUMERFORMU and C.ATE_PRE_TIPOFORMU=A.ATE_PRE_TIPOFORMU and C.CEX_EXS_CODIGPREST=B.CEX_EXS_CODIGPREST and C.CEX_RES_SECUENCIA='83858' and A.ORDTIPO='L' and D.LAB_VAR_CODIGO=C.LAB_FOR_CODIGO " +
                            "and D.LAB_VAR_CODIGO='VDRL' AND TRIM( D.LAB_OPC_CODIGO)=TRIM(C.CEX_RES_TEXTO)  and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO and J.ATE_PRE_CODIGO=C.CEX_EXS_CODIGPREST and J.ATE_PRE_CODIGO=B.CEX_EXS_CODIGPREST  and J.CON_CON_CODIGO='CP      ' " +
                            "and i.PAC_PAC_NUMERO = A.PAC_PAC_NUMERO and i.PAC_PAC_NUMERO not in ('1308','5024') and l.TAB_TIPOIDENTCODIGO = i.PAC_PAC_TIPOIDENTCODIGO and I.PAC_PAC_CIUDAHABIT =M.DVP_PRO_CODIGO AND A.RPA_FOR_TIPOFORMU =J.ATE_PRE_TIPOFORMU AND A.RPA_FOR_NUMERFORMU =J.ATE_PRE_NUMERFORMU " +
                            "and A.ATE_PRE_TIPOFORMU=tfor (+) and A.ATE_PRE_NUMERFORMU=nfor (+) ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet HemocomponentesxProfesional(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select FunGlbgetidpac(a.Pac_Pac_Numero) identificacion, nomcompletopaciente(a.Pac_Pac_Numero) paciente,b.Ser_Pro_Rut rut,Traerprofesional(b.Ser_Pro_Rut) Profesional,E.TIPOPROFENOMBRE TIPOPROFEsional, " +
                                 "a.OrdNumero NumOrden, a.Pre_Pre_Codigo codHem, c.HmcTpoNom hemocomponente " +
                           "from taborddetalle a, tabEncuentros b , TabHmcTpo c,ser_profesiona d,tab_tipoprofe e  " +
                          "where a.ordtipo = 'B' and b.fecprc between to_date('" + FechaIni + "', 'yyyy/mm/dd') and to_date('" + FechaFin + "', 'yyyy/mm/dd') And b.MtvCorrelativo = a. MtvCorrelativo And b.TABENCUENTRO = a.TABENCUENTRO " +
                            "And b.Pac_Pac_Numero = a.Pac_Pac_Numero and b.PAC_PAC_NUMERO not in ('1308','5024') And Trim(c.HMCTPOCOD) =  Trim(a.pre_Pre_codigo) and b.Ser_Pro_Rut=d.SER_PRO_RUT and d.SER_PRO_TIPO=e.TIPOPROFECODIGO order by 4,2,6 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet LabUrgentesUCE(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT Funglbgetrutpac(ORD.PAC_PAC_NUMERO) IDentificacion,NOMCOMPLETOPACIENTE(ORD.PAC_PAC_NUMERO) PACIENTE ,ORD.ORDNUMERO, ODET.PRE_PRE_CODIGO CODIGO,PRE.PRE_PRE_DESCRIPCIO PRESTACION, ORD.ORDFECHA, " +
                                 "Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '3 ') GENERADA, Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '10') CONFIRMADA, " +
                                 "Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '5 ') TRANSMITIDA, Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '6 ') REVISADA, " +
                                "round(to_number (Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '6 ') -  Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '10'))*1440) min_oportunidad " +
                        "FROM TABORDENESSERV ORD, TABORDDETALLE ODET, ATC_ESTADIA ATC, ATC_CUENTA CTA, RPA_FORLAB RPA, CEX_EXAMSOLIC XSOL, PRE_PRESTACION PRE " +
                        "WHERE ORD.ORDNUMERO = ODET.ORDNUMERO AND ORD.PAC_PAC_NUMERO = ODET.PAC_PAC_NUMERO AND ORD.ORDTIPO = ODET.ORDTIPO " +
                             "AND TRUNC(ORD.ORDFECHA)  between TO_DATE('" + FechaIni + "','YYYY/MM/DD') and TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                             "AND ORD.ORDTIPO = 'L' AND ORD.ORDESTADO = 'R' AND ORD.MTVCORRELATIVO = ATC.MTVCORRELATIVO AND ORD.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO " +
                             "AND ATC.ATC_EST_NUMERO = CTA.ATC_EST_NUMERO AND ATC.PAC_PAC_NUMERO = CTA.PAC_PAC_NUMERO AND CTA.RPA_FOR_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU AND CTA.RPA_FOR_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU " +
                             "AND CTA.PAC_PAC_NUMERO = RPA.PAC_PAC_NUMERO AND ORD.ORDTIPO = RPA.ORDTIPO AND ORD.ORDNUMERO = RPA.ORDNUMERO AND RPA.SER_SER_AMBITO = '02' AND RPA.ATE_PRE_TIPOFORMU = XSOL.ATE_PRE_TIPOFORMU " +
                             "AND RPA.ATE_PRE_NUMERFORMU = XSOL.ATE_PRE_NUMERFORMU AND ODET.PRE_PRE_CODIGO = XSOL.CEX_EXS_CODIGPREST AND ODET.PRE_PRE_CODIGO=PRE.PRE_PRE_CODIGO " +
                             "AND EXISTS (SELECT * FROM ATC_OCUPACAMA OC WHERE OC.ATC_EST_NUMERO = ATC.ATC_EST_NUMERO AND OC.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO AND OC.SER_SER_CODIGO ='003     ' AND OC.ATC_EST_NUMERO = ATC.ATC_EST_NUMERO " +
                             "AND OC.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO AND TO_char(ORD.ORDFECHA,'YYYY/MM/DD') >= TO_char(OC.RPA_FDA_HORAINGRESO,'YYYY/MM/DD') AND TO_char(ORD.ORDFECHA,'YYYY/MM/DD') <= TO_char(OC.RPA_FDA_HORAEGRESO,'YYYY/MM/DD')) " +
                        "ORDER BY 1,6 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet LabUrgentesUCI(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT Funglbgetrutpac(ORD.PAC_PAC_NUMERO) IDENTIFICACION, NOMCOMPLETOPACIENTE(ORD.PAC_PAC_NUMERO) PACIENTE, ORD.ORDNUMERO, ODET.PRE_PRE_CODIGO CODIGO,PRE.PRE_PRE_DESCRIPCIO PRESTACION, ORD.ORDFECHA, " +
                                 "Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '3 ') GENERADA, Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '10') CONFIRMADA, " +
                                 "Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '5 ') TRANSMITIDA, Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '6 ') REVISADA, " +
                                 "ROUND(TO_NUMBER(Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '6 ') -  Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '10')) *1440) MIN_OPORTUNIDAD " +
                          "FROM TABORDENESSERV ORD,TABORDDETALLE ODET, ATC_ESTADIA ATC, ATC_CUENTA CTA, RPA_FORLAB RPA, CEX_EXAMSOLIC XSOL,PRE_PRESTACION PRE " +
                         "WHERE ORD.ORDNUMERO = ODET.ORDNUMERO AND ORD.PAC_PAC_NUMERO = ODET.PAC_PAC_NUMERO AND ORD.ORDTIPO = ODET.ORDTIPO AND ORD.ORDTIPO = 'L'  AND ORD.ORDESTADO = 'R'  AND ORD.MTVCORRELATIVO = ATC.MTVCORRELATIVO " +
                             "AND TRUNC(ORD.ORDFECHA) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') AND ORD.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO AND ATC.ATC_EST_NUMERO = CTA.ATC_EST_NUMERO " +
                             "AND ATC.PAC_PAC_NUMERO = CTA.PAC_PAC_NUMERO AND CTA.RPA_FOR_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU AND CTA.RPA_FOR_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU AND CTA.PAC_PAC_NUMERO = RPA.PAC_PAC_NUMERO " +
                             "AND ORD.ORDTIPO = RPA.ORDTIPO AND ORD.ORDNUMERO = RPA.ORDNUMERO AND RPA.SER_SER_AMBITO = '02' AND RPA.ATE_PRE_TIPOFORMU = XSOL.ATE_PRE_TIPOFORMU AND RPA.ATE_PRE_NUMERFORMU = XSOL.ATE_PRE_NUMERFORMU " +
                             "AND ODET.PRE_PRE_CODIGO = XSOL.CEX_EXS_CODIGPREST AND ODET.PRE_PRE_CODIGO=PRE.PRE_PRE_CODIGO " +
                             "AND EXISTS (SELECT * FROM ATC_OCUPACAMA OC WHERE OC.ATC_EST_NUMERO = ATC.ATC_EST_NUMERO AND OC.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO  AND OC.SER_SER_CODIGO ='002     ' AND OC.ATC_EST_NUMERO = ATC.ATC_EST_NUMERO " +
                             "AND OC.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO AND TO_CHAR(ORD.ORDFECHA,'YYYY/MM/DD') >= TO_CHAR(OC.RPA_FDA_HORAINGRESO,'YYYY/MM/DD') AND TO_CHAR(ORD.ORDFECHA,'YYYY/MM/DD') <= TO_CHAR(OC.RPA_FDA_HORAEGRESO,'YYYY/MM/DD')) " +
                         "ORDER BY 1, 6 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet LabUrgentesUrgencias(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT Funglbgetrutpac(ORD.PAC_PAC_NUMERO) IDENTIFICACION,NOMCOMPLETOPACIENTE( ORD.PAC_PAC_NUMERO) PACIENTE, ORD.ORDNUMERO, ODET.PRE_PRE_CODIGO CODIGO,PRE.PRE_PRE_DESCRIPCIO PRESTACION,ORD.ORDFECHA, " +
                                 "Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '3 ') GENERADA, Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '10') CONFIRMADA, " +
                                 "Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '5 ') TRANSMITIDA, Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '6 ') REVISADA, " +
                                 "ROUND(TO_NUMBER(Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '6 ') - Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '3 ') )*1440) MIN_OPORTUNIDAD " +
                            "FROM TABORDENESSERV ORD,TABORDDETALLE ODET, ATC_ESTADIA ATC, ATC_CUENTA CTA,RPA_FORLAB RPA,CEX_EXAMSOLIC XSOL,PRE_PRESTACION PRE " +
                           "WHERE ORD.ORDNUMERO = ODET.ORDNUMERO AND ORD.PAC_PAC_NUMERO = ODET.PAC_PAC_NUMERO AND ORD.ORDTIPO = ODET.ORDTIPO AND TRUNC(ORD.ORDFECHA) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND  TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                             "AND ORD.ORDTIPO = 'L' AND ORD.ORDESTADO = 'R' AND ORD.MTVCORRELATIVO = ATC.MTVCORRELATIVO AND ORD.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO AND ATC.ATC_EST_NUMERO = CTA.ATC_EST_NUMERO " +
                             "AND ATC.PAC_PAC_NUMERO = CTA.PAC_PAC_NUMERO AND CTA.RPA_FOR_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU AND CTA.RPA_FOR_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU AND CTA.PAC_PAC_NUMERO = RPA.PAC_PAC_NUMERO " +
                             "AND ORD.ORDTIPO = RPA.ORDTIPO AND ORD.ORDNUMERO = RPA.ORDNUMERO AND RPA.SER_SER_AMBITO = '02' AND RPA.ATE_PRE_TIPOFORMU = XSOL.ATE_PRE_TIPOFORMU AND RPA.ATE_PRE_NUMERFORMU = XSOL.ATE_PRE_NUMERFORMU " +
                             "AND ODET.PRE_PRE_CODIGO = XSOL.CEX_EXS_CODIGPREST AND ODET.PRE_PRE_CODIGO=PRE.PRE_PRE_CODIGO AND EXISTS (SELECT * FROM ATC_OCUPACAMA OC WHERE OC.ATC_EST_NUMERO = ATC.ATC_EST_NUMERO AND OC.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO " +
                             "AND OC.SER_SER_CODIGO ='001     ' AND OC.ATC_EST_NUMERO = ATC.ATC_EST_NUMERO AND OC.PAC_PAC_NUMERO = ATC.PAC_PAC_NUMERO AND TO_CHAR(ORD.ORDFECHA,'YYYY/MM/DD') >= TO_CHAR(OC.RPA_FDA_HORAINGRESO,'YYYY/MM/DD') " +
                             "AND TO_CHAR(ORD.ORDFECHA,'YYYY/MM/DD') <= TO_CHAR(OC.RPA_FDA_HORAEGRESO,'YYYY/MM/DD')) ORDER BY 1,6 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet HemocomponentesxBanco(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select c.Bco_dot_Nombre Banco, b.Bco_don_fechaDonac FechaDonacion,d.HmcTpoNom Hemocomponente, a.BCO_BOL_VOLUMEN Volumen, a.Bco_Bol_IdenOrig NroOriginal, a.BlsSelCal SelloCalidad " +
                                 ",a.Bco_Bol_FechaVenci FechaVencimiento, a.bco_Bol_VolumenResto VolumenResto, a.BlsNum NumeroBolsa, a.Bco_bol_GrupoSangu Gruposanguineo, a.Bco_Bol_FactorRH FactorRH " +
                                 ",a.Bco_bol_FactorDU FactorDU, b.Bco_don_numerdonac NumeroDonacion " +
                           "from Bco_Bolsas a, Bco_Donac b, Bco_Donante c, TabHmcTpo d " +
                          "Where c.BCO_DOT_NUMERO = b.BCO_DOT_NUMERO And a.BCO_DON_NUMERDONAC = b.BCO_DON_NUMERDONAC And Trim(d.HMCTPOCOD) = Trim(a.HmcTpoCod) " +
                            "and trunc(b.Bco_don_fechaDonac) between to_date('" + FechaIni + "', 'yyyy/mm/dd') and to_date('" + FechaFin + "', 'yyyy/mm/dd') order by 1,2,3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet NOrdenesTransfusion(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select B.PAC_PAC_RUT IDENTIFICACION, NOMCOMPLETOPACIENTE(A.PAC_PAC_NUMERO) PACIENTE, A.ORDNUMERO, A.TAB_GRUPOSANGCODIGO GRP,A.PAC_ANT_FACTORH RH,A.FECPRC FECSOLICITUD,DECODE(A.ORDESTADO,'S','SOLICITADA','C','CONFIRMADA') ESTADO " +
                           "from bco_solictranf a, PAC_PACIENTE B " +
                          "where trunc(a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and  to_date('" + FechaFin + "','yyyy/mm/dd') AND A.PAC_PAC_NUMERO NOT IN ('1308','5024') AND A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO " +
                            "and a.ORDTIPO='B' ORDER BY 1,6 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet NMuestrasMes(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select B.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE( B.PAC_PAC_NUMERO) PACIENTE, a.CEX_EXS_ORDENTRABA ORDEN,count(*) CANTIDAD " +
                          "from cex_examsolic a, PAC_PACIENTE B, CEX_EVENTOS C " +
                         "where trunc(a.CEX_EXS_FECHAMUEST) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ATE_PRE_TIPOFORMU='05' and C.CEX_EVE_TIPOEVENT='6 ' AND A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO AND A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                         "AND A.PAC_PAC_NUMERO=C.ATE_PRE_NUMERPACIE AND A.ATE_PRE_TIPOFORMU=C.ATE_PRE_TIPOFORMU AND A.ATE_PRE_NUMERFORMU=C.ATE_PRE_NUMERFORMU AND A.CEX_EXS_CODIGPREST=C.CEX_EXS_CODIGPREST " +
                         "group by a.CEX_EXS_ORDENTRABA, B.PAC_PAC_RUT,B.PAC_PAC_NUMERO ORDER BY 2,3";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet OportunidadLabambulatorios(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT IDENTIFICACION, PACIENTE, CONVENIO, CODIGO, DESCRIPCION, FEC_SOLICITUD, GENERADA, FEC_GENERACION, ROUND(OPORTUNIDAD_GENERACION,2) OPORTUNIDAD_GENE, EVENTO,FECHADIG FEC_REVISION, ROUND(OPORT,2) OPORTUNIDAD_REVI, NUMERO1 NORDEN  FROM (SELECT a.ATE_PRE_TIPOFORMU tipo1,a.ATE_PRE_NUMERFORMU numero1,b.CEX_EXS_CODIGPREST cod1,a.PAC_PAC_NUMERO pacnum1,c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,b.CEX_EXS_CODIGPREST codigo,f.PRE_PRE_DESCRIPCIO descripcion,a.RPA_FOR_FECHASOLIC FEC_SOLICITUD,DECODE(b.CEX_EVE_TIPOEVENT,3,'GENERADA') generada,b.ATE_PRE_FECHADIGIT FEC_GENERACION,(b.ATE_PRE_FECHADIGIT-a.RPA_FOR_FECHASOLIC) *24 OPORTUNIDAD_GENERACION " +
                           "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                   "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU " +
                                      "and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU " +
                                      "and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                      "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU " +
                                      "and d.CON_CON_CODIGO=e.CON_CON_CODIGO " +
                                      "and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO " +
                                      "and a.ORDTIPO='L' " +
                                      "and a.SER_SER_AMBITO='01' " +
                                      "and b.CEX_EVE_TIPOEVENT in ('3 ') " +
                                      "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                      "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')), " +
                                    "(SELECT a.ATE_PRE_TIPOFORMU tipo,a.ATE_PRE_NUMERFORMU numero,b.CEX_EXS_CODIGPREST cod,a.PAC_PAC_NUMERO pacnum,DECODE(b.CEX_EVE_TIPOEVENT,6,'REVISADA') evento,b.ATE_PRE_FECHADIGIT fechadig,(b.ATE_PRE_FECHADIGIT-a.RPA_FOR_FECHASOLIC)*24 oport  " +
                                                       "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                                               "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU " +
                                                                  "and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU " +
                                                                  "and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                                                  "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU " +
                                                                  "and d.CON_CON_CODIGO=e.CON_CON_CODIGO " +
                                                                  "and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO " +
                                                                  "and a.ORDTIPO='L' " +
                                                                  "and a.SER_SER_AMBITO='01' " +
                                                                  "and b.CEX_EVE_TIPOEVENT in ('6 ') " +
                                                                  "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                                                  "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                    "WHERE TIPO1=TIPO(+) AND NUMERO1=NUMERO(+) AND COD1=COD(+) AND PACNUM1=PACNUM(+) " +
                    "GROUP BY IDENTIFICACION, PACIENTE, CONVENIO, CODIGO, DESCRIPCION, FEC_SOLICITUD, GENERADA, OPORTUNIDAD_GENERACION, EVENTO,FECHADIG, OPORT, FEC_GENERACION, numero1 " +
                    "ORDER BY 1,13 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet PacientesLaboratorio(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select c.ATE_PRE_FECHAENTRE fec_atencion,B.PAC_PAC_RUT identificacion,nomcompletopaciente(B.Pac_Pac_Numero) paciente,b.PAC_PAC_SEXO GENERO,b.PAC_PAC_FECHANACIM fec_nacimiento,round((C.ATE_PRE_FECHAENTRE-B.PAC_PAC_FECHANACIM)/365) edad, replace(d.CON_CON_DESCRIPCIO,',','-') convenio,replace(b.PAC_PAC_DIRECCIONGRALHABIT,',','.') Direccion,replace(b.PAC_PAC_FONO,',','.') telefono,c.ATE_PRE_CODIGO codigo ,replace(e.PRE_PRE_DESCRIPCIO,',','.') prestacion " +
                            "from pac_paciente b,ate_prestacion c,con_convenio d, pre_prestacion e " +
                            "where b.PAC_PAC_NUMERO=c.ATE_PRE_NUMERPACIE and c.CON_CON_CODIGO=d.CON_CON_CODIGO  and c.ATE_PRE_CODIGO=e.PRE_PRE_CODIGO and e.PRE_PRE_TIPO='0029' " +
                            "and TRUNC(c.ATE_PRE_FECHAENTRE) between  to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and b.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                            "order by 3,1  ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet OportunidadLaboratorios(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT IDENTIFICACION, PACIENTE, CONVENIO, CODIGO, DESCRIPCION, FEC_SOLICITUD, GENERADA, FEC_GENERACION, ROUND(OPORTUNIDAD_GENERACION,2) OPORTUNIDAD_GENE, EVENTO,FECHADIG FEC_REVISION, ROUND(OPORT,2) OPORTUNIDAD_REVI, NUMERO1 NORDEN  FROM (SELECT a.ATE_PRE_TIPOFORMU tipo1,a.ATE_PRE_NUMERFORMU numero1,b.CEX_EXS_CODIGPREST cod1,a.PAC_PAC_NUMERO pacnum1,c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,b.CEX_EXS_CODIGPREST codigo,f.PRE_PRE_DESCRIPCIO descripcion,a.RPA_FOR_FECHASOLIC FEC_SOLICITUD,DECODE(b.CEX_EVE_TIPOEVENT,3,'GENERADA') generada,b.ATE_PRE_FECHADIGIT FEC_GENERACION,(b.ATE_PRE_FECHADIGIT-a.RPA_FOR_FECHASOLIC) *24 OPORTUNIDAD_GENERACION " +
                           "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                   "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU " +
                                      "and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU " +
                                      "and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                      "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU " +
                                      "and d.CON_CON_CODIGO=e.CON_CON_CODIGO " +
                                      "and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO " +
                                      "and a.ORDTIPO='L' " +
                                      "and a.SER_SER_AMBITO='01' " +
                                      "and b.CEX_EVE_TIPOEVENT in ('3 ') " +
                                      "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                      "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')), " +
                                    "(SELECT a.ATE_PRE_TIPOFORMU tipo,a.ATE_PRE_NUMERFORMU numero,b.CEX_EXS_CODIGPREST cod,a.PAC_PAC_NUMERO pacnum,DECODE(b.CEX_EVE_TIPOEVENT,6,'REVISADA') evento,b.ATE_PRE_FECHADIGIT fechadig,(b.ATE_PRE_FECHADIGIT-a.RPA_FOR_FECHASOLIC)*24 oport  " +
                                                       "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                                               "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU " +
                                                                  "and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU " +
                                                                  "and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                                                  "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU " +
                                                                  "and d.CON_CON_CODIGO=e.CON_CON_CODIGO " +
                                                                  "and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO " +
                                                                  "and a.ORDTIPO='L' " +
                                                                  "and a.SER_SER_AMBITO='01' " +
                                                                  "and b.CEX_EVE_TIPOEVENT in ('6 ') " +
                                                                  "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                                                  "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                    "WHERE TIPO1=TIPO(+) AND NUMERO1=NUMERO(+) AND COD1=COD(+) AND PACNUM1=PACNUM(+) " +
                    "GROUP BY IDENTIFICACION, PACIENTE, CONVENIO, CODIGO, DESCRIPCION, FEC_SOLICITUD, GENERADA, OPORTUNIDAD_GENERACION, EVENTO,FECHADIG, OPORT, FEC_GENERACION, numero1 " +
                    "UNION ALL " +
                    "SELECT PAC.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(ORD.PAC_PAC_NUMERO) PACIENTE,CONV.CON_CON_DESCRIPCIO CONVENIO,ODET.PRE_PRE_CODIGO CODIGO ,PRE.PRE_PRE_DESCRIPCIO DESCRIPCION, ORD.ORDFECHA FEC_SOLICITUD, 'GENERADA' GENERADA, " +
                           "Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '3 ')   FEC_GENERACION, " +
                           "ROUND( (Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '3 ') -ORD.ORDFECHA)*24,2) OPORTUNIDAD_GENE,  'REVISADA' REVISADA, " +
                           "Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '6 ')   FEC_REVISION, " +
                           "ROUND( (Lab_Evento(RPA.ATE_PRE_TIPOFORMU, RPA.ATE_PRE_NUMERFORMU, XSOL.CEX_EXS_CODIGPREST, '6 ')  -ORD.ORDFECHA)*24,2) OPORTUNIDAD_REVI,RPA.ATE_PRE_NUMERFORMU NORDEN " +  
                    "FROM TABORDENESSERV ORD, TABORDDETALLE ODET,  RPA_FORLAB RPA, CEX_EXAMSOLIC XSOL, PRE_PRESTACION PRE, PAC_PACIENTE PAC,rpa_formulario FORMU, CON_CONVENIO CONV " +
                    "WHERE ORD.ORDNUMERO = ODET.ORDNUMERO AND ORD.PAC_PAC_NUMERO = ODET.PAC_PAC_NUMERO AND ORD.ORDTIPO = ODET.ORDTIPO " +
                    "AND TRUNC(ORD.ORDFECHA) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND  TO_DATE('" + FechaFin + "','YYYY/MM/DD') AND ORD.ORDTIPO = 'L' AND ORD.ORDESTADO = 'R' AND ORD.PAC_PAC_NUMERO =PAC.PAC_PAC_NUMERO " +
                    "AND ORD.ORDTIPO = RPA.ORDTIPO AND ORD.ORDNUMERO = RPA.ORDNUMERO AND RPA.SER_SER_AMBITO IN ('02', '03') AND RPA.PAC_PAC_NUMERO=FORMU.PAC_PAC_NUMERO " +
                    "AND RPA.RPA_FOR_TIPOFORMU =FORMU.RPA_FOR_TIPOFORMU AND RPA.RPA_FOR_NUMERFORMU =FORMU.RPA_FOR_NUMERFORMU AND FORMU.CON_CON_CODIGO=CONV.CON_CON_CODIGO " +
                    "AND RPA.ATE_PRE_TIPOFORMU = XSOL.ATE_PRE_TIPOFORMU AND RPA.ATE_PRE_NUMERFORMU = XSOL.ATE_PRE_NUMERFORMU AND ODET.PRE_PRE_CODIGO = XSOL.CEX_EXS_CODIGPREST AND ODET.PRE_PRE_CODIGO=PRE.PRE_PRE_CODIGO " +
                    "ORDER BY 1,13 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet OrdenesBancoSangre(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select b.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente ,a.ORDNUMERO NORDEN " +
                           "from bco_solictranf a,pac_paciente b " +
                          "where trunc(a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and  to_date('" + FechaFin + "','yyyy/mm/dd') and a.ORDTIPO='B' and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                            "and a.ORDESTADO='S' and a.PAC_PAC_NUMERO not in ('1308','5024') order by 2  ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
