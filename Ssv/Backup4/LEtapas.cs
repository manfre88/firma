﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LEtapas
    {

        public bool EtapaGuardar(Etapas act)
        {
            string[] nomParam = { "@Codigo", "@Nombre", "@Usuario" };
            object[] vlrParam = { act.Codigo, act.Nombre, act.Usuario };

            return EjecutarSentencia("uspEtapaActualizar", nomParam, vlrParam);
        }

        public DataTable EtapaEstado(Etapas act)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                act.Codigo,
                act.Estado
            };

            return getDataTable("uspEtapasEstado", nomParam, vlrParam);
        }

        public DataTable EtapasConsultar(Etapas act)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { act.Codigo };

            return getDataTable("uspEtapasConsultar", nomParam, vlrParam);
        }

        public bool EtapasReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Etapa", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspEtapasReactivar", nomParam, valParam);
        }

        public bool EtapaRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Etapa", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspEtapasRetirar", nomParam, valParam);
        }

        public DataTable ActividadConsultar()
        {
            return getDataTable("uspEtapasConsultarCodigos");
        }

        public bool EtapasRetirar(string xmlEtapa, string Usuario)
        {
            string[] nomParam = { "@xmlEtapa", "@Usuario" };
            object[] valParam = { xmlEtapa, Usuario };

            return EjecutarSentencia("uspEtapasRetirarXml", nomParam, valParam);
        }

        public DataTable ActConsultar(Etapas nvl)
        {
            string[] nomParam = { "@Etapa" };
            object[] vlrParam = { nvl.Codigo };

            return getDataTable("uspActConsultar", nomParam, vlrParam);
        }

        public bool ActXGrupoActualizar(string xml, string etapa) 
        {
            string[] nomParam = { "@xmlPages", "@ProfileID" };
            object[] vlrParam = { xml, etapa };

            return EjecutarSentencia("uspActXGrupoActualizar", nomParam, vlrParam);
        }


        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataset(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion


    }
}
