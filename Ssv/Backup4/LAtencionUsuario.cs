﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAtencionUsuario
    {

        public DataSet LlamadaPOSEgreso(string Especialidad, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Especialidad",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Especialidad,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            if (Especialidad != "-1")
            {
                Especialidad = " and f.TIPOPROFECODIGO = '" + Especialidad + "' ";
            }
            else
            {
                Especialidad = "";
            }

            string sql = "SELECT C.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(C.PAC_PAC_NUMERO) PACIENTE,B.FECQRF FECHA_CIRUGIA,decode(D.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,a.ORDNUMERO N_ORDEN,TRIM(H.PRE_PRE_DESCRIPCIO) PRESTACION, j.CON_CON_DESCRIPCIO convenio ,F.TIPOPROFENOMBRE ESPECIALIDAD, C.PAC_PAC_FONO TELEFONO " +
                      "FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d,tabprdrel e,tab_tipoprofe f,ser_profesiona g, pre_prestacion H, ATE_PRESTACION I, CON_Convenio j  " +
                      "WHERE a.ORDNUMERO=d.ORDNUMERO " +
                            "and A.ORDNUMERO=E.ORDNUMERO and i.CON_CON_CODIGO=j.CON_CON_CODIGO " +
                            "and E.SER_PRO_RUT=G.SER_PRO_RUT " +
                            "and G.SER_PRO_TIPO=F.TIPOPROFECODIGO " +
                            "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO " +
                            "AND A.ORDNUMERO=B.ORDNUMERO " +
                            "AND E.PRE_PRE_CODIGO = H.PRE_PRE_CODIGO " +
                            "and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "AND A.ORDTIPO='P' AND E.PRE_PRE_CODIGO=I.ATE_PRE_CODIGO AND E.RPA_FOR_NUMERFORMU=I.ATE_PRE_NUMERFORMU AND E.RPA_FOR_TIPOFORMU = I.ATE_PRE_TIPOFORMU " +
                            "AND E.PAC_PAC_NUMERO = I.ATE_PRE_NUMERPACIE " + Especialidad + " " +
                            "AND d.ORDCIRESTCOD IN ('V','X') " +
                            "And D.HORAFIN <> '00:00' " +
                            "and trim(C.PAC_PAC_RUT) not in ('10','12') " +
                      "GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,D.AMBITOCODIGO,F.TIPOPROFENOMBRE,a.ORDNUMERO, H.PRE_PRE_DESCRIPCIO, j.CON_CON_DESCRIPCIO, C.PAC_PAC_FONO " +
                      "ORDER BY B.FECQRF,a.ORDNUMERO,D.AMBITOCODIGO ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet LlamadaPOSVentaHosp(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select p.PAC_PAC_Rut IDENTIFICACION, NOMCOMPLETOPACIENTE(a.PAC_PAC_Numero) PACIENTE, a.ATC_EST_FechaHospi FECHA_INGRESO, a.ATC_EST_FECEGRESO FECHA_EGRESO " +
                                 ",p.PAC_PAC_FONO TELEFONO, SubStr(b.SER_SER_Descripcio,1,40) SERVICIO, ROUND((a.ATC_EST_FECEGRESO - a.ATC_EST_FechaHospi),2) DIAS, C.CON_CON_Descripcio CONVENIO " +
                                 ",decode(N.PAC_PAC_Cotizante,0,'BENEFICIARIO',1,'COTIZANTE',2,'BENEF-COTIZ',3,'NINGUNO') as TIPO_AFILIADO, decode(N.PAC_PAC_TipoBenef,'1','1','2','2','3','3','A','A','B','B','C','C',' ',' ') CATEGORIA " +
                                 ",d.DIA_DIA_CODIGO CIE10, e.DIA_DIA_DESCRIPCIO DIAGNOSTICO " +
                           "From ATC_Estadia   a, SER_Servicios b, Pac_Paciente  p, PAC_Novedades N, CON_Convenio  C, tabdiagnosticos d, dia_diagnos e " +
                          "where a.AtcEstado = 'X' and a.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO and a.MTVCORRELATIVO=d.MTVCORRELATIVO and d.DGNPRINCIPAL='S' and d.DIA_DIA_CODIGO=e.DIA_DIA_CODIGO " +
                            "And a.SER_SER_Codigo  = b.SER_SER_Codigo And p.PAC_PAC_Numero  = a.PAC_PAC_Numero and trunc(a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "and N.PAC_PAC_Numero = a.PAC_PAC_NUMERO And N.PAC_PAC_Codigo = C.CON_CON_Codigo and p.PAC_PAC_CODIGO = C.CON_CON_CODIGO Order  by 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
