﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAuditoriaAsistencial
    {
        public DataSet PacientesxPrestacion(string Prestacion, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Prestacion"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Prestacion
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select c.ATE_PRE_FECHAENTRE fec_atencion,b.PAC_PAC_RUT identificacion,nomcompletopaciente(C.ATE_PRE_NUMERPACIE) paciente,B.PAC_PAC_FONO TELEFONO,d.CON_CON_DESCRIPCIO convenio,f.PRE_TIP_DESCRIPCIO tipo_prestacion,e.PRE_PRE_CODIGO cod_prestacion,e.PRE_PRE_DESCRIPCIO prestacion,c.ATE_PRE_MONTOTARIF tarifa  " +
                           "from pac_paciente b,ate_prestacion c,con_convenio d,pre_prestacion e, pre_tipo f " +
                          "where b.PAC_PAC_NUMERO=c.ATE_PRE_NUMERPACIE and c.CON_CON_CODIGO=d.CON_CON_CODIGO and c.ATE_PRE_CODIGO=e.PRE_PRE_CODIGO and B.PAC_PAC_NUMERO not in ('5024', '1308') and C.ATE_PRE_VIGENCIA<>'N' " +
                            "and trunc(c.ATE_PRE_FECHAENTRE) between  to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and e.PRE_PRE_TIPO=f.PRE_TIP_TIPO and c.ATE_PRE_CODIGO in ('" + Prestacion + "') order by 3,7,1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet PacientesOftalmologia(string Ambito, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Ambito"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Ambito
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if (Ambito == "")
            {
                sql = "select d.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) PACIENTE,d.PAC_PAC_FONO TELEFONO,TRIM(c.SER_PRO_APELLPATER) || ' ' ||trim( c.SER_PRO_APELLMATER) || ' ' || trim(c.SER_PRO_NOMBRES) PROFESIONAL,F.TIPOPROFENOMBRE ESPECIALIDAD,e.FECPRC FEC_CITA, decode(G.SER_SER_AMBITO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO " +
                        "from ser_profesiona c,pac_paciente d,tabopcexfojo e, TAB_TIPOPROFE F, TABMOTIVOCONS G " +
                       "where TRUNC(e.FECPRC) between  to_date('" + FechaIni + "','yyyy/mm/dd') and  to_date('" + FechaFin + "','yyyy/mm/dd') AND C.SER_PRO_TIPO=F.TIPOPROFECODIGO and e.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO  and E.PAC_PAC_NUMERO not in ('1308','5024') " +
                         "and e.USRCOD=c.SER_PRO_CUENTA AND E.PAC_PAC_NUMERO=G.PAC_PAC_NUMERO AND E.MTVCORRELATIVO=G.MTVCORRELATIVO order by 6 ";
            }
            else
            {
                sql = "select d.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) PACIENTE,d.PAC_PAC_FONO TELEFONO,TRIM(c.SER_PRO_APELLPATER) || ' ' ||trim( c.SER_PRO_APELLMATER) || ' ' || trim(c.SER_PRO_NOMBRES) PROFESIONAL,F.TIPOPROFENOMBRE ESPECIALIDAD,e.FECPRC FEC_CITA, decode(G.SER_SER_AMBITO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO " +
                        "from ser_profesiona c,pac_paciente d,tabopcexfojo e, TAB_TIPOPROFE F, TABMOTIVOCONS G " +
                       "where TRUNC(e.FECPRC) between  to_date('" + FechaIni + "','yyyy/mm/dd') and  to_date('" + FechaFin + "','yyyy/mm/dd') AND C.SER_PRO_TIPO=F.TIPOPROFECODIGO and e.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO  and E.PAC_PAC_NUMERO not in ('1308','5024') " +
                         "and e.USRCOD=c.SER_PRO_CUENTA AND E.PAC_PAC_NUMERO=G.PAC_PAC_NUMERO AND E.MTVCORRELATIVO=G.MTVCORRELATIVO and  G.SER_SER_AMBITO='" + Ambito + "' order by 6 ";
            
            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet AltasxDestino(string FechaIni, string FechaFin, string Servicio)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_servicio"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Servicio
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if ((Servicio != "-1") && (Servicio != "001     "))
            {
                Servicio = " and Z.SER_SER_CODIGO = '" + Servicio + "' ";

                sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, A.ATC_EST_FECHAHOSPI fec_ingreso , a.ATC_EST_FECEGRESO fec_egreso, " +
                              "a.SER_OBJ_CODIGO cama, c.DIA_DIA_CODIGO dxalta,trim(f.DIA_DIA_DESCRIPCIO) descripcion,g.ALTCNDNOM condicion, H.DSTALTNOM destino, round( (a.ATC_EST_FECEGRESO- A.ATC_EST_FECHAHOSPI) ,2)dias " +
                            "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x " +
                           "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO <> '        ' " +
                             "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') " +
                             "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                             "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO  and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO " +
                             " " + Servicio + " " +
                             "order by 7,8,3";
            }
            else
            {
                if (Servicio == "-1")
                {
                    Servicio = "";
                }
                else
                {
                    Servicio = " and Z.SER_SER_CODIGO = '" + Servicio + "' ";
                }

                sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, A.ATC_EST_FECHAHOSPI fec_ingreso , a.ATC_EST_FECEGRESO fec_egreso, " +
                              "a.SER_OBJ_CODIGO cama, c.DIA_DIA_CODIGO dxalta,trim(f.DIA_DIA_DESCRIPCIO) descripcion,g.ALTCNDNOM condicion, H.DSTALTNOM destino, round( (a.ATC_EST_FECEGRESO- A.ATC_EST_FECHAHOSPI) ,2)dias " +
                            "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x " +
                           "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO <> '        ' " +
                             "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') " +
                             "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                             "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO  and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO " +
                             " " + Servicio + " " +
                       "union all " +
                       "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,S.CON_CON_DESCRIPCIO convenio,b.RPA_FDA_HORAINGRESO Fec_ingreso,b.RPA_FDA_HORAEGRESO fec_egreso,'' cama,G.DIA_DIA_CODIGO dxalta, H.DIA_DIA_DESCRIPCIO descripcion ,'MUERTO' Condicion, 'MORGUE' Destino , round( (b.RPA_FDA_HORAEGRESO- b.RPA_FDA_HORAINGRESO) ,2)dias " +                        
                          "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h, con_convenio s " +
                        "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and b.MTVCORRELATIVO=c.MTVCORRELATIVO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU " +
                            "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and E.ETDING='M' and trunc(B.RPA_FDA_HORAEGRESO ) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and B.CON_CON_CODIGO=S.CON_CON_CODIGO " +
                            "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' " +
                            "and trim(A.PAC_PAC_RUT) not in ('10','12') and A.PAC_PAC_NUMERO not in (select A.PAC_PAC_NUMERO " +                      
                                                             "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h, tabnotpac i " +
                                                             "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO  and b.MTVCORRELATIVO=c.MTVCORRELATIVO " +
                                                                "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and I.NOTCLSCOD='10' and I.NOTITMCOD='16' and I.NOTPACRES='S' " +
                                                                "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU and B.MTVCORRELATIVO=I.MTVCORRELATIVO " +
                                                                "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO " +
                                                                "and trunc(B.RPA_FDA_HORAEGRESO ) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                                                                "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO and trim(A.PAC_PAC_RUT) not in ('10','12') " +
                                                                "and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' ) " +
                        "union all " +
                        "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,S.CON_CON_DESCRIPCIO convenio ,b.RPA_FDA_HORAINGRESO Fec_ingreso,b.RPA_FDA_HORAEGRESO fec_egreso,'' cama,G.DIA_DIA_CODIGO dxalta, H.DIA_DIA_DESCRIPCIO descripcion , 'MUERTO' Condicion, 'MORGUE' Destino,round( (b.RPA_FDA_HORAEGRESO- b.RPA_FDA_HORAINGRESO) ,2)dias " +                      
                          "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h, tabnotpac i, con_convenio s " +
                         "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and b.MTVCORRELATIVO=c.MTVCORRELATIVO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and I.NOTCLSCOD='10' and I.NOTITMCOD='16' and I.NOTPACRES='S' " +
                            "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU and B.MTVCORRELATIVO=I.MTVCORRELATIVO and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO " +
                            "and trunc(B.RPA_FDA_HORAEGRESO) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO " +
                            "and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' and trim(A.PAC_PAC_RUT) not in ('10','12') and B.CON_CON_CODIGO=S.CON_CON_CODIGO " +
                         "order by 7,8,3";
            }

            

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet AlertasxPaciente(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente ,b.HCP_ETI_DESCRIPCIO alerta, trim(D.SER_PRO_APELLPATER) || ' ' || trim(D.SER_PRO_APELLMATER) || ' ' || trim(D.SER_PRO_NOMBRES) Profesional,a.FECPRC fecha " +
                           "from pac_hojaetiqu a,hcp_etiqupobla b,pac_paciente c, ser_profesiona d " +
                          "where a.HCP_ETI_CODIGO=b.HCP_ETI_CODIGO and A.USRCOD=D.SER_PRO_CUENTA and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and trunc(a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "and c.PAC_PAC_NUMERO not in ('1308','5024') order by 2,5 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet PacientesxMedico(string FechaIni, string FechaFin, string Ambito, string Especialidad, string Profesional)
        {
            string Ambito1;

            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Ambito",
                "In_Especialidad",
                "In_Profesional"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Ambito,
                Especialidad,
                Profesional
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            if (Ambito != "")
            {
                Ambito = " and A.AMBITOCODIGO = '" + Ambito + "' ";
                Ambito1 = " and A.SER_SER_AMBITO = '" + Ambito + "' ";
            }
            else
            {
                Ambito = "";
                Ambito1 = "";
            }

            if (Especialidad != "-1")
            {
                Especialidad = " and W.SER_ESP_CODIGO = '" + Especialidad + "' ";
            }
            else
            {
                Especialidad = "";
            }


            if (Profesional != "")
            {
                Profesional = " and trim(C.SER_PRO_RUT) = trim('" + Profesional + "') ";
            }
            else
            {
                Profesional = "";
            }

            string sql = "select B.PAC_PAC_RUT identificacion, nomcompletopaciente(A.PAC_PAC_NUMERO) paciente, decode(a.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO, " +
                                "A.MTVCORRELATIVO evento, A.TABENCUENTRO evolucion,A.FECPRC fecha ,trim(C.SER_PRO_APELLPATER) || ' ' || trim(C.SER_PRO_APELLMATER) || ' ' || trim(C.SER_PRO_NOMBRES) Profesional, W.SER_ESP_DESCRIPCIO especialidad, '' codigo, '' prestacion, b.pac_pac_fono telefono " +
                           "from tabencuentros a, pac_paciente b, ser_profesiona c, Ser_Especiali w,  Net_SerEspSerPro z, (select DISTINCT (E.MTVCORRELATIVO|| E.TABENCUENTRO ||E.PAC_PAC_NUMERO) NUME  FROM tabevosubj e where E.USRCOD<>'ADMSALUD' and trunc(e.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') ) " +
                           "where trunc(A.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO not in ('5024','1308') " +
                             "and A.SER_PRO_RUT=Z.SER_PRO_RUT  and Z.SER_ESP_CODIGO=W.SER_ESP_CODIGO AND C.SER_PRO_RUT=Z.SER_PRO_RUT AND (A.MTVCORRELATIVO|| A.TABENCUENTRO|| A.PAC_PAC_NUMERO)=NUME " + Ambito + "  " + Especialidad + "  " + Profesional + " " +
                          "union all " +
                          "select B.PAC_PAC_RUT identificacion, nomcompletopaciente(A.PAC_PAC_NUMERO) paciente, decode(a.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO, " +
                                "A.MTVCORRELATIVO evento, A.TABENCUENTRO evolucion,A.FECPRC fecha ,trim(C.SER_PRO_APELLPATER) || ' ' || trim(C.SER_PRO_APELLMATER) || ' ' || trim(C.SER_PRO_NOMBRES) Profesional, W.SER_ESP_DESCRIPCIO especialidad,'' codigo, '' prestacion, b.pac_pac_fono telefono " +
                           "from tabencuentros a, pac_paciente b, ser_profesiona c, Ser_Especiali w,  Net_SerEspSerPro z, (select DISTINCT (E.MTVCORRELATIVO|| E.TABENCUENTRO ||E.PAC_PAC_NUMERO) NUME  FROM tabevosubj e where E.USRCOD<>'ADMSALUD' and trunc(e.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') ) " +
                           "where trunc(A.FECAPROBADO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO not in ('5024','1308')  " +
                             "and C.SER_PRO_RUT=Z.SER_PRO_RUT  and Z.SER_ESP_CODIGO=W.SER_ESP_CODIGO AND A.ENCAPROBADO='1'  and C.SER_PRO_CUENTA=A.CODMEDTRAT " + Ambito + "  " + Especialidad + "  " + Profesional + " " +
                             "AND (A.MTVCORRELATIVO|| A.TABENCUENTRO|| A.PAC_PAC_NUMERO)=NUME " +
                          "UNION ALL " +
                          "Select distinct  e.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(E.PAC_PAC_NUMERO) PACIENTE,decode(a.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO , A.MTVCORRELATIVO evento, 0 EVOLUCION,A.FECQRF fecha,  trim(C.SER_PRO_APELLPATER) || ' ' || trim(C.SER_PRO_APELLMATER) || ' ' || trim(C.SER_PRO_NOMBRES) Profesional, " +
                                "W.SER_ESP_DESCRIPCIO especialidad,g.PRE_PRE_CODIGO CODIGO,J.PRE_PRE_DESCRIPCIO PRESTACION,e.PAC_PAC_FONO TELEFONO " +
                          "from taborddetcir a,pre_prestacion J,pac_paciente e,TABPRDREL g, con_convenio i, ser_profesiona C, Ser_Especiali  K,Ser_Especiali w,Net_SerEspSerPro z, tabprgqrf b " +
                          "where a.ORDNUMERO = g.ORDNUMERO and Z.SER_ESP_CODIGO = W.SER_ESP_CODIGO " +
                            "and trunc(B.FECINGQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "and G.PRE_PRE_CODIGO=J.PRE_PRE_CODIGO and A.ORDNUMERO=B.ORDNUMERO and A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO " +
                            "and a.HORAFIN<>'00:00'  and A.PAC_PAC_NUMERO not in ('5024','1308') " +
                            "and e.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                            "AND G.SER_PRO_RUT=C.SER_PRO_RUT AND C.SER_PRO_RUT=Z.SER_PRO_RUT  AND Z.SER_ESP_CODIGO =K.SER_ESP_CODIGO " + Ambito + "  " + Especialidad + "  " + Profesional + " " +
                          "UNION ALL " +
                          "select B.PAC_PAC_RUT identificacion, nomcompletopaciente(A.PAC_PAC_NUMERO) paciente, " +
                                 "decode(A.SER_SER_AMBITO ,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO, A.MTVCORRELATIVO evento, A.TABCANTENCUENTROS evolucion,A.MTVFECHACONSULTA fecha, " +
                                 "trim(C.SER_PRO_APELLPATER) || ' ' || trim(C.SER_PRO_APELLMATER) || ' ' || trim(C.SER_PRO_NOMBRES) Profesional, W.SER_ESP_DESCRIPCIO especialidad, " +
                                 "'' codigo, '' prestacion, b.pac_pac_fono telefono " +
                            "from tabmotivocons a, pac_paciente b, ser_profesiona c, Ser_Especiali w,  Net_SerEspSerPro z " +
                           "where trunc(A.MTVFECHACONSULTA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO " +
                                 "and A.PAC_PAC_NUMERO not in ('5024','1308') and A.SER_PRO_RUT=Z.SER_PRO_RUT  and Z.SER_ESP_CODIGO=W.SER_ESP_CODIGO AND C.SER_PRO_RUT=Z.SER_PRO_RUT " + Ambito1 + "  " + Especialidad + "  " + Profesional + " " +
                                 "AND NOT EXISTS (select DISTINCT E.MTVCORRELATIVO NUME  FROM tabevosubj e where E.USRCOD=C.SER_PRO_CUENTA AND E.MTVCORRELATIVO=A.MTVCORRELATIVO AND E.TABENCUENTRO=A.TABCANTENCUENTROS AND E.PAC_PAC_NUMERO=A.PAC_PAC_NUMERO) " +
                            "order by 2, 3,4,5 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet NOPOSOrdenados(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            
            string sql = "select distinct FECHA_ORDEN, IDENTIFICACION,PACIENTE,CONVENIO, PRODUCTO, PROFESIONAL, CAMA, SERVICIO, DILIGENCIADO, ESTADO, PARAMETRIZADO, CIRUGIA from " +
                        "(SELECT DISTINCT H.FECPRC FECHA_ORDEN, B.PAC_PAC_RUT identificacion ,NOMCOMPLETOPACIENTE(PAC) PACIENTE,G.CON_CON_DESCRIPCIO CONVENIO,C.INS_INS_DESCRIPCIO PRODUCTO, " +
                                        "TRAERPROFESIONAL(D.SER_PRO_RUT) PROFESIONAL, '' CAMA, 'URGENCIAS' SERVICIO, decode(A.FRMCANTIDAD,'','NO','SI') DILIGENCIADO " +
                                        ", decode(A.ETDJUSMED,'','NO APROBADA',decode(A.ETDJUSMED,0,'NO APROBADA','APROBADA'))  ESTADO, DECODE(JUSNOPOS,'','NO','SI') PARAMETRIZADO , 'NO' CIRUGIA " +
                             "FROM  tabfrmjus a, pac_paciente b,ins_insumos c,SER_PROFESIONA D, RPA_FORDAU E, CON_CONVENIO G,tabencuentros H, " +
                                        "(SELECT  Z.PAC_PAC_NUMERO PAC, Z.FRMCORRELATIVO FRMCOR, Z.INS_INS_CODIGINSUM INSUMO, Z.MTVCORRELATIVO CORRE, Z.TABENCUENTRO ENCUE, Z.TIPOFORMUCODIGO TIPOFOR, X.USRCOD usu " +
                                          "FROM TabFrmDetallE Z,TABEVOSUBJ X  WHERE X.PAC_PAC_NUMERO not in ('1308','5024') " +
                                             "and  X.MTVCORRELATIVO=Z.MTVCORRELATIVO AND X.TABENCUENTRO=Z.TABENCUENTRO " +
                                             "AND X.PAC_PAC_NUMERO=Z.PAC_PAC_NUMERO AND X.USRCOD<>'ADMSALUD' " +
                                             "AND TRUNC(Z.FECPRC) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD')), " +
                                          "(SELECT A.XINS_INS_CODIGINSUM JUSNOPOS FROM TabJusContPar A, TabEstMedPos B, TabjusSitRsg C " +
                                            "WHERE A.XINS_INS_CODIGINSUM=B.XINS_INS_CODIGINSUM AND B.XINS_INS_CODIGINSUM=C.XINS_INS_CODIGINSUM " +
                                             "GROUP BY A.XINS_INS_CODIGINSUM) " +
                            "where pac=B.PAC_PAC_NUMERO and insumo=C.INS_INS_CODIGINSUM and usu=D.SER_PRO_CUENTA " +
                                "AND pac=E.PAC_PAC_NUMERO AND CORRE=E.MTVCORRELATIVO and c.ins_ins_pos='N' " +
                                "and pac=H.PAC_PAC_NUMERO and corre=H.MTVCORRELATIVO and encue=H.TABENCUENTRO and H.USRCOD = D.SER_PRO_CUENTA " +
                               "AND E.CON_CON_CODIGO =G.CON_CON_CODIGO AND INSUMO = JUSNOPOS(+) " +
                               "AND PAC=A.PAC_PAC_NUMERO(+) AND FRMCOR=A.FRMCORRELATIVO(+) AND INSUMO=A.INS_INS_CODIGINSUM(+) " +
                               "AND CORRE=A.MTVCORRELATIVO(+) AND ENCUE=A.TABENCUENTRO(+) AND TIPOFOR=A.TIPOFORMUCODIGO(+) " +
                        "UNION " +
                        "SELECT DISTINCT H.FECPRC FECHA_ORDEN, B.PAC_PAC_RUT identificacion, NOMCOMPLETOPACIENTE(PAC) PACIENTE,G.CON_CON_DESCRIPCIO CONVENIO, C.INS_INS_DESCRIPCIO PRODUCTO, " +
                                        "DECODE(USU,' ' ,' ',TRAERPROFESIONAL(D.SER_PRO_RUT)) PROFESIONAL, DECODE(M.EST_CEN_CODIGO,'209     ','', E.SER_OBJ_CODIGO) CAMA, DECODE(M.EST_CEN_CODIGO,'209     ','URGENCIAS', F.SER_SER_DESCRIPCIO)  SERVICIO, decode(A.FRMCANTIDAD,'','NO','SI') DILIGENCIADO " +
                                        ", decode(A.ETDJUSMED,'','NO APROBADA',decode(A.ETDJUSMED,0,'NO APROBADA','APROBADA'))  ESTADO, DECODE(JUSNOPOS,'','NO','SI') PARAMETRIZADO , 'NO' CIRUGIA  " +
                             "FROM  tabfrmjus a, pac_paciente b,ins_insumos c,SER_PROFESIONA D, ATC_ESTADIA E, SER_servicioS F, CON_CONVENIO G,  tabencuentros H, ATE_INSUMOS M, " +
                                        "(SELECT Z.PAC_PAC_NUMERO PAC, Z.FRMCORRELATIVO FRMCOR, Z.INS_INS_CODIGINSUM INSUMO, Z.MTVCORRELATIVO CORRE, Z.TABENCUENTRO ENCUE, Z.TIPOFORMUCODIGO TIPOFOR,X.USRCOD usu  " +
                                          "FROM TabFrmDetallE Z,TABEVOSUBJ X  WHERE X.PAC_PAC_NUMERO not in ('1308','5024') " +
                                             "and  X.MTVCORRELATIVO=Z.MTVCORRELATIVO AND X.TABENCUENTRO=Z.TABENCUENTRO  " +
                                             "AND X.PAC_PAC_NUMERO=Z.PAC_PAC_NUMERO AND X.USRCOD<>'ADMSALUD' " +
                                             "AND TRUNC(X.FECPRC) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD')),  " +
                                          "(SELECT A.XINS_INS_CODIGINSUM JUSNOPOS FROM TabJusContPar A, TabEstMedPos B, TabjusSitRsg C " +
                                            "WHERE A.XINS_INS_CODIGINSUM=B.XINS_INS_CODIGINSUM AND B.XINS_INS_CODIGINSUM=C.XINS_INS_CODIGINSUM " +
                                             "GROUP BY A.XINS_INS_CODIGINSUM)  " +
                            "where pac=B.PAC_PAC_NUMERO and insumo=C.INS_INS_CODIGINSUM and D.SER_PRO_CUENTA = usu(+) and c.ins_ins_pos='N' " +
                                "and pac=H.PAC_PAC_NUMERO and corre=H.MTVCORRELATIVO and encue=H.TABENCUENTRO and H.USRCOD = D.SER_PRO_CUENTA " +
                                "AND pac=E.PAC_PAC_NUMERO AND CORRE=E.MTVCORRELATIVO AND INSUMO = JUSNOPOS(+) " +
                               "AND E.SER_SER_CODIGO = F.SER_SER_CODIGO(+) AND E.CON_CON_CODIGO=G.CON_CON_CODIGO " +
                               "AND PAC=A.PAC_PAC_NUMERO(+) AND FRMCOR=A.FRMCORRELATIVO(+) AND INSUMO=A.INS_INS_CODIGINSUM(+) " +
                               "AND CORRE=A.MTVCORRELATIVO(+) AND ENCUE=A.TABENCUENTRO(+) AND TIPOFOR=A.TIPOFORMUCODIGO(+)  " +
                               "AND PAC=M.ATE_INS_NUMERPACIE(+) AND FRMCOR=M.FRMCORRELATIVO(+) AND INSUMO=M.ATE_INS_CODIGO(+) " +
                        "UNION " +
                        "SELECT DISTINCT H.FECPRC FECHA_ORDEN, B.PAC_PAC_RUT identificacion , NOMCOMPLETOPACIENTE(PAC) PACIENTE,G.CON_CON_DESCRIPCIO CONVENIO ,C.INS_INS_DESCRIPCIO PRODUCTO, " +
                                        "TRAERPROFESIONAL(D.SER_PRO_RUT) PROFESIONAL, DECODE(M.EST_CEN_CODIGO,'209     ','', E.SER_OBJ_CODIGO) CAMA, DECODE(M.EST_CEN_CODIGO,'209     ','URGENCIAS', F.SER_SER_DESCRIPCIO)  SERVICIO, decode(A.FRMCANTIDAD,'','NO','SI') DILIGENCIADO " +
                                        ", decode(A.ETDJUSMED,'','NO APROBADA',decode(A.ETDJUSMED,0,'NO APROBADA','APROBADA'))  ESTADO, DECODE(JUSNOPOS,'','NO','SI') PARAMETRIZADO , 'NO' CIRUGIA  " +
                             "FROM  tabfrmjus a, pac_paciente b,ins_insumos c,SER_PROFESIONA D, ATC_ESTADIA E, SER_servicioS F, CON_CONVENIO G, tabencuentros H, ATE_INSUMOS M, " +  
                                        "(SELECT Z.PAC_PAC_NUMERO PAC, Z.FRMCORRELATIVO FRMCOR, Z.INS_INS_CODIGINSUM INSUMO, Z.MTVCORRELATIVO CORRE, Z.TABENCUENTRO ENCUE, Z.TIPOFORMUCODIGO TIPOFOR  " +
                                            "FROM TabFrmDetallE Z WHERE Z.PAC_PAC_NUMERO not in ('1308','5024') " +
                                               "AND z.USRCOD=' ' AND TRUNC(z.FECPRC) = TO_DATE('1900/01/01','YYYY/MM/DD') " +
                                                "and NOT EXISTS ( select x.MTVCORRELATIVO from TABEVOSUBJ X where X.USRCOD = 'ADMSALUD' " +
                                                                               "and X.MTVCORRELATIVO=Z.MTVCORRELATIVO AND X.TABENCUENTRO=Z.TABENCUENTRO  " +
                                                                               "AND X.PAC_PAC_NUMERO=Z.PAC_PAC_NUMERO AND TRUNC(X.FECPRC) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') )),  " +
                                          "(SELECT A.XINS_INS_CODIGINSUM JUSNOPOS FROM TabJusContPar A, TabEstMedPos B, TabjusSitRsg C " +
                                            "WHERE A.XINS_INS_CODIGINSUM=B.XINS_INS_CODIGINSUM AND B.XINS_INS_CODIGINSUM=C.XINS_INS_CODIGINSUM " +
                                             "GROUP BY A.XINS_INS_CODIGINSUM)  " +
                            "where pac=B.PAC_PAC_NUMERO and insumo=C.INS_INS_CODIGINSUM and H.MTVCORRELATIVO=corre and H.TABENCUENTRO=encue and H.PAC_PAC_NUMERO=pac " +
                                "and D.SER_PRO_CUENTA = H.USRCOD and TRUNC(H.FECPRC) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                                "AND pac=E.PAC_PAC_NUMERO AND CORRE=E.MTVCORRELATIVO AND INSUMO = JUSNOPOS(+) and c.ins_ins_pos='N' " +
                               "AND E.SER_SER_CODIGO = F.SER_SER_CODIGO(+) AND E.CON_CON_CODIGO=G.CON_CON_CODIGO " +
                               "AND PAC=A.PAC_PAC_NUMERO(+) AND FRMCOR=A.FRMCORRELATIVO(+) AND INSUMO=A.INS_INS_CODIGINSUM(+) " +
                               "AND CORRE=A.MTVCORRELATIVO(+) AND ENCUE=A.TABENCUENTRO(+) AND TIPOFOR=A.TIPOFORMUCODIGO(+)  " +    
                               "AND PAC=M.ATE_INS_NUMERPACIE(+) AND FRMCOR=M.FRMCORRELATIVO(+) AND INSUMO=M.ATE_INS_CODIGO(+) " +
                        "UNION  " +                   
                        "SELECT DISTINCT H.FECPRC FECHA_ORDEN, B.PAC_PAC_RUT identificacion , NOMCOMPLETOPACIENTE(PAC) PACIENTE,G.CON_CON_DESCRIPCIO CONVENIO,C.INS_INS_DESCRIPCIO PRODUCTO, " +
                                        "TRAERPROFESIONAL(D.SER_PRO_RUT) PROFESIONAL, DECODE(M.EST_CEN_CODIGO,'209     ','', E.SER_OBJ_CODIGO) CAMA, DECODE(M.EST_CEN_CODIGO,'209     ','URGENCIAS', F.SER_SER_DESCRIPCIO)  SERVICIO, decode(A.FRMCANTIDAD,'','NO','SI') DILIGENCIADO " +
                                        ", decode(A.ETDJUSMED,'','NO APROBADA',decode(A.ETDJUSMED,0,'NO APROBADA','APROBADA'))  ESTADO, DECODE(JUSNOPOS,'','NO','SI') PARAMETRIZADO , 'NO' CIRUGIA  " +
                             "FROM  tabfrmjus a, pac_paciente b,ins_insumos c,SER_PROFESIONA D, ATC_ESTADIA E, SER_servicioS F, CON_CONVENIO G, tabencuentros H, ATE_INSUMOS M, " +         
                                        "(SELECT Z.PAC_PAC_NUMERO PAC, Z.FRMCORRELATIVO FRMCOR, Z.INS_INS_CODIGINSUM INSUMO, Z.MTVCORRELATIVO CORRE, Z.TABENCUENTRO ENCUE, Z.TIPOFORMUCODIGO TIPOFOR  " +
                                            "FROM TabFrmDetallE Z WHERE Z.PAC_PAC_NUMERO not in ('1308','5024')  " +
                                               "AND z.USRCOD <>' ' AND TRUNC(z.FECPRC) <> TO_DATE('1900/01/01','YYYY/MM/DD') " +
                                                "and NOT EXISTS ( select x.MTVCORRELATIVO from TABEVOSUBJ X where X.USRCOD = 'ADMSALUD' AND X.PAC_PAC_NUMERO not in ('1308','5024') " +
                                                                               "and X.MTVCORRELATIVO=Z.MTVCORRELATIVO AND X.TABENCUENTRO=Z.TABENCUENTRO  " +
                                                                               "AND X.PAC_PAC_NUMERO=Z.PAC_PAC_NUMERO AND TRUNC(X.FECPRC) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') )),  " +
                                          "(SELECT A.XINS_INS_CODIGINSUM JUSNOPOS FROM TabJusContPar A, TabEstMedPos B, TabjusSitRsg C " +
                                            "WHERE A.XINS_INS_CODIGINSUM=B.XINS_INS_CODIGINSUM AND B.XINS_INS_CODIGINSUM=C.XINS_INS_CODIGINSUM " +
                                             "GROUP BY A.XINS_INS_CODIGINSUM)  " +
                            "where pac=B.PAC_PAC_NUMERO and insumo=C.INS_INS_CODIGINSUM and H.MTVCORRELATIVO=corre and H.TABENCUENTRO=encue and H.PAC_PAC_NUMERO=pac " +
                                "and D.SER_PRO_CUENTA = H.USRCOD and TRUNC(H.FECPRC) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                                "AND pac=E.PAC_PAC_NUMERO AND CORRE=E.MTVCORRELATIVO AND INSUMO = JUSNOPOS(+) and c.ins_ins_pos='N' " +
                               "AND E.SER_SER_CODIGO = F.SER_SER_CODIGO(+) AND E.CON_CON_CODIGO=G.CON_CON_CODIGO " +
                               "AND PAC=A.PAC_PAC_NUMERO(+) AND FRMCOR=A.FRMCORRELATIVO(+) AND INSUMO=A.INS_INS_CODIGINSUM(+) " +
                               "AND CORRE=A.MTVCORRELATIVO(+) AND ENCUE=A.TABENCUENTRO(+) AND TIPOFOR=A.TIPOFORMUCODIGO(+) " +
                               "AND PAC=M.ATE_INS_NUMERPACIE(+) AND FRMCOR=M.FRMCORRELATIVO(+) AND INSUMO=M.ATE_INS_CODIGO(+) " +
                        "UNION " +
                        "Select  A.ATE_INS_FECHAENTRE fecha,D.PAC_PAC_RUT identificacion ,nomcompletopaciente(A.ATE_INS_NUMERPACIE) paciente,Z.CON_CON_DESCRIPCIO ,b.Fld_ProductoGlosa, TRAERPROFESIONAL(p.SER_PRO_RUT) PROFESIONAL " +
                                    ",E.SER_OBJ_CODIGO CAMA, F.SER_SER_DESCRIPCIO SERVICIO ,  decode(G.FRMCANTIDAD,'','NO','SI') DILIGENCIADO " +
                                        ", decode(G.ETDJUSMED,'','NO APROBADA',decode(G.ETDJUSMED,0,'NO APROBADA','APROBADA'))  ESTADO, DECODE(JUSNOPOS,'','NO','SI') PARAMETRIZADO , 'SI' CIRUGIA  " +
                               "From  ATE_Insumos  a, ABA_Producto b, TabInsQrf  c  ,Ins_Insumos i, pac_paciente d,PrqEquMed P, ATC_ESTADIA E, SER_servicioS F, tabfrmjus G,  CON_CONVENIO Z, " +
                                          "(SELECT A.XINS_INS_CODIGINSUM JUSNOPOS FROM TabJusContPar A, TabEstMedPos B, TabjusSitRsg C " +
                                            "WHERE A.XINS_INS_CODIGINSUM=B.XINS_INS_CODIGINSUM AND B.XINS_INS_CODIGINSUM=C.XINS_INS_CODIGINSUM " +
                                             "GROUP BY A.XINS_INS_CODIGINSUM)  " +
                              "Where a.ATE_INS_Codigo = b.Fld_ProductoCodigo And b.FLD_PRODUCTOCODIGO = i.fld_productocodigo And i.INS_INS_VIGENCIA <> 'N' " +
                                "And a.ATE_INS_NumerPacie not in ('1308','5024') and TRUNC(A.ATE_INS_FECHAENTRE) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                                "And a.ATE_INS_Cantidad - a.FrmCntDev > 0 And a.ATE_INS_NumerPacie = c.PAC_PAC_Numero And a.ATE_INS_Codigo = c.Fld_ProductoCodigo AND a.ATE_INS_Codigo = JUSNOPOS(+) " +
                                "And a.TipoFormuCodigo = c.OrdTipo And a.FrmCorrelativo = c.OrdNumero and I.INS_INS_POS='N' and A.ATE_INS_NUMERPACIE=D.PAC_PAC_NUMERO AND E.CON_CON_CODIGO=Z.CON_CON_CODIGO " +
                                "and  C.PAC_PAC_NUMERO=P.PAC_PAC_NUMERO and A.FRMCORRELATIVO=P.ORDNUMERO and A.TIPOFORMUCODIGO = P.ORDTIPO and P.LBRPRQCOD='01' " +
                                "and A.ATC_EST_NUMERO=E.ATC_EST_NUMERO(+) and A.ATE_INS_NUMERPACIE=E.PAC_PAC_NUMERO(+) and E.SER_SER_CODIGO = F.SER_SER_CODIGO(+) " +
                               "AND A.ATE_INS_NUMERPACIE=G.PAC_PAC_NUMERO(+) AND A.FRMCORRELATIVO=G.FRMCORRELATIVO(+) AND a.ATE_INS_CodigO=G.INS_INS_CODIGINSUM(+)) " +
                        "ORDER BY 3,1,4,5 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet NOPOSFacturados(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select A.CON_CON_CODIGO codigo,E.CON_CON_DESCRIPCIO convenio,a.ATE_INS_CODIGO COD_INSUMO,c.FLD_PRODUCTOGLOSA INSUMO,sum(a.ATE_INS_CANTIDAD- a.FRMCNTDEV) cantidad, round(sum(A.ATE_INS_MONTOTARIF)/sum(a.ATE_INS_CANTIDAD- a.FRMCNTDEV) ,2)precio_unitario, sum(A.ATE_INS_MONTOTARIF) precio_total " +
                           "from ate_insumos a,aba_producto c, con_convenio e, rpa_formulario f, TABFCTEMP B " +
                          "where a.ATE_INS_CODIGO=c.FLD_PRODUCTOCODIGO AND F.FAC_FAC_FACTURA=B.FAC_FAC_FACTURA AND F.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO AND F.CON_CON_CODIGO=B.CON_CON_CODIGO " +
                            "and trunc(B.FAC_FAC_FECHAFACTURACION) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "and A.CON_CON_CODIGO=E.CON_CON_CODIGO AND C.PRDPOS=0 " +
                            "and A.ATE_INS_NUMERPACIE not in ('1308','5024')  and F.FAC_FAC_FACTURA<>' ' " +
                            "AND A.ATE_INS_VIGENCIA<>'N' and A.ATE_INS_TIPOFORMU=F.RPA_FOR_TIPOFORMU and A.ATE_INS_NUMERFORMU=F.RPA_FOR_NUMERFORMU " +
                       "group by a.ATE_INS_CODIGO,c.FLD_PRODUCTOGLOSA, A.CON_CON_CODIGO,E.CON_CON_DESCRIPCIO " +
                          "ORDER BY 2,4 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

    }
}
