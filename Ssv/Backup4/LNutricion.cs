﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LNutricion
    {
        public DataSet PacientesSinDieta(string Fecha)
        {
            string[] nomParam = 
            {
                "In_Fecha"
            };

            object[] vlrParam = 
            {
                Fecha
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT DISTINCT B.PAC_PAC_RUT identificacion, nomcompletopaciente(B.PAC_PAC_NUMERO) paciente, A.SER_OBJ_CODIGO cama, A.ATC_EST_FECHAHOSPI fecha_ingreso, round((SYSDATE-A.ATC_EST_FECHAHOSPI),2) dias, trim(D.SER_PRO_NOMBRES) || ' '  || trim( D.SER_PRO_APELLPATER) || ' '  || trim(D.SER_PRO_APELLMATER) medico_tratante " +
                           "FROM ATC_ESTADIA A, pac_paciente b, tabmotivocons c, ser_profesiona d " +
                          "WHERE A.ATCESTADO='H' and A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO and A.MTVCORRELATIVO=C.MTVCORRELATIVO and C.SER_PRO_RUT=D.SER_PRO_RUT " +
                            "AND TRUNC(A.ATC_EST_FECHAHOSPI) BETWEEN to_date('" + Fecha + "' ,'yyyy/mm/dd')-12/24  AND to_date('" + Fecha + "','yyyy/mm/dd') " +
                            "AND (A.PAC_PAC_NUMERO|| A.MTVCORRELATIVO) NOT IN ( " +
                                        "SELECT DISTINCT m.PAC_PAC_NUMERO||m.MTVCORRELATIVO " +
                                          "FROM ATC_ESTADIA m, TABORDDIE n, TABORDDIEDET l " +
                                         "WHERE m.ATCESTADO='H' " +
                                           "AND m.PAC_PAC_NUMERO=n.PAC_PAC_NUMERO AND m.MTVCORRELATIVO=n.MTVCORRELATIVO " +
                                           "AND n.PAC_PAC_NUMERO=l.PAC_PAC_NUMERO AND n.MTVCORRELATIVO=l.MTVCORRELATIVO " +
                                           "AND n.TABENCUENTRO =l.TABENCUENTRO AND TRUNC(m.ATC_EST_FECHAHOSPI) BETWEEN to_date('" + Fecha + "' ,'yyyy/mm/dd')-12/24  AND to_date('" + Fecha + "','yyyy/mm/dd') " +
                                           "and TO_CHAR(n.FECPRC,'HH24:MI') < '09:01' and m.SER_OBJ_CODIGO<>'        ') " +
                         "ORDER BY 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
    }
}
