﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LHonorarios
    {

        public DataSet Glosas(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select a.NTANRO NOTA,a.FAC_FAC_FACTURA FACTURA,a.OBSNTA OBSERVACION,d.CSANTANOM CAUSA_NOTA,i.FECQRF FECHA_PROCEDIMIENTO,h.HORAINI HORA_INI,h.HORAFIN HORA_FIN,b.PRE_PRE_CODIGO COD_PRESTACION,m.PRE_PRE_DESCRIPCIO PRESTACION,b.VLRNTA VALOR_NOTA, " +
                               " g.CNPQRGCOD CONCEPTO_QX,g.ATE_PRE_MONTOPAGAR MONTO_PAGAR,f.SER_PRO_RUT ID_CIRUJANO,TRIM(j.SER_PRO_NOMBRES) || ' ' || TRIM(j.SER_PRO_APELLPATER) CIRUJANO,f.ANESID ID_anestesiologo, " +
                               " TRIM(k.SER_PRO_NOMBRES)|| ' ' || TRIM(k.SER_PRO_APELLPATER) anestesiologo,f.AYUDID ID_ayudantia,TRIM(l.SER_PRO_NOMBRES)|| ' ' || TRIM(l.SER_PRO_APELLPATER) ayudantia " +
                          " from tabfctnta a,tabfctntadet b,tabcsanta d,ate_prestacion e,tabprdrel f,tabprdreldet g,taborddetcir h, " +
                               " tabprgqrf i,ser_profesiona j,ser_profesiona k,ser_profesiona l,pre_prestacion m " +
                          " where a.NTANRO=b.NTANRO and a.CSANTACOD=d.CSANTACOD and a.CSANTACOD in ('04','06') and a.RPA_FOR_NUMERFORMU=e.ATE_PRE_NUMERFORMU " +
                          " and a.RPA_FOR_TIPOFORMU=e.ATE_PRE_TIPOFORMU and a.PAC_PAC_NUMERO=e.ATE_PRE_NUMERPACIE " +
                          " and trunc(a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                          " and a.NTATPO=b.NTATPO and a.NTANRO=b.NTANRO and b.PRE_PRE_CODIGO <>'        ' and b.ATE_INS_CODIGO='            ' " +
                          " and b.PRE_PRE_CODIGO=e.ATE_PRE_CODIGO and e.ORDNUMERO=f.ORDNUMERO and f.ORDNUMERO=g.ORDNUMERO and g.PRE_PRE_CODIGO=b.PRE_PRE_CODIGO " +
                          " and f.PRE_PRE_CODIGO=g.PRE_PRE_CODIGO and f.ORDNUMERO=h.ORDNUMERO and f.ORDNUMERO=i.ORDNUMERO and e.ATE_PRE_CODIGO=m.PRE_PRE_CODIGO " +
                          " and f.SER_PRO_RUT=j.SER_PRO_RUT and f.ANESID=k.SER_PRO_RUT(+) and f.AYUDID=l.SER_PRO_RUT (+) Order by 1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet EcogEcocar(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select a.ate_pre_codigo codigo,b.pre_pre_descripcio descripcion,b.pre_pre_tipo cod_tipo,c.PRE_TIP_DESCRIPCIO tipo,b.pre_pre_subtipo cod_subtipo,a.ate_pre_cantidad cantidad,  d.PAC_PAC_RUT identificacion, " +
                                "nomcompletopaciente(A.ATE_PRE_NUMERPACIE) paciente ,a.ate_pre_montotarif tarifa,a.VALHONMED valor_Hon_medico,F.FAC_FAC_FACTURA factura, F.FAC_FAC_FECHAFACTURACION fecfact , " +
                                "E.SOCRZN raz_social,a.ATE_PRE_CODIGUSUAR usuario " +
                           "from ate_prestacion a,pre_prestacion b,pre_tipo c,pac_paciente d,tabsoccop e, rpa_formulario f " +
                          "where pre_pre_codigo = ate_pre_codigo and trunc (F.FAC_FAC_FECHAFACTURACION) BETWEEN to_date('" + FechaIni + "','yyyy/mm/dd') AND to_date('" + FechaFin + "','yyyy/mm/dd') and b.PRE_PRE_TIPO =c.PRE_TIP_TIPO " +
                            "and c.PRE_TIP_TIPO in ('0027','0037','0041') and a.ATE_PRE_NUMERPACIE=d.PAC_PAC_NUMERO AND a.SOCCOD=e.SOCCOD and A.ATE_PRE_TIPOFORMU= F.RPA_FOR_TIPOFORMU and F.RPA_FOR_VIGENCIA<>'N' " +
                            "and A.ATE_PRE_NUMERFORMU=F.RPA_FOR_NUMERFORMU and A.ATE_PRE_NUMERPACIE=F.PAC_PAC_NUMERO  and F.FCTANL<>1 and F.FAC_FAC_FACTURA<>' ' and A.ATE_PRE_VIGENCIA<>'N' " +
                       "order by a.ate_pre_codigo,d.PAC_PAC_NOMBRE,F.FAC_FAC_FACTURA ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet Mamografias(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select d.PAC_PAC_RUT identificacion,nomcompletopaciente(A.ATE_PRE_NUMERPACIE) paciente ,a.CON_CON_CODIGO cod_convenio,e.CON_CON_DESCRIPCIO convenio,a.ate_pre_codigo cod_prestacion,b.pre_pre_descripcio descripcion,b.pre_pre_tipo id_tipo, " +
                                "c.PRE_TIP_DESCRIPCIO tipo, b.pre_pre_subtipo ID_subtipo,a.ate_pre_cantidad cantidad,a.ate_pre_centrresul cent_result,a.ate_pre_montotarif tarifa,F.FAC_FAC_FACTURA factura,F.FAC_FAC_FECHAFACTURACION fec_fact " +
                           "from ate_prestacion a,pre_prestacion b,pre_tipo c,pac_paciente d,con_convenio e, rpa_formulario f " +
                          "where pre_pre_codigo = ate_pre_codigo and trunc(F.FAC_FAC_FECHAFACTURACION) BETWEEN to_date('" + FechaIni + "','yyyy/mm/dd') AND to_date('" + FechaFin + "','yyyy/mm/dd') and b.PRE_PRE_TIPO =c.PRE_TIP_TIPO and A.ATE_PRE_VIGENCIA<>'N' " +
                            "and B.PRE_PRE_DESCRIPCIO LIKE '%MAMOGRAFI%' and a.ATE_PRE_NUMERPACIE=d.PAC_PAC_NUMERO and a.CON_CON_CODIGO=e.CON_CON_CODIGO and F.FCTANL<>1 and F.FAC_FAC_FACTURA<>' ' " +
                            "and A.ATE_PRE_TIPOFORMU= F.RPA_FOR_TIPOFORMU and A.ATE_PRE_NUMERFORMU=F.RPA_FOR_NUMERFORMU and A.ATE_PRE_NUMERPACIE=F.PAC_PAC_NUMERO and F.RPA_FOR_VIGENCIA<>'N' " +
                       "order by a.ate_pre_codigo,d.PAC_PAC_NOMBRE,F.FAC_FAC_FACTURA ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }


    }
}
