﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaCES.Entidades;
using System.Data;

namespace ClinicaCES.Logica
{
    public class LNivel
    {
        public bool NivelActualizar(Nivel nvl)
        {
            string[] nomParam = { "@Codigo", "@Nivel" };
            object[] vlrParam = { nvl.Codigo, nvl.nivel };

            return EjecutarSentencia("uspNivelActualizar", nomParam, vlrParam);
        }

        public DataTable NivelConsultar(Nivel nvl)
        { 
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { nvl.Codigo };

            return getDataTable("uspNivelConsultar", nomParam, vlrParam);
        }

        public DataTable MenuConsultar(Nivel nvl)
        {
            string[] nomParam = { "@Nivel" };
            object[] vlrParam = { nvl.Codigo };

            return getDataTable("uspMenuConsultar", nomParam, vlrParam);
        }

        public bool NivelXMenuActualizar(string xml,string nivel)
        {
            string[] nomParam = { "@xmlPages", "@ProfileID" };
            object[] vlrParam = { xml, nivel };

            return EjecutarSentencia("uspMenuXPaginaActualizar", nomParam, vlrParam);
        }

        public string NivelRetirar(Nivel nvl)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { nvl.Codigo };

            DataTable dt = getDataTable("uspNivelRetirar", nomParam, vlrParam);
            string Msg = "3";
            if(dt.Rows.Count > 0)
                Msg = dt.Rows[0]["MSG"].ToString();

            return Msg;
        }

        #region Metodos Privados

        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {            
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion

    }
}
