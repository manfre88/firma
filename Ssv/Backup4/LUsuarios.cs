﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LUsuarios
    {
        public DataTable UsuarioConsultar(Usuarios usr)
        {
            string[] nomParam = { "@Usuario" };
            object[] vlrParam = { usr.Usuario };

            return getDataTable("uspUsuariosConsultar", nomParam, vlrParam);
        }

        public DataTable TemasConsultar()
        {
            return getDataTable("uspTemaListar");
        }

        public string Email(string Usuario)
        {
            string[] nomParam = { "@usuario" };
            object[] vlrParam = { Usuario };

            DataTable dt = getDataTable("uspUsuarioEmail", nomParam, vlrParam);

            return dt.Rows[0]["EMAIL"].ToString();
        }

        public DataTable UsuarioVSConsultar()
        {
            return getDataTable("uspUsuarioVisualSoftistar");
        }

        public DataTable UsuarioConsultar()
        {
            return getDataTable("uspUsuarioListar");
        }

        public bool UsuarioRetirar(Usuarios usr)
        {
            string[] nomParam = { "@Usuario" };
            object[] vlrParam = { usr.Usuario };

            return EjecutarSentencia("uspUsuarioRetirar", nomParam, vlrParam);
        }

        public bool UsuarioActualizar(Usuarios usr)
        {
            string[] Cliente = usr.Cliente.Split('|');

            string[] nomParam = { 
            "@Usuario",
	        "@Clave" ,
	        "@Nivel" ,
	        "@Nombre" ,
	        "@Apellido1" ,
	        "@Apellido2",
	        "@Direccion" ,
	        "@Telefono" ,
	        "@UsrMod" ,
	        "@Email" ,
            "@Tema",
            "@Nit",
            "@Negocio"
            };

            object[] vlrParam = {
            usr.Usuario,
            usr.Clave,
            usr.Nivel,
            usr.Nombres,
            usr.Apellido1,
            usr.Apellido2,
            usr.Direccion,
            usr.Telefono,
            usr.UsrMod,
            usr.Email,
            usr.Tema,
            Cliente[0],Cliente[1]
            };

            return EjecutarSentencia("uspUsuarioActualizar", nomParam, vlrParam);
        }

        #region Metodos Privados

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }
        #endregion
    }
}
