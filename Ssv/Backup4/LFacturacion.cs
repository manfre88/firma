﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LFacturacion
    {

        public DataSet FacturasxConvenio(string FechaIni, string FechaFin, string Convenios)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Convenios"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Convenios
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select A.FAC_FAC_FACTURA factura,C.CON_CON_DESCRIPCIO convenio ,A.FAC_FAC_FECHAFACTURACION fec_factura,A.FAC_FAC_VALORFACT valor_factura,B.PAC_PAC_RUT identificacion,nomcompletopaciente(B.PAC_PAC_NUMERO ) paciente " +
                           "from tabfctemp a, pac_paciente b, con_convenio c " +
                          "where a.CON_CON_CODIGO in ('" + Convenios + "') and b.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO (+) and A.ESTADO<>'A' " +
                            "and trunc(A.FAC_FAC_FECHAFACTURACION) between to_date('" + FechaIni + "','yyyy/mm/dd') and  to_date('" + FechaFin + "','yyyy/mm/dd') and A.CON_CON_CODIGO=C.CON_CON_CODIGO order by 2,3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataTable BuscarUrgencia(string NumUrgencia)
        {
            string[] nomParam = 
            {
                "In_NumUrgencia"
            };

            object[] vlrParam = 
            {
                NumUrgencia
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select nomcompletopaciente(B.PAC_PAC_NUMERO) paciente, A.TRIAGECOD, C.TAB_CONT_CODIGO CAUEXT, A.FECPRC fecha " +
                           "from tabtriage a, pac_paciente b, rpa_fordau c " +
                          "where A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO and A.RPA_FOR_TIPOFORMU=C.RPA_FOR_TIPOFORMU and " +
                                "A.RPA_FOR_NUMERFORMU=C.RPA_FOR_NUMERFORMU and A.RPA_FOR_TIPOFORMU='04' and A.RPA_FOR_NUMERFORMU = '" + NumUrgencia + "' ";

            return new DA.DA("Oracle").getDataTable(sql, nomParam, vlrParam);
        }

        public DataSet PendientexFacturar(string Ambito, string FechaCorte)
        {
            string[] nomParam = 
            {
                "In_FechaCorte",
                "In_Ambito"
            };

            object[] vlrParam = 
            {
                FechaCorte,
                Ambito
            };


            if (Ambito != "")
            {
                Ambito = " and rpa.RPA_FOR_URGENCIA = '" + Ambito + " ' ";
            }
            else
            {
                Ambito = "";
            }

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select Funglbgetrutpac(rpa.PAC_PAC_NUMERO) identificacion ,Glbgetnompac(rpa.PAC_PAC_NUMERO)  paciente ,trim(c.con_con_descripcio) convenio ,decode(rpa.RPA_FOR_URGENCIA,'S','urgencia','H','hospitalario','Ambulatorio') Ambito ,rpa.RPA_FOR_FECHADIGIT fecha_solicitud " +
                                 ",rpa.RPA_FOR_FECHATENCION fecha_atencion ,rpa.RPA_FOR_ETDCTA estado ,funglbGetValdetCta(rpa.RPA_FOR_TIPOFORMU, rpa.RPA_FOR_NUMERFORMU) ValorCta " +
                                 ",RPA.RPA_FOR_TIPOFORMU tipo_formu, RPA.RPA_FOR_NUMERFORMU formuLARIO,rpa.RPA_FOR_CODIGRECEP recepciono ,rpa.RPA_FOR_VIGENCIA vigencia " +
                         "from  rpa_formulario rpa, con_convenio c " +
                        "where TRUNC (rpa.RPA_FOR_FECHATENCION) >= to_date('2008/07/31','yyyy/mm/dd') and TRUNC (rpa.RPA_FOR_FECHATENCION) <=  LAST_DAY(to_date('" + FechaCorte + "','yyyy/mm/dd'))  and rpa.RPA_FOR_TIPOFORMU = '02  ' " +
                        "and rpa.RPA_FOR_VIGENCIA <> 'N' and rpa.RPA_FOR_FECHATENCION <> to_date('1900/01/01','yyyy/mm/dd') AND rpa.FAC_FAC_FECHAFACTURACION = to_date('1900/01/01','yyyy/mm/dd') and rpa.CON_CON_CODIGO <> 'CP      ' and funglbGetValdetCta(rpa.RPA_FOR_TIPOFORMU, rpa.RPA_FOR_NUMERFORMU)>'0' " +
                        "and rpa.CON_CON_CODIGO = c.con_con_codigo " + Ambito + " and not exists (select 'x' from tabfctgrp grp where grp.RPA_FOR_NUMERFORMU = rpa.RPA_FOR_NUMERFORMU and   grp.RPA_FOR_TIPOFORMU  = rpa.RPA_FOR_TIPOFORMU " +
                                                                                                                     "and   grp.PAC_PAC_NUMERO      = rpa.PAC_PAC_NUMERO and   grp.FECPRC <=  LAST_DAY(to_date('" + FechaCorte + "','yyyy/mm/dd')) + 1 ) " +
                        "and (exists (select 'x'  from ate_prestacion ate where ate.ATE_PRE_NUMERPACIE = rpa.PAC_PAC_NUMERO and ate.ATE_PRE_NUMERFORMU = rpa.RPA_FOR_NUMERFORMU " +
                                       "and ate.ATE_PRE_TIPOFORMU  = rpa.RPA_FOR_TIPOFORMU  and ate.ATE_PRE_CONCEPAGO  <> 'TP') " +
                              "or exists (select 'x' from ate_insumos ins where ins.ATE_ins_NUMERPACIE = rpa.PAC_PAC_NUMERO and ins.ATE_ins_NUMERFORMU = rpa.RPA_FOR_NUMERFORMU " +
                                               "and ins.ATE_ins_TIPOFORMU  = rpa.RPA_FOR_TIPOFORMU and ins.CONCEPAGO <> 'TP'   )) " +
                        "order by 6 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }
        
        public DataSet FacturadoMesesAnteriores(string FechaCorte,string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaCorte",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaCorte,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select Funglbgetrutpac(rpa.PAC_PAC_NUMERO) identificacion ,Glbgetnompac(rpa.PAC_PAC_NUMERO) paciente,trim(c.con_con_descripcio) convenio ,decode(rpa.RPA_FOR_URGENCIA,'S','urgencia','H','hospitalario','Ambulatorio') Ambito " +
                               ",rpa.RPA_FOR_FECHATENCION fecha_atencion ,rpa.RPA_FOR_ETDCTA estado,funglbGetValdetCta(rpa.RPA_FOR_TIPOFORMU, rpa.RPA_FOR_NUMERFORMU) ValorCta ,RPA.RPA_FOR_TIPOFORMU TIPOFORMU " +
                               ",RPA.RPA_FOR_NUMERFORMU formulario, rpa.RPA_FOR_CODIGRECEP recepciono ,rpa.RPA_FOR_VIGENCIA vigencia ,RPA.FAC_FAC_FACTURA factura, RPA.FAC_FAC_FECHAFACTURACION FECHAFACTURACION " +
                         "from  rpa_formulario rpa, con_convenio c " +
                        "where TRUNC (rpa.RPA_FOR_FECHATENCION) >= to_date('2008/07/31','yyyy/mm/dd') and TRUNC (rpa.RPA_FOR_FECHATENCION) <=  LAST_DAY(to_date('" + FechaCorte + "','yyyy/mm/dd')) and rpa.RPA_FOR_TIPOFORMU = '02  ' " +
                          "and rpa.RPA_FOR_VIGENCIA <> 'N' and rpa.RPA_FOR_FECHATENCION <> to_date('1900/01/01','yyyy/mm/dd') AND TRUNC(rpa.FAC_FAC_FECHAFACTURACION) BETWEEN to_date('" + FechaIni + "','yyyy/mm/dd') AND to_date('" + FechaFin + "','yyyy/mm/dd') " +
                          "and rpa.CON_CON_CODIGO       <> 'CP      ' and rpa.CON_CON_CODIGO = c.con_con_codigo " +
                          "and not exists (select 'x'  from tabfctanlrgn anl where ANL.FAC_FAC_GENERADA = RPA.FAC_FAC_FACTURA and anl.FECPRC BETWEEN to_date('" + FechaIni + "','yyyy/mm/dd') AND to_date('" + FechaFin + "','yyyy/mm/dd')) order by 5";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet FecEgresoVSFecFacturacion(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };


   

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT IDENTIFICACION,PACIENTE,CONVENIO,formulario, FECSALIDA fecha_egreso,FACTURA,FECHA_FACTURA,SUM(TARIFA) AS TOTAL_FACTURA, ROUND(DIF,0) DIAS, AMBITO " +
                         "FROM " +
                           "(Select  distinct Alt.FecPrc As FecSalida,RPA.RPA_FOR_NUMERFORMU formulario,(Pac.PAC_PAC_Rut) as identificacion,nomcompletopaciente(PAC.PAC_PAC_NUMERO) As paciente " +
                                  ",Cnv.CON_CON_Descripcio as convenio ,DECODE(RPA.FAC_FAC_FACTURA,' ','NO',RPA.FAC_FAC_FACTURA) AS FACTURA , ROUND (SUM (A.Ate_pre_montotarif)) as tarifa " +
                                  ", FCT.FECPRC AS FECHA_FACTURA , (FCT.FECPRC - Alt.FecPrc) AS DIF, 'HOSPITALIZACION' ambito " +
                              "From TabAltOrd Alt,ATC_Estadia Atc,PAC_Paciente Pac,CON_Convenio Cnv ,SER_Objetos Obj ,atc_cuenta cta ,rpa_formulario rpa ,tabpagadores pag,Ate_prestacion A,tabfctgrp fct " +
                             "Where Pac.PAC_PAC_Numero = Alt.PAC_PAC_Numero And Atc.PAC_PAC_Numero = Alt.PAC_PAC_Numero And Atc.ATC_Est_Numero = Alt.ATC_Est_Numero and atc.ATC_EST_NUMERO = cta.ATC_EST_NUMERO " +
                               "and atc.PAC_PAC_NUMERO = cta.PAC_PAC_NUMERO and rpa.RPA_FOR_NUMERFORMU0 = cta.RPA_FOR_NUMERFORMU and rpa.RPA_FOR_TIPOFORMU0 = cta.RPA_FOR_TIPOFORMU and rpa.PAC_PAC_NUMERO = cta.PAC_PAC_NUMERO " +
                               "and rpa.RPA_FOR_NUMERFORMU = pag.RPA_FOR_NUMERFORMU and rpa.RPA_FOR_TIPOFORMU = pag.RPA_FOR_TIPOFORMU and rpa.PAC_PAC_NUMERO = pag.PAC_PAC_NUMERO And Cnv.CON_CON_Codigo  = pag.CON_CON_CODIGO " +
                               "and A.ATC_EST_NUMERO = cta.ATC_EST_NUMERO and A.ATE_PRE_NUMERPACIE = cta.PAC_PAC_NUMERO and A.ATE_PRE_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU and A.ATE_PRE_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU " +
                               "and A.CON_CON_CODIGO = RPA.CON_CON_CODIGO and A.ATE_PRE_VIGENCIA <>'N' And atc.CodigoCentroAten = 'CES  ' and RPA.PAC_PAC_NUMERO = FCT.PAC_PAC_NUMERO and RPA.FAC_FAC_FACTURA = FCT.FAC_FAC_FACTURA " +
                               "and RPA.RPA_FOR_NUMERFORMU = FCT.RPA_FOR_NUMERFORMU And (Alt.HspEst = 'S' Or Alt.HspEst = 'R') And Alt.FecPrc >= To_Date(SubStr('" + FechaIni + "',1,10),'yyyy/mm/dd') " +
                               "And Alt.FecPrc < To_Date(SubStr('" + FechaFin + "',1,10),'yyyy/mm/dd') + 1 And ATC.SER_OBJ_CODIGO = Obj.SER_OBJ_CODIGO(+) And Obj.SER_REC_TIPO(+) = '008' And Obj.CODIGOCENTROATEN(+) = atc.CodigoCentroAten " +
                            "group by Alt.FecPrc,Atc.ATC_EST_FechaHospi,Pac.PAC_PAC_Rut, PAC.PAC_PAC_NUMERO, Atc.SER_OBJ_Codigo ,Cnv.CON_CON_Descripcio ,Atc.ATC_EST_Numero, RPA.FAC_FAC_FACTURA,RPA.RPA_FOR_NUMERFORMU, FCT.FECPRC " +
                            "UNION " +
                             "Select distinct Alt.FecPrc As FecSalida ,RPA.RPA_FOR_NUMERFORMU formulario,(Pac.PAC_PAC_Rut) as identificacion ,nomcompletopaciente(PAC.PAC_PAC_NUMERO) As paciente " +
                                   ",Cnv.CON_CON_Descripcio,DECODE(RPA.FAC_FAC_FACTURA,' ','NO',RPA.FAC_FAC_FACTURA) AS FACTURA ,ROUND (SUM ((Ate_ins_montotarif + NVL (Ate_ins_montoiva, 0))), 0) - ROUND (SUM (Ate_ins_valordevol)) AS TARIFA " +
                                   ",FCT.FECPRC AS FECHA_FACTURA ,(FCT.FECPRC - Alt.FecPrc) AS DIF ,'HOSPITALIZACION' ambito " +
                               "From TabAltOrd Alt ,ATC_Estadia Atc ,PAC_Paciente Pac ,CON_Convenio Cnv ,SER_Objetos Obj,atc_cuenta cta ,rpa_formulario rpa ,tabpagadores pag,Ate_insumos A ,tabfctgrp fct " +
                              "Where Pac.PAC_PAC_Numero = Alt.PAC_PAC_Numero And Atc.PAC_PAC_Numero = Alt.PAC_PAC_Numero And Atc.ATC_Est_Numero = Alt.ATC_Est_Numero and atc.ATC_EST_NUMERO = cta.ATC_EST_NUMERO " +
                                "and atc.PAC_PAC_NUMERO = cta.PAC_PAC_NUMERO and rpa.RPA_FOR_NUMERFORMU0 = cta.RPA_FOR_NUMERFORMU and rpa.RPA_FOR_TIPOFORMU0 = cta.RPA_FOR_TIPOFORMU and rpa.PAC_PAC_NUMERO = cta.PAC_PAC_NUMERO " +
                                "and rpa.RPA_FOR_NUMERFORMU = pag.RPA_FOR_NUMERFORMU and rpa.RPA_FOR_TIPOFORMU = pag.RPA_FOR_TIPOFORMU and rpa.PAC_PAC_NUMERO = pag.PAC_PAC_NUMERO And Cnv.CON_CON_Codigo = pag.CON_CON_CODIGO " +
                                "and A.ATC_EST_NUMERO = cta.ATC_EST_NUMERO and A.ATE_INS_NUMERPACIE = cta.PAC_PAC_NUMERO and A.ATE_INS_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU and A.ATE_INS_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU " +
                                "and A.CON_CON_CODIGO = RPA.CON_CON_CODIGO and RPA.PAC_PAC_NUMERO = FCT.PAC_PAC_NUMERO and RPA.FAC_FAC_FACTURA = FCT.FAC_FAC_FACTURA and RPA.RPA_FOR_NUMERFORMU = FCT.RPA_FOR_NUMERFORMU " +
                                "and A.ATE_INS_VIGENCIA <>'N' AND A.Ate_ins_cantidad - A.Frmcntdev > 0 And atc.CodigoCentroAten = 'CES  ' And (Alt.HspEst = 'S' Or Alt.HspEst = 'R') And Alt.FecPrc >= To_Date(SubStr('" + FechaIni + "',1,10),'yyyy/mm/dd') " +
                                "And Alt.FecPrc< To_Date(SubStr('" + FechaFin + "',1,10),'yyyy/mm/dd') + 1 And ATC.SER_OBJ_CODIGO = Obj.SER_OBJ_CODIGO(+)  And Obj.SER_REC_TIPO(+) = '008'  And Obj.CODIGOCENTROATEN(+) = atc.CodigoCentroAten " +
                           "group by Alt.FecPrc,Atc.ATC_EST_FechaHospi,Pac.PAC_PAC_Rut, PAC.PAC_PAC_NUMERO, Atc.SER_OBJ_Codigo ,Cnv.CON_CON_Descripcio ,Atc.ATC_EST_Numero, RPA.FAC_FAC_FACTURA,RPA.RPA_FOR_NUMERFORMU, FCT.FECPRC " +
                           "union " +
                           "Select distinct DAU.RPA_FDA_HORAEGRESO As FecSalida,RPA.RPA_FOR_NUMERFORMU formulario ,(Pac.PAC_PAC_Rut) as identificacion ,nomcompletopaciente(PAC.PAC_PAC_NUMERO) As paciente " +
                                  ",Cnv.CON_CON_Descripcio as convenio,DECODE(RPA.FAC_FAC_FACTURA,' ','NO',RPA.FAC_FAC_FACTURA) AS FACTURA, ROUND (SUM (A.Ate_pre_montotarif)) as tarifa " +
                                  ",FCT.FECPRC AS FECHA_FACTURA, (FCT.FECPRC - DAU.RPA_FDA_HORAEGRESO) AS DIF , 'URGENCIAS-CONSULTA EXTERNA' ambito " +
                             "From RPA_FORDAU DAU ,PAC_Paciente Pac,CON_Convenio Cnv ,rpa_formulario rpa ,tabpagadores pag ,Ate_prestacion A,tabfctgrp fct " +
                            "Where Pac.PAC_PAC_Numero = DAU.PAC_PAC_NUMERO AND  DAU.ATE_PRE_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU AND DAU.ATE_PRE_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU AND DAU.CON_CON_CODIGO = PAG.CON_CON_CODIGO " +
                              "and rpa.RPA_FOR_NUMERFORMU = pag.RPA_FOR_NUMERFORMU and rpa.RPA_FOR_TIPOFORMU = pag.RPA_FOR_TIPOFORMU and rpa.PAC_PAC_NUMERO = pag.PAC_PAC_NUMERO And Cnv.CON_CON_Codigo = pag.CON_CON_CODIGO " +
                              "and A.ATE_PRE_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU and A.ATE_PRE_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU and A.CON_CON_CODIGO = RPA.CON_CON_CODIGO and A.ATE_PRE_VIGENCIA <>'N' " +
                              "and RPA.PAC_PAC_NUMERO = FCT.PAC_PAC_NUMERO and RPA.FAC_FAC_FACTURA = FCT.FAC_FAC_FACTURA and RPA.RPA_FOR_NUMERFORMU = FCT.RPA_FOR_NUMERFORMU And DAU.RPA_FDA_HORAEGRESO >= To_Date(SubStr('" + FechaIni + "',1,10),'yyyy/mm/dd') " +
                              "And DAU.RPA_FDA_HORAEGRESO  < To_Date(SubStr('" + FechaFin + "',1,10),'yyyy/mm/dd') + 1 " +
                         "group by DAU.RPA_FDA_HORAEGRESO ,Pac.PAC_PAC_Rut, PAC.PAC_PAC_NUMERO, Cnv.CON_CON_Descripcio , RPA.FAC_FAC_FACTURA,RPA.RPA_FOR_NUMERFORMU, FCT.FECPRC " +
                          "UNION " +
                          "Select  distinct DAU.RPA_FDA_HORAEGRESO As FecSalida ,RPA.RPA_FOR_NUMERFORMU formulario ,(Pac.PAC_PAC_Rut) as identificacion,nomcompletopaciente(PAC.PAC_PAC_NUMERO) As paciente " +
                                 ",Cnv.CON_CON_Descripcio as convenio ,DECODE(RPA.FAC_FAC_FACTURA,' ','NO',RPA.FAC_FAC_FACTURA) AS FACTURA , ROUND (SUM (A.Ate_INS_montotarif)) as tarifa " +
                                 ", FCT.FECPRC AS FECHA_FACTURA , (FCT.FECPRC - DAU.RPA_FDA_HORAEGRESO) AS DIF , 'URGENCIAS-CONSULTA EXTERNA' ambito " +
                            "From RPA_FORDAU DAU,PAC_Paciente Pac,CON_Convenio Cnv ,rpa_formulario rpa ,tabpagadores pag,Ate_insumos A,tabfctgrp fct " +
                           "Where Pac.PAC_PAC_Numero = DAU.PAC_PAC_NUMERO AND DAU.ATE_PRE_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU AND DAU.ATE_PRE_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU AND DAU.CON_CON_CODIGO = PAG.CON_CON_CODIGO " +
                             "and rpa.RPA_FOR_NUMERFORMU = pag.RPA_FOR_NUMERFORMU and rpa.RPA_FOR_TIPOFORMU  = pag.RPA_FOR_TIPOFORMU and rpa.PAC_PAC_NUMERO = pag.PAC_PAC_NUMERO And Cnv.CON_CON_Codigo = pag.CON_CON_CODIGO " +
                             "and A.ATE_INS_TIPOFORMU = RPA.RPA_FOR_TIPOFORMU and A.ATE_INS_NUMERFORMU = RPA.RPA_FOR_NUMERFORMU and A.CON_CON_CODIGO = RPA.CON_CON_CODIGO and A.ATE_INS_VIGENCIA <>'N' " +
                             "and RPA.PAC_PAC_NUMERO = FCT.PAC_PAC_NUMERO and RPA.FAC_FAC_FACTURA = FCT.FAC_FAC_FACTURA and RPA.RPA_FOR_NUMERFORMU = FCT.RPA_FOR_NUMERFORMU And DAU.RPA_FDA_HORAEGRESO >= To_Date(SubStr('" + FechaIni + "',1,10),'yyyy/mm/dd') " +
                             "And DAU.RPA_FDA_HORAEGRESO  < To_Date(SubStr('" + FechaFin + "',1,10),'yyyy/mm/dd') + 1 " +
                        "group by DAU.RPA_FDA_HORAEGRESO ,Pac.PAC_PAC_Rut, PAC.PAC_PAC_NUMERO, Cnv.CON_CON_Descripcio , RPA.FAC_FAC_FACTURA,RPA.RPA_FOR_NUMERFORMU, FCT.FECPRC ) " +
                    "GROUP BY FECSALIDA,IDENTIFICACION,PACIENTE,CONVENIO,FACTURA, formulario, FECHA_FACTURA, DIF,AMBITO " +
                    "ORDER BY fecha_egreso ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

    }
}
