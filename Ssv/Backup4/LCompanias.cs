﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LCompanias
    {
        public bool CiaGuardar(Companias cia)
        {
            string[] nomParam = { "@Codigo", "@Nombre", "@Usuario" };
            object[] vlrParam = { cia.Codigo, cia.Nombre, cia.Usuario };

            return EjecutarSentencia("uspCiaActualizar", nomParam, vlrParam);
        }

        public bool CiaEstado(Companias cia)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                cia.Codigo,
                cia.Estado
            };

            return EjecutarSentencia("uspCiaEstado", nomParam, vlrParam);
        }

        public DataTable CiaConsultar(Companias cia)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { cia.Codigo };

            return getDataTable("uspCiaConsultar", nomParam, vlrParam);
        }

        public bool CiaReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Cia", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspCiaReactivar", nomParam, valParam);
        }

        public bool CiaRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Cia", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspCiaRetirar", nomParam, valParam);
        }

        public DataTable CiasConsultar()
        {
            return getDataTable("uspCiaConsultarCodigos");
        }

        public bool CiasRetirar(string xmlCia, string Usuario)
        {
            string[] nomParam = { "@xmlCia", "@Usuario" };
            object[] valParam = { xmlCia, Usuario };

            return EjecutarSentencia("uspCiaRetirarXml", nomParam, valParam);
        }
                
        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
