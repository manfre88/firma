﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LVEPI
    {
        public DataSet MorbilidadHosp(string EdadIni, string EdadFin, string FechaIni, string FechaFin, string Servicio, string Genero)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_EdadIni",
                "In_EdadFin",
                "In_Servicio",
                "In_Genero"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                EdadIni,
                EdadFin,
                Servicio,
                Genero
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            
            if ((EdadIni!="") &&  (EdadFin!=""))
            {
                EdadIni = " and round((G.RPA_FDA_HORAEGRESO-d.PAC_PAC_FECHANACIM)/365, 2) between '" + EdadIni + "' and '" + EdadFin + "' ";
            }

            if (Servicio != "-1")
            {
                Servicio = " and g.SER_SER_CODIGO in " + Servicio + " ";
            }
            else
            {
                Servicio = "";
            }


            if (Genero != "")
            {
                Genero = " and D.PAC_PAC_SEXO = '" + Genero + "' ";
            }

            string sql = "select a.DIA_DIA_CODIGO CIE10,trim(c.DIA_DIA_DESCRIPCIO) DESCRIPCION,count(*) N_PACIENTES " +
                           "from tabdiagnosticos a,tabencuentros b,dia_diagnos c,pac_paciente d,atc_estadia e,SER_SERVICIOS f,atc_ocupacama g " +
                          "where a.MTVCORRELATIVO=b.MTVCORRELATIVO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and a.TABENCUENTRO=b.TABENCUENTRO and a.DGNPRINCIPAL='S' " +
                            "and g.SER_SER_CODIGO=f.SER_SER_CODIGO and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO  and e.ATC_EST_NUMERO=g.ATC_EST_NUMERO " +
                            "and b.AMBITOCODIGO='02' AND G.SER_SER_CODIGO<>'AMB     '  and G.SER_OBJ_CODIGO <> '        '  and d.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                            "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and a.MTVCORRELATIVO=e.MTVCORRELATIVO and SUBSTR(G.SER_OBJ_CODIGO,1,1) <>'V' " +
                            "and g.SER_SER_CODIGO not in ('001','004') AND A.PAC_PAC_NUMERO NOT IN ('1308','5024') " + Servicio + " " +
                            "and trunc (G.RPA_FDA_HORAEGRESO ) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            " " + EdadIni + " " + Genero + " " + 
                          "group by a.DIA_DIA_CODIGO,c.DIA_DIA_DESCRIPCIO,b.AMBITOCODIGO " +
                          "order by 3 desc ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            
        }

        public DataSet MorbilidadURG(string EdadIni, string EdadFin, string FechaIni, string FechaFin, string Genero)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_EdadIni",
                "In_EdadFin",
                "In_Genero"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                EdadIni,
                EdadFin,
                Genero
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            if ((EdadIni != "") && (EdadFin != ""))
            {
                EdadIni = " and round((a.DGNFECHA-d.PAC_PAC_FECHANACIM)/365, 2) between '" + EdadIni + "' and '" + EdadFin + "' ";

            }


            if (Genero != "")
            {
                Genero = " and D.PAC_PAC_SEXO = '" + Genero + "' ";
            }

            string sql = "select cie10, descripcion, sum(n_pacientes) n_pacientes from " +
                             "(select a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion,count(*) n_pacientes " +
                                   "from tabdiagnosticos a,tabencuentros b,dia_diagnos c,pac_paciente d,atc_estadia e " +
                                  "where a.MTVCORRELATIVO=b.MTVCORRELATIVO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and a.TABENCUENTRO=b.TABENCUENTRO and a.DGNPRINCIPAL='S' " +
                                    "and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and b.AMBITOCODIGO='02' and d.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                                    "and a.MTVCORRELATIVO=e.MTVCORRELATIVO and e.SER_SER_CODIGO='001' and a.PAC_PAC_NUMERO not in ('1308','5024') " +
                                    "and trunc (a.DGNFECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                    " " + EdadIni + " " + Genero + " " + 
                                  "group by a.DIA_DIA_CODIGO,c.DIA_DIA_DESCRIPCIO,b.AMBITOCODIGO " +
                            "union all " +
                                "Select a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion,count(*) n_pacientes  " +
                                  "from tabTriage z,rpa_fordau b,tabdiagnosticos a,dia_diagnos c, pac_paciente d " +
                                 "Where z.espdstCod = '01'  and B.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO and z.RPA_FOR_TIPOFORMU=b.RPA_FOR_TIPOFORMU and a.PAC_PAC_NUMERO not in ('1308','5024') " +
                                   "and z.RPA_FOR_NUMERFORMU=b.RPA_FOR_NUMERFORMU and b.MTVCORRELATIVO=a.MTVCORRELATIVO  and b.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                                   "and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and a.DGNPRINCIPAL='S' and trunc(a.DGNFECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   " " + EdadIni + " " + Genero + " " + 
                                 "group by a.DIA_DIA_CODIGO,c.DIA_DIA_DESCRIPCIO " +
                            ") group by cie10, descripcion " +
                            "order by 3 desc ";
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet MorbilidadAmbulatorio(string EdadIni, string EdadFin, string FechaIni, string FechaFin, string Genero)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_EdadIni",
                "In_EdadFin",
                "In_Genero"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                EdadIni,
                EdadFin,
                Genero
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            if ((EdadIni != "") && (EdadFin != ""))
            {
                EdadIni = " and round((a.DGNFECHA-d.PAC_PAC_FECHANACIM)/365, 2) between '" + EdadIni + "' and '" + EdadFin + "' ";

            }


            if (Genero != "")
            {
                Genero = " and D.PAC_PAC_SEXO = '" + Genero + "' ";
            }

            string sql = "select a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion,count(*) n_pacientes " +
                           "from tabdiagnosticos a,tabencuentros b,dia_diagnos c,pac_paciente d " +
                          "where a.MTVCORRELATIVO=b.MTVCORRELATIVO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                            "and a.TABENCUENTRO=b.TABENCUENTRO and a.DGNPRINCIPAL='S' and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and b.AMBITOCODIGO='01' and d.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                            "and a.PAC_PAC_NUMERO not in ('1308','5024') and trunc (a.DGNFECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            " " + EdadIni + " " + Genero + " " +
                          "group by a.DIA_DIA_CODIGO,c.DIA_DIA_DESCRIPCIO " +
                          "order by 3 desc";
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet MorbilidadEgreso(string EdadIni, string EdadFin, string FechaIni, string FechaFin, string Genero)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_EdadIni",
                "In_EdadFin",
                "In_Genero"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                EdadIni,
                EdadFin,
                Genero
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            if ((EdadIni != "") && (EdadFin != ""))
            {
                EdadIni = " and round((a.DGNFECHA-d.PAC_PAC_FECHANACIM)/365, 2) between '" + EdadIni + "' and '" + EdadFin + "' ";
            }


            if (Genero != "")
            {
                Genero = " and D.PAC_PAC_SEXO = '" + Genero + "' ";
            }

            string sql = "select cie10, descripcion, sum(n_pacientes) n_pacientes from " +
                          "(select a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion,count(*) n_pacientes " +
                          "from tabdiagnosticos a,tabencuentros b,dia_diagnos c,pac_paciente d,atc_estadia e " +
                         "where a.MTVCORRELATIVO=b.MTVCORRELATIVO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and a.TABENCUENTRO=b.TABENCUENTRO and a.DGNPRINCIPAL='S' " +
                            "and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and b.AMBITOCODIGO='02' and d.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO and a.PAC_PAC_NUMERO not in ('1308','5024') " +
                            "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and a.MTVCORRELATIVO=e.MTVCORRELATIVO  " +
                            "and e.SER_SER_CODIGO not in ('001','004') AND e.SER_SER_CODIGO<>'AMB     '  and e.SER_OBJ_CODIGO <> '        '  " +
                            "and trunc (E.ATC_EST_FECEGRESO ) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            " " + EdadIni + " " + Genero + " " + 
                         "group by a.DIA_DIA_CODIGO,c.DIA_DIA_DESCRIPCIO,b.AMBITOCODIGO,e.SER_SER_CODIGO  " +
                         ") group by cie10, descripcion order by 3 desc ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet PacientesxDX(string FechaIni, string FechaFin, string DXIni, string DXFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_DXIni",
                "In_DXFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                DXIni,
                DXFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion,decode(b.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,F.SER_SER_DESCRIPCIO servicio ,D.PAC_PAC_RUT identificacion, nomcompletopaciente(D.PAC_PAC_NUMERO) paciente " +
                              "from tabdiagnosticos a,tabencuentros b,dia_diagnos c,pac_paciente d,atc_estadia e, ser_servicios f " +
                             "where a.MTVCORRELATIVO=b.MTVCORRELATIVO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and a.PAC_PAC_NUMERO not in ('1308','5024') " +
                               "and a.TABENCUENTRO=b.TABENCUENTRO and a.DGNPRINCIPAL='S' and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and d.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                               "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO(+) and a.MTVCORRELATIVO=e.MTVCORRELATIVO(+) and E.SER_SER_CODIGO = F.SER_SER_CODIGO(+) " +
                               "and trunc (a.DGNFECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and c.DIA_DIA_CODIGO between '" + DXIni + "' and '" + DXFin + "' " +
                             "ORDER BY 1,4,6";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet DiasEstanciaGlobal(string FechaIni, string FechaFin, string Servicio)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_servicio"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Servicio
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql="";

            if (Servicio != "CXAMB" && Servicio != "LABAMB" && Servicio != "PATAMB" && Servicio != "CEXCES" && Servicio != "CEXCFF" && Servicio != "IMAAMB" && Servicio !="011" && Servicio!="008" )
            {
                Servicio = " and Z.SER_SER_CODIGO IN ('" + Servicio + "') ";

                sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, B.RPA_FDA_HORAINGRESO FEC_INGRESO ,B.RPA_FDA_HORAEGRESO fec_egreso, ROUND( (B.RPA_FDA_HORAEGRESO-B.RPA_FDA_HORAINGRESO) ,2)dias, " +
                              "trim(Z.SER_SER_DESCRIPCIO) servicio, B.SER_OBJ_CODIGO cama " +
                      "from atc_estadia a,atc_ocupacama b,tabaltord c,pac_paciente e, con_convenio x, ser_servicios z " +
                      "where b.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                        "and trunc (b.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                        "and a.SER_OBJ_CODIGO <> '        '    and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and b.ATC_EST_NUMERO=c.ATC_EST_NUMERO " +
                        "and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                        "and A.CON_CON_CODIGO=X.CON_CON_CODIGO AND B.SER_SER_CODIGO=Z.SER_SER_CODIGO " +
                        " " + Servicio + " " +
                        "order by 2,4 ";
            }
            else
            {
                if (Servicio == "011")
                {
                    
                    Servicio = " and Z.SER_SER_CODIGO IN ('" + Servicio + "') ";

                    sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, B.RPA_FDA_HORAINGRESO FEC_INGRESO ,B.RPA_FDA_HORAEGRESO fec_egreso, ROUND( (B.RPA_FDA_HORAEGRESO-B.RPA_FDA_HORAINGRESO) ,2)dias, " +
                                  "trim(Z.SER_SER_DESCRIPCIO) servicio, B.SER_OBJ_CODIGO cama " +
                          "from atc_estadia a,atc_ocupacama b,tabaltord c,pac_paciente e, con_convenio x, ser_servicios z " +
                          "where b.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                            "and trunc (b.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.SER_OBJ_CODIGO <> '        '    and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and b.ATC_EST_NUMERO=c.ATC_EST_NUMERO " +
                            "and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and substr(B.SER_OBJ_CODIGO,1,1)='P' " +
                            "and A.CON_CON_CODIGO=X.CON_CON_CODIGO AND B.SER_SER_CODIGO=Z.SER_SER_CODIGO " +
                            " " + Servicio + " " +
                            "order by 2,4 ";
                }

                if (Servicio == "008")
                {
                    Servicio = " and Z.SER_SER_CODIGO IN ('" + Servicio + "') ";

                    sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, B.RPA_FDA_HORAINGRESO FEC_INGRESO ,B.RPA_FDA_HORAEGRESO fec_egreso, ROUND( (B.RPA_FDA_HORAEGRESO-B.RPA_FDA_HORAINGRESO) ,2)dias, " +
                              "trim(Z.SER_SER_DESCRIPCIO) servicio, B.SER_OBJ_CODIGO cama " +
                              "from atc_estadia a,atc_ocupacama b,tabaltord c,pac_paciente e, con_convenio x, ser_servicios z " +
                              "where b.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                                "and trunc (b.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                "and a.SER_OBJ_CODIGO <> '        '    and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and b.ATC_EST_NUMERO=c.ATC_EST_NUMERO " +
                                "and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                                "and A.CON_CON_CODIGO=X.CON_CON_CODIGO AND B.SER_SER_CODIGO=Z.SER_SER_CODIGO " +
                                " " + Servicio + " " +
                         " union all " +
                        "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, B.RPA_FDA_HORAINGRESO FEC_INGRESO ,B.RPA_FDA_HORAEGRESO fec_egreso, ROUND( (B.RPA_FDA_HORAEGRESO-B.RPA_FDA_HORAINGRESO) ,2)dias, " +
                                  "trim(Z.SER_SER_DESCRIPCIO) servicio, B.SER_OBJ_CODIGO cama " +
                          "from atc_estadia a,atc_ocupacama b,tabaltord c,pac_paciente e, con_convenio x, ser_servicios z " +
                          "where b.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                            "and trunc (b.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.SER_OBJ_CODIGO <> '        '    and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and b.ATC_EST_NUMERO=c.ATC_EST_NUMERO " +
                            "and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and substr(B.SER_OBJ_CODIGO,1,1)<>'P' " +
                            "and A.CON_CON_CODIGO=X.CON_CON_CODIGO AND B.SER_SER_CODIGO=Z.SER_SER_CODIGO " +
                            "and Z.SER_SER_CODIGO IN ('011') " +
                        "order by 2,4 ";

                }

                if (Servicio == "CXAMB")
                {
                    sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, M.FECQRF FEC_INGRESO ,a.ATC_EST_FECEGRESO fec_egreso, ROUND( (a.ATC_EST_FECEGRESO-M.FECQRF) ,2)dias, " +
                                     "'CIRUGIA AMBULATORIA' SERVICIO, '' CAMA " +
                            "from atc_estadia a,tabaltord c,pac_paciente e,con_convenio x, taborddetcir j, TABPRGQRF m " +
                           "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO = '        '  and J.PAC_PAC_NUMERO=M.PAC_PAC_NUMERO and J.ORDNUMERO=M.ORDNUMERO and J.ORDTIPO=M.ORDTIPO " +
                             "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') and A.ATC_EST_NUMERO=J.ATC_EST_NUMERO and A.PAC_PAC_NUMERO=J.PAC_PAC_NUMERO " +
                             "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                             "and A.CON_CON_CODIGO=X.CON_CON_CODIGO and A.ATC_EST_NUMERO not in (select Z.ATC_EST_NUMERO from atc_ocupacama z  where trunc (Z.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                           "order by 2 ";
                }

                if (Servicio == "LABAMB")
                {
                    sql = "SELECT distinct c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,a.RPA_FOR_FECHASOLIC FEC_INGRESO,'' FEC_EGRESO,'' DIAS, 'LABORATORIO CLINICO AMBULATORIO' SERVICIO, '' CAMA " +
                             "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                           "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU  and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                               "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU and d.CON_CON_CODIGO=e.CON_CON_CODIGO and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO and a.ORDTIPO='L' " +
                               "and a.SER_SER_AMBITO='01'  and b.CEX_EVE_TIPOEVENT in ('3 ')  and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                               "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') ORDER BY 2 ";
                }

                if (Servicio == "PATAMB")
                {
                    sql = "SELECT distinct c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,a.RPA_FOR_FECHASOLIC FEC_INGRESO,'' FEC_EGRESO,'' DIAS ,'LABORATORIO PATOLOGIA AMBULATORIO' SERVICIO,'' CAMA " +
                             "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                             "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU  and d.CON_CON_CODIGO=e.CON_CON_CODIGO and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO and a.ORDTIPO='A' " +
                                "and a.SER_SER_AMBITO='01' and b.CEX_EVE_TIPOEVENT in ('1 ') and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') ORDER BY 2 ";
                }

                if (Servicio == "CEXCES")
                {
                    sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) FEC_INGRESO,'' FEC_EGRESO,'' DIAS, 'CONSULTA EXTERNA CENTRO' SERVICIO,'' CAMA  " +
                               "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                               "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                 "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                 "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = 'CES     ' " +
                                 "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO ORDER BY 2 ";
                }

                if (Servicio == "CEXCFF")
                {
                    sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) FEC_INGRESO,'' FEC_EGRESO,'' DIAS, 'CONSULTA EXTERNA FALABELLA' SERVICIO,'' CAMA " +
                               "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                               "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                 "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                 "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = 'CCF     ' " +
                                 "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO ORDER BY 2 ";
                }

                if (Servicio == "IMAAMB")
                {
                    sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) FEC_INGRESO, '' FEC_EGRESO,'' DIAS, 'IMAGINOLOGIA AMBULATORIA' SERVICIO,'' CAMA " +
                               "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                               "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                 "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                 "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' " +
                                 "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO='57' AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO ORDER BY 2 ";
                }
            }

            

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet MorbilidadAPH(string EdadIni, string EdadFin, string FechaIni, string FechaFin, string Genero)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_EdadIni",
                "In_EdadFin",
                "In_Genero"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                EdadIni,
                EdadFin,
                Genero
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            if ((EdadIni != "") && (EdadFin != ""))
            {
                EdadIni = " and round((a.DGNFECHA-d.PAC_PAC_FECHANACIM)/365, 2) between '" + EdadIni + "' and '" + EdadFin + "' ";

            }


            if (Genero != "")
            {
                Genero = " and D.PAC_PAC_SEXO = '" + Genero + "' ";
            }

            string sql = "select cie10, descripcion, sum(n_pacientes) n_pacientes from " +
                             "(select a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion,count(*) n_pacientes " +
                                   "from tabdiagnosticos a,tabencuentros b,dia_diagnos c,pac_paciente d,atc_estadia e " +
                                  "where a.MTVCORRELATIVO=b.MTVCORRELATIVO and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and a.TABENCUENTRO=b.TABENCUENTRO and a.DGNPRINCIPAL='S' " +
                                    "and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and b.AMBITOCODIGO='02' and d.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                                    "and a.MTVCORRELATIVO=e.MTVCORRELATIVO and e.SER_SER_CODIGO='010' and a.PAC_PAC_NUMERO not in ('1308','5024') " +
                                    "and trunc (a.DGNFECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                    " " + EdadIni + " " + Genero + " " +
                                  "group by a.DIA_DIA_CODIGO,c.DIA_DIA_DESCRIPCIO,b.AMBITOCODIGO " +
                            "union all " +
                                "Select a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion,count(*) n_pacientes  " +
                                  "from tabTriage z,rpa_fordau b,tabdiagnosticos a,dia_diagnos c, pac_paciente d " +
                                 "Where z.espdstCod = '04'  and B.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO and z.RPA_FOR_TIPOFORMU=b.RPA_FOR_TIPOFORMU and a.PAC_PAC_NUMERO not in ('1308','5024') " +
                                   "and z.RPA_FOR_NUMERFORMU=b.RPA_FOR_NUMERFORMU and b.MTVCORRELATIVO=a.MTVCORRELATIVO  and b.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                                   "and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and a.DGNPRINCIPAL='S' and trunc(a.DGNFECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   " " + EdadIni + " " + Genero + " " +
                                 "group by a.DIA_DIA_CODIGO,c.DIA_DIA_DESCRIPCIO " +
                            ") group by cie10, descripcion " +
                            "order by 3 desc ";
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet MorbilidadURGODONTO(string EdadIni, string EdadFin, string FechaIni, string FechaFin, string Genero)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_EdadIni",
                "In_EdadFin",
                "In_Genero"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                EdadIni,
                EdadFin,
                Genero
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            if ((EdadIni != "") && (EdadFin != ""))
            {
                EdadIni = " and round((a.DGNFECHA-d.PAC_PAC_FECHANACIM)/365, 2) between '" + EdadIni + "' and '" + EdadFin + "' ";

            }


            if (Genero != "")
            {
                Genero = " and D.PAC_PAC_SEXO = '" + Genero + "' ";
            }

            string sql = "Select a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion,count(*) n_pacientes  " +
                                  "from tabTriage z,rpa_fordau b,tabdiagnosticos a,dia_diagnos c, pac_paciente d " +
                                 "Where z.espdstCod = '05'  and B.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO and z.RPA_FOR_TIPOFORMU=b.RPA_FOR_TIPOFORMU and a.PAC_PAC_NUMERO not in ('1308','5024') " +
                                   "and z.RPA_FOR_NUMERFORMU=b.RPA_FOR_NUMERFORMU and b.MTVCORRELATIVO=a.MTVCORRELATIVO  and b.PAC_PAC_NUMERO=a.PAC_PAC_NUMERO " +
                                   "and a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and a.DGNPRINCIPAL='S' and trunc(a.DGNFECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   " " + EdadIni + " " + Genero + " " +
                                 "group by a.DIA_DIA_CODIGO,c.DIA_DIA_DESCRIPCIO order by 3 desc ";
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet EgresosGlobal(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select SERVICIO,COUNT(*) N_EGRESOS from " +
                        "(select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, a.ATC_EST_FECHAHOSPI FEC_INGRESO ,a.ATC_EST_FECEGRESO fec_egreso, ROUND( (a.ATC_EST_FECEGRESO-a.ATC_EST_FECHAHOSPI) ,2)dias,  " +
                                   "trim(Z.SER_SER_DESCRIPCIO) servicio, a.SER_OBJ_CODIGO cama,c.DIA_DIA_CODIGO dxalta,trim(f.DIA_DIA_DESCRIPCIO) descripcion,g.ALTCNDNOM condicion,h.DSTALTNOM destino,decode( I.PAC_PAC_NUMERO,'','NO','SI') REAPERTURA  " +
                         "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x  " +
                         "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO <> '        '  " +
                         "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024')  " +
                         "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                         "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO  and A.SER_SER_CODIGO NOT IN ('011','008') and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO  " +
                         "order by 6,7,3 " +
                        ") GROUP BY SERVICIO " +
                        "UNION ALL " +
                        "select SERVICIO,COUNT(*) N_EGRESOS from " +
                        "(select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, a.ATC_EST_FECHAHOSPI FEC_INGRESO ,a.ATC_EST_FECEGRESO fec_egreso, ROUND( (a.ATC_EST_FECEGRESO-a.ATC_EST_FECHAHOSPI) ,2)dias,  " +
                                   "trim(Z.SER_SER_DESCRIPCIO) servicio, a.SER_OBJ_CODIGO cama,c.DIA_DIA_CODIGO dxalta,trim(f.DIA_DIA_DESCRIPCIO) descripcion,g.ALTCNDNOM condicion,h.DSTALTNOM destino,decode( I.PAC_PAC_NUMERO,'','NO','SI') REAPERTURA  " +
                         "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x  " +
                         "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO <> '        '  " +
                         "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024')  " +
                         "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                         "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO AND A.SER_SER_CODIGO='011     ' AND SUBSTR(a.SER_OBJ_CODIGO,1,1)='P' and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO  " +
                         "order by 6,7,3 " +
                        ") GROUP BY SERVICIO " +
                        "UNION ALL " +
                        "select SERVICIO,COUNT(*) N_EGRESOS from " +
                        "(select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, a.ATC_EST_FECHAHOSPI FEC_INGRESO ,a.ATC_EST_FECEGRESO fec_egreso, ROUND( (a.ATC_EST_FECEGRESO-a.ATC_EST_FECHAHOSPI) ,2)dias,  " +
                                   "'CUARTO PISO TORRE 2 HOSPITALIZACION' servicio, a.SER_OBJ_CODIGO cama,c.DIA_DIA_CODIGO dxalta,trim(f.DIA_DIA_DESCRIPCIO) descripcion,g.ALTCNDNOM condicion,h.DSTALTNOM destino,decode( I.PAC_PAC_NUMERO,'','NO','SI') REAPERTURA  " +
                         "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x  " +
                         "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO <> '        '  " +
                         "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024')  " +
                         "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                         "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO AND A.SER_SER_CODIGO='011     ' AND SUBSTR(a.SER_OBJ_CODIGO,1,1)<>'P' and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO  " +
                         "UNION ALL " +
                         "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, a.ATC_EST_FECHAHOSPI FEC_INGRESO ,a.ATC_EST_FECEGRESO fec_egreso, ROUND( (a.ATC_EST_FECEGRESO-a.ATC_EST_FECHAHOSPI) ,2)dias,  " +
                                   "trim(Z.SER_SER_DESCRIPCIO) servicio, a.SER_OBJ_CODIGO cama,c.DIA_DIA_CODIGO dxalta,trim(f.DIA_DIA_DESCRIPCIO) descripcion,g.ALTCNDNOM condicion,h.DSTALTNOM destino,decode( I.PAC_PAC_NUMERO,'','NO','SI') REAPERTURA  " +
                         "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x  " +
                         "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO <> '        '  " +
                         "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024')  " +
                         "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                         "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO AND A.SER_SER_CODIGO='008     ' and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO  " +
                         "order by 6,7,3 " +
                        ") GROUP BY SERVICIO " +
                        "UNION ALL " +
                        "select DECODE(SERVICIO,'        ','CIRUGIA_AMBULARORIA','' ) SERVICIO,COUNT(*) from " +
                        "(select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, a.ATC_EST_FECHAHOSPI FEC_INGRESO ,a.ATC_EST_FECEGRESO fec_egreso, ROUND( (a.ATC_EST_FECEGRESO-a.ATC_EST_FECHAHOSPI) ,2)dias,  " +
                                   "a.SER_OBJ_CODIGO SERVICIO,c.DIA_DIA_CODIGO dxalta,trim(f.DIA_DIA_DESCRIPCIO) descripcion,g.ALTCNDNOM condicion,h.DSTALTNOM destino,decode( I.PAC_PAC_NUMERO,'','NO','SI') REAPERTURA  " +
                         "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, con_convenio x, taborddetcir j, TABPRGQRF m " +
                         "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO = '        ' and J.PAC_PAC_NUMERO=M.PAC_PAC_NUMERO and J.ORDNUMERO=M.ORDNUMERO and J.ORDTIPO=M.ORDTIPO " +
                         "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') and A.ATC_EST_NUMERO=J.ATC_EST_NUMERO and A.PAC_PAC_NUMERO=J.PAC_PAC_NUMERO " +
                         "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                         "and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO and A.ATC_EST_NUMERO not in (select Z.ATC_EST_NUMERO from atc_ocupacama z  where trunc (Z.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                         "order by 6,7,3 " +
                        ") GROUP BY SERVICIO " +
                        "UNION ALL " +
                        "SELECT 'LABORATORIO_CLINICO_AMBULATORIO' SERVICIO,COUNT(*) FROM " +
                        "(SELECT a.ATE_PRE_TIPOFORMU tipo1,a.ATE_PRE_NUMERFORMU numero1,b.CEX_EXS_CODIGPREST cod1,a.PAC_PAC_NUMERO pacnum1,c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,b.CEX_EXS_CODIGPREST codigo,f.PRE_PRE_DESCRIPCIO descripcion,a.RPA_FOR_FECHASOLIC FEC_SOLICITUD,DECODE(b.CEX_EVE_TIPOEVENT,3,'GENERADA') generada,b.ATE_PRE_FECHADIGIT FEC_GENERACION,(b.ATE_PRE_FECHADIGIT-a.RPA_FOR_FECHASOLIC) *24 OPORTUNIDAD_GENERACION  " +
                             "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f  " +
                           "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU  and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU  " +
                               "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU and d.CON_CON_CODIGO=e.CON_CON_CODIGO and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO and a.ORDTIPO='L'  " +
                               "and a.SER_SER_AMBITO='01'  and b.CEX_EVE_TIPOEVENT in ('3 ')  and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024')  " +
                               "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                        "UNION ALL " +
                        "SELECT 'LABORATORIO_PATOLOGIA_AMBULATORIO' SERVICIO,COUNT(*) FROM " +
                        "(SELECT a.ATE_PRE_TIPOFORMU tipo1,a.ATE_PRE_NUMERFORMU numero1,b.CEX_EXS_CODIGPREST cod1,a.PAC_PAC_NUMERO pacnum1,c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,b.CEX_EXS_CODIGPREST codigo,f.PRE_PRE_DESCRIPCIO descripcion,a.RPA_FOR_FECHASOLIC FEC_SOLICITUD,DECODE(b.CEX_EVE_TIPOEVENT,1,'RECEPCION') RECEPCION,b.ATE_PRE_FECHADIGIT FEC_RECEPCION,(b.ATE_PRE_FECHADIGIT-a.RPA_FOR_FECHASOLIC)*24 OPORTUNIDAD_RECEPCION  " +
                             "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f  " +
                             "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU  " +
                                "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU  and d.CON_CON_CODIGO=e.CON_CON_CODIGO and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO and a.ORDTIPO='A'  " +
                                "and a.SER_SER_AMBITO='01' and b.CEX_EVE_TIPOEVENT in ('1 ') and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024')  " +
                                "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                        "UNION ALL " +
                        "SELECT 'CONSULTA_EXTERNA_CENTRO' SERVICIO,COUNT(*) FROM " +
                        "(select e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente,a.PCA_AGE_FECHASOLICITUD FEC_SOLICITUD,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) FEC_CITA,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) -(trunc(a.PCA_AGE_FECHASOLICITUD))),2) OPORTUNIDAD, F.TIPOPROFENOMBRE especialidad, G.PRE_TIP_DESCRIPCIO TIPO, H.PRE_SUB_DESCRIPCIO SUBTIPO, D.PRE_PRE_CODIGO codigo , D.PRE_PRE_DESCRIPCIO prestacion, I.CON_CON_DESCRIPCIO CONVENIO " +
                               "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                               "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                 "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                 "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = 'CES     ' " +
                                 "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO ORDER BY 2) " +
                        "UNION ALL " +
                        "SELECT 'CONSULTA_EXTERNA_FALABELLA' SERVICIO,COUNT(*) FROM " +
                        "(select e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente,a.PCA_AGE_FECHASOLICITUD FEC_SOLICITUD,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) FEC_CITA,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) -(trunc(a.PCA_AGE_FECHASOLICITUD))),2) OPORTUNIDAD, F.TIPOPROFENOMBRE especialidad, G.PRE_TIP_DESCRIPCIO TIPO, H.PRE_SUB_DESCRIPCIO SUBTIPO, D.PRE_PRE_CODIGO codigo , D.PRE_PRE_DESCRIPCIO prestacion, I.CON_CON_DESCRIPCIO CONVENIO " +
                               "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                               "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                 "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                 "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = 'CCF     ' " +
                                 "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO ORDER BY 2) " +
                        "union all " +
                        "SELECT 'IMAGINOLOGIA_AMBULATORIA' SERVICIO,COUNT(*) FROM " +
                        "(select e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente,a.PCA_AGE_FECHASOLICITUD FEC_SOLICITUD,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) FEC_CITA,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) -(trunc(a.PCA_AGE_FECHASOLICITUD))),2) OPORTUNIDAD, F.TIPOPROFENOMBRE especialidad, G.PRE_TIP_DESCRIPCIO TIPO, H.PRE_SUB_DESCRIPCIO SUBTIPO, D.PRE_PRE_CODIGO codigo , D.PRE_PRE_DESCRIPCIO prestacion, I.CON_CON_DESCRIPCIO CONVENIO  " +
                               "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                               "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE  " +
                                 "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC  " +
                                 "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S'  " +
                                 "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO='57' AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO ORDER BY 2) " +
                        "order by 1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet GeneroEdadPagador(string FechaIni, string FechaFin, string Servicio, string EdadIni, string EdadFin, string Genero, string Pagador)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Servicio",
                "In_EdadIni",
                "In_EdadFin",
                "In_Genero",
                "In_Pagador"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Servicio,
                EdadIni,
                EdadFin,
                Genero,
                Pagador
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql =""; 
            string condicion = "";


            if (Servicio != "TODOS")
            {

                if (Servicio != "CXAMB" && Servicio != "LABAMB" && Servicio != "PATAMB" && Servicio != "CEXCES" && Servicio != "CEXCFF" && Servicio != "IMAAMB" && Servicio != "011" && Servicio != "008" )
                {
                    Servicio = " and Z.SER_SER_CODIGO in ('" + Servicio + "') ";

                    if (Genero != "")
                    {
                        Genero = " and E.PAC_PAC_SEXO = '" + Genero + "' ";
                    }

                    if (Pagador != "-1")
                    {
                        Pagador = " and A.CON_CON_CODIGO = '" + Pagador + "' ";
                    }
                    else
                    {
                        Pagador = "";
                    }

                    if ((EdadIni != "") && (EdadFin != ""))
                    {
                        EdadIni = " and round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                    }

                    sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio,trim(Z.SER_SER_DESCRIPCIO) servicio, decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from A.ATC_EST_FECEGRESO)  || '-' || extract(year from A.ATC_EST_FECEGRESO) mes_año, A.SER_OBJ_CODIGO cama " +
                            "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x " +
                           "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and SUBSTR(a.SER_OBJ_CODIGO,1,1) <>'V' and a.SER_OBJ_CODIGO <> '        ' " +
                             "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') " +
                             "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                             "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO  and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO " +
                             " " + Servicio + " " + " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                             "order by 2 ";
                }
            }

            if (Servicio == "011")
            {
                Servicio = " and Z.SER_SER_CODIGO in ('" + Servicio + "') ";

                if (Genero != "")
                {
                    Genero = " and E.PAC_PAC_SEXO = '" + Genero + "' ";
                }

                if (Pagador != "-1")
                {
                    Pagador = " and A.CON_CON_CODIGO = '" + Pagador + "' ";
                }
                else
                {
                    Pagador = "";
                }

                if ((EdadIni != "") && (EdadFin != ""))
                {
                    EdadIni = " and round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                }

                sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio,trim(Z.SER_SER_DESCRIPCIO) servicio, decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from A.ATC_EST_FECEGRESO)  || '-' || extract(year from A.ATC_EST_FECEGRESO) mes_año, A.SER_OBJ_CODIGO cama  " +
                        "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x " +
                       "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and SUBSTR(a.SER_OBJ_CODIGO,1,1) <>'V' and a.SER_OBJ_CODIGO <> '        ' " +
                         "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') " +
                         "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and SUBSTR(a.SER_OBJ_CODIGO,1,1)='P' " +
                         "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO  and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO " +
                         " " + Servicio + " " + " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                         "order by 2 ";

            }


            if (Servicio == "008")
            {
                Servicio = " and Z.SER_SER_CODIGO in ('" + Servicio + "') ";

                if (Genero != "")
                {
                    Genero = " and E.PAC_PAC_SEXO = '" + Genero + "' ";
                }

                if (Pagador != "-1")
                {
                    Pagador = " and A.CON_CON_CODIGO = '" + Pagador + "' ";
                }
                else
                {
                    Pagador = "";
                }

                if ((EdadIni != "") && (EdadFin != ""))
                {
                    EdadIni = " and round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                }

                sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio,trim(Z.SER_SER_DESCRIPCIO) servicio, decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from A.ATC_EST_FECEGRESO)  || '-' || extract(year from A.ATC_EST_FECEGRESO) mes_año, A.SER_OBJ_CODIGO cama  " +
                        "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x " +
                       "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and SUBSTR(a.SER_OBJ_CODIGO,1,1) <>'V' and a.SER_OBJ_CODIGO <> '        ' " +
                         "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') " +
                         "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                         "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO  and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO " +
                         " " + Servicio + " " + " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                       "union all " +
                       "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio,trim(Z.SER_SER_DESCRIPCIO) servicio, decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from A.ATC_EST_FECEGRESO)  || '-' || extract(year from A.ATC_EST_FECEGRESO) mes_año, A.SER_OBJ_CODIGO cama  " +
                        "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x " +
                       "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and SUBSTR(a.SER_OBJ_CODIGO,1,1) <>'V' and a.SER_OBJ_CODIGO <> '        ' " +
                         "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') " +
                         "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and SUBSTR(a.SER_OBJ_CODIGO,1,1)<>'P' " +
                         "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO  and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO and trim(Z.SER_SER_CODIGO) ='011'  " +
                         " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                         "order by 2 ";

            }


            if (Servicio == "CXAMB")
            {

                    if (Genero != "")
                    {
                        Genero = " and E.PAC_PAC_SEXO = '" + Genero + "' ";
                    }

                    if (Pagador != "-1")
                    {
                        Pagador = " and A.CON_CON_CODIGO = '" + Pagador + "' ";
                    }
                    else
                    {
                        Pagador = "";
                    }

                    if ((EdadIni != "") && (EdadFin != ""))
                    {
                        EdadIni = " and round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                    }



                    sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, 'CIRUGIA AMBULATORIA' SERVICIO,decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from A.ATC_EST_FECEGRESO)  || '-' || extract(year from A.ATC_EST_FECEGRESO) mes_año, A.SER_OBJ_CODIGO cama  " +
                            "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, con_convenio x, taborddetcir j, TABPRGQRF m " +
                           "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO = '        '  and J.PAC_PAC_NUMERO=M.PAC_PAC_NUMERO and J.ORDNUMERO=M.ORDNUMERO and J.ORDTIPO=M.ORDTIPO " +
                             "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') and A.ATC_EST_NUMERO=J.ATC_EST_NUMERO and A.PAC_PAC_NUMERO=J.PAC_PAC_NUMERO " +
                             "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                             "and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO and A.ATC_EST_NUMERO not in (select Z.ATC_EST_NUMERO from atc_ocupacama z  where trunc (Z.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                             " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                           "order by 2 ";
            }


            if (Servicio == "LABAMB")
            {
                        if (Genero != "")
                        {
                            Genero = " and C.PAC_PAC_SEXO = '" + Genero + "' ";
                        }

                        if (Pagador != "-1")
                        {
                            Pagador = " and d.CON_CON_CODIGO = '" + Pagador + "' ";
                        }
                        else
                        {
                            Pagador = "";
                        }

                        if ((EdadIni != "") && (EdadFin != ""))
                        {
                            EdadIni = " and round((a.RPA_FOR_FECHASOLIC- C.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                        }

                        sql = "SELECT distinct c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,'LABORATORIO CLINICO AMBULATORIO' SERVICIO,decode(C.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.RPA_FOR_FECHASOLIC- C.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from A.RPA_FOR_FECHASOLIC) || '-' || extract(year from A.RPA_FOR_FECHASOLIC) mes_año, '' cama " +
                                 "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                               "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU  and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                   "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU and d.CON_CON_CODIGO=e.CON_CON_CODIGO and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO and a.ORDTIPO='L' " +
                                   "and a.SER_SER_AMBITO='01'  and b.CEX_EVE_TIPOEVENT in ('3 ')  and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                   "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                                   "ORDER BY 2 ";
            }


            if (Servicio == "PATAMB")
            {
                        if (Genero != "")
                        {
                            Genero = " and C.PAC_PAC_SEXO = '" + Genero + "' ";
                        }

                        if (Pagador != "-1")
                        {
                            Pagador = " and d.CON_CON_CODIGO = '" + Pagador + "' ";
                        }
                        else
                        {
                            Pagador = "";
                        }

                        if ((EdadIni != "") && (EdadFin != ""))
                        {
                            EdadIni = " and round((a.RPA_FOR_FECHASOLIC- C.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                        }

                        sql = "select DISTINCT c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,'LABORATORIO PATOLOGIA AMBULATORIO' SERVICIO,decode(C.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.RPA_FOR_FECHASOLIC- C.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from A.RPA_FOR_FECHASOLIC) || '-' || extract(year from A.RPA_FOR_FECHASOLIC) mes_año, '' cama " +
                                "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                   "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU  and d.CON_CON_CODIGO=e.CON_CON_CODIGO and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO and a.ORDTIPO='A' " +
                                   "and a.SER_SER_AMBITO='01' and b.CEX_EVE_TIPOEVENT in ('1 ') and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                   "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                                   "ORDER BY 2 ";
            }

            if (Servicio == "CEXCES")
            {
                        if (Genero != "")
                        {
                            Genero = " and E.PAC_PAC_SEXO = '" + Genero + "' ";
                        }

                        if (Pagador != "-1")
                        {
                            Pagador = " and B.CON_CON_CODIGO = '" + Pagador + "' ";
                        }
                        else
                        {
                            Pagador = "";
                        }

                        if ((EdadIni != "") && (EdadFin != ""))
                        {
                            EdadIni = " and round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                        }

                        sql = "select DISTINCT e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO ,'CONSULTA EXTERNA CENTRO' SERVICIO,decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from b.RPA_FCI_FECHACITAC) || '-' || extract(year from b.RPA_FCI_FECHACITAC) mes_año, '' cama " +
                               "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                               "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                 "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                 "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = 'CES     ' " +
                                 "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO " +
                                 " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                                 "ORDER BY 2 ";
            }

            if (Servicio == "CEXCFF")
            {
                        if (Genero != "")
                        {
                            Genero = " and E.PAC_PAC_SEXO = '" + Genero + "' ";
                        }

                        if (Pagador != "-1")
                        {
                            Pagador = " and B.CON_CON_CODIGO = '" + Pagador + "' ";
                        }
                        else
                        {
                            Pagador = "";
                        }

                        if ((EdadIni != "") && (EdadFin != ""))
                        {
                            EdadIni = " and round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                        }

                        sql = "select DISTINCT e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO, 'CONSULTA EXTERNA FALABELLA' SERVICIO, decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from b.RPA_FCI_FECHACITAC) || '-' || extract(year from b.RPA_FCI_FECHACITAC) mes_año, '' cama " +
                               "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                               "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                 "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                 "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = 'CCF     ' " +
                                 "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO " +
                                 " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                                 "ORDER BY 2 ";
            }


             if (Servicio == "IMAAMB")
             {
                        if (Genero != "")
                        {
                            Genero = " and E.PAC_PAC_SEXO = '" + Genero + "' ";
                        }

                        if (Pagador != "-1")
                        {
                            Pagador = " and B.CON_CON_CODIGO = '" + Pagador + "' ";
                        }
                        else
                        {
                            Pagador = "";
                        }

                        if ((EdadIni != "") && (EdadFin != ""))
                        {
                            EdadIni = " and round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) between '" + EdadIni + "' and '" + EdadFin + "' ";

                        }

                        sql = "select DISTINCT e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO, 'IMAGINOLOGIA AMBULATORIA' SERVICIO,decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) edad, extract(month from b.RPA_FCI_FECHACITAC) || '-' || extract(year from b.RPA_FCI_FECHACITAC) mes_año, '' cama " +
                                             "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                                             "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                               "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                               "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' " +
                                               "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO='57' AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO " +
                                               " " + Genero + " " + " " + Pagador + " " + " " + EdadIni + " " +
                                               "ORDER BY 2 ";
             }


             if (Servicio == "TODOS")
             {
                 condicion = "where ";
                 if (Pagador != "-1")
                 {
                     Pagador = " CODCONVENIO = '" + Pagador + "' ";


                     if ((EdadIni != "") && (EdadFin != ""))
                     {
                         EdadIni = " and EDAD between '" + EdadIni + "' and '" + EdadFin + "' ";
                     }

                     if (Genero != "")
                     {
                         Genero = " and CODSEXO = '" + Genero + "' ";
                     }
                 }
                 else
                 {
                     Pagador = "";
                     if ((EdadIni != "") && (EdadFin != ""))
                     {
                         EdadIni = " EDAD between '" + EdadIni + "' and '" + EdadFin + "' ";

                         if (Genero != "")
                         {
                             Genero = " and CODSEXO = '" + Genero + "' ";
                         }

                     }
                     else
                     {
                         if (Genero != "")
                         {
                             Genero = " CODSEXO = '" + Genero + "' ";
                         }
                         else
                         {
                             condicion = ""; 
                         }
                     }
                 }

                 sql = "select identificacion, paciente ,convenio, servicio, GENERO, edad, MES_AÑO, CAMA from ( " +
                         "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio,trim(Z.SER_SER_DESCRIPCIO) servicio, decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) edad, E.PAC_PAC_SEXO CODSEXO,A.CON_CON_CODIGO CODCONVENIO, extract(month from A.ATC_EST_FECEGRESO)  || '-' || extract(year from A.ATC_EST_FECEGRESO) mes_año, A.SER_OBJ_CODIGO cama  " +
                                    "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, ser_servicios z, con_convenio x " +
                                   "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and SUBSTR(a.SER_OBJ_CODIGO,1,1) <>'V' and a.SER_OBJ_CODIGO <> '        ' " +
                                     "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') " +
                                     "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                                     "and A.SER_SER_CODIGO=Z.SER_SER_CODIGO  and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO " +
                           "union all " +
                           "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente , X.CON_CON_DESCRIPCIO convenio, 'CIRUGIA AMBULATORIA' SERVICIO,decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.ATC_EST_FECEGRESO- E.PAC_PAC_FECHANACIM)/365,2) edad, E.PAC_PAC_SEXO, A.CON_CON_CODIGO, extract(month from A.ATC_EST_FECEGRESO)  || '-' || extract(year from A.ATC_EST_FECEGRESO) mes_año, A.SER_OBJ_CODIGO cama  " +
                                   "from atc_estadia a,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,tabrepthst i, con_convenio x, taborddetcir j, TABPRGQRF m " +
                                  "where trunc (a.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.SER_OBJ_CODIGO = '        '  and J.PAC_PAC_NUMERO=M.PAC_PAC_NUMERO and J.ORDNUMERO=M.ORDNUMERO and J.ORDTIPO=M.ORDTIPO " +
                                    "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO and e.PAC_PAC_NUMERO not in ('1308','5024') and A.ATC_EST_NUMERO=J.ATC_EST_NUMERO and A.PAC_PAC_NUMERO=J.PAC_PAC_NUMERO " +
                                    "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD and c.DSTALTCOD=h.DSTALTCOD and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                                    "and A.ATC_EST_NUMERO=I.ATC_EST_NUMERO(+) and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO(+) and A.CON_CON_CODIGO=X.CON_CON_CODIGO and A.ATC_EST_NUMERO not in (select Z.ATC_EST_NUMERO from atc_ocupacama z  where trunc (Z.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                           "union all " +
                           "SELECT distinct c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,'LABORATORIO CLINICO AMBULATORIO' SERVICIO,decode(C.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.RPA_FOR_FECHASOLIC- C.PAC_PAC_FECHANACIM)/365,2) edad,C.PAC_PAC_SEXO,d.CON_CON_CODIGO, extract(month from A.RPA_FOR_FECHASOLIC) || '-' || extract(year from A.RPA_FOR_FECHASOLIC) MES_AÑO, '' cama " +
                                    "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                  "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU  and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                      "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU and d.CON_CON_CODIGO=e.CON_CON_CODIGO and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO and a.ORDTIPO='L' " +
                                      "and a.SER_SER_AMBITO='01'  and b.CEX_EVE_TIPOEVENT in ('3 ')  and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                      "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                           "UNION ALL " +
                           "select DISTINCT c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,'LABORATORIO PATOLOGIA AMBULATORIO' SERVICIO,decode(C.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((a.RPA_FOR_FECHASOLIC- C.PAC_PAC_FECHANACIM)/365,2) edad,C.PAC_PAC_SEXO,d.CON_CON_CODIGO, extract(month from A.RPA_FOR_FECHASOLIC) || '-' || extract(year from A.RPA_FOR_FECHASOLIC) MES_AÑO, '' cama " +
                                    "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                    "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                       "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU  and d.CON_CON_CODIGO=e.CON_CON_CODIGO and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO and a.ORDTIPO='A' " +
                                       "and a.SER_SER_AMBITO='01' and b.CEX_EVE_TIPOEVENT in ('1 ') and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                       "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                           "union all " +
                           "select DISTINCT e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO ,'CONSULTA EXTERNA CENTRO' SERVICIO,decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) edad,E.PAC_PAC_SEXO,B.CON_CON_CODIGO, extract(month from b.RPA_FCI_FECHACITAC) || '-' || extract(year from b.RPA_FCI_FECHACITAC) mes_año, '' cama " +
                                      "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                                      "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                        "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                        "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = 'CES     ' " +
                                        "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO " +
                           "union all " +
                           "select DISTINCT e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO, 'CONSULTA EXTERNA FALABELLA' SERVICIO, decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) edad,E.PAC_PAC_SEXO,B.CON_CON_CODIGO, extract(month from b.RPA_FCI_FECHACITAC) || '-' || extract(year from b.RPA_FCI_FECHACITAC) mes_año, '' cama  " +
                                      "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                                      "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                        "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                        "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = 'CCF     ' " +
                                        "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO  " +
                           "union all " +
                           "select DISTINCT e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente, I.CON_CON_DESCRIPCIO CONVENIO, 'IMAGINOLOGIA AMBULATORIA' SERVICIO,decode(E.PAC_PAC_SEXO,'F','FEMENINO','M','MASCULINO','') GENERO,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' )- E.PAC_PAC_FECHANACIM)/365,2) edad, E.PAC_PAC_SEXO,B.CON_CON_CODIGO, extract(month from b.RPA_FCI_FECHACITAC) || '-' || extract(year from b.RPA_FCI_FECHACITAC) mes_año, '' cama " +
                                      "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I " +
                                      "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                                        "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                                        "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' " +
                                        "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO='57' AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO) " +
                                        " " + condicion + " " + " " + Pagador + " " + " " + Genero + " " + " " + EdadIni + " " +
                           "order by 2  ";
             }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet MortalidadIntraHospitalaria(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) PACIENTE ,a.ATC_EST_FECHAHOSPI fecha_ingreso,E.SER_SER_DESCRIPCIO SERVICIO ,A.SER_OBJ_CODIGO cama,b.DIA_DIA_CODIGO CIE10,d.DIA_DIA_DESCRIPCIO DESCRIPCION,b.FECPRC FECHA_EGRESO,ROUND((b.FECPRC-a.ATC_EST_FECHAHOSPI)*24 ,2)HORAS " +
                          "FROM ATC_ESTADIA A,tabaltord b,pac_paciente c,dia_diagnos d, SER_SERVICIOS E " +
                          "where trunc (b.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO " +
                          "and b.ALTCNDCOD='01      ' and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and b.DIA_DIA_CODIGO=d.DIA_DIA_CODIGO AND A.SER_SER_CODIGO=E.SER_SER_CODIGO " +
                          "ORDER BY 2";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet AltasMuerto(string FechaIni, string FechaFin, string Servicio)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Servicio"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin,
                Servicio
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            
            string sql="";

            if (Servicio == "CONS")
            {
                sql = "SELECT c.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente ,a.ATC_EST_FECHAHOSPI fecha_ingreso,b.FECPRC fecha_Egreso,E.SER_SER_DESCRIPCIO SERVICIO ,A.SER_OBJ_CODIGO cama, " +
                                        "b.DIA_DIA_CODIGO cie10,d.DIA_DIA_DESCRIPCIO diagnostico, round((b.FECPRC-a.ATC_EST_FECHAHOSPI)*24,2) horas " +
                              "FROM ATC_ESTADIA A,tabaltord b,pac_paciente c,dia_diagnos d, SER_SERVICIOS E  " +
                              "where trunc (b.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and b.ALTCNDCOD='01      ' " +
                                  "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and trim(c.PAC_PAC_RUT) not in ('10','12') and b.DIA_DIA_CODIGO=d.DIA_DIA_CODIGO AND A.SER_SER_CODIGO=E.SER_SER_CODIGO " +
                            "union all " +
                            "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,b.RPA_FDA_HORAINGRESO Fecha_ingreso,b.RPA_FDA_HORAEGRESO fecha_egreso,'URGENCIAS' SERVICIO ,'' cama,G.DIA_DIA_CODIGO cie10, H.DIA_DIA_DESCRIPCIO diagnostico ,round((b.RPA_FDA_HORAEGRESO-b.RPA_FDA_HORAINGRESO)*24,2) horas " +
                              "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h " +
                            "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and b.MTVCORRELATIVO=c.MTVCORRELATIVO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU  " +
                                "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and E.ETDING='M' and trunc(B.RPA_FDA_HORAEGRESO ) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd')  " +
                                "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' " +
                                "and trim(A.PAC_PAC_RUT) not in ('10','12') and A.PAC_PAC_NUMERO not in (select A.PAC_PAC_NUMERO " +
                                                                                         "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h, tabnotpac i " +
                                                                                         "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO  and b.MTVCORRELATIVO=c.MTVCORRELATIVO " +
                                                                                            "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and I.NOTCLSCOD='10' and I.NOTITMCOD='16' and I.NOTPACRES='S' " +
                                                                                            "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU and B.MTVCORRELATIVO=I.MTVCORRELATIVO " +
                                                                                            "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO " +
                                                                                            "and trunc(B.RPA_FDA_HORAEGRESO ) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                                                                                            "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO and trim(A.PAC_PAC_RUT) not in ('10','12') " +
                                                                                            "and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' ) " +
                            "union all " +
                            "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,b.RPA_FDA_HORAINGRESO Fecha_ingreso,b.RPA_FDA_HORAEGRESO fecha_egreso,'URGENCIAS' SERVICIO ,'' cama,G.DIA_DIA_CODIGO cie10, H.DIA_DIA_DESCRIPCIO diagnostico ,round((b.RPA_FDA_HORAEGRESO-b.RPA_FDA_HORAINGRESO)*24,2) horas " +
                              "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h, tabnotpac i " +
                            "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and b.MTVCORRELATIVO=c.MTVCORRELATIVO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and I.NOTCLSCOD='10' and I.NOTITMCOD='16' and I.NOTPACRES='S' " +
                                "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU and B.MTVCORRELATIVO=I.MTVCORRELATIVO and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO " +
                                "and trunc(B.RPA_FDA_HORAEGRESO) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO " +
                                "and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' and trim(A.PAC_PAC_RUT) not in ('10','12') " +
                            "order by 7 ";
            }

            if (Servicio != "011" && Servicio != "008" && Servicio != "001" && Servicio != "CONS")
            {
                sql = "SELECT c.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente ,a.ATC_EST_FECHAHOSPI fecha_ingreso,b.FECPRC fecha_Egreso,E.SER_SER_DESCRIPCIO SERVICIO ,A.SER_OBJ_CODIGO cama, " +
                                        "b.DIA_DIA_CODIGO cie10,d.DIA_DIA_DESCRIPCIO diagnostico, round((b.FECPRC-a.ATC_EST_FECHAHOSPI)*24,2) horas " +
                              "FROM ATC_ESTADIA A,tabaltord b,pac_paciente c,dia_diagnos d, SER_SERVICIOS E  " +
                              "where trunc (b.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and b.ALTCNDCOD='01      ' " +
                                  "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and trim(c.PAC_PAC_RUT) not in ('10','12') and b.DIA_DIA_CODIGO=d.DIA_DIA_CODIGO AND A.SER_SER_CODIGO=E.SER_SER_CODIGO and A.SER_SER_CODIGO IN ('" + Servicio + "') ";
            }


            if (Servicio == "001")
            {
                sql = "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,b.RPA_FDA_HORAINGRESO Fecha_ingreso,b.RPA_FDA_HORAEGRESO fecha_egreso,'URGENCIAS' SERVICIO ,'' cama,G.DIA_DIA_CODIGO cie10, H.DIA_DIA_DESCRIPCIO diagnostico ,round((b.RPA_FDA_HORAEGRESO-b.RPA_FDA_HORAINGRESO)*24,2) horas " +
                              "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h " +
                            "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and b.MTVCORRELATIVO=c.MTVCORRELATIVO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU  " +
                                "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and E.ETDING='M' and trunc(B.RPA_FDA_HORAEGRESO ) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd')  " +
                                "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' " +
                                "and trim(A.PAC_PAC_RUT) not in ('10','12') and A.PAC_PAC_NUMERO not in (select A.PAC_PAC_NUMERO " +
                                                                                         "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h, tabnotpac i " +
                                                                                         "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO  and b.MTVCORRELATIVO=c.MTVCORRELATIVO " +
                                                                                            "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and I.NOTCLSCOD='10' and I.NOTITMCOD='16' and I.NOTPACRES='S' " +
                                                                                            "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU and B.MTVCORRELATIVO=I.MTVCORRELATIVO " +
                                                                                            "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO " +
                                                                                            "and trunc(B.RPA_FDA_HORAEGRESO ) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                                                                                            "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO and trim(A.PAC_PAC_RUT) not in ('10','12') " +
                                                                                            "and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' ) " +
                            "union all " +
                            "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,b.RPA_FDA_HORAINGRESO Fecha_ingreso,b.RPA_FDA_HORAEGRESO fecha_egreso,'URGENCIAS' SERVICIO ,'' cama,G.DIA_DIA_CODIGO cie10, H.DIA_DIA_DESCRIPCIO diagnostico ,round((b.RPA_FDA_HORAEGRESO-b.RPA_FDA_HORAINGRESO)*24,2) horas " +
                              "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e, tabdiagnosticos g, dia_diagnos h, tabnotpac i " +
                            "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and b.MTVCORRELATIVO=c.MTVCORRELATIVO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and I.NOTCLSCOD='10' and I.NOTITMCOD='16' and I.NOTPACRES='S' " +
                                "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU and B.MTVCORRELATIVO=I.MTVCORRELATIVO and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and A.PAC_PAC_NUMERO=I.PAC_PAC_NUMERO " +
                                "and trunc(B.RPA_FDA_HORAEGRESO) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO and G.DIA_DIA_CODIGO=H.DIA_DIA_CODIGO " +
                                "and e.ESPDSTCOD='01'  and G.MTVCORRELATIVO=B.MTVCORRELATIVO and G.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and G.DGNPRINCIPAL='S' and trim(A.PAC_PAC_RUT) not in ('10','12') " +
                            "order by 7 ";
            }



            if (Servicio == "011")
            {
                sql = "SELECT c.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente ,a.ATC_EST_FECHAHOSPI fecha_ingreso,b.FECPRC fecha_Egreso,E.SER_SER_DESCRIPCIO SERVICIO ,A.SER_OBJ_CODIGO cama, " +
                                        "b.DIA_DIA_CODIGO cie10,d.DIA_DIA_DESCRIPCIO diagnostico, round((b.FECPRC-a.ATC_EST_FECHAHOSPI)*24,2) horas " +
                              "FROM ATC_ESTADIA A,tabaltord b,pac_paciente c,dia_diagnos d, SER_SERVICIOS E  " +
                              "where trunc (b.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and b.ALTCNDCOD='01      ' " +
                                  "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and trim(c.PAC_PAC_RUT) not in ('10','12') and b.DIA_DIA_CODIGO=d.DIA_DIA_CODIGO AND A.SER_SER_CODIGO=E.SER_SER_CODIGO and substr(A.SER_OBJ_CODIGO,1,1)='P' and A.SER_SER_CODIGO IN ('" + Servicio + "') ";
            }

            
            if (Servicio == "008")
            {
                sql = "SELECT c.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente ,a.ATC_EST_FECHAHOSPI fecha_ingreso,b.FECPRC fecha_Egreso,E.SER_SER_DESCRIPCIO SERVICIO ,A.SER_OBJ_CODIGO cama, " +
                                        "b.DIA_DIA_CODIGO cie10,d.DIA_DIA_DESCRIPCIO diagnostico, round((b.FECPRC-a.ATC_EST_FECHAHOSPI)*24,2) horas " +
                              "FROM ATC_ESTADIA A,tabaltord b,pac_paciente c,dia_diagnos d, SER_SERVICIOS E  " +
                              "where trunc (b.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and b.ALTCNDCOD='01      ' " +
                                  "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and trim(c.PAC_PAC_RUT) not in ('10','12') and b.DIA_DIA_CODIGO=d.DIA_DIA_CODIGO AND A.SER_SER_CODIGO=E.SER_SER_CODIGO and substr(A.SER_OBJ_CODIGO,1,1)<>'P' and A.SER_SER_CODIGO IN ('011') " +
                       "union all " +
                       "SELECT c.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente ,a.ATC_EST_FECHAHOSPI fecha_ingreso,b.FECPRC fecha_Egreso,E.SER_SER_DESCRIPCIO SERVICIO ,A.SER_OBJ_CODIGO cama, " +
                                        "b.DIA_DIA_CODIGO cie10,d.DIA_DIA_DESCRIPCIO diagnostico, round((b.FECPRC-a.ATC_EST_FECHAHOSPI)*24,2) horas " +
                              "FROM ATC_ESTADIA A,tabaltord b,pac_paciente c,dia_diagnos d, SER_SERVICIOS E  " +
                              "where trunc (b.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO and b.ALTCNDCOD='01      ' " +
                                  "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and trim(c.PAC_PAC_RUT) not in ('10','12') and b.DIA_DIA_CODIGO=d.DIA_DIA_CODIGO AND A.SER_SER_CODIGO=E.SER_SER_CODIGO and A.SER_SER_CODIGO IN ('" + Servicio + "') ";
            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet PacientesDXNotificacionOBLIG(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select distinct D.PAC_PAC_RUT identificacion, nomcompletopaciente(D.PAC_PAC_NUMERO) paciente, a.DIA_DIA_CODIGO cie10,trim(c.DIA_DIA_DESCRIPCIO) descripcion, a.DGNFECHA fecha_diagnostico " +
                           "from tabdiagnosticos a,dia_diagnos c,pac_paciente d " +
                          "where a.DIA_DIA_CODIGO=c.DIA_DIA_CODIGO and C.DIA_DIA_INFOBLIGA='1'  and a.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO and a.PAC_PAC_NUMERO not in ('1308','5024') " +
                            "and trunc (a.DGNFECHA) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') ORDER BY 2,5 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

        public DataSet MorbilidadCirugia(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT F.PRE_PRE_CODIGO codigo,F.PRE_PRE_DESCRIPCIO procedimiento,COUNT(*) cantidad " +
                           "FROM TABORDENESSERV A,taborddetcir d,TABPRDREL E,PRE_PRESTACION F " +
                          "WHERE  a.ORDNUMERO=d.ORDNUMERO AND trunc(A.ORDFECHA) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                            "AND A.ORDTIPO='P' AND D.HORAFIN<>'00:00' AND d.ORDCIRESTCOD IN ('V','X') AND A.ORDNUMERO=E.ORDNUMERO AND A.ORDTIPO=E.ORDTIPO " +
                            "AND E.PRE_PRE_CODIGO=F.PRE_PRE_CODIGO GROUP BY F.PRE_PRE_CODIGO,F.PRE_PRE_DESCRIPCIO order by 3 desc ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);

        }

    }
}
