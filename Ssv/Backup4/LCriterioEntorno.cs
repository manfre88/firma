﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LCriterioEntorno
    {
        public bool CriterioEntornoEstado(CriterioEntorno ctec)
        {
            string[] nomParam = 
            {
                "@Codigo",
	            "@Estado",
	            "@Usr"
            };

            object[] vlrParam = 
            {
                ctec.Codigo,
                ctec.Estado,
                ctec.usr
            };

            return EjecutarSentencia("uspCriteriosEntornoEstado", nomParam, vlrParam);
        }

        public bool CriterioEntornoRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlCriterio" ,
                "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspCriteriosEntornoRetirarXml", nomParam, vlrParam);
        }

        public DataTable CriterioEntornoConsultar(CriterioEntorno est)
        {
            string[] nomParam = 
            {
                "@Codigo"
            };

            object[] vlrParam = 
            {
                est.Codigo
            };

            return getDataTable("uspCriteriosEntornoConsultar", nomParam, vlrParam);

        }

        public DataTable CriterioEntornoConsultar()
        {
            return getDataTable("uspCriteriosEntornoConsultar");
        }

        public bool CriterioEntornoActualizar(CriterioEntorno est)
        {
            string[] nomParam = 
            {
               	"@Codigo",
	            "@Nombre",
	            "@Usr"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Nombre,
                est.usr
            };

            return EjecutarSentencia("uspCriteriosEntornoActualizar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
