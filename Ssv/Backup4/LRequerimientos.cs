﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LRequerimientos
    {
        public DataTable EstadosListar()
        {
            return getDataTable("uspEstadosListar");
        }

        public DataTable EncargadoListar()
        {
            return getDataTable("uspEncargadoListar");
        }

        public DataTable CiaConsultar()
        {
            return getDataTable("uspCompaniaListar");
        }

        public bool ReqDetalleActualizar(Requerimientos req)
        {
            string[] nomParam = 
            { 
            	"@idReq" 
	            ,"@idEstado" 
	            ,"@idUsuClie" 
	            ,"@Informacion" 
	            ,"@IdUsuario" 
	            ,"@Hora" 
                ,"@IdResponsable"
                ,"@IdAsignado"
            };

            object[] vlrParam = 
            { 
            	req.IdReq,
                req.idEstado,
                req.idUsuClie,
                req.Informacion,
                req.idatendio,
                req.Hora,
                req.IdResponsable,
                req.idAsignado
            };

            return EjecutarSentencia("uspReqDetalleActualizar", nomParam, vlrParam);
        }

        public DataSet ReqConsultar(string IdReq)
        {
            string[] nomParam = 
            {
                "@idReq"
            };

            object[] vlrParam = 
            {
                IdReq
            };

            return getDataSet("uspReqConsultar", nomParam, vlrParam);
        }

        public bool ReqValidar(string IdReq)
        {
            string[] nomParam = 
            {
                "@idReq"
            };

            object[] vlrParam = 
            {
                IdReq
            };

            DataTable dt = getDataTable("uspReqValidar", nomParam, vlrParam);

            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

        public string ReqActualizar(Requerimientos req)
        {
            string[] nomParam = 
            { 
                "@IdReq"
                ,"@horaReq"
                ,"@idcliente"
                ,"@negocio"
                ,"@idusuario"
                ,"@telefono"
                ,"@email"
                ,"@idaplicacion"
                ,"@idestado"
                ,"@idencargado"
                ,"@idatendio"
                ,"@fechacompromiso"
                ,"@Descripcion"
                ,"@facturable"
            };

            object[] vlrParam = 
            {
                req.IdReq
                ,req.horaReq
                ,req.idcliente
                ,req.negocio
                ,req.idusuario
                ,req.telefono
                ,req.email
                ,req.idaplicacion
                ,req.idestado
                ,req.idencargado
                ,req.idatendio
                ,req.fechaComp
                ,req.Descripcion
                ,req.facturable
            };

            DataTable dt = getDataTable("uspReqActualizar", nomParam, vlrParam);

            if (dt.Rows.Count > 0)
                return dt.Rows[0]["IDREQUERIMIENTO"].ToString();
            else
                return null;
        }

        public DataSet ConsultarPendientes(string IdReq, string idEstado)
        {
            string[] nomParam = 
            {
                "@idReq",
                "@idEstado"
            };

            object[] vlrParam = 
            {
                IdReq,
                idEstado
            };

            return getDataSet("uspReqPendientes", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataset(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
