﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LAplicacion
    {
        public bool AplicacionEstado(Aplicacion app)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                app.Codigo,
                app.Estado
            };

            return EjecutarSentencia("uspAplicacionEstado", nomParam, vlrParam);
        }

        public bool AplicacionRetirar(string xml,string Usr)
        {
            string[] nomParam = 
            {
                "@xmlApp" ,
                "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspAppRetirarXml", nomParam, vlrParam);
        } 

        public DataTable AplicacionConsultar(Aplicacion app)
        { 
            string[] nomParam = 
            {
                "@Codigo"
            };

            object[] vlrParam = 
            {
                app.Codigo
            };

            return getDataTable("uspAplicacionConsultar", nomParam, vlrParam);
            
        }

        public DataTable AplicacionConsultar()
        {
            return getDataTable("uspAplicacionListar");
        }

        public bool AplicacionActualizar(Aplicacion app)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Nombre" ,
                "@Usr"
            };

            object[] vlrParam = 
            {
                app.Codigo,
                app.Nombre,
                app.Usuario
            };

            return EjecutarSentencia("uspAplicacionActualizar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion

    }
}
