﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LClientes
    {
        public DataTable MpioConsultar(string Pais,string Dpto)
        {
            string[] nomParam = 
            {
                "@CodPais"
                ,"@CodDpto"
            };

            object[] vlrParam = 
            {
                Pais,
                Dpto
            };

            return getDataTable("uspMunicipioConsultar", nomParam, vlrParam);
        }

        public DataSet ClienteConsultar(Clientes cli)
        { 
            string[] nomParam = 
            {
                "@nit"
                ,"@tipo"
                ,"@negocio"
            };

            object[] vlrParam = 
            {
                cli.nit,
                cli.tipo,
                cli.negocio
            };

            return getDataSet("uspClientesConsultar", nomParam, vlrParam);            
        }

        public DataTable ClienteConsultarCia(Clientes cli)
        {
            string[] nomParam = 
            {
                "@nit"
	            ,"@nom"
	            ,"@cia"
            };

            object[] vlrParam = 
            {
                cli.nit,
                cli.nomcomcial,
                cli.Cia
            };

            return getDataTable("uspClientesConsultarCompania", nomParam, vlrParam);
        }

        public string NegocioConsultar(string nit, string tipo)
        { 
            string[] nomParam = 
            {
                "@Nit"
                ,"@Tipo"
            };

            object[] vlrParam = 
            {
                nit,
                tipo
            };

            return getDataTable("uspNegocioConsultar", nomParam, vlrParam).Rows[0]["NEGOCIO"].ToString();            
        }

        

        public DataTable ClienteConsultar()
        {

            return getDataTable("uspClienteListar");
        }

        public DataTable EncargadoConsultar()
        {

            return getDataTable("uspEncargadoConsultar");
        }

        public DataTable TipoConsultar()
        {
            return getDataTable("uspTipoConsultar");
        }

        public DataTable DptoConsultar(string Pais)
        {
            string[] nomParam = 
            {
                "@CodPais"
            };

            object[] vlrParam = 
            {
                Pais
            };

            return getDataTable("uspDepartamentoConsultar", nomParam, vlrParam);
        }

        

        public DataTable UsuarioReporta(string Nit,string Negocio)
        {
            string[] nomParam = 
            {
                "@Nit",
	            "@Negocio"
            };

            object[] vlrParam = 
            {
                Nit,
                Negocio
            };

            return getDataTable("uspUsuarioReportaListar", nomParam, vlrParam);
        }

        public bool ClientesRetirar(string xmlClientes, string Usuario)
        {
            string[] nomParam = 
            {
                "@xmlClientes",
	            "@Usuario"

            };

            object[] vlrParam = 
            {
               xmlClientes, Usuario
            };

            return EjecutarSentencia("uspClienteRetirarXml", nomParam, vlrParam);
        }
        

        public bool ClientesEstado(Clientes cli)
        {
            string[] nomParam = 
            {
                "@nit"
	            ,"@negocio"
	            ,"@tipo"
	            ,"@estado"

            };

            object[] vlrParam = 
            {
                cli.nit ,
                cli.negocio ,
                cli.tipo,
                cli.estado
            };

            return EjecutarSentencia("uspClientesCambiarEstado", nomParam, vlrParam);
        }

        public bool ClientesActualizar(Clientes cli)
        {
            string[] nomParam = 
            {
                "@nit"
               ,"@negocio"
               ,"@razsocial"
               ,"@nomcomcial"
               ,"@direccion"
               ,"@telefono"
               ,"@fax"
               ,"@idpais"
               ,"@iddepartamento"
               ,"@idmunicipio"
               ,"@email"
               ,"@celular"
               ,"@tipo"              
               ,"@sopremoto"
               ,"@internet"
               ,"@usr"
               ,"@xmlCia"
            };

            object[] vlrParam = 
            {
                cli.nit ,
                cli.negocio ,
                cli.razsocial ,
                cli.nomcomcial,
                cli.direccion ,
                cli.telefono ,
                cli.fax,
                cli.idpais ,
                cli.iddepartamento,
                cli.idmunicipio,
                cli.email ,
                cli.celular,
                cli.tipo ,
                cli.sopremoto,
                cli.internet,
                cli.usr,
                cli.xmlCia
            };

            return EjecutarSentencia("uspClientesActualizar", nomParam, vlrParam);
        }

            #region Metodos Privados
            private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
            {
                return new DA.DA().getDataTable(sql, nomParam, valParam);
            }

            private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
            {
                return new DA.DA().getDataset(sql, nomParam, valParam);
            }

            private DataTable getDataTable(string sql)
            {
                return new DA.DA().getDataTable(sql);
            }

            private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
            {
                return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
            }

            #endregion
    }
}
