﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LHospitalizacion
    {
        public DataSet OcupaCama(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select CAMA,ROUND(OCUPACION,2) DIAS_OCUPACION,ROUND(MANTENIMIENTO,2) DIAS_MANTENIMIENTO,decode(Reabertura,'','','SI') reapertura from (select cama,sum(ocupacion) OCUPACION, sum(Reabertura) Reabertura  from ( " +
                                    "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-to_date('" + FechaIni + "','yyyy/mm/dd')) ocupacion,'','',C.ATC_EST_NUMERO Reabertura ,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'1' " +
                                    "from atc_ocupacama b,tabrepthst c " +
                                    "where trunc (B.RPA_FDA_HORAEGRESO) = to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)<to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                    "and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+)  " +
                                    "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('002     ','003     ','005     ','006     ' ,'007     ','008     ','010     ','011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' " +
                                "union all " +
                                    "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-B.RPA_FDA_HORAINGRESO) ocupacion,'','',C.ATC_EST_NUMERO Reabertura,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'2' " +
                                    "from atc_ocupacama b,tabrepthst c  " +
                                    "where trunc (B.RPA_FDA_HORAEGRESO) = to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                    "and trunc(B.RPA_FDA_HORAINGRESO)<=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+)  " +
                                    "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('002     ','003     ','005     ','006     ' ,'007     ','008     ','010     ','011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' " +
                                "union all " +
                                    "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-to_date('" + FechaIni + "','yyyy/mm/dd')) ocupacion ,'','',C.ATC_EST_NUMERO Reabertura,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'3' " +
                                    "from atc_ocupacama b,tabrepthst c where trunc (B.RPA_FDA_HORAEGRESO) <> to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)<to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                    "and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+) " +
                                    "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('002     ','003     ','005     ','006     ' ,'007     ','008     ','010     ','011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' " +
                                "union all " +
                                    "select  distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama ,(b.RPA_FDA_HORAEGRESO-B.RPA_FDA_HORAINGRESO) ocupacion,'','',C.ATC_EST_NUMERO Reabertura ,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'4' " +
                                    "from atc_ocupacama b,tabrepthst c where trunc (B.RPA_FDA_HORAEGRESO) between   to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                    "and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+) and trunc(B.RPA_FDA_HORAINGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                    "and b.SER_OBJ_CODIGO <> '        '   AND B.PAC_PAC_NUMERO NOT IN ('1308','5024')  and B.SER_SER_CODIGO in ('002     ','003     ','005     ','006     ' ,'007     ','008     ','010     ','011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' " +
                         ") group by cama), " +
                         "( select a.SER_OBJ_CODIGO CAMA1, decode(a.MNTTPO,'C', (count(*)*8/24),'P',(count(*)*3),(count(*)*8/24)) MANTENIMIENTO from tabmntsal a " +
                             "where trunc (a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd')  and to_date('" + FechaFin + "','yyyy/mm/dd')  and a.SER_OBJ_CODIGO <> '        ' group by a.SER_OBJ_CODIGO,a.MNTTPO) " +
                        "where cama = cama1(+) " +
                        "ORDER BY CAMA ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet OcupaCamaServicio(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select Z.SER_SER_DESCRIPCIO servicio, sum(ROUND(OCUPACION,2)) DIAS_OCUPACION,sum(ROUND(MANTENIMIENTO,2)) DIAS_MANTENIMIENTO from (select cama,sum(ocupacion) OCUPACION, sum(Reabertura) Reabertura, servicio  from (  " +
                                     "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-to_date('" + FechaIni + "','yyyy/mm/dd')) ocupacion,'','',C.ATC_EST_NUMERO Reabertura ,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'1' , B.SER_SER_CODIGO servicio " +
                                     "from atc_ocupacama b,tabrepthst c " + 
                                     "where trunc (B.RPA_FDA_HORAEGRESO) = to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)<to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+ ) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('002     ','003     ','005     ','006     ' ,'007     ','008     ','010     ','012     ','013     ') and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' " +
                                 "union all " +
                                     "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-B.RPA_FDA_HORAINGRESO) ocupacion,'','',C.ATC_EST_NUMERO Reabertura,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'2' , B.SER_SER_CODIGO servicio " +   
                                     "from atc_ocupacama b,tabrepthst c " + 
                                     "where trunc (B.RPA_FDA_HORAEGRESO) = to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)<=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO ( +) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('002     ','003     ','005     ','006     ' ,'007     ','008     ','010     ','012     ','013     ') and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' " + 
                                 "union all " +
                                     "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-to_date('" + FechaIni + "','yyyy/mm/dd')) ocupacion ,'','',C.ATC_EST_NUMERO Reabertura,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'3' , B.SER_SER_CODIGO servicio  " +     
                                     "from atc_ocupacama b,tabrepthst c where trunc (B.RPA_FDA_HORAEGRESO) <> to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)<to_date('" + FechaIni + "','yyyy/mm/dd')  " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+ ) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('002     ','003     ','005     ','006     ' ,'007     ','008     ','010     ','012     ','013     ') and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' " + 
                                 "union all " +
                                     "select  distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama ,(b.RPA_FDA_HORAEGRESO-B.RPA_FDA_HORAINGRESO) ocupacion,'','',C.ATC_EST_NUMERO Reabertura ,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'4' , B.SER_SER_CODIGO servicio " +     
                                     "from atc_ocupacama b,tabrepthst c where trunc (B.RPA_FDA_HORAEGRESO) between   to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                     "and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO ( +) and trunc(B.RPA_FDA_HORAINGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                     "and b.SER_OBJ_CODIGO <> '        '   AND B.PAC_PAC_NUMERO NOT IN ('1308','5024')  and B.SER_SER_CODIGO in ('002     ','003     ','005     ','006     ' ,'007     ','008     ','010     ','012     ','013     ') and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' " + 
                                 "union all " +
                                 "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-to_date('" + FechaIni + "','yyyy/mm/dd')) ocupacion,'','',C.ATC_EST_NUMERO Reabertura ,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'1' , B.SER_SER_CODIGO servicio " +
                                     "from atc_ocupacama b,tabrepthst c " +
                                     "where trunc (B.RPA_FDA_HORAEGRESO) = to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)<to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+ ) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' and SUBSTR(b.SER_OBJ_CODIGO,1,1)='P' " +
                                 "union all " +
                                     "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-B.RPA_FDA_HORAINGRESO) ocupacion,'','',C.ATC_EST_NUMERO Reabertura,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'2' , B.SER_SER_CODIGO servicio " +
                                     "from atc_ocupacama b,tabrepthst c " +
                                     "where trunc (B.RPA_FDA_HORAEGRESO) = to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)<=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO ( +) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' and SUBSTR(b.SER_OBJ_CODIGO,1,1)='P' " +
                                 "union all " +
                                     "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-to_date('" + FechaIni + "','yyyy/mm/dd')) ocupacion ,'','',C.ATC_EST_NUMERO Reabertura,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'3' , B.SER_SER_CODIGO servicio  " +
                                     "from atc_ocupacama b,tabrepthst c where trunc (B.RPA_FDA_HORAEGRESO) <> to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)<to_date('" + FechaIni + "','yyyy/mm/dd')  " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+ ) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' and SUBSTR(b.SER_OBJ_CODIGO,1,1)='P' " +
                                 "union all " +
                                     "select  distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama ,(b.RPA_FDA_HORAEGRESO-B.RPA_FDA_HORAINGRESO) ocupacion,'','',C.ATC_EST_NUMERO Reabertura ,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'4' , B.SER_SER_CODIGO servicio " +
                                     "from atc_ocupacama b,tabrepthst c where trunc (B.RPA_FDA_HORAEGRESO) between   to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                     "and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO ( +) and trunc(B.RPA_FDA_HORAINGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                     "and b.SER_OBJ_CODIGO <> '        '   AND B.PAC_PAC_NUMERO NOT IN ('1308','5024')  and B.SER_SER_CODIGO in ('011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' and SUBSTR(b.SER_OBJ_CODIGO,1,1)='P'  " + 
                                 "union all " +
                                 "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-to_date('" + FechaIni + "','yyyy/mm/dd')) ocupacion,'','',C.ATC_EST_NUMERO Reabertura ,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'1' , '008     ' servicio " +
                                     "from atc_ocupacama b,tabrepthst c " +
                                     "where trunc (B.RPA_FDA_HORAEGRESO) = to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)<to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+ ) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' and SUBSTR(b.SER_OBJ_CODIGO,1,1)<>'P' " +
                                 "union all " +
                                     "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-B.RPA_FDA_HORAINGRESO) ocupacion,'','',C.ATC_EST_NUMERO Reabertura,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'2' , '008     ' servicio " +
                                     "from atc_ocupacama b,tabrepthst c " +
                                     "where trunc (B.RPA_FDA_HORAEGRESO) = to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaIni + "','yyyy/mm/dd') " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)<=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO ( +) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' and SUBSTR(b.SER_OBJ_CODIGO,1,1)<>'P' " +
                                 "union all " +
                                     "select distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama,(to_date('" + FechaFin + "','yyyy/mm/dd')-to_date('" + FechaIni + "','yyyy/mm/dd')) ocupacion ,'','',C.ATC_EST_NUMERO Reabertura,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'3' , '008     ' servicio  " +
                                     "from atc_ocupacama b,tabrepthst c where trunc (B.RPA_FDA_HORAEGRESO) <> to_date('1900/01/01','yyyy/mm/dd')  and trunc(B.RPA_FDA_HORAINGRESO)<to_date('" + FechaIni + "','yyyy/mm/dd')  " +
                                     "and trunc(B.RPA_FDA_HORAINGRESO)>=to_date('" + FechaFin + "','yyyy/mm/dd') and b.SER_OBJ_CODIGO <> '        '  and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO (+ ) " +
                                     "AND B.PAC_PAC_NUMERO NOT IN ('1308','5024') and B.SER_SER_CODIGO in ('011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' and SUBSTR(b.SER_OBJ_CODIGO,1,1)<>'P' " +
                                 "union all " +
                                     "select  distinct B.ATC_EST_NUMERO ,b.SER_OBJ_CODIGO cama ,(b.RPA_FDA_HORAEGRESO-B.RPA_FDA_HORAINGRESO) ocupacion,'','',C.ATC_EST_NUMERO Reabertura ,b.RPA_FDA_HORAEGRESO,b.RPA_FDA_HORAINGRESO,'4' , '008     ' servicio " +
                                     "from atc_ocupacama b,tabrepthst c where trunc (B.RPA_FDA_HORAEGRESO) between   to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                     "and B.ATC_EST_NUMERO=C.ATC_EST_NUMERO ( +) and trunc(B.RPA_FDA_HORAINGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                     "and b.SER_OBJ_CODIGO <> '        '   AND B.PAC_PAC_NUMERO NOT IN ('1308','5024')  and B.SER_SER_CODIGO in ('011     ' ) and SUBSTR(SER_OBJ_CODIGO,1,1) <>'V' and SUBSTR(b.SER_OBJ_CODIGO,1,1)<>'P'  " + 
                          ") group by cama, servicio),  " +  
                          "( select a.SER_OBJ_CODIGO CAMA1, decode(a.MNTTPO,'C', (count(*)*8/24),'P',(count(*)*3),(count(*)*8/24)) MANTENIMIENTO from tabmntsal a  " +
                              "where trunc (a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd')  and to_date('" + FechaFin + "','yyyy/mm/dd')  and a.SER_OBJ_CODIGO <> '        ' group by a.SER_OBJ_CODIGO,a.MNTTPO), ser_servicios z " +
                         "where cama = cama1(+ )  and servicio=Z.SER_SER_CODIGO " +
                         "group by Z.SER_SER_DESCRIPCIO order by 1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet IndiceReingresos(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select * from ( " +
                            "SELECT B.PAC_PAC_RUT identificacion ,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,a.ATC_EST_FECHAHOSPI fecha_hospitalizacion, a.ATC_EST_FECEGRESO fecha_egreso, A.SER_OBJ_CODIGO cama,D.DIA_DIA_CODIGO CIE10,E.DIA_DIA_DESCRIPCIO diagnostico, " +
                            "round(((LEAD(a.ATC_EST_FECEGRESO, 1, NULL) OVER(ORDER BY a.PAC_PAC_NUMERO,a.ATC_EST_FECHAHOSPI desc)  - a.ATC_EST_FECHAHOSPI)*-1),2)AS total_dias, c.MTVCORRELATIVO EVENTO " +
                            "from atc_estadia a,pac_paciente b,tabencuentros c,tabdiagnosticos d,dia_diagnos e, (select * from ( select a.PAC_PAC_NUMERO paciente,count(*) ingresos from  atc_estadia a " +
                                                                                                                                "where trunc(A.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd')-20 and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                                                                                                                "or trunc(a.ATC_EST_FECHAHOSPI) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                                                                                                                "OR (trunc(a.ATC_EST_FECHAHOSPI) = TO_DATE('1900/01/01','YYYY/MM/DD') AND trunc(A.ATC_EST_FECEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') ) " +
                                                                                                                                "group by a.PAC_PAC_NUMERO order by 2 desc) where ingresos>1) " +
                            "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO " +
                             "and a.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO and c.MTVCORRELATIVO=d.MTVCORRELATIVO and c.TABENCUENTRO=d.TABENCUENTRO " +
                             "and c.AMBITOCODIGO='02' and d.DGNPRINCIPAL='S' and a.MTVCORRELATIVO=c.MTVCORRELATIVO " +
                             "and d.DIA_DIA_CODIGO=e.DIA_DIA_CODIGO and A.PAC_PAC_NUMERO=paciente " +
                            "ORDER BY B.PAC_PAC_RUT,a.ATC_EST_FECHAHOSPI " +
                            ") where total_dias <=20 and total_dias >0 and (trunc (fecha_egreso) >= to_date ('" + FechaIni + "','yyyy/mm/dd') OR trunc (fecha_egreso) =to_date( '1900/01/01','yyyy/mm/dd'))and identificacion not in ('10           ','12           ') and trunc (fecha_hospitalizacion) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd')  ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet HospitalxEspec(string Especialidad)
        {
            string[] nomParam = 
            {
                "In_Especialidad"
            };

            object[] vlrParam = 
            {
                Especialidad
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select b.PAC_PAC_RUT identificacion,nomcompletopaciente(b.PAC_PAC_NUMERO)paciente,E.SER_ESP_DESCRIPCIO especialidad,F.CON_CON_DESCRIPCIO convenio,A.SER_OBJ_CODIGO cama " +
                           "from atc_estadia a, pac_paciente b, Tabmotivocons c, Ser_Especiali e, con_convenio f, Net_SerEspSerPro g " +
                          "where trunc(A.ATC_EST_FECEGRESO) = to_date ('1900/01/01','yyyy/mm/dd') " +
                            "and a.SER_SER_CODIGO <> '        ' and A.SER_OBJ_CODIGO <>'        '  and A.SER_OBJ_CODIGO not like 'V%' " +
                            "and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                            "and A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO and A.MTVCORRELATIVO=C.MTVCORRELATIVO and C.MTVESTADO='A' " +
                            "and C.SER_PRO_RUT = G.SER_PRO_RUT and E.SER_ESP_CODIGO=G.SER_ESP_CODIGO " +
                            "and A.CON_CON_CODIGO=F.CON_CON_CODIGO and G.SER_ESP_CODIGO = '" + Especialidad + "' " +
                       "order by paciente ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet SalaPosEgreso(string Fecha)
        {
            string[] nomParam = 
            {
                "In_Fecha"
            };

            object[] vlrParam = 
            {
                Fecha
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select TRIM(SIL.NOTITMOBS) silla ,nomcompletopaciente(pac.PAC_PAC_NUMERO) paciente ,PAC.FECPRC fecha_ingreso,egr.FECPRC fecha_egreso, TRIM(PAC.NOTITMOBS) observacion " +
                           "from (TabNotPac Pac left join tabnotpac sil on SIL.PAC_PAC_NUMERO=PAC.PAC_PAC_NUMERO and PAC.MTVCORRELATIVO=SIL.MTVCORRELATIVO and PAC.NOTCLSCOD=SIL.NOTCLSCOD and SIL.NOTITMCOD='02' and SIL.NOTPACRES='S' ) " +
                           "left join tabnotpac egr on egr.PAC_PAC_NUMERO=PAC.PAC_PAC_NUMERO and egr.MTVCORRELATIVO = PAC.MTVCORRELATIVO and egr.NOTCLSCOD=PAC.NOTCLSCOD and egr.NOTITMCOD='03' and EGR.NOTPACRES='S' " +
                          "where trunc(PAC.NOTPACFEC) = to_date('" + Fecha + "','yyyy/mm/dd') and  PAC.NOTCLSCOD='14' and PAC.NOTITMCOD='01' and PAC.NOTPACRES='S' order by 1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
