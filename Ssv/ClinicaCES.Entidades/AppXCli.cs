﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class AppXCli
    {
        public string Nit;
        public string Negocio;
        public string Aplicacion;
        public string UsrInstala;
        public string Usr;
        public string FecIns;
        public bool? Estado;
    }
}
