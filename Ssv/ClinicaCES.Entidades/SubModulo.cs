﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class SubModulo
    {
        public string Codigo;
        public string SubMod;
        public string Aplicacion;
        public string Menu;
        public string Mod;
        public string Usr;
        public bool? Estado;
    }
}
