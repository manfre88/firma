﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Usuarios
    {
        public string Usuario;
        public string Clave;
        public string Nivel;
        public string Nombres;
        public string Apellido1;
        public string Apellido2;
        public string Email;
        public string Direccion;
        public string Telefono;
        public string UsrMod;
        public string Tema;
        public string Cliente;
    }
}
