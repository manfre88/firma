﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Estados
    {
        public string Codigo;
        public string Nombre;
        public string Usuario;
        public bool? Estado;
    }
}
