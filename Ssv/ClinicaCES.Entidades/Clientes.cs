﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Clientes
    {
        public string nit ;
        public string negocio ;
        public string razsocial ;
        public string nomcomcial;
        public string direccion ;
        public string telefono ;
        public string fax;
        public string idpais ;
        public string iddepartamento;
        public string idmunicipio;
        public string email ;
        public string celular;
        public string tipo ;      
        public bool? sopremoto;
        public bool? internet;
        public bool? estado;
        public string usr;
        public string xmlCia;
        public string Cia;
    }
}
