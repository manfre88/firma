﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Cargos
    {
        public string Codigo;
        public string Area;
        public string Nombre;
        public bool? Estado;
        public string Usuario;
        public string VlrHora;
    }
}
