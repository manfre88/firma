﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Ambulatorios
    {
        public string Id;
        public string Identificacion;
        public string Nombre;
        public string Convenio;
        public string TipoSolicitud;
        public string SoporteEnv;
        public string NumEvento;
        public DateTime FechaPosResp;
        public string TipoProced;
        public string Especialidad;
        public string ProcIndicado;
        public string ProcedAprobado;
        public string Prestador;
        public DateTime FechaProcedim;
        public DateTime FechaCx;
        public string Observacion;
        public string Estado;
        public string Tiempo;
        public string UndTiempo;
        public string Usuario;
        public string tipo;
        public bool cambioestado;
        
    }
}
