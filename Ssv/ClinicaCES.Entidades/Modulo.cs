﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Modulo
    {
        public string Codigo;
        public string Mod;
        public string Aplicacion;
        public string Menu;
        public string Usr;
        public bool? Estado;
    }
}
