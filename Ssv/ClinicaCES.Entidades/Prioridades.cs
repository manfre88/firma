﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Prioridades
    {
        public string Codigo;
        public string Nombre;
        public string Horas;
        public string Usuario;
        public bool? Estado;
    }
}
