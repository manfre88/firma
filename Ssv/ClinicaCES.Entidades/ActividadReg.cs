﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class ActividadReg
    {
        public string Semana;
        public string Responsable;
        public string Cliente;
        public string Negocio;
        public string Linea;
        public string Aplicacion;
        public string Programa;
        public string CNA;
        public string Asunto;
        public string Actividad;
        public bool? Planeada;
        public string Fecha;
        public string Tiempo;
        public string Codigo;
        public string Borrar;
        public string requerimiento;
        public bool? Finalizado;

    }
}
