﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Solicitudes
    {
        public string fecha;
        public string reporto;
        public string tiposolicitud;
        public string origen;
        public string destino;
        public string descripcion;
        public string fechahorafin;
        public string tiempo;
    }
}
