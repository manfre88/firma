﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Anexo1
    {
        public string Numero;
        public string Fecha;
        public string Hora;
        public string TipoInconsistencia;
        public string Cobertura;
        public string InfoBD;
        public string Correccion;
        public string Observacion;
        public string Solicitante;
        public string pdf;
        public string Usuario;
        public string Para;
        public string Mensaje;
        public string Mensaje2;
        public string TipoAnexo;
        public string Asunto;
        public string Ruta;
        public string Identificacion;
       
   
    }
}
