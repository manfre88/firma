﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Asunto
    {
        public string fecha;
        public string horaini;
        public string idcliente;
        public string negocio;
        public string idusuario;
        public string telefono;
        public string email;
        public string idaplicacion;
        public string idmenu;
        public string idmodulo;
        public string idsubmodulo;
        public string version;
        public string asunto;
        public string idestado;
        public string idencargado;
        public string solicita;
        public string idatendio;
        public string idclasificacion;
        public string idprioridad;
        public string horagra;
        public string IdAsunto;
        public string Descripcion;

	    public string idEstado;
	    public string idEncargado;
	    public string idUsuClie;
	    public string Informacion ;
	    public string IdUsuario;
	    public string Fecha ;
	    public string HoraIni;
	    public string HoraFin;
	    public bool? Recibieron ;
	    public string FRecibieron ;
        public string IdUsuarioRecibio;
        public string IdResponsable;
        public string xmlArchivos;
        public string Asignado;

    }
}
