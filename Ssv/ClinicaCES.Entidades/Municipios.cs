﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Municipios
    {
        public string Codigo;
        public string Municipio;
        public string Pais;
        public string Depar;
        public string Usr;
        public bool? Estado;
    }
}
