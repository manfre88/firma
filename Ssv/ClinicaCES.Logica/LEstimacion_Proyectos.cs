﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LEstimacion_Proyectos
    {
        /*Movimientos Estimacion Proyecto*/

        public DataSet ProyectoConsultar(Estimacion_Proyectos est)
        {
            string[] nomParam = 
            {
                "@Codigo"
            };

            object[] vlrParam = 
            {
                est.Codigo
            };

            return getDataSet("uspProyectoConsultar", nomParam, vlrParam);

        }

        public bool ProyectosActualizar(Estimacion_Proyectos est)
        {
            string[] nomParam = 
            {
               	"@Codigo",
	            "@Proyecto",
	            "@Nit",
	            "@Negocio",
	            "@Gerente",
	            "@Usr",
                "@xmlUsos",
                "@Simple",
	            "@Medio",
	            "@Complejo",
                "@xmlComplejidad",
                "@xmlFactor",
                "@Horas",
                "@xmlValores"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Descripcion,
                est.Nit,
                est.Negocio,
                est.Gerente,
                est.usr,
                est.xmlUsos,
                est.simple,
                est.medio,
                est.complejo,
                est.xmlComplejidad,
                est.xmlFactor,
                est.Horas,
                est.xmlEstimacion
            };

            return EjecutarSentencia("uspProyectoActualizar", nomParam, vlrParam);
        }

        /*---------------------------*/


        public bool Estimacion_ProyectosEstado(Estimacion_Proyectos est)
        {
            string[] nomParam = 
            {
                "@Codigo",
	            "@Estado",
	            "@Usr"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Estado,
                est.usr
            };

            return EjecutarSentencia("uspEstimacionProyectosEstado", nomParam, vlrParam);
        }


        public bool Estimacion_ProyectosRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlProyectos" ,
                "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspEstimacionProyectosRetirarXml", nomParam, vlrParam);
        }

        public DataTable ConsultarEsfCosto(Estimacion_Proyectos cod)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { cod.Codigo };

            return getDataTable("uspConsultarEsfCosto", nomParam, vlrParam);
        }

        public DataTable Estimacion_ProyectosConsultar(Estimacion_Proyectos est)
        {
            string[] nomParam = 
            {
                "@Codigo",
                "@Proyecto"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Proyecto
            };

            return getDataTable("uspEstimacionProyectosBuscar", nomParam, vlrParam);

        }

        public DataTable Estimacion_ProyectosConsultar()
        {
            return getDataTable("uspEstimacionProyectosBuscar");
        }

        public DataTable ActoresConsultar(Estimacion_Proyectos est)
        {
            string[] nomParam = 
            {
                "@Codigo"
            };

            object[] vlrParam = 
            {
                est.Codigo
            };

            return getDataTable("uspActoresListar", nomParam, vlrParam);
        }

        public DataTable ActoresConsultar()
        {
            return getDataTable("uspActoresListar");
        }

        public bool Estimacion_ProyectosActualizar(Estimacion_Proyectos est)
        {
            string[] nomParam = 
            {
               	"@Codigo",
	            "@Descripcion",
	            "@Porcentaje",
	            "@Usr",
                "@Valor"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Descripcion,
                est.Porcentaje,
                est.usr,
                est.Vlr
            };

            return EjecutarSentencia("uspEstimacionProyectoActualizar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataset(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
