﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LComplejidadTecnica
    {
        public bool ComplejidadTecnicaEstado(ComplejidadTecnica ctec)
        {
            string[] nomParam = 
            {
                "@Codigo",
	            "@Estado",
	            "@Usr"
            };

            object[] vlrParam = 
            {
                ctec.Codigo,
                ctec.Estado,
                ctec.usr
            };

            return EjecutarSentencia("uspComplejidadTecnicaEstado", nomParam, vlrParam);
        }


        public bool ComplejidadTecnicaRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlComple" ,
                "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspComplejidadTecnicaRetirarXml", nomParam, vlrParam);
        }

        public DataTable ComplejidadTecnicaConsultar(ComplejidadTecnica est)
        {
            string[] nomParam = 
            {
                "@Codigo",
                "@Proyecto"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Proyecto
            };

            return getDataTable("uspComplejidadTecnicaConsultar", nomParam, vlrParam);

        }

        public DataTable ComplejidadTecnicaConsultar()
        {
            return getDataTable("uspComplejidadTecnicaConsultar");
        }

        public bool ComplejidadTecnicaActualizar(ComplejidadTecnica est)
        {
            string[] nomParam = 
            {
               	"@Codigo",
	            "@Descripcion",
	            "@Nombre",
	            "@Usr",
                "@Peso"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Descripcion,
                est.Nombre,
                est.usr,
                est.Peso
            };

            return EjecutarSentencia("uspComplejidadTecnicaActualizar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
