﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LEpidemiologia
    {
        public DataSet InfEspecificacionHerida(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };
            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "select distinct H.ATE_PRE_FECHAENTRE FECHA ,A.PRE_PRE_CODIGO CODPRESTACION,d.PRE_PRE_DESCRIPCIO PRESTACION, " +
                        "c.PAC_PAC_RUT IDENTIFICACION, GLBGETNOMPAC(A.PAC_PAC_NUMERO) NOMBRE,TRIM(B.LAB_OPC_DESCRIPCIO) TIPOHERIDA,g.SER_OBJ_CODIGO CAMA" +
                            " from hcp_resultados a,lab_opciones b,pac_paciente c,pre_prestacion d,taborddetcir f,atc_estadia g,ate_prestacion h " +
                            " where rtrim(a.CEX_RES_TEXTO)=rtrim(b.LAB_OPC_CODIGO) and b.LAB_VAR_CODIGO='HEQX' " +
                            "  and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO  " +
                            "  and A.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO " +
                            "  and A.HCP_INF_NUMERFORMU=f.ORDNUMERO " +
                            "  and f.HORAINI <> '00:00'  " +
                            "  and trunc(H.ATE_PRE_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')  " +
                            "   and f.ATC_EST_NUMERO=g.ATC_EST_NUMERO " +
                            "  and A.PAC_PAC_NUMERO=H.ATE_PRE_NUMERPACIE  " +
                            "  and A.HCP_INF_NUMERFORMU=H.ORDNUMERO " +
                            "  and A.PRE_PRE_CODIGO=H.ATE_PRE_CODIGO " +
                            "  and A.CEX_RES_SECUENCIA=91383" +
                            " ORDER BY c.PAC_PAC_RUT";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
    } 
}
