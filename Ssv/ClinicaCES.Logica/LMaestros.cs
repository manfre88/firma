﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LMaestros
    {
        //public bool NivelActualizar(Nivel nvl)
        //{
        //    string[] nomParam = { "@Codigo", "@Nivel" };
        //    object[] vlrParam = { nvl.Codigo, nvl.nivel };

        //    return EjecutarSentencia("uspNivelActualizar", nomParam, vlrParam);
        //}

        public string ConsultarPrestacionesOracle1(int TipoVigencia)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql="";
            switch (TipoVigencia)
            {
                case 1:
                    {
                        sql = "select decode(b.estadoaut, 0, 'False', 1, 'True', '', 'False') ESTADO_AUT,  TRIM(a.pre_pre_codigo) CODPRESTACION, TRIM(a.PRE_PRE_DESCRIPCIO) DESCRIPCION from pre_prestacion a left join CENAUT_PRESTACION b on TRIM(a.pre_pre_codigo) =TRIM(b.codprestacion)  WHERE a.PRE_PRE_VIGENCIA='S'  ORDER BY TRIM(a.pre_pre_codigo)";
                        break;
                    }
                case 2:
                    {
                        sql = "select decode(b.estadoaut, 0, 'False', 1, 'True', '', 'False') ESTADO_AUT,  TRIM(a.pre_pre_codigo) CODPRESTACION, TRIM(a.PRE_PRE_DESCRIPCIO) DESCRIPCION from pre_prestacion a left join CENAUT_PRESTACION b on TRIM(a.pre_pre_codigo) =TRIM(b.codprestacion)  WHERE a.PRE_PRE_VIGENCIA='N'  ORDER BY TRIM(a.pre_pre_codigo)";
                        break;
                    }
            }
            return getDataTablePrueba(sql, "Oracle");
        }
        public DataTable EstadosConsultar()
        {
            return getDataTable("uspEstadosConsultar");
        }
        public DataTable ConsultarPrestacionesOracle(int TipoVigencia)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            switch (TipoVigencia)
            {
                case 1:
                    {
                        sql = "select decode(b.estadoaut, 0, 'False', 1, 'True', '', 'False') ESTADO_AUT,  TRIM(a.pre_pre_codigo) CODPRESTACION, TRIM(a.PRE_PRE_DESCRIPCIO) DESCRIPCION from pre_prestacion a left join CENAUT_PRESTACION b on TRIM(a.pre_pre_codigo) =TRIM(b.codprestacion)  WHERE a.PRE_PRE_VIGENCIA='S'  ORDER BY TRIM(a.pre_pre_codigo)";
                        break;
                    }
                case 2:
                    {
                        sql = "select decode(b.estadoaut, 0, 'False', 1, 'True', '', 'False') ESTADO_AUT,  TRIM(a.pre_pre_codigo) CODPRESTACION, TRIM(a.PRE_PRE_DESCRIPCIO) DESCRIPCION from pre_prestacion a left join CENAUT_PRESTACION b on TRIM(a.pre_pre_codigo) =TRIM(b.codprestacion)  WHERE a.PRE_PRE_VIGENCIA='N'  ORDER BY TRIM(a.pre_pre_codigo)";
                        break;
                    }
            }
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarEmpresasOracle()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "select TRIM(a.con_emp_rut) CODEMPRESA, TRIM(a.CON_EMP_DESCRIPCIO) DESCRIPCION, b.correo CORREO  from con_EMPRESA a left join cenaut_empresas b on trim(a.con_emp_rut) = trim(b.codempresa) where a.con_emp_vigencia='S' order by TRIM(a.con_emp_rut)";
                      
               
            return getDataTable(sql, "Oracle");
        }
        public DataTable ListarProfesionales()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = " select TRIM(PROF.SER_PRO_RUT) ID, TRIM(FuncHspGetProf(1, PROF.SER_PRO_RUT)) NOMBRE,  FuncHspGetProf(2, PROF.SER_PRO_RUT) ESPECIALIDAD  " +
                " from ser_profesiona PROF, TAB_TIPOPROFE TIPROFE, TBL_USER USUARIO " +
                " WHERE PROF.SER_PRO_TIPO=TIPROFE.TIPOPROFECODIGO " +
               " AND PROF.SER_PRO_CUENTA= USUARIO.FLD_USERCODE " +
                " AND PROF.SER_PRO_ESTADO='A' AND (USUARIO.DPCTPOCOD='0016' OR USUARIO.DPCTPOCOD='0026') " +
                " ORDER BY 2";


            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarProfesional(string Id)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "  select TRIM(PROF.SER_PRO_APELLPATER) PATERNO, TRIM(PROF.SER_PRO_APELLMATER) MATERNO, trim(SUBSTR(PROF.SER_PRO_NOMBRES, 1, INSTR(PROF.SER_PRO_NOMBRES, ' '))) PNOMBRE, " +
                  "   trim(SUBSTR(PROF.SER_PRO_NOMBRES, INSTR(PROF.SER_PRO_NOMBRES, ' '))) SNOMBRE, PROF.SER_PRO_TIPOIDENT TIPOID,  TRIM(PROF.SER_PRO_RUT) ID, '5767272' TEL, '0' EXT, FuncHspGetProf(2, PROF.SER_PRO_RUT) ESPECIALIDAD " +
                   "  from ser_profesiona PROF, TAB_TIPOPROFE TIPROFE, TBL_USER USUARIO " +
                    " WHERE PROF.SER_PRO_TIPO=TIPROFE.TIPOPROFECODIGO " +
                  "  AND PROF.SER_PRO_CUENTA= USUARIO.FLD_USERCODE " +
                  "   AND PROF.SER_PRO_ESTADO='A' AND (USUARIO.DPCTPOCOD='0016' OR USUARIO.DPCTPOCOD='0026') " +
                  "   and TRIM(PROF.SER_PRO_RUT)='" + Id + "' " +
                   "  ORDER BY 2";


            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarAsigCamaServicios()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select DISTINCT A.SER_sER_CODIGO CODIGO, TRIM(A.SER_SER_DESCRIPCIO) SERVICIO from ser_servicios A,  ser_objetos B where A.ser_Ser_vigencia='S' and A.ser_ser_ambito='02' AND A.SER_SER_CODIGO=B.SER_SER_CODIGO and A.SER_sER_CODIGO<>'016     ' order by 2";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarCamaServicio(string  cama)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select ser_ser_codigo from ser_objetos where ser_obj_codigo='" + cama + "' ";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarServicioCama(string Servicio)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select ser_obj_codigo  from ser_objetos where ser_ser_codigo='" + Servicio + "' ";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarAsigCamaServiciosRes( )
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select DISTINCT trim(b.ser_obj_codigo)  || ' - ' || TRIM(A.SER_SER_DESCRIPCIO) CamaServicio ,  A.SER_sER_CODIGO CODIGO, TRIM(A.SER_SER_DESCRIPCIO) SERVICIO, b.ser_obj_codigo cama,  EST.PAC_PAC_NUMERO, EST.MTVCORRELATIVO, '' RESPONSABLE  " +
                   "  from ser_servicios A,  ser_objetos B, ATC_ESTADIA EST, TABORDENESSERV ord, TABORDDETALLE orddet, cenaut_prestacion pres " +
                   "  where A.ser_Ser_vigencia='S' and A.ser_ser_ambito='02' AND A.SER_SER_CODIGO=B.SER_SER_CODIGO and b.ser_obj_codigo= EST.SER_OBJ_CODIGO and ATCESTADO ='H'  " +
                   "  and EST.MTVCORRELATIVO=ORD.MTVCORRELATIVO and EST.PAC_PAC_NUMERO=ORD.PAC_PAC_NUMERO and ORD.MTVCORRELATIVO=ORDDET.MTVCORRELATIVO " +
                   "  and ORD.PAC_PAC_NUMERO=ORDDET.PAC_PAC_NUMERO and ORD.ORDNUMERO=ORDDET.ORDNUMERO and ORDDET.PRE_PRE_CODIGO=PRES.CODPRESTACION " +
                   "   order by 2";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarEspecialidades()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select ordtipo, ordnombre from tabordsrvtpo order by 2";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarPrestaciones(string OrdTipo)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "SELECT PRES.PRE_PRE_CODIGO CODIGO, PRE_PRE_DESCRIPCIO PRESTACION  " +
                   " FROM PRE_PRESTACION PRES,  tabordsrvtpo TIPO, CENAUT_PRESTACION CENPRES " +
                   "  WHERE TIPO.ORDTIPO='" + OrdTipo + "' AND PRES.PRE_PRE_VIGENCIA='S' " +
                   "  AND PRES.PRE_PRE_TIPO=TIPO.PRE_TIP_TIPO " +
                   "  AND PRES.PRE_PRE_CODIGO=CENPRES.CODPRESTACION AND CENPRES.ESTADOAUT=1 " +
                   "  UNION ALL " +
                   " SELECT PRES.PRE_PRE_CODIGO CODIGO, PRE_PRE_DESCRIPCIO PRESTACION " +
                   " FROM PRE_PRESTACION PRES,  tabordsrvtpo TIPO, CENAUT_PRESTACION CENPRES " +
                  "   WHERE TIPO.ORDTIPO='" + OrdTipo + "' AND PRES.PRE_PRE_VIGENCIA='S' " +
                   "  AND PRES.PRE_PRE_SUBTIPO=TIPO.PRE_TIP_TIPO " +
                  "   AND PRES.PRE_PRE_CODIGO=CENPRES.CODPRESTACION  AND CENPRES.ESTADOAUT=1 " +
                  "   UNION ALL " +
                  "    SELECT HEMO.HMCTPOCOD CODIGO, HEMO.HMCTPONOM PRESTACION FROM TABHMCTPO HEMO, CENAUT_PRESTACION CENPRES " +
                 "    WHERE HEMO.VIGENCIA= DECODE('" + OrdTipo + "','B','S','N') AND HEMO.HMCTPOCOD<>'AU' AND HEMO.HMCTPOCOD=CENPRES.CODPRESTACION AND CENPRES.ESTADOAUT=1 " +
                 "    ORDER BY 2";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarAsigCamaServiciosResVDos()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "  SELECT OrdNombre,  " +
"     OrdNumero,  " +
"     PreCod,  " +
"     PreNom,  " +
"     FechaRevis,  " +
"     FechaSolic,  " +
"     ORDEN,  " +
"     Estado,  " +
"     FechaConfirm,  " +
"     tipoformu,  " +
"     numerformu,  " +
"     ordtipo,  " +
"     ExamCit,  " +
"     forcod,  " +
"     forvrs,  " +
"     ulteve   " +
"     ,NVL(CODESTADO,1) CODESTADO,  " +
"       OBSERVACION,  " +
"       ALERTA,  " +
"       NVL(VALORALERTA,0) VALORALERTA,  " +
"       CODTIEMPOALERTA  " +
"     ,MTVCORRELATIVO  " +
"      , FunGlbGetRutPac(PAC_PAC_NUMERO) ID, PAC_PAC_NUMERO  " +
"       ,CamaServicio " +
"               , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
"     FROM  " +
"     (SELECT b.*,  " +
"         rownum registro  " +
"       FROM  " +
"         (SELECT OrdNombre,  " +
"           OrdNumero,  " +
"           PreCod,  " +
"           PreNom,  " +
"           FechaRevis,  " +
"           FechaSolic,  " +
"           ORDEN,  " +
"           Estado,  " +
"           FechaConfirm,  " +
"           tipoformu,  " +
"           numerformu,  " +
"           ordtipo,  " +
"           ExamCit,  " +
"           forcod,  " +
"           forvrs,  " +
"           ulteve  " +
"           ,CODESTADO,  " +
"           OBSERVACION,  " +
"           ALERTA,  " +
"           VALORALERTA,  " +
"           CODTIEMPOALERTA  " +
"           ,MTVCORRELATIVO  " +
"           ,PAC_PAC_NUMERO  " +
"            ,CamaServicio " +
"               , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
"         FROM  " +
"           (SELECT OrdNombre,  " +
"             OrdNumero,  " +
"             PreCod,  " +
"             PreNom,  " +
"             FechaRevis,  " +
"             FechaSolic,  " +
"             ORDEN,  " +
"             Estado,  " +
"             FechaConfirm,  " +
"             tipoformu,  " +
"             numerformu,  " +
"             ordtipo,  " +
"             ExamCit,  " +
"             forcod,  " +
"             forvrs,  " +
"             ulteve  " +
"              ,CODESTADO,  " +
"               OBSERVACION,  " +
"               ALERTA,  " +
"               VALORALERTA,  " +
"               CODTIEMPOALERTA  " +
"               ,MTVCORRELATIVO  " +
"               ,PAC_PAC_NUMERO " +
"               ,CamaServicio " +
"               , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
"           FROM  " +
"             (  " +
"             SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ordnombre ,  " +
"               ord.ordnumero ordnumero ,  " +
"               exa.cex_exs_codigprest precod ,  " +
"               glbgetnompre (exa.cex_exs_codigprest) prenom ,  " +
"               DECODE (inf.cex_inf_fecharevis, NULL, exa.cex_exs_fechamuest, inf.cex_inf_fecharevis) fecharevis ,  " +
"               ord.ORDFECHA fechasolic ,  " +
"               estord.ORDEN ,  " +
"               est.EXMESTNOM Estado ,  " +
"               DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm ,  " +
"               exa.ate_pre_tipoformu tipoformu ,  " +
"               exa.ate_pre_numerformu numerformu ,  " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo ,  " +
"               patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit ,  " +
"               inf.lab_for_codigo forcod ,  " +
"               inf.lab_for_version forvrs ,  " +
"               exa.cex_exs_estado ulteve  " +
"               ,GESAUT.CODESTADO,  " +
"               GESAUT.OBSERVACION,  " +
"               GESAUT.ALERTA,  " +
"               GESAUT.VALORALERTA,  " +
"               GESAUT.CODTIEMPOALERTA  " +
"               ,ORD.MTVCORRELATIVO  " +
"               ,ORD.PAC_PAC_NUMERO  " +
"               ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo  cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM  " +
"               (SELECT ordtipo ,  " +
"                 ordnumero ,  " +
"                 pac_pac_numero ,  " +
"                 mtvcorrelativo ,  " +
"                 tabencuentro ,  " +
"                 ordfecha ,  " +
"                 ordestado  " +
"               FROM tabordenesserv  " +
"              " +
"               ) ord ,  " +
"               Gt_Paraclinicos exa ,  " +
"               cex_informes inf ,  " +
"               tabexmest est ,  " +
"               tabordsrvtpo tip ,  " +
"               HCP_ESTORDEN estord,  " +
"               CENAUT_GESTIONAUT GESAUT " +
"               ,ATC_ESTADIA EST " +
"               , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"             WHERE ord.ordtipo            IN ('L', 'I', 'A')   " +
"             AND exa.ordtipo               = ord.ordtipo  " +
"             AND exa.ordnumero             = ord.ordnumero  " +
"             AND exa.pac_pac_numero        = ord.pac_pac_numero  " +
"             AND inf.ate_pre_tipoformu(+)  = exa.ate_pre_tipoformu  " +
"             AND inf.ate_pre_numerformu(+) = exa.ate_pre_numerformu  " +
"             AND inf.cex_exs_codigprest(+) = exa.cex_exs_codigprest  " +
"             AND est.exmestcod             = exa.cex_exs_estado  " +
"             AND tip.ordtipo(+)            = exa.ordtipo  " +
"             AND estord.EXMESTCOD          = est.exmestcod  " +
"                AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+)  " +
"              AND EXA.ordnumero=GESAUT.ORDNUMERO(+)  " +
"              AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+)  " +
"              AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO  " +
"              and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND EXA.cex_exs_codigprest=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        ' and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
"             UNION ALL  " +
"             SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ,  " +
"               NVL(ord.ordnumero,exa.ate_pre_numerformu) ,  " +
"               exa.cex_exs_codigprest ,  " +
"               glbgetnompre(exa.cex_exs_codigprest) ,  " +
"               DECODE (inf.hcp_inf_fechadigit,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),inf.hcp_inf_fechadigit) ,  " +
"               ord.ORDFECHA ,  " +
"               estord.ORDEN ,  " +
"               est.EXMESTNOM Estado ,  " +
"               DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm ,  " +
"               exa.ate_pre_tipoformu tipoformu ,  " +
"               exa.ate_pre_numerformu numerformu ,  " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo ,  " +
"               patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit ,  " +
"               inf.hcp_inf_codigforma ,  " +
"               inf.hcp_inf_version ,  " +
"               est.exmestcod  " +
"                ,GESAUT.CODESTADO,  " +
"               GESAUT.OBSERVACION,  " +
"               GESAUT.ALERTA,  " +
"               GESAUT.VALORALERTA,  " +
"               GESAUT.CODTIEMPOALERTA  " +
"                ,ORD.MTVCORRELATIVO  " +
"               ,ORD.PAC_PAC_NUMERO  " +
"                ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO , trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM  " +
"               (SELECT ordtipo ,  " +
"                 ordnumero ,  " +
"                 pac_pac_numero ,  " +
"                 mtvcorrelativo ,  " +
"                 tabencuentro ,  " +
"                 ordfecha ,  " +
"                 ordestado  " +
"               FROM tabordenesserv  " +
"              " +
"               ) ord ,  " +
"               Gt_Paraclinicos exa ,  " +
"               hcp_informes inf ,  " +
"               tabexmest est ,  " +
"               tabordsrvtpo tip ,  " +
"               HCP_ESTORDEN estord,  " +
"               CENAUT_GESTIONAUT GESAUT  " +
"                ,ATC_ESTADIA EST  " +
"                 , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig , CON_CONVENIO CON" +
"             WHERE exa.ordtipo             = 'D'  " +
"             AND ord.ordtipo NOT          IN ('L', 'I', 'A', 'P')  " +
"             AND exa.ordnumero             = ord.ordnumero  " +
"             AND exa.pac_pac_numero        = ord.pac_pac_numero  " +
"             AND inf.hcp_inf_numerpacie(+) = exa.pac_pac_numero  " +
"             AND inf.hcp_inf_tipoformu(+)  = exa.ate_pre_tipoformu  " +
"             AND inf.hcp_inf_numerformu(+) = exa.ate_pre_numerformu  " +
"             AND inf.pre_pre_codigo(+)     = exa.cex_exs_codigprest  " +
"             AND est.exmestcod             = exa.cex_exs_estado  " +
"             AND estord.EXMESTCOD          = est.exmestcod  " +
"             AND tip.ordtipo(+)            = ord.ordtipo  " +
"            AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+)  " +
"              AND EXA.ordnumero=GESAUT.ORDNUMERO(+)  " +
"              AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+)  " +
"               AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO  " +
"               and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND EXA.cex_exs_codigprest=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+)  " +
"             UNION ALL  " +
"             SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ,  " +
"               det.ordnumero ,  " +
"               det.pre_pre_codigo ,  " +
"               glbgetnompre(det.pre_pre_codigo) ,  " +
"               DECODE(srv.ordfecha,NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'),srv.ordfecha) ,  " +
"               srv.ORDFECHA ,  " +
"               0 ORDEN ,  " +
"               'Examen Solicitado' ,  " +
"               TO_DATE('1900/01/01', 'YYYY/MM/DD') ,  " +
"               '' tipoformu ,  " +
"               '' numerformu ,  " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo ,  " +
"               0 ,  " +
"               ' ' ,  " +
"               ' ' ,  " +
"               '00'  " +
"                ,GESAUT.CODESTADO,  " +
"               GESAUT.OBSERVACION,  " +
"               GESAUT.ALERTA,  " +
"               GESAUT.VALORALERTA,  " +
"               GESAUT.CODTIEMPOALERTA  " +
"                ,srv.MTVCORRELATIVO  " +
"               ,srv.PAC_PAC_NUMERO  " +
"                ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO , trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM  " +
"               (SELECT ordtipo ,  " +
"                 ordnumero ,  " +
"                 pac_pac_numero ,  " +
"                 mtvcorrelativo ,  " +
"                 tabencuentro ,  " +
"                 ordfecha ,  " +
"                 ordestado  " +
"               FROM tabordenesserv  " +
"              " +
"               ) srv ,  " +
"               taborddetalle det ,  " +
"               tabordsrvtpo tip,  " +
"               CENAUT_GESTIONAUT GESAUT " +
"               ,ATC_ESTADIA EST " +
"                , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"             WHERE srv.ordtipo NOT IN ('P','B')  " +
"             AND det.mtvcorrelativo = srv.mtvcorrelativo  " +
"             AND det.ordtipo        = srv.ordtipo  " +
"             AND det.ordnumero      = srv.ordnumero  " +
"             AND det.tabencuentro   = srv.tabencuentro  " +
"             AND det.pac_pac_numero = srv.pac_pac_numero  " +
"             AND tip.ordtipo(+)     = det.ordtipo  " +
"            AND DET.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+)  " +
"              AND DET.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+)  " +
"              AND DET.ordnumero=GESAUT.ORDNUMERO(+)  " +
"              AND DET.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+)  " +
"               AND srv.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND srv.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"               and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND DET.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+)  " +
"             AND (SELECT COUNT (1)  " +
"               FROM rpa_forlab lab,  " +
"                 cex_examsolic ex  " +
"               WHERE lab.ordtipo          = DECODE(srv.ordtipo, 'A', 'A', 'I', 'I', 'L', 'L', 'D')  " +
"               AND lab.ordnumero          = srv.ordnumero  " +
"               AND lab.pac_pac_numero     = srv.pac_pac_numero  " +
"               AND lab.ATE_PRE_TIPOFORMU  = ex.ate_pre_tipoformu  " +
"               AND lab.ATE_PRE_NUMERFORMU = ex.ate_pre_numerformu  " +
"               AND lab.PAC_PAC_NUMERO     = ex.PAC_PAC_NUMERO  " +
"               AND ex.CEX_EXS_CODIGPREST  = det.pre_pre_codigo ) >= 0  " +
"             UNION ALL  " +
"             SELECT DISTINCT tip.ordnombre ,  " +
"               f.ordnumero ,  " +
"               f.pre_pre_codigo ,  " +
"               glbgetnompre(f.pre_pre_codigo) ,  " +
"               DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) ,  " +
"               a.ordfecha ,  " +
"               estord.ORDEN,  " +
"               toce.ORDCIRESTNOM,  " +
"               a.FECINGQRF ,  " +
"               '' tipoformu ,  " +
"               '' numerformu ,  " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo ,  " +
"               0 ,  " +
"               ' ' ,  " +
"               ' ' ,  " +
"               '00'  " +
"                ,GESAUT.CODESTADO,  " +
"               GESAUT.OBSERVACION,  " +
"               GESAUT.ALERTA,  " +
"               GESAUT.VALORALERTA,  " +
"               GESAUT.CODTIEMPOALERTA  " +
"                ,A.MTVCORRELATIVO  " +
"               ,A.PAC_PAC_NUMERO  " +
"                ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM  " +
"               (SELECT tabordenesserv.ordtipo ,  " +
"                 tabordenesserv.ordnumero ,  " +
"                 tabordenesserv.pac_pac_numero ,  " +
"                 tabordenesserv.mtvcorrelativo ,  " +
"                 tabordenesserv.tabencuentro ,  " +
"                 ordfecha ,  " +
"                 FECINGQRF,  " +
"                 ordestado ,  " +
"                 '00'  " +
"               FROM tabordenesserv ,  " +
"                 TabPrgQrf  " +
"               WHERE " +
"               TabOrdenesServ.ORDTIPO          = TabPrgQrf.ORDTIPO  " +
"               AND TabOrdenesServ.ORDNUMERO        = TabPrgQrf.ORDNUMERO  " +
"               AND TabOrdenesServ.PAC_PAC_NUMERO   = TabPrgQrf.PAC_PAC_NUMERO  " +
"               ) a ,  " +
"               taborddetalle f ,  " +
"               taborddetcir todc ,  " +
"               tabordcirest toce ,  " +
"               HCP_ESTORDEN estord,  " +
"               tabordsrvtpo tip, " +
"               CENAUT_GESTIONAUT GESAUT " +
"               ,ATC_ESTADIA EST " +
"                , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"             WHERE a.ordtipo       = 'P' " +
"             AND a.ordtipo         = tip.ordtipo " +
"             AND a.pac_pac_numero  = f.pac_pac_numero " +
"             AND a.ordtipo         = f.ordtipo " +
"             AND a.ordnumero       = f.ordnumero " +
"             AND a.ordtipo         = todc.ordtipo " +
"             AND a.ordnumero       = todc.ordnumero " +
"             AND a.pac_pac_numero  = todc.pac_pac_numero " +
"             AND todc.ordcirestcod = toce.ordcirestcod " +
"             AND estord.EXMESTCOD  = toce.ordcirestcod " +
"               AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"              AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"              AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
"              AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"              AND a.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND a.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"               and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND f.PRE_PRE_CODIGO=PRES.CODPRESTACION   AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
"             UNION " +
"             SELECT DISTINCT tip.ordnombre , " +
"               f.ordnumero , " +
"               f.pre_pre_codigo , " +
"               admsalud.glbgetnompre(f.pre_pre_codigo) , " +
"               DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) , " +
"               a.ordfecha , " +
"               estord.ORDEN , " +
"               toce.ORDCIRESTNOM, " +
"               NVL(a.FECINGQRF,TO_DATE('1900/01/01', 'YYYY/MM/DD')) , " +
"               '' tipoformu , " +
"               '' numerformu , " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"               0 , " +
"               ' ' , " +
"               ' ' , " +
"               '00' " +
"                ,GESAUT.CODESTADO, " +
"               GESAUT.OBSERVACION, " +
"               GESAUT.ALERTA, " +
"               GESAUT.VALORALERTA, " +
"               GESAUT.CODTIEMPOALERTA " +
"                ,A.MTVCORRELATIVO " +
"               ,A.PAC_PAC_NUMERO " +
"                ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM " +
"               (SELECT tabordenesserv.ordtipo, " +
"                 tabordenesserv.ordnumero, " +
"                 tabordenesserv.pac_pac_numero, " +
"                 tabordenesserv.mtvcorrelativo, " +
"                 tabordenesserv.tabencuentro, " +
"                 ordfecha, " +
"                 FECINGQRF, " +
"                 ordestado, " +
"                 '00' " +
"               FROM tabordenesserv, " +
"                 TabOrdDetCir, " +
"                 TabPrgQrf " +
"               WHERE TabPrgQrf.QRFEST(+)         = 'C' " +
"               AND TabOrdenesServ.ORDTIPO        = TabPrgQrf.ORDTIPO(+) " +
"               AND TabOrdenesServ.ORDNUMERO      = TabPrgQrf.ORDNUMERO(+) " +
"               AND TabOrdenesServ.PAC_PAC_NUMERO = TabPrgQrf.PAC_PAC_NUMERO(+) " +
"               AND TabOrdDetCir.PAC_PAC_NUMERO   = TabOrdenesServ.PAC_PAC_NUMERO(+) " +
"               AND TabOrdDetCir.MTVCORRELATIVO   = TabOrdenesServ.MTVCORRELATIVO(+) " +
"               AND TabOrdDetCir.TABENCUENTRO     = TabOrdenesServ.TABENCUENTRO(+) " +
"               AND TabOrdDetCir.ORDTIPO          = TabOrdenesServ.ORDTIPO(+) " +
"               AND TabOrdDetCir.ORDNUMERO        = TabOrdenesServ.ORDNUMERO(+) " +
"               ) a , " +
"               taborddetalle f , " +
"               taborddetcir todc , " +
"               tabordcirest toce , " +
"               HCP_ESTORDEN estord, " +
"               tabordsrvtpo tip, " +
"               CENAUT_GESTIONAUT GESAUT " +
"               ,atc_estadia est " +
"                , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"             WHERE a.ordtipo       = 'P' " +
"             AND a.ordtipo         = tip.ordtipo " +
"             AND a.pac_pac_numero  = f.pac_pac_numero " +
"             AND a.ordtipo         = f.ordtipo " +
"             AND a.ordnumero       = f.ordnumero " +
"             AND a.ordtipo         = todc.ordtipo " +
"             AND a.ordnumero       = todc.ordnumero " +
"             AND a.pac_pac_numero  = todc.pac_pac_numero(+) " +
"             AND todc.ordcirestcod = toce.ordcirestcod " +
"             AND estord.EXMESTCOD  = toce.ordcirestcod " +
"              AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"              AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"             AND a.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND a.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H'  AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"               and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND f.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  and est.ser_obj_codigo is not null  and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
"             ORDER BY 25 ASC, " +
"               26 ASC " +
"             ) " +
"           ) " +
"         ) b " +
"     )   " +
" UNION  " +
"            select  " +
"    ADDPRES.ORDNOMBRE, ADDPRES.ORDNUMERO, ADDPRES.CODPRESTACION, PRES.PRE_PRE_DESCRIPCIO, SYSDATE, ADDPRES.FECHASOLIC, 0, '', SYSDATE,'', '', ADDPRES.ORDTIPO, 0,'','','',  " +
"    NVL(GESAUT.CODESTADO, 1), GESAUT.OBSERVACION, GESAUT.ALERTA, NVL(GESAUT.VALORALERTA,0) VALORALERTA, GESAUT.CODTIEMPOALERTA, ADDPRES.MTVCORRELATIVO, ADDPRES.ID, ADDPRES.PAC_PAC_NUMERO " +
"      ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio                ,est.ser_obj_codigo cama,  asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"     from cenaut_agregarprestacion addpres, PRE_PRESTACION PRES, CENAUT_GESTIONAUT GESAUT, CENAUT_PRESTACION CENPRES,  ATC_ESTADIA EST , ser_servicios ser, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"     WHERE  " +
"     ADDPRES.CODPRESTACION=PRES.PRE_PRE_CODIGO " +
"     AND ADDPRES.CODPRESTACION=GESAUT.CODPRESTACION(+) " +
"     AND ADDPRES.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"     AND ADDPRES.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"     AND ADDPRES.ORDNUMERO=GESAUT.ORDNUMERO(+) " +
"     AND ADDPRES.CODPRESTACION=CENPRES.CODPRESTACION " +
"     AND ADDPRES.MTVCORRELATIVO = EST.MTVCORRELATIVO " +
"     AND ADDPRES.PAC_PAC_NUMERO =EST.PAC_PAC_NUMERO " +
"     AND EST.ATCESTADO='H' " +
"     AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02'      and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO " +
"     AND CENPRES.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"     UNION " +
"     select  " +
"    ADDPRES.ORDNOMBRE, ADDPRES.ORDNUMERO, ADDPRES.CODPRESTACION, PRES.HMCTPONOM, SYSDATE, ADDPRES.FECHASOLIC, 0, '', SYSDATE,'', '', ADDPRES.ORDTIPO, 0,'','','',  " +
"    NVL(GESAUT.CODESTADO, 1), GESAUT.OBSERVACION, GESAUT.ALERTA, GESAUT.VALORALERTA, GESAUT.CODTIEMPOALERTA, ADDPRES.MTVCORRELATIVO, ADDPRES.ID, ADDPRES.PAC_PAC_NUMERO " +
"      ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio                ,est.ser_obj_codigo cama, NVL(GESAUT.RESPONSABLE, asig.responsable) RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"     from cenaut_agregarprestacion addpres, TABHMCTPO PRES, CENAUT_GESTIONAUT GESAUT, CENAUT_PRESTACION CENPRES,  ATC_ESTADIA EST , ser_servicios ser, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"     WHERE  " +
"     ADDPRES.CODPRESTACION=PRES.HMCTPOCOD " +
"     AND ADDPRES.CODPRESTACION=GESAUT.CODPRESTACION(+) " +
"     AND ADDPRES.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"     AND ADDPRES.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"     AND ADDPRES.ORDNUMERO=GESAUT.ORDNUMERO(+) " +
"     AND ADDPRES.CODPRESTACION=CENPRES.CODPRESTACION " +
"     AND ADDPRES.MTVCORRELATIVO = EST.MTVCORRELATIVO " +
"     AND ADDPRES.PAC_PAC_NUMERO =EST.PAC_PAC_NUMERO " +
"     AND EST.ATCESTADO='H' " +
"     AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     '     and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO " +
"     AND CENPRES.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " + 
"            UNION " +
"  SELECT 'ENFERMERIA' ordnombre ,  " +
"               'ENF' || ord.IDCTLESP    ,   " +
"               ORD.PRE_PRE_CODIGO precod ,   " +
"                PRM.PRE_PRE_DESCRIPCIO prenom ,   " +
"               SYSDATE,   " +
"               ord.FEPRC fechasolic ,   " +
"               0 ,   " +
"               '' Estado ,   " +
"               SYSDATE fechaconfirm ,   " +
"              '' tipoformu ,   " +
"               '' ,   " +
"               '' ,   " +
"               0 ,   " +
"               '' , " +
"              '' ,   " +
"                '' " +
"               ,NVL(GESAUT.CODESTADO, 1),   " +
"               GESAUT.OBSERVACION,   " +
"               GESAUT.ALERTA,   " +
"               NVL(GESAUT.VALORALERTA,0),   " +
"               GESAUT.CODTIEMPOALERTA   " +
"              ,ORD.MTVCORRELATIVO   " +
"                 ,CAST(GlbGetPacCar( ORD.PAC_PAC_NUMERO) AS VARCHAR(13))  " +
"               ,ORD.PAC_PAC_NUMERO   " +
"               ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio  " +
"               ,est.ser_obj_codigo  cama,  asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo  " +
"             FROM  HCP_CTLESPECIALES " +
"            ord ,  HCP_PARAMCTLESP PRM, " +
"               CENAUT_GESTIONAUT GESAUT  " +
"               ,ATC_ESTADIA EST  " +
"               , ser_servicios ser   " +
"               , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON  " +
"             WHERE ORD.PRE_PRE_CODIGO= prm.PRE_PRE_CODIGO " +
"             AND PRM.PRE_PRE_VIGENCIA='S' " +
"           AND ORD.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"           AND ORD.PAC_PAC_NUMERO = GESAUT.PAC_PAC_NUMERO(+) " +
"           AND ORD.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"           and 'ENF' || IDCTLESP = GESAUT.ORDNUMERO(+) " +
"              AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO (+) " +
"              AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO  " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO  " +
"              and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND ORD.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        ' and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
"              AND ORD.PRE_PRE_CODIGO IN ('GESATE  ', 'SALCAS  ', 'GESAMB  ')  " +
"               UNION  " +
" SELECT DISTINCT  '' ordnombre ,  " +
"             '' ,  " +
"              '' precod ,   " +
"               '' ,  " +
"              SYSDATE,    " +
"              TO_DATE('1900/01/01', 'YYYY/MM/DD') ,   " +
"              0 ,   " +
"              '' Estado ,   " +
"              SYSDATE fechaconfirm ,   " +
"            '' tipoformu ,   " +
"              '' ,   " +
"              '' ,    " +
"              0 ,    " +
"              '' ,  " +
"             '' ,   " +
"               ''  " +
"              ,1,   " +
"              '',    " +
"             0,    " +
"              0,   " +
"             0  " +
"             ,EST.MTVCORRELATIVO   " +
"                ,CAST(GlbGetPacCar( EST.PAC_PAC_NUMERO) AS VARCHAR(13))  " +
"              ,EST.PAC_PAC_NUMERO   " +
"              ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio  " +
"              ,est.ser_obj_codigo  cama,  asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"            FROM  ATC_ESTADIA EST " +
"              , ser_servicios ser   " +
"              ,  ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON   " +
"            WHERE " +
"              EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO  " +
"             and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO  " +
"              AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  " +
"             and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and est.ser_obj_codigo=asig.cama(+) " +
"            ORDER BY 25 ASC, 26 ASC";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");
            
        }

        public DataTable ConsultarAsigCamaServiciosResVTres()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "  SELECT OrdNombre,  " +
"     OrdNumero,  " +
"     PreCod,  " +
"     PreNom,  " +
"     FechaRevis,  " +
"     FechaSolic,  " +
"     ORDEN,  " +
"     Estado,  " +
"     FechaConfirm,  " +
"     tipoformu,  " +
"     numerformu,  " +
"     ordtipo,  " +
"     ExamCit,  " +
"     forcod,  " +
"     forvrs,  " +
"     ulteve   " +
"     ,NVL(CODESTADO,1) CODESTADO,  " +
"       OBSERVACION,  " +
"       ALERTA,  " +
"       NVL(VALORALERTA,0) VALORALERTA,  " +
"       CODTIEMPOALERTA  " +
"     ,MTVCORRELATIVO  " +
"      , FunGlbGetRutPac(PAC_PAC_NUMERO) ID, PAC_PAC_NUMERO  " +
"       ,CamaServicio " +
"               , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
"     FROM  " +
"     (SELECT b.*,  " +
"         rownum registro  " +
"       FROM  " +
"         (SELECT OrdNombre,  " +
"           OrdNumero,  " +
"           PreCod,  " +
"           PreNom,  " +
"           FechaRevis,  " +
"           FechaSolic,  " +
"           ORDEN,  " +
"           Estado,  " +
"           FechaConfirm,  " +
"           tipoformu,  " +
"           numerformu,  " +
"           ordtipo,  " +
"           ExamCit,  " +
"           forcod,  " +
"           forvrs,  " +
"           ulteve  " +
"           ,CODESTADO,  " +
"           OBSERVACION,  " +
"           ALERTA,  " +
"           VALORALERTA,  " +
"           CODTIEMPOALERTA  " +
"           ,MTVCORRELATIVO  " +
"           ,PAC_PAC_NUMERO  " +
"            ,CamaServicio " +
"               , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
"         FROM  " +
"           (SELECT OrdNombre,  " +
"             OrdNumero,  " +
"             PreCod,  " +
"             PreNom,  " +
"             FechaRevis,  " +
"             FechaSolic,  " +
"             ORDEN,  " +
"             Estado,  " +
"             FechaConfirm,  " +
"             tipoformu,  " +
"             numerformu,  " +
"             ordtipo,  " +
"             ExamCit,  " +
"             forcod,  " +
"             forvrs,  " +
"             ulteve  " +
"              ,CODESTADO,  " +
"               OBSERVACION,  " +
"               ALERTA,  " +
"               VALORALERTA,  " +
"               CODTIEMPOALERTA  " +
"               ,MTVCORRELATIVO  " +
"               ,PAC_PAC_NUMERO " +
"               ,CamaServicio " +
"               , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
"           FROM  " +
"             (  " +
"             SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ordnombre ,  " +
"               ord.ordnumero ordnumero ,  " +
"               exa.cex_exs_codigprest precod ,  " +
"               glbgetnompre (exa.cex_exs_codigprest) prenom ,  " +
"               DECODE (inf.cex_inf_fecharevis, NULL, exa.cex_exs_fechamuest, inf.cex_inf_fecharevis) fecharevis ,  " +
"               ord.ORDFECHA fechasolic ,  " +
"               estord.ORDEN ,  " +
"               est.EXMESTNOM Estado ,  " +
"               DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm ,  " +
"               exa.ate_pre_tipoformu tipoformu ,  " +
"               exa.ate_pre_numerformu numerformu ,  " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo ,  " +
"               patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit ,  " +
"               inf.lab_for_codigo forcod ,  " +
"               inf.lab_for_version forvrs ,  " +
"               exa.cex_exs_estado ulteve  " +
"               ,GESAUT.CODESTADO,  " +
"               GESAUT.OBSERVACION,  " +
"               GESAUT.ALERTA,  " +
"               GESAUT.VALORALERTA,  " +
"               GESAUT.CODTIEMPOALERTA  " +
"               ,ORD.MTVCORRELATIVO  " +
"               ,ORD.PAC_PAC_NUMERO  " +
"               ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo  cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM  " +
"               (SELECT ordtipo ,  " +
"                 ordnumero ,  " +
"                 pac_pac_numero ,  " +
"                 mtvcorrelativo ,  " +
"                 tabencuentro ,  " +
"                 ordfecha ,  " +
"                 ordestado  " +
"               FROM tabordenesserv  " +
"              " +
"               ) ord ,  " +
"               Gt_Paraclinicos exa ,  " +
"               cex_informes inf ,  " +
"               tabexmest est ,  " +
"               tabordsrvtpo tip ,  " +
"               HCP_ESTORDEN estord,  " +
"               CENAUT_GESTIONAUT GESAUT " +
"               ,ATC_ESTADIA EST " +
"               , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"             WHERE ord.ordtipo            IN ('L', 'I', 'A')   " +
"             AND exa.ordtipo               = ord.ordtipo  " +
"             AND exa.ordnumero             = ord.ordnumero  " +
"             AND exa.pac_pac_numero        = ord.pac_pac_numero  " +
"             AND inf.ate_pre_tipoformu(+)  = exa.ate_pre_tipoformu  " +
"             AND inf.ate_pre_numerformu(+) = exa.ate_pre_numerformu  " +
"             AND inf.cex_exs_codigprest(+) = exa.cex_exs_codigprest  " +
"             AND est.exmestcod             = exa.cex_exs_estado  " +
"             AND tip.ordtipo(+)            = exa.ordtipo  " +
"             AND estord.EXMESTCOD          = est.exmestcod  " +
"                AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+)  " +
"              AND EXA.ordnumero=GESAUT.ORDNUMERO(+)  " +
"              AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+)  " +
"              AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO  " +
"              and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND EXA.cex_exs_codigprest=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        ' and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
"             UNION ALL  " +
"             SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ,  " +
"               NVL(ord.ordnumero,exa.ate_pre_numerformu) ,  " +
"               exa.cex_exs_codigprest ,  " +
"               glbgetnompre(exa.cex_exs_codigprest) ,  " +
"               DECODE (inf.hcp_inf_fechadigit,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),inf.hcp_inf_fechadigit) ,  " +
"               ord.ORDFECHA ,  " +
"               estord.ORDEN ,  " +
"               est.EXMESTNOM Estado ,  " +
"               DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm ,  " +
"               exa.ate_pre_tipoformu tipoformu ,  " +
"               exa.ate_pre_numerformu numerformu ,  " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo ,  " +
"               patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit ,  " +
"               inf.hcp_inf_codigforma ,  " +
"               inf.hcp_inf_version ,  " +
"               est.exmestcod  " +
"                ,GESAUT.CODESTADO,  " +
"               GESAUT.OBSERVACION,  " +
"               GESAUT.ALERTA,  " +
"               GESAUT.VALORALERTA,  " +
"               GESAUT.CODTIEMPOALERTA  " +
"                ,ORD.MTVCORRELATIVO  " +
"               ,ORD.PAC_PAC_NUMERO  " +
"                ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO , trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM  " +
"               (SELECT ordtipo ,  " +
"                 ordnumero ,  " +
"                 pac_pac_numero ,  " +
"                 mtvcorrelativo ,  " +
"                 tabencuentro ,  " +
"                 ordfecha ,  " +
"                 ordestado  " +
"               FROM tabordenesserv  " +
"              " +
"               ) ord ,  " +
"               Gt_Paraclinicos exa ,  " +
"               hcp_informes inf ,  " +
"               tabexmest est ,  " +
"               tabordsrvtpo tip ,  " +
"               HCP_ESTORDEN estord,  " +
"               CENAUT_GESTIONAUT GESAUT  " +
"                ,ATC_ESTADIA EST  " +
"                 , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig , CON_CONVENIO CON" +
"             WHERE exa.ordtipo             = 'D'  " +
"             AND ord.ordtipo NOT          IN ('L', 'I', 'A', 'P')  " +
"             AND exa.ordnumero             = ord.ordnumero  " +
"             AND exa.pac_pac_numero        = ord.pac_pac_numero  " +
"             AND inf.hcp_inf_numerpacie(+) = exa.pac_pac_numero  " +
"             AND inf.hcp_inf_tipoformu(+)  = exa.ate_pre_tipoformu  " +
"             AND inf.hcp_inf_numerformu(+) = exa.ate_pre_numerformu  " +
"             AND inf.pre_pre_codigo(+)     = exa.cex_exs_codigprest  " +
"             AND est.exmestcod             = exa.cex_exs_estado  " +
"             AND estord.EXMESTCOD          = est.exmestcod  " +
"             AND tip.ordtipo(+)            = ord.ordtipo  " +
"            AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+)  " +
"              AND EXA.ordnumero=GESAUT.ORDNUMERO(+)  " +
"              AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+)  " +
"               AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO  " +
"               and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND EXA.cex_exs_codigprest=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+)  " +
"             UNION ALL  " +
"             SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ,  " +
"               det.ordnumero ,  " +
"               det.pre_pre_codigo ,  " +
"               glbgetnompre(det.pre_pre_codigo) ,  " +
"               DECODE(srv.ordfecha,NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'),srv.ordfecha) ,  " +
"               srv.ORDFECHA ,  " +
"               0 ORDEN ,  " +
"               'Examen Solicitado' ,  " +
"               TO_DATE('1900/01/01', 'YYYY/MM/DD') ,  " +
"               '' tipoformu ,  " +
"               '' numerformu ,  " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo ,  " +
"               0 ,  " +
"               ' ' ,  " +
"               ' ' ,  " +
"               '00'  " +
"                ,GESAUT.CODESTADO,  " +
"               GESAUT.OBSERVACION,  " +
"               GESAUT.ALERTA,  " +
"               GESAUT.VALORALERTA,  " +
"               GESAUT.CODTIEMPOALERTA  " +
"                ,srv.MTVCORRELATIVO  " +
"               ,srv.PAC_PAC_NUMERO  " +
"                ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO , trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM  " +
"               (SELECT ordtipo ,  " +
"                 ordnumero ,  " +
"                 pac_pac_numero ,  " +
"                 mtvcorrelativo ,  " +
"                 tabencuentro ,  " +
"                 ordfecha ,  " +
"                 ordestado  " +
"               FROM tabordenesserv  " +
"              " +
"               ) srv ,  " +
"               taborddetalle det ,  " +
"               tabordsrvtpo tip,  " +
"               CENAUT_GESTIONAUT GESAUT " +
"               ,ATC_ESTADIA EST " +
"                , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"             WHERE srv.ordtipo NOT IN ('P','B')  " +
"             AND det.mtvcorrelativo = srv.mtvcorrelativo  " +
"             AND det.ordtipo        = srv.ordtipo  " +
"             AND det.ordnumero      = srv.ordnumero  " +
"             AND det.tabencuentro   = srv.tabencuentro  " +
"             AND det.pac_pac_numero = srv.pac_pac_numero  " +
"             AND tip.ordtipo(+)     = det.ordtipo  " +
"            AND DET.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+)  " +
"              AND DET.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+)  " +
"              AND DET.ordnumero=GESAUT.ORDNUMERO(+)  " +
"              AND DET.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+)  " +
"               AND srv.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND srv.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"               and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND DET.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+)  " +
"             AND (SELECT COUNT (1)  " +
"               FROM rpa_forlab lab,  " +
"                 cex_examsolic ex  " +
"               WHERE lab.ordtipo          = DECODE(srv.ordtipo, 'A', 'A', 'I', 'I', 'L', 'L', 'D')  " +
"               AND lab.ordnumero          = srv.ordnumero  " +
"               AND lab.pac_pac_numero     = srv.pac_pac_numero  " +
"               AND lab.ATE_PRE_TIPOFORMU  = ex.ate_pre_tipoformu  " +
"               AND lab.ATE_PRE_NUMERFORMU = ex.ate_pre_numerformu  " +
"               AND lab.PAC_PAC_NUMERO     = ex.PAC_PAC_NUMERO  " +
"               AND ex.CEX_EXS_CODIGPREST  = det.pre_pre_codigo ) >= 0  " +
"             UNION ALL  " +
"             SELECT DISTINCT tip.ordnombre ,  " +
"               f.ordnumero ,  " +
"               f.pre_pre_codigo ,  " +
"               glbgetnompre(f.pre_pre_codigo) ,  " +
"               DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) ,  " +
"               a.ordfecha ,  " +
"               estord.ORDEN,  " +
"               toce.ORDCIRESTNOM,  " +
"               a.FECINGQRF ,  " +
"               '' tipoformu ,  " +
"               '' numerformu ,  " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo ,  " +
"               0 ,  " +
"               ' ' ,  " +
"               ' ' ,  " +
"               '00'  " +
"                ,GESAUT.CODESTADO,  " +
"               GESAUT.OBSERVACION,  " +
"               GESAUT.ALERTA,  " +
"               GESAUT.VALORALERTA,  " +
"               GESAUT.CODTIEMPOALERTA  " +
"                ,A.MTVCORRELATIVO  " +
"               ,A.PAC_PAC_NUMERO  " +
"                ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM  " +
"               (SELECT tabordenesserv.ordtipo ,  " +
"                 tabordenesserv.ordnumero ,  " +
"                 tabordenesserv.pac_pac_numero ,  " +
"                 tabordenesserv.mtvcorrelativo ,  " +
"                 tabordenesserv.tabencuentro ,  " +
"                 ordfecha ,  " +
"                 FECINGQRF,  " +
"                 ordestado ,  " +
"                 '00'  " +
"               FROM tabordenesserv ,  " +
"                 TabPrgQrf  " +
"               WHERE " +
"               TabOrdenesServ.ORDTIPO          = TabPrgQrf.ORDTIPO  " +
"               AND TabOrdenesServ.ORDNUMERO        = TabPrgQrf.ORDNUMERO  " +
"               AND TabOrdenesServ.PAC_PAC_NUMERO   = TabPrgQrf.PAC_PAC_NUMERO  " +
"               ) a ,  " +
"               taborddetalle f ,  " +
"               taborddetcir todc ,  " +
"               tabordcirest toce ,  " +
"               HCP_ESTORDEN estord,  " +
"               tabordsrvtpo tip, " +
"               CENAUT_GESTIONAUT GESAUT " +
"               ,ATC_ESTADIA EST " +
"                , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"             WHERE a.ordtipo       = 'P' " +
"             AND a.ordtipo         = tip.ordtipo " +
"             AND a.pac_pac_numero  = f.pac_pac_numero " +
"             AND a.ordtipo         = f.ordtipo " +
"             AND a.ordnumero       = f.ordnumero " +
"             AND a.ordtipo         = todc.ordtipo " +
"             AND a.ordnumero       = todc.ordnumero " +
"             AND a.pac_pac_numero  = todc.pac_pac_numero " +
"             AND todc.ordcirestcod = toce.ordcirestcod " +
"             AND estord.EXMESTCOD  = toce.ordcirestcod " +
"               AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"              AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"              AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
"              AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"              AND a.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND a.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"               and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND f.PRE_PRE_CODIGO=PRES.CODPRESTACION   AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
"             UNION " +
"             SELECT DISTINCT tip.ordnombre , " +
"               f.ordnumero , " +
"               f.pre_pre_codigo , " +
"               admsalud.glbgetnompre(f.pre_pre_codigo) , " +
"               DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) , " +
"               a.ordfecha , " +
"               estord.ORDEN , " +
"               toce.ORDCIRESTNOM, " +
"               NVL(a.FECINGQRF,TO_DATE('1900/01/01', 'YYYY/MM/DD')) , " +
"               '' tipoformu , " +
"               '' numerformu , " +
"               NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"               0 , " +
"               ' ' , " +
"               ' ' , " +
"               '00' " +
"                ,GESAUT.CODESTADO, " +
"               GESAUT.OBSERVACION, " +
"               GESAUT.ALERTA, " +
"               GESAUT.VALORALERTA, " +
"               GESAUT.CODTIEMPOALERTA " +
"                ,A.MTVCORRELATIVO " +
"               ,A.PAC_PAC_NUMERO " +
"                ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
"               ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"             FROM " +
"               (SELECT tabordenesserv.ordtipo, " +
"                 tabordenesserv.ordnumero, " +
"                 tabordenesserv.pac_pac_numero, " +
"                 tabordenesserv.mtvcorrelativo, " +
"                 tabordenesserv.tabencuentro, " +
"                 ordfecha, " +
"                 FECINGQRF, " +
"                 ordestado, " +
"                 '00' " +
"               FROM tabordenesserv, " +
"                 TabOrdDetCir, " +
"                 TabPrgQrf " +
"               WHERE TabPrgQrf.QRFEST(+)         = 'C' " +
"               AND TabOrdenesServ.ORDTIPO        = TabPrgQrf.ORDTIPO(+) " +
"               AND TabOrdenesServ.ORDNUMERO      = TabPrgQrf.ORDNUMERO(+) " +
"               AND TabOrdenesServ.PAC_PAC_NUMERO = TabPrgQrf.PAC_PAC_NUMERO(+) " +
"               AND TabOrdDetCir.PAC_PAC_NUMERO   = TabOrdenesServ.PAC_PAC_NUMERO(+) " +
"               AND TabOrdDetCir.MTVCORRELATIVO   = TabOrdenesServ.MTVCORRELATIVO(+) " +
"               AND TabOrdDetCir.TABENCUENTRO     = TabOrdenesServ.TABENCUENTRO(+) " +
"               AND TabOrdDetCir.ORDTIPO          = TabOrdenesServ.ORDTIPO(+) " +
"               AND TabOrdDetCir.ORDNUMERO        = TabOrdenesServ.ORDNUMERO(+) " +
"               ) a , " +
"               taborddetalle f , " +
"               taborddetcir todc , " +
"               tabordcirest toce , " +
"               HCP_ESTORDEN estord, " +
"               tabordsrvtpo tip, " +
"               CENAUT_GESTIONAUT GESAUT " +
"               ,atc_estadia est " +
"                , ser_servicios ser  " +
"               , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"             WHERE a.ordtipo       = 'P' " +
"             AND a.ordtipo         = tip.ordtipo " +
"             AND a.pac_pac_numero  = f.pac_pac_numero " +
"             AND a.ordtipo         = f.ordtipo " +
"             AND a.ordnumero       = f.ordnumero " +
"             AND a.ordtipo         = todc.ordtipo " +
"             AND a.ordnumero       = todc.ordnumero " +
"             AND a.pac_pac_numero  = todc.pac_pac_numero(+) " +
"             AND todc.ordcirestcod = toce.ordcirestcod " +
"             AND estord.EXMESTCOD  = toce.ordcirestcod " +
"              AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"              AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"             AND a.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
"              AND a.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
"              AND EST.ATCESTADO='H'  AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"               and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND f.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S'  and est.ser_obj_codigo is not null  and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
"             ORDER BY 25 ASC, " +
"               26 ASC " +
"             ) " +
"           ) " +
"         ) b " +
"     )   " +
" UNION  " +
"            select  " +
"    ADDPRES.ORDNOMBRE, ADDPRES.ORDNUMERO, ADDPRES.CODPRESTACION, PRES.PRE_PRE_DESCRIPCIO, SYSDATE, ADDPRES.FECHASOLIC, 0, '', SYSDATE,'', '', ADDPRES.ORDTIPO, 0,'','','',  " +
"    NVL(GESAUT.CODESTADO, 1), GESAUT.OBSERVACION, GESAUT.ALERTA, NVL(GESAUT.VALORALERTA,0) VALORALERTA, GESAUT.CODTIEMPOALERTA, ADDPRES.MTVCORRELATIVO, ADDPRES.ID, ADDPRES.PAC_PAC_NUMERO " +
"      ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio                ,est.ser_obj_codigo cama,  asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"     from cenaut_agregarprestacion addpres, PRE_PRESTACION PRES, CENAUT_GESTIONAUT GESAUT, CENAUT_PRESTACION CENPRES,  ATC_ESTADIA EST , ser_servicios ser, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"     WHERE  " +
"     ADDPRES.CODPRESTACION=PRES.PRE_PRE_CODIGO " +
"     AND ADDPRES.CODPRESTACION=GESAUT.CODPRESTACION(+) " +
"     AND ADDPRES.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"     AND ADDPRES.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"     AND ADDPRES.ORDNUMERO=GESAUT.ORDNUMERO(+) " +
"     AND ADDPRES.CODPRESTACION=CENPRES.CODPRESTACION " +
"     AND ADDPRES.MTVCORRELATIVO = EST.MTVCORRELATIVO " +
"     AND ADDPRES.PAC_PAC_NUMERO =EST.PAC_PAC_NUMERO " +
"     AND EST.ATCESTADO='H' " +
"     AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02'      and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO " +
"     AND CENPRES.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"     UNION " +
"     select  " +
"    ADDPRES.ORDNOMBRE, ADDPRES.ORDNUMERO, ADDPRES.CODPRESTACION, PRES.HMCTPONOM, SYSDATE, ADDPRES.FECHASOLIC, 0, '', SYSDATE,'', '', ADDPRES.ORDTIPO, 0,'','','',  " +
"    NVL(GESAUT.CODESTADO, 1), GESAUT.OBSERVACION, GESAUT.ALERTA, GESAUT.VALORALERTA, GESAUT.CODTIEMPOALERTA, ADDPRES.MTVCORRELATIVO, ADDPRES.ID, ADDPRES.PAC_PAC_NUMERO " +
"      ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio                ,est.ser_obj_codigo cama, NVL(GESAUT.RESPONSABLE, asig.responsable) RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
"     from cenaut_agregarprestacion addpres, TABHMCTPO PRES, CENAUT_GESTIONAUT GESAUT, CENAUT_PRESTACION CENPRES,  ATC_ESTADIA EST , ser_servicios ser, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
"     WHERE  " +
"     ADDPRES.CODPRESTACION=PRES.HMCTPOCOD " +
"     AND ADDPRES.CODPRESTACION=GESAUT.CODPRESTACION(+) " +
"     AND ADDPRES.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"     AND ADDPRES.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"     AND ADDPRES.ORDNUMERO=GESAUT.ORDNUMERO(+) " +
"     AND ADDPRES.CODPRESTACION=CENPRES.CODPRESTACION " +
"     AND ADDPRES.MTVCORRELATIVO = EST.MTVCORRELATIVO " +
"     AND ADDPRES.PAC_PAC_NUMERO =EST.PAC_PAC_NUMERO " +
"     AND EST.ATCESTADO='H' " +
"     AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     '     and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO " +
"     AND CENPRES.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
"            UNION " +
"  SELECT 'ENFERMERIA' ordnombre ,  " +
"               'ENF' || ord.IDCTLESP    ,   " +
"               ORD.PRE_PRE_CODIGO precod ,   " +
"                PRM.PRE_PRE_DESCRIPCIO prenom ,   " +
"               SYSDATE,   " +
"               ord.FEPRC fechasolic ,   " +
"               0 ,   " +
"               '' Estado ,   " +
"               SYSDATE fechaconfirm ,   " +
"              '' tipoformu ,   " +
"               '' ,   " +
"               '' ,   " +
"               0 ,   " +
"               '' , " +
"              '' ,   " +
"                '' " +
"               ,NVL(GESAUT.CODESTADO, 1),   " +
"               GESAUT.OBSERVACION,   " +
"               GESAUT.ALERTA,   " +
"               NVL(GESAUT.VALORALERTA,0),   " +
"               GESAUT.CODTIEMPOALERTA   " +
"              ,ORD.MTVCORRELATIVO   " +
"                 ,CAST(GlbGetPacCar( ORD.PAC_PAC_NUMERO) AS VARCHAR(13))  " +
"               ,ORD.PAC_PAC_NUMERO   " +
"               ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio  " +
"               ,est.ser_obj_codigo  cama,  asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo  " +
"             FROM  HCP_CTLESPECIALES " +
"            ord ,  HCP_PARAMCTLESP PRM, " +
"               CENAUT_GESTIONAUT GESAUT  " +
"               ,ATC_ESTADIA EST  " +
"               , ser_servicios ser   " +
"               , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON  " +
"             WHERE ORD.PRE_PRE_CODIGO= prm.PRE_PRE_CODIGO " +
"             AND PRM.PRE_PRE_VIGENCIA='S' " +
"           AND ORD.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"           AND ORD.PAC_PAC_NUMERO = GESAUT.PAC_PAC_NUMERO(+) " +
"           AND ORD.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"           and 'ENF' || IDCTLESP = GESAUT.ORDNUMERO(+) " +
"              AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO (+) " +
"              AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO  " +
"              AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO  " +
"              and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND ORD.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        ' and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
"              AND ORD.PRE_PRE_CODIGO IN ('GESATE  ', 'SALCAS  ', 'GESAMB  ')  " +
"            ORDER BY 25 ASC, 26 ASC";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");

        }




        public DataTable ConsultarConvenioPac(string MtvCorrelativo, string Pac_Pac_Numero)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select DISTINCT EST.CON_CON_CODIGO CODCONVENIO, CON_CON_DESCRIPCIO CONVENIO  " +
                   "  from ser_servicios A,  ser_objetos B, ATC_ESTADIA EST, CON_CONVENIO CON " +
                   "  where A.ser_Ser_vigencia='S' and A.ser_ser_ambito='02' and est.mtvcorrelativo='" + MtvCorrelativo + "' and est.pac_pac_numero='" + Pac_Pac_Numero + "' AND EST.CON_CON_CODIGO = CON.CON_CON_CODIGO AND A.SER_SER_CODIGO=B.SER_SER_CODIGO and b.ser_obj_codigo= EST.SER_OBJ_CODIGO and ATCESTADO ='H'  " +
                   "   order by 2";
            //sql = "select SER_sER_CODIGO CODIGO, TRIM(SER_SER_DESCRIPCIO) SERVICIO from ser_servicios where ser_Ser_vigencia='S' and  (ser_Ser_codigo='006     ' or ser_Ser_codigo='007     ') order by 2";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarAsigCamaObjetos(string Objeto)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select a.ser_obj_codigo CODIGO, a.SER_OBJ_DESCRIPCIO OBJETO, trim(pac_pac_rut) ID, GlbGetNomPac(b.pac_pac_numero)  " +
                  "  PACIENTE, A.SER_OBJ_ESTADO, trim(D.CON_CON_DESCRIPCIO) CONVENIO, ASIG.NOMBRES RESPONSABLE, '" + Objeto + "' SERVICIO  " +
                   "  from ser_objetos a, atc_estadia b, pac_paciente c , con_convenio d, CENAUT_ASIGNACAMA ASIG   " +
                   " WHERE a.SER_OBJ_VIGENCIA='S' AND  a.ser_Ser_codigo='" + Objeto + "' " +
                   " AND a.ser_obj_codigo=b.ser_obj_codigo and a.ser_ser_codigo=b.ser_ser_codigo  " +
                   " and b.pac_pac_numero=c.pac_pac_numero  " +
                   " and b.atcestado='H'  " +
                   " AND B.ATC_EST_FECEGRESO=TO_DATE('1900/01/01', 'YYYY/MM/DD') and B.CON_CON_CODIGO=D.CON_CON_CODIGO   " +
                   " AND A.SER_OBJ_CODIGO=ASIG.CAMA(+) " +
                   " UNION all  " +
                   " select   " +
                   " a.ser_obj_codigo CODIGO, a.SER_OBJ_DESCRIPCIO OBJETO, '','','', '', ASIG.NOMBRES RESPONSABLE, '" + Objeto + "' " +
                   " from ser_objetos a, CENAUT_ASIGNACAMA ASIG   where   " +
                   " a.SER_OBJ_VIGENCIA='S' AND  a.ser_Ser_codigo='" + Objeto + "' " +
                   " AND A.SER_OBJ_CODIGO=ASIG.CAMA(+) " +
                   "  and  " +
                   " a.ser_obj_codigo not in (  " +
                   " select a.ser_obj_codigo   " +
                   "  from ser_objetos a, atc_estadia b, pac_paciente c  " +
                   " WHERE a.SER_OBJ_VIGENCIA='S' AND  a.ser_Ser_codigo='" + Objeto + "'   " +
                   " AND a.ser_obj_codigo=b.ser_obj_codigo and a.ser_ser_codigo=b.ser_ser_codigo  " +
                   " and b.pac_pac_numero=c.pac_pac_numero  " +
                   " and b.atcestado='H'  " +
                   " AND B.ATC_EST_FECEGRESO=TO_DATE('1900/01/01', 'YYYY/MM/DD'))   " +
                   "  ORDER BY 1";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarAlertas(string Usuario, int Perfil)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            if (Perfil >= 3)
            {
                sql = "select DISTINCT ROWNUM, TRIM(PRES.PRE_PRE_DESCRIPCIO) PRESTACION, FunGlbGetRutPac(GESAUT.PAC_PAC_NUMERO) ID, " +
                      "   FNCGETNOMPACIENTE(GESAUT.PAC_PAC_NUMERO) NOMBRE, trim(EST.ser_obj_codigo) CAMA, " +
                       "  '1' TIPO, GESAUT.RESPONSABLE " +
                      "  from cenaut_gestionaut gesaut, ATC_ESTADIA EST, PRE_PRESTACION PRES  " +
                      "  where  " +
                     "   GESAUT.PAC_PAC_NUMERO=est.pac_pac_numero and gesaut.mtvcorrelativo=est.MTVCORRELATIVO and est.ATCESTADO ='H'  AND GESAUT.CODPRESTACION=PRES.PRE_PRE_CODIGO AND CODESTADO IN (2) " +
                      "  and GESAUT.ALERTA=1 and sysdate>  GESAUT.FECPENDGESTION+GESAUT.VALORALERTA/1440 and GESAUT.RESPONSABLE='" + Usuario + "' " +
                "UNION " +
               " SELECT " +
               "  rownum " +
               " , PRE.PRE_PRE_DESCRIPCIO PRESTACION " +
               " , AMB.IDENTIFICACION, AMB.NOMBRE, 'AMBULATORIO' CAMA, '2' TIPO " +
               " , AMB.USUARIO RESPONSABLE " +
               " FROM CENAUT_AMBULATORIOS AMB, PRE_PRESTACION PRE " +
               " WHERE TRIM(AMB.PROCIND) = TRIM(PRE.PRE_PRE_CODIGO) " +
               " AND AMB.ESTADO NOT IN (3,4,5) " +
               " AND SYSDATE > (select  max(evento.fechaevento) from cenaut_eventosambulatorio evento " +
    "where   EVENTO.IDAMB = amb.id " +
    "and evento.estado = amb.estado)  + AMB.TIEMPO / 1440 " +
               " AND AMB.USUARIO = '" + Usuario + "'  " ;
;
            }
            else
            {
                sql = "select DISTINCT ROWNUM, TRIM(PRES.PRE_PRE_DESCRIPCIO) PRESTACION, FunGlbGetRutPac(GESAUT.PAC_PAC_NUMERO) ID, " +
                        "   FNCGETNOMPACIENTE(GESAUT.PAC_PAC_NUMERO) NOMBRE, trim(EST.ser_obj_codigo) CAMA, " +
                        "  '1' TIPO, GESAUT.RESPONSABLE " +
                        "  from cenaut_gestionaut gesaut, ATC_ESTADIA EST, PRE_PRESTACION PRES  " +
                        "  where  " +
                    "   GESAUT.PAC_PAC_NUMERO=est.pac_pac_numero and gesaut.mtvcorrelativo=est.MTVCORRELATIVO and est.ATCESTADO ='H'  AND GESAUT.CODPRESTACION=PRES.PRE_PRE_CODIGO AND CODESTADO IN (2) " +
                        "  and GESAUT.ALERTA=1 and sysdate>  GESAUT.FECPENDGESTION+GESAUT.VALORALERTA/1440 "  +
                "UNION " +
                " SELECT  " +
                " rownum " +
                ", PRE.PRE_PRE_DESCRIPCIO PRESTACION " +
                ", AMB.IDENTIFICACION, AMB.NOMBRE, 'AMBULATORIO' CAMA, '2' TIPO " +
                ", AMB.USUARIO RESPONSABLE " +
               " FROM CENAUT_AMBULATORIOS AMB, PRE_PRESTACION PRE " +
               " WHERE TRIM(AMB.PROCIND) = TRIM(PRE.PRE_PRE_CODIGO) " +
               " AND AMB.ESTADO NOT IN (3,4,5) " +
               " AND SYSDATE > (select  max(evento.fechaevento) from cenaut_eventosambulatorio evento " +
               "where   EVENTO.IDAMB = amb.id " +
               "and evento.estado = amb.estado)  + AMB.TIEMPO / 1440 " ;
            }
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarPrestacionesPendientes(string MtvCorrelativo, string Pac_pac_numero)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = " SELECT OrdNombre, " +
"    OrdNumero, " +
"    PreCod, " +
"    PreNom, " +
"    FechaRevis, " +
"    FechaSolic, " +
"    ORDEN, " +
"    Estado, " +
"    FechaConfirm, " +
"    tipoformu, " +
"    numerformu, " +
"    ordtipo, " +
"    ExamCit, " +
"    forcod, " +
"    forvrs, " +
"    ulteve  " +
"    ,NVL(CODESTADO,1) CODESTADO, " +
"      OBSERVACION, " +
"      ALERTA, " +
"      VALORALERTA, " +
"      CODTIEMPOALERTA " +
"    ,MTVCORRELATIVO " +
"     , FunGlbGetRutPac(PAC_PAC_NUMERO) ID, PAC_PAC_NUMERO " +
"    FROM " +
"    (SELECT b.*, " +
"        rownum registro " +
"      FROM " +
"        (SELECT OrdNombre, " +
"          OrdNumero, " +
"          PreCod, " +
"          PreNom, " +
"          FechaRevis, " +
"          FechaSolic, " +
"          ORDEN, " +
"          Estado, " +
"          FechaConfirm, " +
"          tipoformu, " +
"          numerformu, " +
"          ordtipo, " +
"          ExamCit, " +
"          forcod, " +
"          forvrs, " +
"          ulteve " +
"          ,CODESTADO, " +
"          OBSERVACION, " +
"          ALERTA, " +
"          VALORALERTA, " +
"          CODTIEMPOALERTA " +
"          ,MTVCORRELATIVO " +
"          ,PAC_PAC_NUMERO " +
"        FROM " +
"          (SELECT OrdNombre, " +
"            OrdNumero, " +
"            PreCod, " +
"            PreNom, " +
"            FechaRevis, " +
"            FechaSolic, " +
"            ORDEN, " +
"            Estado, " +
"            FechaConfirm, " +
"            tipoformu, " +
"            numerformu, " +
"            ordtipo, " +
"            ExamCit, " +
"            forcod, " +
"            forvrs, " +
"            ulteve " +
"             ,CODESTADO, " +
"              OBSERVACION, " +
"              ALERTA, " +
"              VALORALERTA, " +
"              CODTIEMPOALERTA " +
"              ,MTVCORRELATIVO " +
"              ,PAC_PAC_NUMERO " +
"          FROM " +
"            ( " +
"            SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ordnombre , " +
"              ord.ordnumero ordnumero , " +
"              exa.cex_exs_codigprest precod , " +
"              glbgetnompre (exa.cex_exs_codigprest) prenom , " +
"              DECODE (inf.cex_inf_fecharevis, NULL, exa.cex_exs_fechamuest, inf.cex_inf_fecharevis) fecharevis , " +
"              ord.ORDFECHA fechasolic , " +
"              estord.ORDEN , " +
"              est.EXMESTNOM Estado , " +
"              DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm , " +
"              exa.ate_pre_tipoformu tipoformu , " +
"              exa.ate_pre_numerformu numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit , " +
"              inf.lab_for_codigo forcod , " +
"              inf.lab_for_version forvrs , " +
"              exa.cex_exs_estado ulteve " +
"              ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"              ,ORD.MTVCORRELATIVO " +
"              ,ORD.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT ordtipo , " +
"                ordnumero , " +
"                pac_pac_numero , " +
"                mtvcorrelativo , " +
"                tabencuentro , " +
"                ordfecha , " +
"                ordestado " +
"              FROM tabordenesserv " +
"              WHERE mtvcorrelativo = '" + MtvCorrelativo + "' " +
"              AND pac_pac_numero   = '" + Pac_pac_numero + "' " +
"              ) ord , " +
"              Gt_Paraclinicos exa , " +
"              cex_informes inf , " +
"              tabexmest est , " +
"              tabordsrvtpo tip , " +
"              HCP_ESTORDEN estord, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE ord.ordtipo            IN ('L', 'I', 'A')  " +
"            AND exa.ordtipo               = ord.ordtipo " +
"            AND exa.ordnumero             = ord.ordnumero " +
"            AND exa.pac_pac_numero        = ord.pac_pac_numero " +
"            AND inf.ate_pre_tipoformu(+)  = exa.ate_pre_tipoformu " +
"            AND inf.ate_pre_numerformu(+) = exa.ate_pre_numerformu " +
"            AND inf.cex_exs_codigprest(+) = exa.cex_exs_codigprest " +
"            AND est.exmestcod             = exa.cex_exs_estado " +
"            AND tip.ordtipo(+)            = exa.ordtipo " +
"            AND estord.EXMESTCOD          = est.exmestcod " +
"               AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND EXA.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+) " +
"            UNION ALL " +
"            SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') , " +
"              NVL(ord.ordnumero,exa.ate_pre_numerformu) , " +
"              exa.cex_exs_codigprest , " +
"              glbgetnompre(exa.cex_exs_codigprest) , " +
"              DECODE (inf.hcp_inf_fechadigit,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),inf.hcp_inf_fechadigit) , " +
"              ord.ORDFECHA , " +
"              estord.ORDEN , " +
"              est.EXMESTNOM Estado , " +
"              DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm , " +
"              exa.ate_pre_tipoformu tipoformu , " +
"              exa.ate_pre_numerformu numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit , " +
"              inf.hcp_inf_codigforma , " +
"              inf.hcp_inf_version , " +
"              est.exmestcod " +
"               ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"               ,ORD.MTVCORRELATIVO " +
"              ,ORD.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT ordtipo , " +
"                ordnumero , " +
"                pac_pac_numero , " +
"                mtvcorrelativo , " +
"                tabencuentro , " +
"                ordfecha , " +
"                ordestado " +
"              FROM tabordenesserv " +
"              WHERE mtvcorrelativo = '" + MtvCorrelativo + "' " +
"              AND pac_pac_numero   = '" + Pac_pac_numero + "' " +
"              ) ord , " +
"              Gt_Paraclinicos exa , " +
"              hcp_informes inf , " +
"              tabexmest est , " +
"              tabordsrvtpo tip , " +
"              HCP_ESTORDEN estord, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE exa.ordtipo             = 'D' " +
"            AND ord.ordtipo NOT          IN ('L', 'I', 'A', 'P') " +
"            AND exa.ordnumero             = ord.ordnumero " +
"            AND exa.pac_pac_numero        = ord.pac_pac_numero " +
"            AND inf.hcp_inf_numerpacie(+) = exa.pac_pac_numero " +
"            AND inf.hcp_inf_tipoformu(+)  = exa.ate_pre_tipoformu " +
"            AND inf.hcp_inf_numerformu(+) = exa.ate_pre_numerformu " +
"            AND inf.pre_pre_codigo(+)     = exa.cex_exs_codigprest " +
"            AND est.exmestcod             = exa.cex_exs_estado " +
"            AND estord.EXMESTCOD          = est.exmestcod " +
"            AND tip.ordtipo(+)            = ord.ordtipo " +
"           AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND EXA.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+) " +
"            UNION ALL " +
"            SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') , " +
"              det.ordnumero , " +
"              det.pre_pre_codigo , " +
"              glbgetnompre(det.pre_pre_codigo) , " +
"              DECODE(srv.ordfecha,NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'),srv.ordfecha) , " +
"              srv.ORDFECHA , " +
"              0 ORDEN , " +
"              'Examen Solicitado' , " +
"              TO_DATE('1900/01/01', 'YYYY/MM/DD') , " +
"              '' tipoformu , " +
"              '' numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              0 , " +
"              ' ' , " +
"              ' ' , " +
"              '00' " +
"               ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"               ,srv.MTVCORRELATIVO " +
"              ,srv.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT ordtipo , " +
"                ordnumero , " +
"                pac_pac_numero , " +
"                mtvcorrelativo , " +
"                tabencuentro , " +
"                ordfecha , " +
"                ordestado " +
"              FROM tabordenesserv " +
"              WHERE mtvcorrelativo = '" + MtvCorrelativo + "' " +
"              AND pac_pac_numero   = '" + Pac_pac_numero + "' " +
"              ) srv , " +
"              taborddetalle det , " +
"              tabordsrvtpo tip, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE srv.ordtipo NOT IN ('P','B') " +
"            AND det.mtvcorrelativo = srv.mtvcorrelativo " +
"            AND det.ordtipo        = srv.ordtipo " +
"            AND det.ordnumero      = srv.ordnumero " +
"            AND det.tabencuentro   = srv.tabencuentro " +
"            AND det.pac_pac_numero = srv.pac_pac_numero " +
"            AND tip.ordtipo(+)     = det.ordtipo " +
"           AND DET.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"             AND DET.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND DET.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND DET.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"            AND (SELECT COUNT (1) " +
"              FROM rpa_forlab lab, " +
"                cex_examsolic ex " +
"              WHERE lab.ordtipo          = DECODE(srv.ordtipo, 'A', 'A', 'I', 'I', 'L', 'L', 'D') " +
"              AND lab.ordnumero          = srv.ordnumero " +
"              AND lab.pac_pac_numero     = srv.pac_pac_numero " +
"              AND lab.ATE_PRE_TIPOFORMU  = ex.ate_pre_tipoformu " +
"              AND lab.ATE_PRE_NUMERFORMU = ex.ate_pre_numerformu " +
"              AND lab.PAC_PAC_NUMERO     = ex.PAC_PAC_NUMERO " +
"              AND ex.CEX_EXS_CODIGPREST  = det.pre_pre_codigo ) = 0 " +
"            UNION ALL " +
"            SELECT DISTINCT tip.ordnombre , " +
"              f.ordnumero , " +
"              f.pre_pre_codigo , " +
"              glbgetnompre(f.pre_pre_codigo) , " +
"              DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) , " +
"              a.ordfecha , " +
"              estord.ORDEN, " +
"              toce.ORDCIRESTNOM, " +
"              a.FECINGQRF , " +
"              '' tipoformu , " +
"              '' numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              0 , " +
"              ' ' , " +
"              ' ' , " +
"              '00' " +
"               ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"               ,A.MTVCORRELATIVO " +
"              ,A.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT tabordenesserv.ordtipo , " +
"                tabordenesserv.ordnumero , " +
"                tabordenesserv.pac_pac_numero , " +
"                tabordenesserv.mtvcorrelativo , " +
"                tabordenesserv.tabencuentro , " +
"                ordfecha , " +
"                FECINGQRF, " +
"                ordestado , " +
"                '00' " +
"              FROM tabordenesserv , " +
"                TabPrgQrf " +
"              WHERE tabordenesserv.mtvcorrelativo = '" + MtvCorrelativo + "' " +
"              AND tabordenesserv.pac_pac_numero   = '" + Pac_pac_numero + "' " +
"              AND TabOrdenesServ.ORDTIPO          = TabPrgQrf.ORDTIPO " +
"              AND TabOrdenesServ.ORDNUMERO        = TabPrgQrf.ORDNUMERO " +
"              AND TabOrdenesServ.PAC_PAC_NUMERO   = TabPrgQrf.PAC_PAC_NUMERO " +
"              ) a , " +
"              taborddetalle f , " +
"              taborddetcir todc , " +
"              tabordcirest toce , " +
"              HCP_ESTORDEN estord, " +
"              tabordsrvtpo tip, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE a.ordtipo       = 'P' " +
"            AND a.ordtipo         = tip.ordtipo " +
"            AND a.pac_pac_numero  = f.pac_pac_numero " +
"            AND a.ordtipo         = f.ordtipo " +
"            AND a.ordnumero       = f.ordnumero " +
"            AND a.ordtipo         = todc.ordtipo " +
"            AND a.ordnumero       = todc.ordnumero " +
"            AND a.pac_pac_numero  = todc.pac_pac_numero " +
"            AND todc.ordcirestcod = toce.ordcirestcod " +
"            AND estord.EXMESTCOD  = toce.ordcirestcod " +
"              AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"             AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"            UNION " +
"            SELECT DISTINCT tip.ordnombre , " +
"              f.ordnumero , " +
"              f.pre_pre_codigo , " +
"              admsalud.glbgetnompre(f.pre_pre_codigo) , " +
"              DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) , " +
"              a.ordfecha , " +
"              estord.ORDEN , " +
"              toce.ORDCIRESTNOM, " +
"              NVL(a.FECINGQRF,TO_DATE('1900/01/01', 'YYYY/MM/DD')) , " +
"              '' tipoformu , " +
"              '' numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              0 , " +
"              ' ' , " +
"              ' ' , " +
"              '00' " +
"               ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"               ,A.MTVCORRELATIVO " +
"              ,A.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT tabordenesserv.ordtipo, " +
"                tabordenesserv.ordnumero, " +
"                tabordenesserv.pac_pac_numero, " +
"                tabordenesserv.mtvcorrelativo, " +
"                tabordenesserv.tabencuentro, " +
"                ordfecha, " +
"                FECINGQRF, " +
"                ordestado, " +
"                '00' " +
"              FROM tabordenesserv, " +
"                TabOrdDetCir, " +
"                TabPrgQrf " +
"              WHERE TabPrgQrf.QRFEST(+)         = 'C' " +
"              AND TabOrdenesServ.ORDTIPO        = TabPrgQrf.ORDTIPO(+) " +
"              AND TabOrdenesServ.ORDNUMERO      = TabPrgQrf.ORDNUMERO(+) " +
"              AND TabOrdenesServ.PAC_PAC_NUMERO = TabPrgQrf.PAC_PAC_NUMERO(+) " +
"              AND TabOrdDetCir.PAC_PAC_NUMERO   = TabOrdenesServ.PAC_PAC_NUMERO(+) " +
"              AND TabOrdDetCir.MTVCORRELATIVO   = TabOrdenesServ.MTVCORRELATIVO(+) " +
"              AND TabOrdDetCir.TABENCUENTRO     = TabOrdenesServ.TABENCUENTRO(+) " +
"              AND TabOrdDetCir.ORDTIPO          = TabOrdenesServ.ORDTIPO(+) " +
"              AND TabOrdDetCir.ORDNUMERO        = TabOrdenesServ.ORDNUMERO(+) " +
"              AND TabOrdenesServ.PAC_PAC_NUMERO = '" + Pac_pac_numero + "' " +
"              AND TABORDENESSERV.MTVCORRELATIVO = '" + MtvCorrelativo + "' " +
"              ) a , " +
"              taborddetalle f , " +
"              taborddetcir todc , " +
"              tabordcirest toce , " +
"              HCP_ESTORDEN estord, " +
"              tabordsrvtpo tip, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE a.ordtipo       = 'P' " +
"            AND a.ordtipo         = tip.ordtipo " +
"            AND a.pac_pac_numero  = f.pac_pac_numero " +
"            AND a.ordtipo         = f.ordtipo " +
"            AND a.ordnumero       = f.ordnumero " +
"            AND a.ordtipo         = todc.ordtipo " +
"            AND a.ordnumero       = todc.ordnumero " +
"            AND a.pac_pac_numero  = todc.pac_pac_numero(+) " +
"            AND todc.ordcirestcod = toce.ordcirestcod " +
"            AND estord.EXMESTCOD  = toce.ordcirestcod " +
"             AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"             AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"            AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
"            AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"            ORDER BY 24 ASC, " +
"              25 ASC " +
"            ) " +
"          ) " +
"        ) b " +
"    ) ";

            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarPrestacionesPendientesPR(string MtvCorrelativo, string Pac_pac_numero)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = " SELECT OrdNombre, " +
"    OrdNumero, " +
"    PreCod, " +
"    PreNom, " +
"    FechaRevis, " +
"    FechaSolic, " +
"    ORDEN, " +
"    Estado, " +
"    FechaConfirm, " +
"    tipoformu, " +
"    numerformu, " +
"    ordtipo, " +
"    ExamCit, " +
"    forcod, " +
"    forvrs, " +
"    ulteve  " +
"    ,NVL(CODESTADO,1) CODESTADO, " +
"      OBSERVACION, " +
"      ALERTA, " +
"      VALORALERTA, " +
"      CODTIEMPOALERTA " +
"    ,MTVCORRELATIVO " +
"     , FunGlbGetRutPac(PAC_PAC_NUMERO) ID, PAC_PAC_NUMERO " +
"    FROM " +
"    (SELECT b.*, " +
"        rownum registro " +
"      FROM " +
"        (SELECT OrdNombre, " +
"          OrdNumero, " +
"          PreCod, " +
"          PreNom, " +
"          FechaRevis, " +
"          FechaSolic, " +
"          ORDEN, " +
"          Estado, " +
"          FechaConfirm, " +
"          tipoformu, " +
"          numerformu, " +
"          ordtipo, " +
"          ExamCit, " +
"          forcod, " +
"          forvrs, " +
"          ulteve " +
"          ,CODESTADO, " +
"          OBSERVACION, " +
"          ALERTA, " +
"          VALORALERTA, " +
"          CODTIEMPOALERTA " +
"          ,MTVCORRELATIVO " +
"          ,PAC_PAC_NUMERO " +
"        FROM " +
"          (SELECT OrdNombre, " +
"            OrdNumero, " +
"            PreCod, " +
"            PreNom, " +
"            FechaRevis, " +
"            FechaSolic, " +
"            ORDEN, " +
"            Estado, " +
"            FechaConfirm, " +
"            tipoformu, " +
"            numerformu, " +
"            ordtipo, " +
"            ExamCit, " +
"            forcod, " +
"            forvrs, " +
"            ulteve " +
"             ,CODESTADO, " +
"              OBSERVACION, " +
"              ALERTA, " +
"              VALORALERTA, " +
"              CODTIEMPOALERTA " +
"              ,MTVCORRELATIVO " +
"              ,PAC_PAC_NUMERO " +
"          FROM " +
"            ( " +
"            SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ordnombre , " +
"              ord.ordnumero ordnumero , " +
"              exa.cex_exs_codigprest precod , " +
"              glbgetnompre (exa.cex_exs_codigprest) prenom , " +
"              DECODE (inf.cex_inf_fecharevis, NULL, exa.cex_exs_fechamuest, inf.cex_inf_fecharevis) fecharevis , " +
"              ord.ORDFECHA fechasolic , " +
"              estord.ORDEN , " +
"              est.EXMESTNOM Estado , " +
"              DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm , " +
"              exa.ate_pre_tipoformu tipoformu , " +
"              exa.ate_pre_numerformu numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit , " +
"              inf.lab_for_codigo forcod , " +
"              inf.lab_for_version forvrs , " +
"              exa.cex_exs_estado ulteve " +
"              ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"              ,ORD.MTVCORRELATIVO " +
"              ,ORD.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT ordtipo , " +
"                ordnumero , " +
"                pac_pac_numero , " +
"                mtvcorrelativo , " +
"                tabencuentro , " +
"                ordfecha , " +
"                ordestado " +
"              FROM tabordenesserv " +
"              WHERE mtvcorrelativo = '" + MtvCorrelativo + "' " +
"              AND pac_pac_numero   = '" + Pac_pac_numero + "' " +
"              ) ord , " +
"              Gt_Paraclinicos exa , " +
"              cex_informes inf , " +
"              tabexmest est , " +
"              tabordsrvtpo tip , " +
"              HCP_ESTORDEN estord, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE ord.ordtipo            IN ('L', 'I', 'A')  " +
"            AND exa.ordtipo               = ord.ordtipo " +
"            AND exa.ordnumero             = ord.ordnumero " +
"            AND exa.pac_pac_numero        = ord.pac_pac_numero " +
"            AND inf.ate_pre_tipoformu(+)  = exa.ate_pre_tipoformu " +
"            AND inf.ate_pre_numerformu(+) = exa.ate_pre_numerformu " +
"            AND inf.cex_exs_codigprest(+) = exa.cex_exs_codigprest " +
"            AND est.exmestcod             = exa.cex_exs_estado " +
"            AND tip.ordtipo(+)            = exa.ordtipo " +
"            AND estord.EXMESTCOD          = est.exmestcod " +
"               AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND EXA.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+) " +
"            UNION ALL " +
"            SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') , " +
"              NVL(ord.ordnumero,exa.ate_pre_numerformu) , " +
"              exa.cex_exs_codigprest , " +
"              glbgetnompre(exa.cex_exs_codigprest) , " +
"              DECODE (inf.hcp_inf_fechadigit,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),inf.hcp_inf_fechadigit) , " +
"              ord.ORDFECHA , " +
"              estord.ORDEN , " +
"              est.EXMESTNOM Estado , " +
"              DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm , " +
"              exa.ate_pre_tipoformu tipoformu , " +
"              exa.ate_pre_numerformu numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit , " +
"              inf.hcp_inf_codigforma , " +
"              inf.hcp_inf_version , " +
"              est.exmestcod " +
"               ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"               ,ORD.MTVCORRELATIVO " +
"              ,ORD.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT ordtipo , " +
"                ordnumero , " +
"                pac_pac_numero , " +
"                mtvcorrelativo , " +
"                tabencuentro , " +
"                ordfecha , " +
"                ordestado " +
"              FROM tabordenesserv " +
"              WHERE mtvcorrelativo = '" + MtvCorrelativo + "' " +
"              AND pac_pac_numero   = '" + Pac_pac_numero + "' " +
"              ) ord , " +
"              Gt_Paraclinicos exa , " +
"              hcp_informes inf , " +
"              tabexmest est , " +
"              tabordsrvtpo tip , " +
"              HCP_ESTORDEN estord, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE exa.ordtipo             = 'D' " +
"            AND ord.ordtipo NOT          IN ('L', 'I', 'A', 'P') " +
"            AND exa.ordnumero             = ord.ordnumero " +
"            AND exa.pac_pac_numero        = ord.pac_pac_numero " +
"            AND inf.hcp_inf_numerpacie(+) = exa.pac_pac_numero " +
"            AND inf.hcp_inf_tipoformu(+)  = exa.ate_pre_tipoformu " +
"            AND inf.hcp_inf_numerformu(+) = exa.ate_pre_numerformu " +
"            AND inf.pre_pre_codigo(+)     = exa.cex_exs_codigprest " +
"            AND est.exmestcod             = exa.cex_exs_estado " +
"            AND estord.EXMESTCOD          = est.exmestcod " +
"            AND tip.ordtipo(+)            = ord.ordtipo " +
"           AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND EXA.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+) " +
"            UNION ALL " +
"            SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') , " +
"              det.ordnumero , " +
"              det.pre_pre_codigo , " +
"              glbgetnompre(det.pre_pre_codigo) , " +
"              DECODE(srv.ordfecha,NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'),srv.ordfecha) , " +
"              srv.ORDFECHA , " +
"              0 ORDEN , " +
"              'Examen Solicitado' , " +
"              TO_DATE('1900/01/01', 'YYYY/MM/DD') , " +
"              '' tipoformu , " +
"              '' numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              0 , " +
"              ' ' , " +
"              ' ' , " +
"              '00' " +
"               ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"               ,srv.MTVCORRELATIVO " +
"              ,srv.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT ordtipo , " +
"                ordnumero , " +
"                pac_pac_numero , " +
"                mtvcorrelativo , " +
"                tabencuentro , " +
"                ordfecha , " +
"                ordestado " +
"              FROM tabordenesserv " +
"              WHERE mtvcorrelativo = '" + MtvCorrelativo + "' " +
"              AND pac_pac_numero   = '" + Pac_pac_numero + "' " +
"              ) srv , " +
"              taborddetalle det , " +
"              tabordsrvtpo tip, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE srv.ordtipo NOT IN ('P','B') " +
"            AND det.mtvcorrelativo = srv.mtvcorrelativo " +
"            AND det.ordtipo        = srv.ordtipo " +
"            AND det.ordnumero      = srv.ordnumero " +
"            AND det.tabencuentro   = srv.tabencuentro " +
"            AND det.pac_pac_numero = srv.pac_pac_numero " +
"            AND tip.ordtipo(+)     = det.ordtipo " +
"           AND DET.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"             AND DET.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND DET.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND DET.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"            AND (SELECT COUNT (1) " +
"              FROM rpa_forlab lab, " +
"                cex_examsolic ex " +
"              WHERE lab.ordtipo          = DECODE(srv.ordtipo, 'A', 'A', 'I', 'I', 'L', 'L', 'D') " +
"              AND lab.ordnumero          = srv.ordnumero " +
"              AND lab.pac_pac_numero     = srv.pac_pac_numero " +
"              AND lab.ATE_PRE_TIPOFORMU  = ex.ate_pre_tipoformu " +
"              AND lab.ATE_PRE_NUMERFORMU = ex.ate_pre_numerformu " +
"              AND lab.PAC_PAC_NUMERO     = ex.PAC_PAC_NUMERO " +
"              AND ex.CEX_EXS_CODIGPREST  = det.pre_pre_codigo ) = 0 " +
"            UNION ALL " +
"            SELECT DISTINCT tip.ordnombre , " +
"              todc.ate_pre_numerformu , " +
"              f.pre_pre_codigo , " +
"              glbgetnompre(f.pre_pre_codigo) , " +
"              DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) , " +
"              a.ordfecha , " +
"              estord.ORDEN, " +
"              toce.ORDCIRESTNOM, " +
"              a.FECINGQRF , " +
"              '' tipoformu , " +
"              '' numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              0 , " +
"              ' ' , " +
"              ' ' , " +
"              '00' " +
"               ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"               ,A.MTVCORRELATIVO " +
"              ,A.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT tabordenesserv.ordtipo , " +
"                tabordenesserv.ordnumero , " +
"                tabordenesserv.pac_pac_numero , " +
"                tabordenesserv.mtvcorrelativo , " +
"                tabordenesserv.tabencuentro , " +
"                ordfecha , " +
"                FECINGQRF, " +
"                ordestado , " +
"                '00' " +
"              FROM tabordenesserv , " +
"                TabPrgQrf " +
"              WHERE tabordenesserv.mtvcorrelativo = '" + MtvCorrelativo + "' " +
"              AND tabordenesserv.pac_pac_numero   = '" + Pac_pac_numero + "' " +
"              AND TabOrdenesServ.ORDTIPO          = TabPrgQrf.ORDTIPO " +
"              AND TabOrdenesServ.ORDNUMERO        = TabPrgQrf.ORDNUMERO " +
"              AND TabOrdenesServ.PAC_PAC_NUMERO   = TabPrgQrf.PAC_PAC_NUMERO " +
"              ) a , " +
"              taborddetalle f , " +
"              taborddetcir todc , " +
"              tabordcirest toce , " +
"              HCP_ESTORDEN estord, " +
"              tabordsrvtpo tip, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE a.ordtipo       = 'P' " +
"            AND a.ordtipo         = tip.ordtipo " +
"            AND a.pac_pac_numero  = f.pac_pac_numero " +
"            AND a.ordtipo         = f.ordtipo " +
"            AND a.ordnumero       = f.ordnumero " +
"            AND a.ordtipo         = todc.ordtipo " +
"            AND a.ordnumero       = todc.ordnumero " +
"            AND a.pac_pac_numero  = todc.pac_pac_numero " +
"            AND todc.ordcirestcod = toce.ordcirestcod " +
"            AND estord.EXMESTCOD  = toce.ordcirestcod " +
"              AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"             AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"             AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
"             AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"            UNION " +
"            SELECT DISTINCT tip.ordnombre , " +
"              todc.ate_pre_numerformu , " +
"              f.pre_pre_codigo , " +
"              admsalud.glbgetnompre(f.pre_pre_codigo) , " +
"              DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) , " +
"              a.ordfecha , " +
"              estord.ORDEN , " +
"              toce.ORDCIRESTNOM, " +
"              NVL(a.FECINGQRF,TO_DATE('1900/01/01', 'YYYY/MM/DD')) , " +
"              '' tipoformu , " +
"              '' numerformu , " +
"              NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
"              0 , " +
"              ' ' , " +
"              ' ' , " +
"              '00' " +
"               ,GESAUT.CODESTADO, " +
"              GESAUT.OBSERVACION, " +
"              GESAUT.ALERTA, " +
"              GESAUT.VALORALERTA, " +
"              GESAUT.CODTIEMPOALERTA " +
"               ,A.MTVCORRELATIVO " +
"              ,A.PAC_PAC_NUMERO " +
"            FROM " +
"              (SELECT tabordenesserv.ordtipo, " +
"                tabordenesserv.ordnumero, " +
"                tabordenesserv.pac_pac_numero, " +
"                tabordenesserv.mtvcorrelativo, " +
"                tabordenesserv.tabencuentro, " +
"                ordfecha, " +
"                FECINGQRF, " +
"                ordestado, " +
"                '00' " +
"              FROM tabordenesserv, " +
"                TabOrdDetCir, " +
"                TabPrgQrf " +
"              WHERE TabPrgQrf.QRFEST(+)         = 'C' " +
"              AND TabOrdenesServ.ORDTIPO        = TabPrgQrf.ORDTIPO(+) " +
"              AND TabOrdenesServ.ORDNUMERO      = TabPrgQrf.ORDNUMERO(+) " +
"              AND TabOrdenesServ.PAC_PAC_NUMERO = TabPrgQrf.PAC_PAC_NUMERO(+) " +
"              AND TabOrdDetCir.PAC_PAC_NUMERO   = TabOrdenesServ.PAC_PAC_NUMERO(+) " +
"              AND TabOrdDetCir.MTVCORRELATIVO   = TabOrdenesServ.MTVCORRELATIVO(+) " +
"              AND TabOrdDetCir.TABENCUENTRO     = TabOrdenesServ.TABENCUENTRO(+) " +
"              AND TabOrdDetCir.ORDTIPO          = TabOrdenesServ.ORDTIPO(+) " +
"              AND TabOrdDetCir.ORDNUMERO        = TabOrdenesServ.ORDNUMERO(+) " +
"              AND TabOrdenesServ.PAC_PAC_NUMERO = '" + Pac_pac_numero + "' " +
"              AND TABORDENESSERV.MTVCORRELATIVO = '" + MtvCorrelativo + "' " +
"              ) a , " +
"              taborddetalle f , " +
"              taborddetcir todc , " +
"              tabordcirest toce , " +
"              HCP_ESTORDEN estord, " +
"              tabordsrvtpo tip, " +
"              CENAUT_GESTIONAUT GESAUT " +
"            WHERE a.ordtipo       = 'P' " +
"            AND a.ordtipo         = tip.ordtipo " +
"            AND a.pac_pac_numero  = f.pac_pac_numero " +
"            AND a.ordtipo         = f.ordtipo " +
"            AND a.ordnumero       = f.ordnumero " +
"            AND a.ordtipo         = todc.ordtipo " +
"            AND a.ordnumero       = todc.ordnumero " +
"            AND a.pac_pac_numero  = todc.pac_pac_numero(+) " +
"            AND todc.ordcirestcod = toce.ordcirestcod " +
"            AND estord.EXMESTCOD  = toce.ordcirestcod " +
"             AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
"             AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
"            AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
"            AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
"            ORDER BY 6 DESC, " +
"              7 ASC " +
"            ) " +
"          ) " +
"        ) b " +
"    ) ";

            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarAsigCamaObjetosSql()
        {
            return getDataTable("uspUsuariosCamaConsultar");
        }
        public DataTable ListaEmpresas()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "select TRIM(a.con_emp_rut) CODEMPRESA, TRIM(a.CON_EMP_DESCRIPCIO) DESCRIPCION  from con_EMPRESA a  where a.con_emp_vigencia='S' ORDER BY 2";


            return getDataTable(sql, "Oracle");
        }
        public DataTable ListarDepartamentos()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "Select trim(dvp_reg_codigo) CODDEP, trim(DVP_REG_GLOSA) DEP from DVP_Region order by 2";


            return getDataTable(sql, "Oracle");
        }

        public DataTable ListarMunicipios()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "Select trim(dvp_reg_codigo) CODDEP, trim(DVP_REG_GLOSA) DEP from DVP_Region order by 2";


            return getDataTable(sql, "Oracle");
        }


        public DataTable ListarConvenios()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "select b.correo CORREO, a.con_con_descripcio CONVENIO from con_convenio a, cenaut_empresas b  where  trim(a.con_emp_rut) = trim(b.codempresa) order by 2";


            return getDataTable(sql, "Oracle");
        }
        public DataTable ListarEmpresasAnexo()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "select b.correo CORREO, a.con_emp_descripcio CONVENIO from con_EMPRESA a, cenaut_empresas b  where  trim(a.con_emp_rut) = trim(b.codempresa) order by 2";


            return getDataTable(sql, "Oracle");
        }
        public DataTable ListarMunicipios(string CodDep)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "Select trim(dvp_pro_CODIGO) CODMUN, trim(dvp_pro_glosa) MUN  from DVP_Provincia where trim(dvp_reg_codigo) = '" + CodDep + "' order by DVP_PRO_CODIGO";


            return getDataTable(sql, "Oracle");
        }
        public DataTable CargarSolicitante(string Usuario)
        {
            string[] nomParam = { "@Usuario" };
            object[] vlrParam = { Usuario };

            return getDataTable("uspUsuariosConsultar", nomParam, vlrParam);
        }
        public DataTable TraerCargo(string Usuario)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "select trim(Usrcargo) CARGO from tbl_user where trim(FLD_USERCODE)='" + Usuario + "'";


            return getDataTable(sql, "Oracle");
        }
        public DataTable generarSolicitudAne1()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select nvl(max(numero), to_char(sysdate,'YYYY') || to_char(sysdate,'MM') ||  to_char(sysdate,'DD') || '0000') + 1 ID  from cenaut_anexo1 WHERE TO_DATE(FECHA, 'YYYY/MM/DD')=tO_DATE(sysdate, 'YYYY/MM/DD') ";
            return getDataTable(sql, "Oracle");
        }
        public DataTable generarSolicitudAne2()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select nvl(max(num_solicitud), to_char(sysdate,'YYYY') || to_char(sysdate,'MM') ||  to_char(sysdate,'DD') || '0000') + 1 ID  from cenaut_anexo2 WHERE TO_DATE(FECHA, 'YYYY/MM/DD')=tO_DATE(sysdate, 'YYYY/MM/DD') ";
            return getDataTable(sql, "Oracle");
        }
        public DataTable generarSolicitudAne3()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select nvl(max(nosolicitud), to_char(sysdate,'YYYY') || to_char(sysdate,'MM') ||  to_char(sysdate,'DD') || '0000') + 1 ID  from cenaut_anexo3 WHERE TO_DATE(FECHA, 'YYYY/MM/DD')=tO_DATE(sysdate, 'YYYY/MM/DD') ";
            return getDataTable(sql, "Oracle");
        }
        public DataTable generarSolicitudAneSoat()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select nvl(max(num_solicitud), to_char(sysdate,'YYYY') || to_char(sysdate,'MM') ||  to_char(sysdate,'DD') || '0000') + 1 ID  from cenaut_anexo_soat WHERE TO_DATE(FECHA, 'YYYY/MM/DD')=tO_DATE(sysdate, 'YYYY/MM/DD')  ";
            return getDataTable(sql, "Oracle");
        }
        public DataTable ListaTiposIdentificacion()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select TRIM(a.TAB_TIPOIDENTCODIGO) TIPOID, TRIM(a.TAB_TIPOIDENTGLOSA) ID  from tab_tipoident a   ORDER BY 2";
            return getDataTable(sql, "Oracle");
        }
        public DataTable consultarConveniosxEmpresa(string CodEmpresa)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "select a.con_con_codigo CODCONVENIO, a.con_con_descripcio DESCRIPCION, decode(b.estado, 0, 'False', 1, 'True', '', 'False') ESTADO from con_convenio a left join cenaut_convenios b on a.con_con_codigo=b.codconvenio where a.con_con_vigencia='S' and trim(a.con_emp_rut)=trim('" + CodEmpresa + "') order by 2";


            return getDataTable(sql, "Oracle");
        }

        public string pruebacontrasena(string Usuario)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            
            sql = "DECLARE " +
    "Ltemp                                 VARCHAR2 (20); " +
    "Lrta                                    VARCHAR2 (100); " +
   " Li                                        NUMBER; " +
   " Lj                                        NUMBER; " +
    "Clave                                 VARCHAR2 (20); " +
   " Gv_fld_userpassword     VARCHAR2 (20); " +
   " K                                         NUMBER; " +
   " Xvalbin                             VARCHAR2 (4000); " +
   " Xbin                                    CHAR (1); " +
   " Xbinhex                             CHAR (1); " +
   " Xbinindex                         CHAR (1); " +
  "  Xvalhex                             VARCHAR2 (9) := '000001110'; " +
   " Xvalindex                         VARCHAR2 (9); " +
   " Xcalculo                            VARCHAR2 (100); " +
  "  In_usuario varchar2(8); " +
" BEGIN " +
 "   In_usuario := ' " + Usuario + "'; " +
  "  BEGIN " +
   "     SELECT fld_userpassword " +
   "     INTO Gv_fld_userpassword " +
    "    FROM TBL_USER " +
     "   WHERE FLD_USERCODE = In_usuario;         " +
  "  END; " +
  "  Clave := TRIM (Gv_fld_userpassword); " +
  "  Lrta := ''; " +
  "  Clave := UPPER (Clave); " +
  "  Li := 1; " +
  "  WHILE Li < (LENGTH (Clave) + 1) LOOP " +
   "     Xvalhex := '000001110'; " +
   "     Ltemp := SUBSTR (Clave, Li, 1); " +
   "     Lj := ASCII (Ltemp); " +
    "    SELECT LPAD (Fnc_number_to_binary (Lj), 9, '0') INTO Xvalbin FROM DUAL; " +
    "    SELECT LPAD (Fnc_number_to_binary (Li), 9, '0') INTO Xvalindex FROM DUAL; " +
     "   K := 1; " +
    "    WHILE K < (LENGTH (TRIM (Xvalbin)) + 1) LOOP " +
      "      Xbin := SUBSTR (Xvalbin, K, 1); " +
       "     Xbinhex := SUBSTR (Xvalhex, K, 1); " +
        "    IF Xbinhex <> Xbin THEN               " +                                                                                                                                        
       "         Xcalculo := Xcalculo || '1'; " +
       "     ELSE    " +                                                                                                                                                      
        "        Xcalculo := Xcalculo || '0'; " +
        "    END IF; " +
       "     K := K + 1; " +
     "   END LOOP; " +
      "  K := 1; " +
      "  Xvalhex := Xcalculo; " +
      "  Xcalculo := ''; " +
     "   WHILE K < (LENGTH (TRIM (Xvalbin)) + 1) LOOP " +
      "      Xbinindex := SUBSTR (Xvalindex, K, 1); " +
      "      Xbinhex := SUBSTR (Xvalhex, K, 1); " +
      "      IF Xbinhex <> Xbinindex THEN           " +                                                                                                                                      
       "         Xcalculo := Xcalculo || '1'; " +
        "    ELSE                                  " +                                                                                                                                                        
       "         Xcalculo := Xcalculo || '0'; " +
       "     END IF; " +
       "     K := K + 1; " +
     "   END LOOP; " +
      "  SELECT Fnc_number_from_binary (Xcalculo) INTO Lj FROM DUAL; " +
      "  Xcalculo := ''; " +
       " Lrta := Lrta || CHR (Lj); " +
       " Li := Li + 1; " +
   " END LOOP; " +
  "  DBMS_OUTPUT.Put_line (In_usuario||'->'||Lrta); " +
" END;";

            return new DA.DA("Oracle").GetDbmsOutputLine(sql);
            //return GetDbmsOutputLine(sql, );
        }
        public DataTable ConsultarInsumosOracle(int TipoVigencia)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            switch (TipoVigencia)
            {
                case 1:
                    {
                        sql = "select decode(b.estadoaut, 0, 'False', 1, 'True', '', 'False') ESTADO_AUT,  TRIM(a.fld_productocodigo) CODINSUMO, TRIM(a.FLD_PRODUCTOGLOSA) DESCRIPCION from aba_producto a left join CENAUT_INSUMOS b on TRIM(a.fld_productocodigo) =TRIM(b.codinsumo)  WHERE a.fld_vigente='1'  ORDER BY TRIM(a.fld_productocodigo)";
                        break;
                    }
                case 2:
                    {
                        sql = "select decode(b.estadoaut, 0, 'False', 1, 'True', '', 'False') ESTADO_AUT,  TRIM(a.fld_productocodigo) CODINSUMO, TRIM(a.FLD_PRODUCTOGLOSA) DESCRIPCION from aba_producto a left join CENAUT_INSUMOS b on TRIM(a.fld_productocodigo) =TRIM(b.codinsumo)  WHERE a.fld_vigente='0'  ORDER BY TRIM(a.fld_productocodigo)";
                        break;
                    }
            }
            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarPrestacionesSql(int TipoVigencia)
        {
            string[] nomParam = { "@TipoVigencia" };
            object[] vlrParam = { TipoVigencia };

            return getDataTable("uspPrestacionesConsultar", nomParam, vlrParam);
            //return getDataTable("uspPrestacionesVigentesConsultar", "Sql");
        }

        public bool PrestacionesActualizar(string CodPrestacion, bool Estado, string Usuario)
        {
            bool bandera;
            string sqlconsulta = "Select codprestacion from cenaut_prestacion where codprestacion='" + CodPrestacion + "'";
            DataTable existe =getDataTable(sqlconsulta, "Oracle");
            string SQL = "";
            if (existe.Rows.Count == 0)
            {
                if (Estado == true)
                {
                    SQL = "INSERT INTO cenaut_prestacion (CODPRESTACION, ESTADOAUT,USUADI,FECADI) VALUES ('" + CodPrestacion + "', 1, '" + Usuario + "',sysdate)";
                }
            }
            else
            {
                int valorestado = 0;
                if (Estado == true)
                { valorestado = 1; }
                else { valorestado = 0; }
                //if (Estado == false)
                //{
                    SQL = "UPDATE cenaut_prestacion SET ESTADOAUT =" + valorestado + ", USUMOD ='" + Usuario + "', FECMOD = sysdate WHERE CODPRESTACION ='" + CodPrestacion + "'";
                    //SQL = "Delete cenaut_prestacion WHERE CODPRESTACION ='" + CodPrestacion + "'";
                //}
            }
            if (SQL == "")
            {
                bandera = true;
            }
            else 
            { 
                bandera= EjecutarSentenciaOracle(SQL); 
            }
            return bandera;
        }
        public bool ConveniosActualizar(string CodConvenio, bool Estado, string Usuario)
        {
            bool bandera;
            string sqlconsulta = "Select codconvenio from cenaut_convenios where CODCONVENIO='" + CodConvenio + "'";
            DataTable existe = getDataTable(sqlconsulta, "Oracle");
            string SQL = "";
            if (existe.Rows.Count == 0)
            {
                if (Estado == true)
                {
                    SQL = "INSERT INTO cenaut_convenios (CODCONVENIO, ESTADO,USUADI,FECADI) VALUES ('" + CodConvenio + "', 1, '" + Usuario + "',sysdate)";
                }
            }
            else
            {
                int valorestado = 0;
                if (Estado == true)
                { valorestado = 1; }
                else { valorestado = 0; }
                //if (Estado == false)
                //{
                SQL = "UPDATE cenaut_convenios SET ESTADO =" + valorestado + ", USUMOD ='" + Usuario + "', FECMOD = sysdate WHERE codconvenio ='" + CodConvenio + "'";
                //SQL = "Delete cenaut_prestacion WHERE CODPRESTACION ='" + CodPrestacion + "'";
                //}
            }
            if (SQL == "")
            {
                bandera = true;
            }
            else
            {
                bandera = EjecutarSentenciaOracle(SQL);
            }
            return bandera;
        }
        public bool InsumosActualizar(string CodInsumo, bool Estado, string Usuario)
        {
            bool bandera;
            string sqlconsulta = "Select codinsumo from cenaut_insumos where codinsumo='" + CodInsumo + "'";
            DataTable existe = getDataTable(sqlconsulta, "Oracle");
            string SQL = "";
            if (existe.Rows.Count == 0)
            {
                if (Estado == true)
                {
                    SQL = "INSERT INTO cenaut_insumos (CODINSUMO, ESTADOAUT,USUADI,FECADI) VALUES ('" + CodInsumo + "', 1, '" + Usuario + "',sysdate)";
                }
            }
            else
            {
                int valorestado = 0;
                if (Estado == true)
                { valorestado = 1; }
                else { valorestado = 0; }
                //if (Estado == false)
                //{
                SQL = "UPDATE cenaut_insumos SET ESTADOAUT =" + valorestado + ", USUMOD ='" + Usuario + "', FECMOD = sysdate WHERE CODINSUMO ='" + CodInsumo + "'";
                //SQL = "Delete cenaut_prestacion WHERE CODPRESTACION ='" + CodPrestacion + "'";
                //}
            }
            if (SQL == "")
            {
                bandera = true;
            }
            else
            {
                bandera = EjecutarSentenciaOracle(SQL);
            }
            return bandera;
        }
        public bool EmpresasActualizar(string CodEmpresa, string Correo, string Usuario)
        {
            bool bandera=true;
            string sqlconsulta = "Select codempresa from cenaut_empresas where codempresa='" + CodEmpresa + "'";
            DataTable existe = getDataTable(sqlconsulta, "Oracle");
            string SQL = "";
            if (Correo != "")
            {
                if (existe.Rows.Count == 0)
                {
                    SQL = "INSERT INTO cenaut_empresas (CODEMPRESA, CORREO,USUADI,FECADI) VALUES ('" + CodEmpresa + "', '" + Correo + "', '" + Usuario + "',sysdate)";
                }
                else
                {
                    SQL = "UPDATE cenaut_empresas SET CORREO ='" + Correo + "', USUMOD ='" + Usuario + "', FECMOD = sysdate WHERE codempresa ='" + CodEmpresa + "'";
                }
                if (SQL == "")
                {
                    bandera = true;
                }
                else
                {
                    bandera = EjecutarSentenciaOracle(SQL);
                }
            }
            return bandera;
        }
        public bool EmpresasActualizarUnitaria(string CodEmpresa, string Correo, string Usuario)
        {
            
            string SQL = "UPDATE cenaut_empresas SET CORREO ='" + Correo + "', USUMOD ='" + Usuario + "', FECMOD = sysdate WHERE codempresa ='" + CodEmpresa + "'";
            return EjecutarSentenciaOracle(SQL);
        }
        public bool gestionAutActualizar(string Pac_pac_numero, string MtvCorrelativo, string Orden, string CodPrestacion, string Convenio, string Responsable, string CodEstado, string Observacion, int Alerta, int valorALerta, string CodTiempoAlerta, string FecSolicitud, string valorFecha, DateTime fechaConvertir, string Usuario)
        {
            bool bandera=false;
            string sql1 = "";
            string SQL = "";
            string FechaEvento = "FECSOLICITUD";
            string CodEstadobd = "";
            sql1 = "select COUNT(ORDNUMERO) CANTIDAD from CENAUT_GESTIONAUT  where PAC_PAC_NUMERO='" + Pac_pac_numero + "' AND MTVCORRELATIVO='" + MtvCorrelativo + "' AND ORDNUMERO='" + Orden + "' AND CODPRESTACION='" + CodPrestacion + "' ";
            DataRow Valor = getDataTable1(sql1, "Oracle").Rows[0];
            string sqlConsulta = "select CODESTADO from CENAUT_GESTIONAUT  where PAC_PAC_NUMERO='" + Pac_pac_numero + "' AND MTVCORRELATIVO='" + MtvCorrelativo + "' AND ORDNUMERO='" + Orden + "' AND CODPRESTACION='" + CodPrestacion + "' ";
            if (getDataTable1(sqlConsulta, "Oracle").Rows.Count > 0)
            {
                CodEstadobd = getDataTable1(sqlConsulta, "Oracle").Rows[0].ToString();
            }
            else
            {
                CodEstadobd = "0";
            }

            
            if (Convert.ToInt32(Valor[0].ToString()) == 0)
            {
                string FECPRO = "FECPROAUT";
                string USUPRO = "USUPROAUT";
                if (CodEstado == "5")
                {
                    Alerta = 0;
                    FECPRO = "FECCANCELA";
                    USUPRO = "USUCANCELA";
                }
                SQL = "INSERT INTO CENAUT_GESTIONAUT " +
                       "(CODPRESTACION " +
                       ",PAC_PAC_NUMERO " +
                       ",CON_CON_CODIGO " +
                       ",RESPONSABLE " +
                       ",CODESTADO " +
                       ",OBSERVACION " +
                       ",ALERTA " +
                       ",VALORALERTA " +
                       ",CODTIEMPOALERTA " +
                       ",FECPENDGESTION " +
                       ",FECSOLICITUD " +
                       "," + FECPRO + " " +
                       "," + USUPRO + " " +
                       ",MTVCORRELATIVO " +
                       ",ORDNUMERO ) " +
                       " VALUES " +
                       "('" + CodPrestacion + "' " +
                       ", '" + Pac_pac_numero + "' " +
                       ", '" + Convenio + "' " +
                       ", '" + Responsable + "' " +
                       ", '" + CodEstado + "' " +
                       ", '" + Observacion + "' " +
                       ", " + Alerta + " " +
                       ", '" + valorALerta + "' " +
                       ", '" + CodTiempoAlerta + "' " +
                       ", TO_DATE('" + fechaConvertir.ToString("yyyy/MM/dd") + " " + FecSolicitud + " " + valorFecha + "', 'YYYY/MM/DD HH:MI:SS AM') " +
                       ", TO_DATE('" + fechaConvertir.ToString("yyyy/MM/dd") + " " + FecSolicitud + " " + valorFecha + "', 'YYYY/MM/DD HH:MI:SS AM') " +
                       ", SYSDATE " +
                       ", '" + Usuario + "' " +
                       ", '" + MtvCorrelativo + "' " +
                       ", '" + Orden + "') ";
            }
            string FECUSU = "", ValAlerta = "";
            if (CodEstado == "2")
            {
                FECUSU = " FECPROAUT=sysdate, USUPROAUT='" + Usuario + "' ";
                ValAlerta = " ALERTA= " + Alerta + " ";
                FechaEvento = "FECPROAUT";
            }
            if (CodEstado == "3" || CodEstado == "4")
            {
                FECUSU = " FECHA_APRO_NEG=sysdate, USUA_APRO_NEG='" + Usuario + "' ";
                ValAlerta = " ALERTA=0 ";
                FechaEvento = "FECHA_APRO_NEG";
            }
            if (CodEstado == "5")
            {
                FECUSU = " FECCANCELA=sysdate, USUCANCELA='" + Usuario + "' ";
                ValAlerta = " ALERTA= 0 ";
                FechaEvento = "FECCANCELA";
            }
            if (CodEstado == "6")
            {
                FECUSU = " FECDEVUELTA=sysdate, USUDEVUELTA='" + Usuario + "' ";
                ValAlerta = " ALERTA= " + Alerta + " ";
                FechaEvento = "FECDEVUELTA";
            }
            if (Convert.ToInt32(Valor[0].ToString()) > 0)
            {
                
                
                SQL = "UPDATE CENAUT_GESTIONAUT SET " +
                       " CODESTADO = '" + CodEstado + "' " +
                       ",OBSERVACION = '" + Observacion + "' " +
                       "," + ValAlerta + " " +
                       ",VALORALERTA= '" + valorALerta + "' " +
                       ",CODTIEMPOALERTA= '" + CodTiempoAlerta + "' " +
                       "," + FECUSU + " " +
                       "WHERE PAC_PAC_NUMERO= '" + Pac_pac_numero + "' AND MTVCORRELATIVO='" + MtvCorrelativo + "' AND ORDNUMERO='" + Orden + "' AND CODPRESTACION='" + CodPrestacion + "' ";

            }
            if (EjecutarSentenciaOracle(SQL))
            {
                if (CodEstado != CodEstadobd[0].ToString())
                {
                    string sqlEventos = "INSERT INTO CENAUT_AUTEVENTOS " +
                                                " SELECT '" + Orden + "', '" + CodPrestacion + "', '" + Pac_pac_numero + "', '" + MtvCorrelativo + "', '" + CodEstado + "', " + FechaEvento + ", '" + Usuario + "' " +
                                                " from cenaut_gestionaut " +
                                                " WHERE PAC_PAC_NUMERO= '" + Pac_pac_numero + "' AND MTVCORRELATIVO='" + MtvCorrelativo + "' AND ORDNUMERO='" + Orden + "' AND CODPRESTACION='" + CodPrestacion + "' ";
                    if (EjecutarSentenciaOracle(sqlEventos))
                    {
                        bandera = true;
                    }
                }
            }
            return bandera;
        }
        public string gestiontraersql(string Pac_pac_numero, string MtvCorrelativo, string Orden, string CodPrestacion, string Convenio, string Responsable, string CodEstado, string Observacion, int Alerta, int valorALerta, string CodTiempoAlerta, string FecSolicitud, string valorFecha, DateTime fechaConvertir, string Usuario)
        {
            
            string SQL = "";
            SQL = "INSERT INTO CENAUT_GESTIONAUT " +
                    "(CODPRESTACION " +
                    ",PAC_PAC_NUMERO " +
                    ",CON_CON_CODIGO " +
                    ",RESPONSABLE " +
                    ",CODESTADO " +
                    ",OBSERVACION " +
                    ",ALERTA " +
                    ",VALORALERTA " +
                    ",CODTIEMPOALERTA " +
                    ",FECPENDGESTION " +
                    ",FECSOLICITUD " +
                    ",FECPROAUT " +
                    ",USUPROAUT " +
                    ",MTVCORRELATIVO " +
                    ",ORDNUMERO ) " +
                    " VALUES " +
                    "('" + CodPrestacion + "' " +
                    ", '" + Pac_pac_numero + "' " +
                    ", '" + Convenio + "' " +
                    ", '" + Responsable + "' " +
                    ", '" + CodEstado + "' " +
                    ", '" + Observacion + "' " +
                    ", " + Alerta + " " +
                    ", '" + valorALerta + "' " +
                    ", '" + CodTiempoAlerta + "' " +
                    ", TO_DATE('" + fechaConvertir.ToString("yyyy/MM/dd") + " " + FecSolicitud + " " + valorFecha + "', 'YYYY/MM/DD HH:MI:SS A.M.') " +
                    ", TO_DATE('" + fechaConvertir.ToString("yyyy/MM/dd") + " " + FecSolicitud + " " + valorFecha + "', 'YYYY/MM/DD HH:MI:SS A.M.') " +
                    ", SYSDATE " +
                    ", '" + Responsable + "' " +
                    ", '" + MtvCorrelativo + "' " +
                    ", '" + Orden + "') ";
                return SQL;
        }
        public bool prestacionAutActualizar(string OrdNombre, string OrdNumero, string CodPrestacion, string FechaSolic, string OrdTipo, string Pac_Pac_Rut, string Pac_Pac_Numero, string MtvCorrelativo)
        {
            bool bandera = false;
            string SQL = "";
            SQL = "INSERT INTO CENAUT_AGREGARPRESTACION " +
                    "(ORDNOMBRE " +
                    ",ORDNUMERO " +
                    ",CODPRESTACION " +
                    ",FECHASOLIC " +
                    ",ORDTIPO " +
                    ",ID " +
                    ",PAC_PAC_NUMERO " +
                    ",MTVCORRELATIVO ) " +
                    " VALUES " +
                    "('" + OrdNombre + "' " +
                    ", '" + OrdNumero + "' " +
                    ", '" + CodPrestacion + "' " +
                    ", " + FechaSolic + " " +
                    ", '" + OrdTipo + "' " +
                    ", '" + Pac_Pac_Rut + "' " +
                    ", '" + Pac_Pac_Numero + "' " +
                    ", '" + MtvCorrelativo + "') ";
            if (EjecutarSentenciaOracle(SQL))
            {
                bandera = true;
            }
            return bandera;
        }
        public bool ResponsableActualizarC(string Cama, string IdResponsable, string Usuario)
        {
            string SQL = "";
            string sql1 = "Select COUNT(*) from CENAUT_ASIGNACAMA where cama='" + Cama + "'";
            DataRow Valor = getDataTable1(sql1, "Oracle").Rows[0];
            string[] nomParam = { "@Usuario" };
            object[] vlrParam = { IdResponsable };


            DataRow Nombres = getDataTable("uspNombreUsuarioConsultar", nomParam, vlrParam).Rows[0];
            if (Convert.ToInt32(Valor[0].ToString()) == 0)
            {
                SQL = "INSERT INTO CENAUT_ASIGNACAMA (ID, CAMA, RESPONSABLE, NOMBRES, FECCRE, USUCRE) " +
                       " select nvl(max(id)+1,1), '" + Cama + "', '" + IdResponsable + "', '" + Nombres[0].ToString() + "', SYSDATE, '" + Usuario + "' from CENAUT_ASIGNACAMA";
            }
            else
            {
                SQL = "UPDATE CENAUT_ASIGNACAMA " +
                   " SET  " +
                    "  Responsable = '" + IdResponsable + "' " +
                     "  ,nombres = '" + Nombres[0].ToString() + "' " +
                    "  ,FecMod = SYSDATE " +
                    "  ,UsuMod = '" + Usuario + "' " +
                    " WHERE cama='" + Cama + "' ";
            }

            return (EjecutarSentenciaOracle(SQL));
            

        }

        public string NivelRetirar(Nivel nvl)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { nvl.Codigo };

            DataTable dt = getDataTable("uspNivelRetirar", nomParam, vlrParam);
            string Msg = "3";
            if(dt.Rows.Count > 0)
                Msg = dt.Rows[0]["MSG"].ToString();

            return Msg;
        }

        #region Metodos Privados

        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }
        private DataTable getDataTable(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }
        private string getDataTablePrueba(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTableprueba(sql);
        }
        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {            
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }
        private bool EjecutarSentenciaOracle(string sql)
        {
            return new DA.DA("Oracle").EjecutarSentencia(sql);
            //return new DA.DA().EjecutarSentenciaOracle(sql);
        }
        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }
        private DataTable getDataTable1(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }
        #endregion

    }
}
