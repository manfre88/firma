﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LConsultas

    {
        public DataSet ConsultarGestion(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT " +
                          "  A.CODPRESTACION, TRIM(GlbGetNomPre(A.CODPRESTACION)) NOMPRESTACION, " +
                          "   A.PAC_PAC_NUMERO, GlbGetPacCar(A.PAC_PAC_NUMERO) IDENTIFICACION, GlbGetNomPac(A.PAC_PAC_NUMERO) NOMBREPAC, " +
                           "   A.CON_CON_CODIGO, B.CON_CON_DESCRIPCIO NOMCONVENIO, " +
                           "   A.RESPONSABLE, A.CODESTADO, DECODE(NVL(A.CODESTADO, 1), '1','Pendiente de Gestión', '2','En proceso de autorización','3','Autorizado','4','Negado','5','Cancelado','6','Devuelto') DESESTADO, " +
                            "  A.OBSERVACION, " +
                            "  A.FECPENDGESTION, A.FECPROAUT, A.USUPROAUT,  A.FECHA_APRO_NEG, A.USUA_APRO_NEG, A.FECSOLICITUD,  " +
                            "  A.FECCANCELA, A.USUCANCELA, A.FECDEVUELTA, A.USUDEVUELTA, A.MTVCORRELATIVO EVENTO, A.ORDNUMERO, TRIM(FLD_USERNAME) NOMRESPONSABLE " +
                            "  FROM CENAUT_GESTIONAUT A, CON_CONVENIO B, TBL_USER C " +
                            "  WHERE A.CON_CON_CODIGO=B.CON_CON_CODIGO AND A.RESPONSABLE=C.FLD_USERCODE " +
                            "  AND A.FECPENDGESTION >= TO_DATE('" + FechaIni + "', 'YYYY/MM/DD') AND A.FECPENDGESTION <= TO_DATE('" + FechaFin + "', 'YYYY/MM/DD')+1  " +
                            "  ORDER BY  A.FECPENDGESTION DESC";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataTable ConsultarPrueba(string FechaIni, string FechaFin)
        {

            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "  SELECT '1', " +
                         "  A.CODPRESTACION, TRIM(GlbGetNomPre(A.CODPRESTACION)) NOMPRESTACION, " +
                         "   A.PAC_PAC_NUMERO, GlbGetPacCar(A.PAC_PAC_NUMERO) IDENTIFICACION, GlbGetNomPac(A.PAC_PAC_NUMERO) NOMBREPAC, " +
                         "   A.CON_CON_CODIGO, B.CON_CON_DESCRIPCIO NOMCONVENIO, " +
                         "    A.RESPONSABLE, A.CODESTADO, DECODE(NVL(A.CODESTADO, 1), '1','Pendiente de Gestión', '2','En proceso de autorización','3','Autorizado','4','Negado','5','Cancelado','6','Devuelto') DESESTADO, " +
                         //"    A.OBSERVACION, " +
                         "   REPLACE(REPLACE(REPLACE(A.OBSERVACION,CHR(10),' ') ,CHR(13),' ') ,' ',' ') OBSERVACION, "  +
                         "    A.FECPENDGESTION, A.FECPROAUT, A.USUPROAUT,  A.FECHA_APRO_NEG, A.USUA_APRO_NEG, A.FECSOLICITUD,  " +
                         "    A.FECCANCELA, A.USUCANCELA, A.FECDEVUELTA, A.USUDEVUELTA, A.MTVCORRELATIVO EVENTO, A.ORDNUMERO " +
                         "    , TO_NUMBER(ROUND ((A.FECPROAUT-A.FECSOLICITUD)*24, 4)) HORASREALES " +
                         "    FROM CENAUT_GESTIONAUT A, CON_CONVENIO B " +
                         "    WHERE A.CON_CON_CODIGO=B.CON_CON_CODIGO " +
                         "    AND A.FECPENDGESTION >= TO_DATE('" + FechaIni + "', 'YYYY/MM/DD') AND A.FECPENDGESTION <= TO_DATE('" + FechaFin + "', 'YYYY/MM/DD')+1 " +
                         "    and A.FECPROAUT is not null AND A.PAC_PAC_NUMERO  NOT IN (43624, 5024, 1308) " +
                         //"    and a.codprestacion in ( '879113B ', '879111  ') and a.ordnumero in ('1704002391', '1704002471') " +
                         "    order by A.FECSOLICITUD";

            return getDataTable(sql, "Oracle");
        }
        public DataTable ConsultarFestivos(string Fecha)
        {


            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select DCH_FER_DIA from DCH_FERIADO where DCH_FER_DIA=TO_DATE('" + Fecha + "', 'YYYY/MM/DD')";

            return getDataTable(sql, "Oracle");
        }




        public string ConsultarGestion1(int TipoVigencia)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql="";
            switch (TipoVigencia)
            {
                case 1:
                    {
                        sql = "select decode(b.estadoaut, 0, 'False', 1, 'True', '', 'False') ESTADO_AUT,  TRIM(a.pre_pre_codigo) CODPRESTACION, TRIM(a.PRE_PRE_DESCRIPCIO) DESCRIPCION from pre_prestacion a left join CENAUT_PRESTACION b on TRIM(a.pre_pre_codigo) =TRIM(b.codprestacion)  WHERE a.PRE_PRE_VIGENCIA='S'  ORDER BY TRIM(a.pre_pre_codigo)";
                        break;
                    }
                case 2:
                    {
                        sql = "select decode(b.estadoaut, 0, 'False', 1, 'True', '', 'False') ESTADO_AUT,  TRIM(a.pre_pre_codigo) CODPRESTACION, TRIM(a.PRE_PRE_DESCRIPCIO) DESCRIPCION from pre_prestacion a left join CENAUT_PRESTACION b on TRIM(a.pre_pre_codigo) =TRIM(b.codprestacion)  WHERE a.PRE_PRE_VIGENCIA='N'  ORDER BY TRIM(a.pre_pre_codigo)";
                        break;
                    }
            }
            return getDataTablePrueba(sql, "Oracle");
        }
        public DataTable EstadosConsultar()
        {
            return getDataTable("uspEstadosConsultar");
        }
        public DataTable ConsultarPendientes()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = " SELECT OrdNombre, " +
" OrdNumero, " +
" PreCod, " +
" PreNom, " +
" FechaRevis, " +
" FechaSolic, " +
" ORDEN, " +
" Estado, " +
" FechaConfirm, " +
" tipoformu, " +
" numerformu, " +
" ordtipo, " +
" ExamCit, " +
" forcod, " +
" forvrs, " +
" ulteve " +
" , DECODE(NVL(CODESTADO,1), 1, 'Pendiente de Gestión', 2, 'En proceso de autorización', 3, 'Autorizado', 4, 'Negado', 5, 'Cancelado', 6, 'Devuelto') CODESTADO,  " +
" OBSERVACION, " +
" ALERTA, " +
" NVL(VALORALERTA,0) VALORALERTA, " +
" CODTIEMPOALERTA " +
" ,MTVCORRELATIVO " +
" , FunGlbGetRutPac(PAC_PAC_NUMERO) ID, PAC_PAC_NUMERO " +
" ,CamaServicio " +
" , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
" FROM " +
" (SELECT b.*, " +
" rownum registro " +
" FROM " +
" (SELECT OrdNombre, " +
" OrdNumero, " +
" PreCod, " +
" PreNom, " +
" FechaRevis, " +
" FechaSolic, " +
" ORDEN, " +
" Estado, " +
" FechaConfirm, " +
" tipoformu, " +
" numerformu, " +
" ordtipo, " +
" ExamCit, " +
" forcod, " +
" forvrs, " +
" ulteve " +
" ,CODESTADO, " +
" OBSERVACION, " +
" ALERTA, " +
" VALORALERTA, " +
" CODTIEMPOALERTA " +
" ,MTVCORRELATIVO " +
" ,PAC_PAC_NUMERO " +
" ,CamaServicio " +
" , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
" FROM " +
" (SELECT OrdNombre, " +
" OrdNumero, " +
" PreCod, " +
" PreNom, " +
" FechaRevis, " +
" FechaSolic, " +
" ORDEN, " +
" Estado, " +
" FechaConfirm, " +
" tipoformu, " +
" numerformu, " +
" ordtipo, " +
" ExamCit, " +
" forcod, " +
" forvrs, " +
" ulteve " +
" ,CODESTADO, " +
" OBSERVACION, " +
" ALERTA, " +
" VALORALERTA, " +
" CODTIEMPOALERTA " +
" ,MTVCORRELATIVO " +
" ,PAC_PAC_NUMERO " +
" ,CamaServicio " +
" , cama, RESPONSABLE, NOMBRES, CONVENIO, ser_ser_codigo " +
" FROM " +
" ( " +
" SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') ordnombre , " +
" ord.ordnumero ordnumero , " +
" exa.cex_exs_codigprest precod , " +
" glbgetnompre (exa.cex_exs_codigprest) prenom , " +
" DECODE (inf.cex_inf_fecharevis, NULL, exa.cex_exs_fechamuest, inf.cex_inf_fecharevis) fecharevis , " +
" ord.ORDFECHA fechasolic , " +
" estord.ORDEN , " +
" est.EXMESTNOM Estado , " +
" DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm , " +
" exa.ate_pre_tipoformu tipoformu , " +
" exa.ate_pre_numerformu numerformu , " +
" NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
" patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit , " +
" inf.lab_for_codigo forcod , " +
" inf.lab_for_version forvrs , " +
" exa.cex_exs_estado ulteve " +
" ,GESAUT.CODESTADO, " +
" GESAUT.OBSERVACION, " +
" GESAUT.ALERTA, " +
" GESAUT.VALORALERTA, " +
" GESAUT.CODTIEMPOALERTA " +
" ,ORD.MTVCORRELATIVO " +
" ,ORD.PAC_PAC_NUMERO " +
" ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
" ,est.ser_obj_codigo  cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
" FROM " +
" (SELECT ordtipo , " +
" ordnumero , " +
" pac_pac_numero , " +
" mtvcorrelativo , " +
" tabencuentro , " +
" ordfecha , " +
" ordestado " +
" FROM tabordenesserv " +
" ) ord , " +
" Gt_Paraclinicos exa , " +
" cex_informes inf , " +
" tabexmest est , " +
" tabordsrvtpo tip , " +
" HCP_ESTORDEN estord, " +
" CENAUT_GESTIONAUT GESAUT " +
" ,ATC_ESTADIA EST " +
" , ser_servicios ser " +
" , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
" WHERE ord.ordtipo            IN ('L', 'I', 'A') " +
" AND exa.ordtipo               = ord.ordtipo " +
" AND exa.ordnumero             = ord.ordnumero " +
" AND exa.pac_pac_numero        = ord.pac_pac_numero " +
" AND inf.ate_pre_tipoformu(+)  = exa.ate_pre_tipoformu " +
" AND inf.ate_pre_numerformu(+) = exa.ate_pre_numerformu " +
" AND inf.cex_exs_codigprest(+) = exa.cex_exs_codigprest " +
" AND est.exmestcod             = exa.cex_exs_estado " +
" AND tip.ordtipo(+)            = exa.ordtipo " +
" AND estord.EXMESTCOD          = est.exmestcod " +
" AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
" AND EXA.ordnumero=GESAUT.ORDNUMERO(+) " +
" AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+) " +
" AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
" AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
" AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
" and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND EXA.cex_exs_codigprest=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' and est.ser_obj_codigo " +
" is not null and est.ser_obj_codigo<>'        ' and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+)  AND GESAUT.CODESTADO IN (1,2,6) " +
" UNION ALL " +
" SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') , " +
" NVL(ord.ordnumero,exa.ate_pre_numerformu) , " +
" exa.cex_exs_codigprest , " +
" glbgetnompre(exa.cex_exs_codigprest) , " +
" DECODE (inf.hcp_inf_fechadigit,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),inf.hcp_inf_fechadigit) , " +
" ord.ORDFECHA , " +
" estord.ORDEN , " +
" est.EXMESTNOM Estado , " +
" DECODE (exa.fechaconfirm, NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'), exa.fechaconfirm) fechaconfirm , " +
" exa.ate_pre_tipoformu tipoformu , " +
" exa.ate_pre_numerformu numerformu , " +
" NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
" patgettpores(exa.ate_pre_tipoformu, exa.ate_pre_numerformu, exa.pac_pac_numero) ExamCit , " +
" inf.hcp_inf_codigforma , " +
" inf.hcp_inf_version , " +
" est.exmestcod " +
" ,GESAUT.CODESTADO, " +
" GESAUT.OBSERVACION, " +
" GESAUT.ALERTA, " +
" GESAUT.VALORALERTA, " +
" GESAUT.CODTIEMPOALERTA " +
" ,ORD.MTVCORRELATIVO " +
" ,ORD.PAC_PAC_NUMERO " +
" ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
" ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO , trim(ser.ser_ser_codigo) ser_ser_codigo " +
" FROM " +
" (SELECT ordtipo , " +
" ordnumero , " +
" pac_pac_numero , " +
" mtvcorrelativo , " +
" tabencuentro , " +
" ordfecha , " +
" ordestado " +
" FROM tabordenesserv " +
" ) ord , " +
" Gt_Paraclinicos exa , " +
" hcp_informes inf , " +
" tabexmest est , " +
" tabordsrvtpo tip , " +
" HCP_ESTORDEN estord, " +
" CENAUT_GESTIONAUT GESAUT " +
" ,ATC_ESTADIA EST " +
" , ser_servicios ser " +
" , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig , CON_CONVENIO CON " +
" WHERE exa.ordtipo             = 'D' " +
" AND ord.ordtipo NOT          IN ('L', 'I', 'A', 'P') " +
" AND exa.ordnumero             = ord.ordnumero " +
" AND exa.pac_pac_numero        = ord.pac_pac_numero " +
" AND inf.hcp_inf_numerpacie(+) = exa.pac_pac_numero " +
" AND inf.hcp_inf_tipoformu(+)  = exa.ate_pre_tipoformu " +
" AND inf.hcp_inf_numerformu(+) = exa.ate_pre_numerformu " +
" AND inf.pre_pre_codigo(+)     = exa.cex_exs_codigprest " +
" AND est.exmestcod             = exa.cex_exs_estado " +
" AND estord.EXMESTCOD          = est.exmestcod " +
" AND tip.ordtipo(+)            = ord.ordtipo " +
" AND EXA.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
" AND EXA.ordnumero=GESAUT.ORDNUMERO(+) " +
" AND EXA.cex_exs_codigprest=GESAUT.CODPRESTACION(+) " +
" AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
" AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
" AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
" and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND EXA.cex_exs_codigprest=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' " +
" and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+)  AND GESAUT.CODESTADO IN (1,2,6) " +
" UNION ALL " +
" SELECT NVL(tip.ordnombre, 'Orden Sin Nombre') , " +
" det.ordnumero , " +
" det.pre_pre_codigo , " +
" glbgetnompre(det.pre_pre_codigo) , " +
" DECODE(srv.ordfecha,NULL, TO_DATE('1900/01/01', 'YYYY/MM/DD'),srv.ordfecha) , " +
" srv.ORDFECHA , " +
" 0 ORDEN , " +
" 'Examen Solicitado' , " +
" TO_DATE('1900/01/01', 'YYYY/MM/DD') , " +
" '' tipoformu , " +
" '' numerformu , " +
" NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
" 0 , " +
" ' ' , " +
" ' ' , " +
" '00' " +
" ,GESAUT.CODESTADO, " +
" GESAUT.OBSERVACION, " +
" GESAUT.ALERTA, " +
" GESAUT.VALORALERTA, " +
" GESAUT.CODTIEMPOALERTA " +
" ,srv.MTVCORRELATIVO " +
" ,srv.PAC_PAC_NUMERO " +
" ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
" ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO , trim(ser.ser_ser_codigo) ser_ser_codigo " +
" FROM " +
" (SELECT ordtipo , " +
" ordnumero , " +
" pac_pac_numero , " +
" mtvcorrelativo , " +
" tabencuentro , " +
" ordfecha , " +
" ordestado " +
" FROM tabordenesserv " +
" ) srv , " +
" taborddetalle det , " +
" tabordsrvtpo tip, " +
" CENAUT_GESTIONAUT GESAUT " +
" ,ATC_ESTADIA EST " +
" , ser_servicios ser " +
" , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
" WHERE srv.ordtipo NOT IN ('P','B') " +
" AND det.mtvcorrelativo = srv.mtvcorrelativo " +
" AND det.ordtipo        = srv.ordtipo " +
" AND det.ordnumero      = srv.ordnumero " +
" AND det.tabencuentro   = srv.tabencuentro " +
" AND det.pac_pac_numero = srv.pac_pac_numero " +
" AND tip.ordtipo(+)     = det.ordtipo " +
" AND DET.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
" AND DET.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
" AND DET.ordnumero=GESAUT.ORDNUMERO(+) " +
" AND DET.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
" AND srv.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
" AND srv.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
" AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
" and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND DET.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' " +
" and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+)   AND GESAUT.CODESTADO IN (1,2,6) " +
" AND (SELECT COUNT (1) " +
" FROM rpa_forlab lab, " +
" cex_examsolic ex " +
" WHERE lab.ordtipo          = DECODE(srv.ordtipo, 'A', 'A', 'I', 'I', 'L', 'L', 'D') " +
" AND lab.ordnumero          = srv.ordnumero " +
" AND lab.pac_pac_numero     = srv.pac_pac_numero " +
" AND lab.ATE_PRE_TIPOFORMU  = ex.ate_pre_tipoformu " +
" AND lab.ATE_PRE_NUMERFORMU = ex.ate_pre_numerformu " +
" AND lab.PAC_PAC_NUMERO     = ex.PAC_PAC_NUMERO " +
" AND ex.CEX_EXS_CODIGPREST  = det.pre_pre_codigo ) >= 0 " +
" UNION ALL " +
" SELECT DISTINCT tip.ordnombre , " +
" f.ordnumero , " +
" f.pre_pre_codigo , " +
" glbgetnompre(f.pre_pre_codigo) , " +
" DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) , " +
" a.ordfecha , " +
" estord.ORDEN, " +
" toce.ORDCIRESTNOM, " +
" a.FECINGQRF , " +
" '' tipoformu , " +
" '' numerformu , " +
" NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
" 0 , " +
" ' ' , " +
" ' ' , " +
" '00' " +
" ,GESAUT.CODESTADO, " +
" GESAUT.OBSERVACION, " +
" GESAUT.ALERTA, " +
" GESAUT.VALORALERTA, " +
" GESAUT.CODTIEMPOALERTA " +
" ,A.MTVCORRELATIVO " +
" ,A.PAC_PAC_NUMERO " +
" ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
" ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
" FROM " +
" (SELECT tabordenesserv.ordtipo , " +
" tabordenesserv.ordnumero , " +
" tabordenesserv.pac_pac_numero , " +
" tabordenesserv.mtvcorrelativo , " +
" tabordenesserv.tabencuentro , " +
" ordfecha , " +
" FECINGQRF, " +
" ordestado , " +
" '00' " +
" FROM tabordenesserv , " +
" TabPrgQrf " +
" WHERE " +
" TabOrdenesServ.ORDTIPO          = TabPrgQrf.ORDTIPO " +
" AND TabOrdenesServ.ORDNUMERO        = TabPrgQrf.ORDNUMERO " +
" AND TabOrdenesServ.PAC_PAC_NUMERO   = TabPrgQrf.PAC_PAC_NUMERO " +
" ) a , " +
" taborddetalle f , " +
" taborddetcir todc , " +
" tabordcirest toce , " +
" HCP_ESTORDEN estord, " +
" tabordsrvtpo tip, " +
" CENAUT_GESTIONAUT GESAUT " +
" ,ATC_ESTADIA EST " +
" , ser_servicios ser " +
" , cenaut_prestacion pres , ser_objetos OBJ , CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
" WHERE a.ordtipo       = 'P' " +
" AND a.ordtipo         = tip.ordtipo " +
" AND a.pac_pac_numero  = f.pac_pac_numero " +
" AND a.ordtipo         = f.ordtipo " +
" AND a.ordnumero       = f.ordnumero " +
" AND a.ordtipo         = todc.ordtipo " +
" AND a.ordnumero       = todc.ordnumero " +
" AND a.pac_pac_numero  = todc.pac_pac_numero " +
" AND todc.ordcirestcod = toce.ordcirestcod " +
" AND estord.EXMESTCOD  = toce.ordcirestcod " +
" AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
" AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
" AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
" AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
" AND a.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
" AND a.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
" AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
" and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND f.PRE_PRE_CODIGO=PRES.CODPRESTACION   AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' " +
" and est.ser_obj_codigo is not null and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) AND GESAUT.CODESTADO IN (1,2,6) " +
" UNION " +
" SELECT DISTINCT tip.ordnombre , " +
" f.ordnumero , " +
" f.pre_pre_codigo , " +
" admsalud.glbgetnompre(f.pre_pre_codigo) , " +
" DECODE(a.ordfecha,NULL,TO_DATE('1900/01/01', 'YYYY/MM/DD'),a.ordfecha) , " +
" a.ordfecha , " +
" estord.ORDEN , " +
" toce.ORDCIRESTNOM, " +
" NVL(a.FECINGQRF,TO_DATE('1900/01/01', 'YYYY/MM/DD')) , " +
" '' tipoformu , " +
" '' numerformu , " +
" NVL(tip.ordtipo, 'Orden Sin Tipo') ordtipo , " +
" 0 , " +
" ' ' , " +
" ' ' , " +
" '00' " +
" ,GESAUT.CODESTADO, " +
" GESAUT.OBSERVACION, " +
" GESAUT.ALERTA, " +
" GESAUT.VALORALERTA, " +
" GESAUT.CODTIEMPOALERTA " +
" ,A.MTVCORRELATIVO " +
" ,A.PAC_PAC_NUMERO " +
" ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
" ,est.ser_obj_codigo cama, asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
" FROM " +
" (SELECT tabordenesserv.ordtipo, " +
" tabordenesserv.ordnumero, " +
" tabordenesserv.pac_pac_numero, " +
" tabordenesserv.mtvcorrelativo, " +
" tabordenesserv.tabencuentro, " +
" ordfecha, " +
" FECINGQRF, " +
" ordestado, " +
" '00' " +
" FROM tabordenesserv, " +
" TabOrdDetCir, " +
" TabPrgQrf " +
" WHERE TabPrgQrf.QRFEST(+)         = 'C' " +
" AND TabOrdenesServ.ORDTIPO        = TabPrgQrf.ORDTIPO(+) " +
" AND TabOrdenesServ.ORDNUMERO      = TabPrgQrf.ORDNUMERO(+) " +
" AND TabOrdenesServ.PAC_PAC_NUMERO = TabPrgQrf.PAC_PAC_NUMERO(+) " +
" AND TabOrdDetCir.PAC_PAC_NUMERO   = TabOrdenesServ.PAC_PAC_NUMERO(+) " +
" AND TabOrdDetCir.MTVCORRELATIVO   = TabOrdenesServ.MTVCORRELATIVO(+) " +
" AND TabOrdDetCir.TABENCUENTRO     = TabOrdenesServ.TABENCUENTRO(+) " +
" AND TabOrdDetCir.ORDTIPO          = TabOrdenesServ.ORDTIPO(+) " +
" AND TabOrdDetCir.ORDNUMERO        = TabOrdenesServ.ORDNUMERO(+) " +
" ) a , " +
" taborddetalle f , " +
" taborddetcir todc , " +
" tabordcirest toce , " +
" HCP_ESTORDEN estord, " +
" tabordsrvtpo tip, " +
" CENAUT_GESTIONAUT GESAUT " +
" ,atc_estadia est " +
" , ser_servicios ser " +
" , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
" WHERE a.ordtipo       = 'P' " +
" AND a.ordtipo         = tip.ordtipo " +
" AND a.pac_pac_numero  = f.pac_pac_numero " +
" AND a.ordtipo         = f.ordtipo " +
" AND a.ordnumero       = f.ordnumero " +
" AND a.ordtipo         = todc.ordtipo " +
" AND a.ordnumero       = todc.ordnumero " +
" AND a.pac_pac_numero  = todc.pac_pac_numero(+) " +
" AND todc.ordcirestcod = toce.ordcirestcod " +
" AND estord.EXMESTCOD  = toce.ordcirestcod " +
" AND f.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
" AND f.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
" AND f.ordnumero=GESAUT.ORDNUMERO(+) " +
" AND f.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
" AND a.MTVCORRELATIVO=EST.MTVCORRELATIVO " +
" AND a.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
" AND EST.ATCESTADO='H'  AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
" and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND f.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' " +
" and est.ser_obj_codigo is not null  and est.ser_obj_codigo<>'        '  and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) AND GESAUT.CODESTADO IN (1,2,6) " +
" ORDER BY 25 ASC, " +
" 26 ASC " +
" ) " +
" ) " +
" ) b " +
" ) " +
" UNION " +
" select " +
" ADDPRES.ORDNOMBRE, ADDPRES.ORDNUMERO, ADDPRES.CODPRESTACION, PRES.PRE_PRE_DESCRIPCIO, SYSDATE, ADDPRES.FECHASOLIC, 0, '', SYSDATE,'', '', ADDPRES.ORDTIPO, 0,'','','', " +
"  DECODE(NVL(CODESTADO,1), 1, 'Pendiente de Gestión', 2, 'En proceso de autorización', 3, 'Autorizado', 4, 'Negado', 5, 'Cancelado', 6, 'Devuelto') CODESTADO,  GESAUT.OBSERVACION, GESAUT.ALERTA, NVL(GESAUT.VALORALERTA,0) VALORALERTA, GESAUT.CODTIEMPOALERTA, ADDPRES.MTVCORRELATIVO, ADDPRES.ID, ADDPRES.PAC_PAC_NUMERO " +
" ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio                ,est.ser_obj_codigo cama,  asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
" from cenaut_agregarprestacion addpres, PRE_PRESTACION PRES, CENAUT_GESTIONAUT GESAUT, CENAUT_PRESTACION CENPRES,  ATC_ESTADIA EST , ser_servicios ser, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
" WHERE " +
" ADDPRES.CODPRESTACION=PRES.PRE_PRE_CODIGO " +
" AND ADDPRES.CODPRESTACION=GESAUT.CODPRESTACION(+) " +
" AND ADDPRES.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
" AND ADDPRES.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
" AND ADDPRES.ORDNUMERO=GESAUT.ORDNUMERO(+) " +
" AND ADDPRES.CODPRESTACION=CENPRES.CODPRESTACION " +
" AND ADDPRES.MTVCORRELATIVO = EST.MTVCORRELATIVO " +
" AND ADDPRES.PAC_PAC_NUMERO =EST.PAC_PAC_NUMERO " +
" AND EST.ATCESTADO='H' " +
" AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02'      and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO " +
" AND CENPRES.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO AND GESAUT.CODESTADO IN (1,2,6) " +
" UNION " +
" select " +
" ADDPRES.ORDNOMBRE, ADDPRES.ORDNUMERO, ADDPRES.CODPRESTACION, PRES.HMCTPONOM, SYSDATE, ADDPRES.FECHASOLIC, 0, '', SYSDATE,'', '', ADDPRES.ORDTIPO, 0,'','','', " +
"  DECODE(NVL(CODESTADO,1), 1, 'Pendiente de Gestión', 2, 'En proceso de autorización', 3, 'Autorizado', 4, 'Negado', 5, 'Cancelado', 6, 'Devuelto') CODESTADO,  GESAUT.OBSERVACION, GESAUT.ALERTA, GESAUT.VALORALERTA, GESAUT.CODTIEMPOALERTA, ADDPRES.MTVCORRELATIVO, ADDPRES.ID, ADDPRES.PAC_PAC_NUMERO " +
" ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio                ,est.ser_obj_codigo cama, NVL(GESAUT.RESPONSABLE, asig.responsable) RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
" from cenaut_agregarprestacion addpres, TABHMCTPO PRES, CENAUT_GESTIONAUT GESAUT, CENAUT_PRESTACION CENPRES,  ATC_ESTADIA EST , ser_servicios ser, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
" WHERE " +
" ADDPRES.CODPRESTACION=PRES.HMCTPOCOD " +
" AND ADDPRES.CODPRESTACION=GESAUT.CODPRESTACION(+) " +
" AND ADDPRES.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
" AND ADDPRES.PAC_PAC_NUMERO=GESAUT.PAC_PAC_NUMERO(+) " +
" AND ADDPRES.ORDNUMERO=GESAUT.ORDNUMERO(+) " +
" AND ADDPRES.CODPRESTACION=CENPRES.CODPRESTACION " +
" AND ADDPRES.MTVCORRELATIVO = EST.MTVCORRELATIVO " +
" AND ADDPRES.PAC_PAC_NUMERO =EST.PAC_PAC_NUMERO " +
" AND EST.ATCESTADO='H' " +
" AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     '     and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO " +
" AND CENPRES.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO  AND GESAUT.CODESTADO IN (1,2,6) " +
" UNION " +
" SELECT 'ENFERMERIA' ordnombre , " +
" 'ENFERMERIA' , " +
" ORD.PRE_PRE_CODIGO precod , " +
" PRM.PRE_PRE_DESCRIPCIO prenom , " +
" SYSDATE, " +
" ord.FEPRC fechasolic , " +
" 0 , " +
" '' Estado , " +
" SYSDATE fechaconfirm , " +
" '' tipoformu , " +
" '' , " +
" '' , " +
" 0 , " +
" '' , " +
" '' , " +
" '' " +
" , DECODE(NVL(CODESTADO,1), 1, 'Pendiente de Gestión', 2, 'En proceso de autorización', 3, 'Autorizado', 4, 'Negado', 5, 'Cancelado', 6, 'Devuelto') CODESTADO,  " +
" GESAUT.OBSERVACION, " +
" GESAUT.ALERTA, " +
" NVL(GESAUT.VALORALERTA,0), " +
" GESAUT.CODTIEMPOALERTA " +
" ,ORD.MTVCORRELATIVO " +
" ,CAST(GlbGetPacCar( ORD.PAC_PAC_NUMERO) AS VARCHAR(13)) " +
" ,ORD.PAC_PAC_NUMERO " +
" ,trim(est.ser_obj_codigo)  || ' - ' || TRIM(ser.SER_SER_DESCRIPCIO) CamaServicio " +
" ,est.ser_obj_codigo  cama,  asig.RESPONSABLE RESPONSABLE,  asig.NOMBRES NOMBRES, TRIM(CON.CON_CON_DESCRIPCIO) CONVENIO, trim(ser.ser_ser_codigo) ser_ser_codigo " +
" FROM  HCP_CTLESPECIALES " +
" ord ,  HCP_PARAMCTLESP PRM, " +
" CENAUT_GESTIONAUT GESAUT " +
" ,ATC_ESTADIA EST " +
" , ser_servicios ser " +
" , cenaut_prestacion pres , ser_objetos OBJ, CENAUT_ASIGNACAMA asig, CON_CONVENIO CON " +
" WHERE ORD.PRE_PRE_CODIGO= prm.PRE_PRE_CODIGO " +
" AND PRM.PRE_PRE_VIGENCIA='S' " +
" AND ORD.MTVCORRELATIVO=GESAUT.MTVCORRELATIVO(+) " +
" AND ORD.PAC_PAC_NUMERO = GESAUT.PAC_PAC_NUMERO(+) " +
" AND ORD.PRE_PRE_CODIGO=GESAUT.CODPRESTACION(+) " +
" AND ORD.MTVCORRELATIVO=EST.MTVCORRELATIVO (+) " +
" AND ORD.PAC_PAC_NUMERO=EST.PAC_PAC_NUMERO " +
" AND EST.ATCESTADO='H' AND  SER.ser_Ser_vigencia='S' and SER.ser_ser_ambito='02' and est.SER_sER_CODIGO<>'016     ' AND EST.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
" and EST.SER_SER_CODIGO=ser.SER_SER_CODIGO AND ORD.PRE_PRE_CODIGO=PRES.CODPRESTACION  AND OBJ.ser_obj_codigo= EST.SER_OBJ_CODIGO AND OBJ.SER_OBJ_VIGENCIA='S' and est.ser_obj_codigo is not null " +
" and est.ser_obj_codigo<>'        ' and pres.ESTADOAUT=1 and est.ser_obj_codigo=asig.cama(+) " +
" AND ORD.PRE_PRE_CODIGO IN ('GESATE  ', 'SALCAS  ', 'GESAMB  ')  AND GESAUT.CODESTADO IN (1,2,6) " +
" ORDER BY 25 ASC, 26 ASC ";

                   
            return getDataTable(sql, "Oracle");
        }
       
      
      

        #region Metodos Privados

        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }
        private DataTable getDataTable(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }
        private string getDataTablePrueba(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTableprueba(sql);
        }
        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {            
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }
        private bool EjecutarSentenciaOracle(string sql)
        {
            return new DA.DA("Oracle").EjecutarSentencia(sql);
            //return new DA.DA().EjecutarSentenciaOracle(sql);
        }
        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }
        private DataTable getDataTable1(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }
        #endregion

    }
}
