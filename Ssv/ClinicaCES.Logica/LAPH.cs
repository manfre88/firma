﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAPH
    {
        public DataSet PuntualidadAPH(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,b.RPA_FDA_HORAINGRESO triage_admon,c.MTVFECHACONSULTA apertura_hc,b.RPA_FDA_HORAEGRESO egreso_urg, " +
                                 "round( (c.MTVFECHACONSULTA-b.RPA_FDA_HORAINGRESO)*24*60 ,0) OPORTUNIDAD " +
                         "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e,CON_Convenio D,tabtriagecal f,tabespdsturg g,TAB_Contigencia h " +
                         "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                            "and b.MTVCORRELATIVO=c.MTVCORRELATIVO " +
                            "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO " +
                            "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU " +
                            "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU " +
                            "and trunc(b.FECPRC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                            "and e.ESPDSTCOD='04' and a.PAC_PAC_NUMERO NOT IN ('1308','5024')" +
                            "AND B.CON_CON_CODIGO=D.CON_CON_CODIGO " +
                            "and e.TRIAGECOD=f.TRIAGECOD " +
                            "and e.ESPDSTCOD=g.ESPDSTCOD " +
                            "and c.MTVCON_ORIGEN=h.TAB_CONT_CODIGO " +
                         "order by 3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet TiempoRespueInterconsulta(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select PAC.PAC_PAC_RUT identificacion,nomcompletopaciente(PAC.PAC_PAC_NUMERO) paciente, Rem.MTVCORRELATIVO evento,Decode(Rem.PAC_REM_TipoFormu,'11  ','Control','10  ','Remisión','12  ','Interconsulta') tipo " +
                               ",Decode(FunLeeParam('HCP ','INTSU'),'SI', GlbGetNomPre(Rem.SER_ESP_Codigo) ,GlbGetNomSubEsp(1,Rem.SER_ESP_Codigo) )  Especialidad ,Rem.RIntFecha fecha_solicitud " +
                               ", decode(OrdEstado,'S','SOLICITADA','R','ATENDIDO') ESTADO, RIntFecRes  FECHA_RESPUESTA, decode(OrdEstado,'S',0,'R',round( (RIntFecRes-RIntFecha)*24 ,1) ) oportunidad " +
                               ",FuncHspGetProf(1,Rem.SER_PRO_Rut) ATENDIO, rpa.RPA_FDA_HORAEGRESO alta_u_hosp " +
                          "From  TabRemisInterCon Rem ,TabMotivoCons  Mtv ,rpa_fordau rpa, tabtriage tri, pac_paciente pac " +
                         "Where trunc(Rem.RIntFecha) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') And  Rem.MtvCorrelativo = Mtv.MtvCorrelativo And  Rem.PAC_PAC_Numero = Mtv.PAC_PAC_Numero " +
                           "and  Rem.MTVCORRELATIVO = rpa.MTVCORRELATIVO  and  Rem.PAC_PAC_Numero = rpa.PAC_PAC_NUMERO and  rpa.RPA_FOR_NUMERFORMU = tri.RPA_FOR_NUMERFORMU and Rem.PAC_PAC_Numero = tri.PAC_PAC_NUMERO " +
                           "and REM.PAC_PAC_NUMERO=PAC.PAC_PAC_NUMERO and tri.ESPDSTCOD ='04' and pac.PAC_PAC_NUMERO not in ('1308','5024') order by 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
