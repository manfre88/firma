﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LMAsuntos
    {
        public bool MAsuntoGuardar(MAsuntos masunto)
        {
            string[] nomParam = { "@Codigo", "@Nombre", "@Usuario" };
            object[] vlrParam = { masunto.Codigo, masunto.Nombre, masunto.Usuario };

            return EjecutarSentencia("uspMAsuntoActualizar", nomParam, vlrParam);
        }

        public bool MAsuntoEstado(MAsuntos app)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                app.Codigo,
                app.Estado
            };

            return EjecutarSentencia("uspMAsuntoEstado", nomParam, vlrParam);
        }

        public DataTable MAsuntoConsultar(MAsuntos masunto)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { masunto.Codigo };

            return getDataTable("uspMAsuntoConsultar", nomParam, vlrParam);
        }

        public bool MAsuntoReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_MAsunto", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspMAsuntoReactivar", nomParam, valParam);
        }

        public bool MAsuntoRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_MAsunto", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspMAsuntoRetirar", nomParam, valParam);
        }

        public DataTable MAsuntoConsultar()
        {
            return getDataTable("uspMAsuntoConsultarCodigos");
        }

        public bool MAsuntosRetirar(string xmlMAsunto, string Usuario)
        {
            string[] nomParam = { "@xmlMAsunto", "@Usuario" };
            object[] valParam = { xmlMAsunto, Usuario };

            return EjecutarSentencia("uspMAsuntoRetirarXml", nomParam, valParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
