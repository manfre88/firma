﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LEstados
    {
        public bool EstadoGuardar(Estados estado)
        {
            string[] nomParam = { "@Codigo", "@Nombre", "@Usuario" };
            object[] vlrParam = { estado.Codigo, estado.Nombre, estado.Usuario };

            return EjecutarSentencia("uspEstadoActualizar", nomParam, vlrParam);
        }

        public bool EstadoEstado(Estados app)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                app.Codigo,
                app.Estado
            };

            return EjecutarSentencia("uspEstadoEstado", nomParam, vlrParam);
        }

        public DataTable EstadoConsultar(Estados estado)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { estado.Codigo };

            return getDataTable("uspEstadoConsultar", nomParam, vlrParam);
        }

        public bool EstadoReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Estado", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspEstadoReactivar", nomParam, valParam);
        }

        public bool EstadoRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Estado", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspEstadoRetirar", nomParam, valParam);
        }

        public DataTable EstadoConsultar()
        {
            return getDataTable("uspEstadoConsultarCodigos");
        }

        public bool EstadosRetirar(string xmlEstado, string Usuario)
        {
            string[] nomParam = { "@xmlEstado", "@Usuario" };
            object[] valParam = { xmlEstado, Usuario };

            return EjecutarSentencia("uspEstadoRetirarXml", nomParam, valParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
