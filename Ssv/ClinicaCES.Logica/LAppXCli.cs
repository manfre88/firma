﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaCES.Entidades;
using System.Data;

namespace ClinicaCES.Logica
{
    public class LAppXCli
    {
        public bool AppXCliActualizar(AppXCli axc)
        {
            string[] nomParam = 
            {
                "@Nit",
	            "@Negocio",
	            "@Aplicacion",
	            "@UsrInstala",
	            "@Usr",
	            "@FecIns"
            };

            object[] vlrParam = 
            {
                axc.Nit,
                axc.Negocio,
                axc.Aplicacion,
                axc.UsrInstala,
                axc.Usr,
                axc.FecIns
            };

            return EjecutarSentencia("uspAppClienteActualizar", nomParam, vlrParam);
        }

        public DataTable AppXCliCambiarEstado(AppXCli axc)
        {
            string[] nomParam = 
            {
                "@Nit",
	            "@Negocio",
	            "@Aplicacion",
	            "@Estado",
                "@Usr"
            };

            object[] vlrParam = 
            {
                axc.Nit,
                axc.Negocio,
                axc.Aplicacion,
                axc.Estado,
                axc.Usr
            };

            return getDataTable("uspAppClienteCambiarEstado", nomParam, vlrParam);
        }

        public bool AppXCliRetirar(string xml,string Usr)
        {
            string[] nomParam = 
            {
               	"@xmlClientes",
	            "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspAppClienteRetirarXml", nomParam, vlrParam);
        }


        public DataTable AppXCliBuscar(AppXCli axc)
        {
            string[] nomParam = 
            {
                "@Nit",
	            "@Negocio",
	            "@Aplicacion"
            };

            object[] vlrParam = 
            {
                axc.Nit,
                axc.Negocio,
                axc.Aplicacion
            };

            return getDataTable("uspAppClienteBuscar", nomParam, vlrParam);

        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
