﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LCargos
    {
        public DataTable AreaListar()
        {
            return getDataTable("uspAreaListar");
        }

        public bool CargoEstado(Cargos car)
        {
            string[] nomParam = 
            {
                "@Cargo",
	            "@Area",
                "@Estado"
            };

            object[] vlrParam = 
            {
                car.Codigo,
                car.Area,
                car.Estado
            };

            return EjecutarSentencia("uspCargocambiarEstado", nomParam, vlrParam);
        }

        public bool CargoRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlCargo" ,
	            "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspCargoRetirarXml", nomParam, vlrParam);
        }

        public bool CargoActualizar(Cargos car)
        {
            string[] nomParam = 
            {
               	"@Codigo",
	            "@Area",
	            "@Descripcion",
	            "@Usr",
                "@VlrHora"
            };

            object[] vlrParam = 
            {
                car.Codigo,
                car.Area,
                car.Nombre,
                car.Usuario,
                car.VlrHora
            };

            return EjecutarSentencia("uspCargoActualizar", nomParam, vlrParam);
        }

        public DataTable CargoConsultar(Cargos car)
        {
            string[] nomParam = 
            {
                "@Area",
	            "@Codigo"
            };

            object[] vlrParam = 
            {
                car.Area,
                car.Codigo
            };

            return getDataTable("uspCargoConsultar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
