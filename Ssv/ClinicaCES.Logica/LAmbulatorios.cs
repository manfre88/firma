﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAmbulatorios
    {
        public DataTable CmbPrestaciones(string especialidad)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT TRIM(PRE.PRE_PRE_CODIGO) CODIGO, PRE.PRE_PRE_DESCRIPCIO DESCRIPCION FROM PRE_PRESTACION PRE " +
                         "WHERE PRE.PRE_PRE_CODIGO IN (SELECT CPRE.CODPRESTACION FROM CENAUT_PRESTACION CPRE WHERE CPRE.ESTADOAUT = 1) " +
                         "AND TRIM(PRE_PRE_TIPO) = TRIM('" + especialidad + "')";
            return getDataTable(sql, "Oracle");
        }

        public DataTable CmbEspecialidad()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT PRE.PRE_TIP_TIPO CODIGO, PRE.PRE_TIP_DESCRIPCIO DESCRIPCION FROM PRE_TIPO PRE ";

            return getDataTable(sql, "Oracle");
        }


        public DataTable CmbAsegurador(string identificacion)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };



            string sql = "";


            if (identificacion != "")
            {
                sql = "SELECT B.CON_CON_CODIGO CODIGO, B.CON_CON_DESCRIPCIO DESCRIPCION " +
                             "FROM PAC_NOVEDADES A, CON_CONVENIO B " +
                             "WHERE A.PAC_PAC_CODIGO = B.CON_CON_CODIGO " +
                             "AND TRIM(A.PAC_PAC_RUT) = '" + identificacion + "'";

            }
            else
            {

                sql = "SELECT CON_CON_CODIGO CODIGO, CON_CON_DESCRIPCIO DESCRIPCION " +
                         "FROM CON_CONVENIO " +
                         "WHERE CON_CON_VIGENCIA = 'S' " +
                         "ORDER BY CON_CON_DESCRIPCIO ";
            }
            return getDataTable(sql, "Oracle");
        }

        public bool AmbulatorioActualizar(Ambulatorios amb)
        {
            string idEvento = "";
            bool bandera = false;
            string[] nomParam = 
            {
            };

            object[] vlrParam = 
            {
            };

            string SQL = "";

            string fec1 = amb.FechaPosResp.ToString();
            string fec2 = amb.FechaProcedim.ToString();
            string fec3 = amb.FechaCx.ToString();


            if (amb.Id != "")
            {
                idEvento = amb.Id;


                SQL = "UPDATE CENAUT_AMBULATORIOS SET " +
                            "CONVENIO = '" + amb.Convenio + "'," +
                            "TIPOSOL = '" + amb.TipoSolicitud + "'," +
                            "SOPENV = '" + amb.SoporteEnv + "'," +
                            "EVENTO= '" + amb.NumEvento + "',";

                if (fec1 == "") { SQL = SQL + "FECRES= TO_DATE('1900/01/01','YYYY/MM/DD'),"; } else { SQL = SQL + "FECRES= TO_DATE('" + amb.FechaPosResp.ToString("yyyy/MM/dd") + "','YYYY/MM/DD'),"; }

                SQL = SQL + "TIPSOLPRO= '" + amb.TipoProced + "'," +
                                    "ESPREQU= '" + amb.Especialidad + "'," +
                                    "PROCIND= '" + amb.ProcIndicado + "'," +
                                    "PROCAUT= '" + amb.ProcedAprobado + "'," +
                                    "PRESTADOR= '" + amb.Prestador + "',";
                if (fec2 == "") { SQL = SQL + "FECPROC= TO_DATE('1900/01/01','YYYY/MM/DD'),"; } else { SQL = SQL + "FECPROC= TO_DATE('" + amb.FechaProcedim.ToString("yyyy/MM/dd") + "','YYYY/MM/DD'),"; }
                if (fec3 == "") { SQL = SQL + "FECCX= TO_DATE('1900/01/01','YYYY/MM/DD'),"; } else { SQL = SQL + "FECCX= TO_DATE('" + amb.FechaCx.ToString("yyyy/MM/dd") + "','YYYY/MM/DD'),"; }

                SQL = SQL + "OBSERVACION = '" + amb.Observacion + "'," +
                            "ESTADO= '" + amb.Estado + "'," +
                            "TIEMPO= '" + amb.Tiempo + "'," +
                            "UNDTIEMPO= '" + amb.UndTiempo + "'" +
                            "WHERE ID = " + amb.Id;


            }
            else
            {
                string sql1 = "select MAX(ID) + 1 FROM CENAUT_AMBULATORIOS";
                DataRow Valor = getDataTable(sql1, "Oracle").Rows[0];
                int Id = 0;

                if (Valor[0] is DBNull || Valor[0] == "")
                {
                    Id = 1;
                    idEvento = "1";
                }
                else
                {
                    Id = Convert.ToInt32(Valor[0]);
                    idEvento = Convert.ToString(Convert.ToInt32(Valor[0]));
                }

                SQL = "INSERT INTO CENAUT_AMBULATORIOS ( ID,IDENTIFICACION,NOMBRE,CONVENIO,TIPOSOL,SOPENV,EVENTO,FECRES,TIPSOLPRO, ESPREQU,PROCIND,PROCAUT " +
                         ",PRESTADOR,FECPROC,FECCX,OBSERVACION, ESTADO,TIEMPO,UNDTIEMPO,FECAPRC,USUARIO)   " +
                         " VALUES " +
                         "(" + Id + " " +
                         ", '" + amb.Identificacion + "' " +
                         ", '" + amb.Nombre + "' " +
                         ", '" + amb.Convenio + "' " +
                         ", '" + amb.TipoSolicitud + "' " +
                         ", '" + amb.SoporteEnv + "' " +
                         ", '" + amb.NumEvento + "' ";
                if (fec1 == "") { SQL = SQL + ",TO_DATE('1900/01/01','YYYY/MM/DD')"; } else { SQL = SQL + ",TO_DATE('" + amb.FechaPosResp.ToString("yyyy/MM/dd") + "','YYYY/MM/DD')"; }
                SQL = SQL + ", '" + amb.TipoSolicitud + "' " +
                         ", '" + amb.Especialidad + "' " +
                         ", '" + amb.ProcIndicado + "' " +
                         ", '" + amb.ProcedAprobado + "' " +
                         ", '" + amb.Prestador + "' ";
                if (fec2 == "") { SQL = SQL + ",TO_DATE('1900/01/01','YYYY/MM/DD')"; } else { SQL = SQL + ",TO_DATE('" + amb.FechaProcedim.ToString("yyyy/MM/dd") + "','YYYY/MM/DD')"; }
                if (fec3 == "") { SQL = SQL + ",TO_DATE('1900/01/01','YYYY/MM/DD')"; } else { SQL = SQL + ",TO_DATE('" + amb.FechaCx.ToString("yyyy/MM/dd") + "','YYYY/MM/DD')"; }
                SQL = SQL + ", '" + amb.Observacion + "' " +
                        ", '" + amb.Estado + "' " +
                        ", '" + amb.Tiempo + "' " +
                        ", '" + amb.UndTiempo + "' " +
                        ", SYSDATE" +
                        ", '" + amb.Usuario + "') ";

                amb.cambioestado = true;


            }

            string sqlevento = "";
            if (EjecutarSentenciaOracle(SQL))
            {
                if (amb.cambioestado)
                {
                    sqlevento = "INSERT INTO CENAUT_EVENTOSAMBULATORIO (IDAMB,IDENTIFICACION,ESTADO,FECHAEVENTO,USUARIO) " +
                        "VALUES ('" + idEvento + "', '" + amb.Identificacion + "', '" + amb.Estado + "', sysdate, '" + amb.Usuario + "') ";
                    EjecutarSentenciaOracle(sqlevento);
                }
                bandera = true;
            }
            return bandera;
        }

        public string AmbulatorioActualizarconsulta(Ambulatorios amb)
        {
            string idEvento = "";
            bool bandera = false;
            string[] nomParam = 
            {
            };

            object[] vlrParam = 
            {
            };

            string SQL = "";

            string fec1 = amb.FechaPosResp.ToString();
            string fec2 = amb.FechaProcedim.ToString();
            string fec3 = amb.FechaCx.ToString();


            if (amb.Id != "")
            {
                idEvento = amb.Id;

                SQL = "UPDATE CENAUT_AMBULATORIOS SET " +
                            "CONVENIO = '" + amb.Convenio + "'," +
                            "TIPOSOL = '" + amb.TipoSolicitud + "'," +
                            "SOPENV = '" + amb.SoporteEnv + "'," +
                            "EVENTO= '" + amb.NumEvento + "',";

                if (fec1 == "") { SQL = SQL + "FECRES= TO_DATE('1900/01/01','YYYY/MM/DD'),"; } else { SQL = SQL + "FECRES= TO_DATE('" + amb.FechaPosResp.ToString("yyyy/MM/dd") + "','YYYY/MM/DD'),"; }

                SQL = SQL + "TIPSOLPRO= '" + amb.TipoProced + "'," +
                                    "ESPREQU= '" + amb.Especialidad + "'," +
                                    "PROCIND= '" + amb.ProcIndicado + "'," +
                                    "PROCAUT= '" + amb.ProcedAprobado + "'," +
                                    "PRESTADOR= '" + amb.Prestador + "',";
                if (fec2 == "") { SQL = SQL + "FECPROC= TO_DATE('1900/01/01','YYYY/MM/DD'),"; } else { SQL = SQL + "FECPROC= TO_DATE('" + amb.FechaProcedim.ToString("yyyy/MM/dd") + "','YYYY/MM/DD'),"; }
                if (fec3 == "") { SQL = SQL + "FECCX= TO_DATE('1900/01/01','YYYY/MM/DD'),"; } else { SQL = SQL + "FECCX= TO_DATE('" + amb.FechaCx.ToString("yyyy/MM/dd") + "','YYYY/MM/DD'),"; }

                SQL = SQL + "OBSERVACION = '" + amb.Observacion + "'," +
                            "ESTADO= '" + amb.Estado + "'," +
                            "TIEMPO= '" + amb.Tiempo + "'," +
                            "UNDTIEMPO= '" + amb.UndTiempo + "'" +
                            "WHERE ID = " + amb.Id;


            }
            else
            {
                string sql1 = "select MAX(ID) + 1 FROM CENAUT_AMBULATORIOS";
                DataRow Valor = getDataTable(sql1, "Oracle").Rows[0];
                int Id = 0;

                if (Valor[0] is DBNull || Valor[0] == "")
                {
                    Id = 1;
                    idEvento = "1";
                }
                else
                {
                    Id = Convert.ToInt32(Valor[0]);
                    idEvento = Convert.ToString(Convert.ToInt32(Valor[0]));
                }

                SQL = "INSERT INTO CENAUT_AMBULATORIOS ( ID,IDENTIFICACION,NOMBRE,CONVENIO,TIPOSOL,SOPENV,EVENTO,FECRES,TIPSOLPRO, ESPREQU,PROCIND,PROCAUT " +
                         ",PRESTADOR,FECPROC,FECCX,OBSERVACION, ESTADO,TIEMPO,UNDTIEMPO,FECAPRC,USUARIO)   " +
                         " VALUES " +
                         "(" + Id + " " +
                         ", '" + amb.Identificacion + "' " +
                         ", '" + amb.Nombre + "' " +
                         ", '" + amb.Convenio + "' " +
                         ", '" + amb.TipoSolicitud + "' " +
                         ", '" + amb.SoporteEnv + "' " +
                         ", '" + amb.NumEvento + "' ";
                if (fec1 == "") { SQL = SQL + ",TO_DATE('1900/01/01','YYYY/MM/DD')"; } else { SQL = SQL + ",TO_DATE('" + amb.FechaPosResp.ToString("yyyy/MM/dd") + "','YYYY/MM/DD')"; }
                SQL = SQL + ", '" + amb.TipoProced + "' " +
                         ", '" + amb.Especialidad + "' " +
                         ", '" + amb.ProcIndicado + "' " +
                         ", '" + amb.ProcedAprobado + "' " +
                         ", '" + amb.Prestador + "' ";
                if (fec2 == "") { SQL = SQL + ",TO_DATE('1900/01/01','YYYY/MM/DD')"; } else { SQL = SQL + ",TO_DATE('" + amb.FechaProcedim.ToString("yyyy/MM/dd") + "','YYYY/MM/DD')"; }
                if (fec3 == "") { SQL = SQL + ",TO_DATE('1900/01/01','YYYY/MM/DD')"; } else { SQL = SQL + ",TO_DATE('" + amb.FechaCx.ToString("yyyy/MM/dd") + "','YYYY/MM/DD')"; }
                SQL = SQL + ", '" + amb.Observacion + "' " +
                        ", '" + amb.Estado + "' " +
                        ", '" + amb.Tiempo + "' " +
                        ", '" + amb.UndTiempo + "' " +
                        ", SYSDATE" +
                        ", '" + amb.Usuario + "') ";

                amb.cambioestado = true;


            }

            string sqlevento = "";
            if (EjecutarSentenciaOracle(SQL))
            {
                if (amb.cambioestado)
                {
                    sqlevento = "INSERT INTO CENAUT_EVENTOSAMBULATORIO (IDAMB,IDENTIFICACION,ESTADO,FECHAEVENTO,USUARIO) " +
                        "VALUES ('" + idEvento + "', '" + amb.Identificacion + "', '" + amb.Estado + "', sysdate, '" + amb.Usuario + "') ";
                    // EjecutarSentenciaOracle(sqlevento);
                }
                bandera = true;
            }
            return sqlevento;
        }


        public DataTable AmbulatoriosConsultar(Ambulatorios amb)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "SELECT AMB.*, PRE.PRE_PRE_DESCRIPCIO " +
                  ", DECODE(AMB.TIPOSOL,0,'NO APLICA',1,'DESCARGA ORDEN',2,'SOLICITUD CAMBIO',3,'SOLICITUD RADICACION',4, 'SOLICITUD DE CODIGO') TIPO_SOLICITUD " +
                  ", DECODE(ESTADO,1,'PENDIENTE GESTION',2,'PROCESO AUTORIZACION',3,'APROBADO',4,'NEGADO',5,'CANCELADO',6,'DEVUELTO') ESTADOSOLICITUD " +
                  ", CON.CON_CON_DESCRIPCIO CON_DESCRIP " +
                  "FROM CENAUT_AMBULATORIOS AMB, PRE_PRESTACION PRE, CON_CONVENIO CON " +
                  "WHERE TRIM(AMB.PROCIND) = TRIM(PRE.PRE_PRE_CODIGO) " +
                  "AND TRIM(AMB.CONVENIO) = TRIM(CON.CON_CON_CODIGO) ";

            if (amb.Id != "")
            { sql = sql + " AND AMB.ID = " + amb.Id; }
            else { sql = sql + " ORDER BY FECAPRC DESC "; }

            return getDataTable(sql, "Oracle");
        }



        public string AmbulatoriosConsultarConsulta(Ambulatorios amb)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "SELECT AMB.*, PRE.PRE_PRE_DESCRIPCIO " +
                  ", DECODE(AMB.TIPOSOL,0,'NO APLICA',1,'DESCARGA ORDEN',2,'SOLICITUD CAMBIO',3,'SOLICITUD RADICACION') TIPO_SOLICITUD " +
                  ", DECODE(ESTADO,1,'PENDIENTE GESTION',2,'PROCESO AUTORIZACION',3,'APROBADO',4,'NEGADO',5,'CANCELADO',6,'DEVUELTO') ESTADOSOLICITUD " +
                  ", CON.CON_CON_DESCRIPCIO CON_DESCRIP " +
                  "FROM CENAUT_AMBULATORIOS AMB, PRE_PRESTACION PRE, CON_CONVENIO CON " +
                  "WHERE TRIM(AMB.PROCIND) = TRIM(PRE.PRE_PRE_CODIGO) " +
                  "AND TRIM(AMB.CONVENIO) = TRIM(CON.CON_CON_CODIGO) ";

            if (amb.Id != "")
            { sql = sql + " AND AMB.ID = " + amb.Id; }
            else { sql = sql + " ORDER BY FECAPRC DESC "; }


            return sql;
        }



        public DataTable ConsultarNombre(Ambulatorios amb)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "SELECT PAC.PAC_PAC_RUT IDENTIFICACION " +
                  ",TRIM(PAC.PAC_PAC_NOMBRE) || ' ' || TRIM(PAC.PAC_PAC_APELLPATER) || ' ' || TRIM(PAC.PAC_PAC_APELLMATER) NOMBRES " +
                  ", PAC.PAC_PAC_CODIGO CONVENIO " +
                  "FROM PAC_PACIENTE PAC " +
                  "WHERE TRIM(PAC.PAC_PAC_RUT) = '" + amb.Identificacion + "'";

            return getDataTable(sql, "Oracle");
        }


        public DataSet InformeAmbulatorio(string FechaIni, string FechaFin, string Tipo)
        {

            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };


            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            string sql = "";

            switch(Tipo) {
            
                case "1": // Prestaciones
                    sql = "SELECT PRE.PRE_PRE_DESCRIPCIO ITEM, DECODE(AMB.ESTADO,1, 'PENDIENTE GESTION',2, 'EN PROCESO AUTORIZACION',3,'AUTORIZADO',4,'NEGADO',5,'CANCELADO',6,'DEVUELTO') ESTADO " +
                          ", USR.FLD_USERNAME USUARIO, COUNT(*) CANTIDAD " + 
                          "FROM CENAUT_AMBULATORIOS AMB, PRE_PRESTACION PRE, PRIV_ADM.TBL_USER USR " +
                          "WHERE AMB.PROCIND = PRE.PRE_PRE_CODIGO AND AMB.USUARIO = USR.FLD_USERCODE AND AMB.FECAPRC BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD')+1 " +
                          "GROUP BY  AMB.PROCIND, PRE.PRE_PRE_DESCRIPCIO , AMB.ESTADO, USR.FLD_USERNAME " +
                          "ORDER BY 2,3"; 
                    break;
                case "2": //Estados

                    sql = "SELECT '' ITEM ,DECODE(AMB.ESTADO,1, 'PENDIENTE GESTION',2, 'EN PROCESO AUTORIZACION',3,'AUTORIZADO',4,'NEGADO',5,'CANCELADO',6,'DEVUELTO') ESTADO " +
                          ", USR.FLD_USERNAME USUARIO, COUNT(*) CANTIDAD " +
                          "FROM CENAUT_AMBULATORIOS AMB, PRIV_ADM.TBL_USER USR " +
                          "WHERE AMB.USUARIO = USR.FLD_USERCODE AND AMB.FECAPRC BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD')+1 " +
                          "GROUP BY ESTADO, USR.FLD_USERNAME ORDER BY 1,2 ";
                    break;
                case "3": // Convenios

                    sql = "SELECT CONV.CON_CON_DESCRIPCIO ITEM  ,DECODE(AMB.ESTADO,1, 'PENDIENTE GESTION',2, 'EN PROCESO AUTORIZACION',3,'AUTORIZADO',4,'NEGADO',5,'CANCELADO',6,'DEVUELTO') ESTADO " +
                          ", USR.FLD_USERNAME USUARIO, COUNT(*) CANTIDAD " +
                          "FROM CENAUT_AMBULATORIOS AMB, PRE_PRESTACION PRE, CON_CONVENIO CONV ,PRIV_ADM.TBL_USER USR " +
                          "WHERE AMB.PROCIND = PRE.PRE_PRE_CODIGO AND AMB.CONVENIO = CONV.CON_CON_CODIGO  " +
                          "AND AMB.USUARIO = USR.FLD_USERCODE AND AMB.FECAPRC BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD')+1 " +
                          "GROUP BY CONV.CON_CON_DESCRIPCIO, ESTADO, USR.FLD_USERNAME " +
                          "ORDER BY 1,2,3 ";
                    break;
            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet InformeAmbulatorioPendientes (string FechaIni, string FechaFin)
        {

            string[] nomParam = 
            {

            };

            object[] vlrParam = 
            {

            };


            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            string sql = "";


            sql = "SELECT AMB.IDENTIFICACION, AMB.NOMBRE, PRE.PRE_PRE_DESCRIPCIO PROCEDIMIENTO, CONV.CON_CON_DESCRIPCIO CONVENIO " +
                  ",DECODE(AMB.ESTADO,1, 'PENDIENTE GESTION',2, 'EN PROCESO AUTORIZACION',3,'AUTORIZADO',4,'NEGADO',5,'CANCELADO',6,'DEVUELTO') ESTADO " +
                  ", USR.FLD_USERNAME USUARIO , AMB.FECAPRC FECHA " +
                  "FROM CENAUT_AMBULATORIOS AMB, PRE_PRESTACION PRE, CON_CONVENIO CONV ,PRIV_ADM.TBL_USER USR " +
                  "WHERE AMB.PROCIND = PRE.PRE_PRE_CODIGO AND AMB.CONVENIO = CONV.CON_CON_CODIGO AND AMB.USUARIO = USR.FLD_USERCODE " +
                  "AND AMB.FECPROC BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') " +
                  "ORDER BY FECAPRC,AMB.IDENTIFICACION, AMB.NOMBRE ";
 
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        
        
        public DataTable EstadosConsultar()
        {
            return getDataTable("uspEstadosConsultar");
        }










        #region Metodos Privados

        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }
        private DataTable getDataTable(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }
        private string getDataTablePrueba(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTableprueba(sql);
        }
        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }
        private bool EjecutarSentenciaOracle(string sql)
        {
            return new DA.DA("Oracle").EjecutarSentencia(sql);
            //return new DA.DA().EjecutarSentenciaOracle(sql);
        }
        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }
        #endregion

    }
}