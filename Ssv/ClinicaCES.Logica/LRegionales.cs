﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LRegionales
    {
        public bool RegionalRetirar(string xmlRegional, string usr)
        { 
            string[] nomParam = 
            {
                "@xmlRegional",
	            "@Usuario"
            };

            object[] vlrParam = 
            {
                xmlRegional,usr
            };

            return EjecutarSentencia("uspRegionalesRetirarXml", nomParam, vlrParam);
        }

        public bool RegionalesCambiarEstado(Regionales reg)
        { 
            string[] nomParam = 
            {
                "@Codigo" ,
	            "@Compania" ,
	            "@Estado" ,
	            "@Usr" 
            };

            object[] vlrParam = 
            {
                reg.Codigo,
                reg.Compania,
                reg.Estado,
                reg.Usr
            };

            return EjecutarSentencia("uspRegionalesCambiarEstado", nomParam, vlrParam);
        }

        public bool RegionalesActualizar(Regionales reg)
        { 
            string[] nomParam = 
            {
                "@Codigo",
                "@Regional",
                "@Compania",
                "@Usr"
            };

            object[] vlrParam = 
            {
                reg.Codigo,
                reg.Regional,
                reg.Compania,
                reg.Usr
            };

            return EjecutarSentencia("uspRegionalesActualizar", nomParam, vlrParam);
        }


        public DataTable CompaniaListar()
        {
            return getDataTable("uspCompaniasListar");
        }

        public DataTable RegionalListar()
        {
            return getDataTable("uspRegionalesListar");
        }

        public DataTable RegionalesConsultar(Regionales reg)
        { 
            string[] nomParam = 
            {
                "@Codigo" ,
	            "@Compania" 
            };

            object[] vlrParam = 
            {
                reg.Codigo,
                reg.Compania
            };

            return  getDataTable("uspRegionalesConsultar", nomParam, vlrParam);            
        }
        

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
