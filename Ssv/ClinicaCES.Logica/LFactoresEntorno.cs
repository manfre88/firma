﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LFactoresEntorno
    {
        public bool FactoresEntornoEstado(FactoresEntorno ctec)
        {
            string[] nomParam = 
            {
                "@Codigo",
	            "@Estado",
	            "@Usr"
            };

            object[] vlrParam = 
            {
                ctec.Codigo,
                ctec.Estado,
                ctec.usr
            };

            return EjecutarSentencia("uspFactoresEntornoEstado", nomParam, vlrParam);
        }

        public bool FactoresEntornoRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlFact" ,
                "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspFactoresEntornoRetirarXml", nomParam, vlrParam);
        }

        public DataSet FactoresEntornoConsultar(FactoresEntorno est)
        {
            string[] nomParam = 
            {
                "@Codigo",
                "@Proyecto"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Proyecto
            };

            return getDataSet("uspFactoresEntornoConsultar", nomParam, vlrParam);

        }

        public DataTable FactoresEntornoConsultar()
        {
            return getDataTable("uspFactoresEntornoConsultar");
        }

        public bool FactoresEntornoActualizar(FactoresEntorno est)
        {
            string[] nomParam = 
            {
               	"@Codigo",
	            "@Descripcion",
	            "@Nombre",
	            "@Usr",
                //"@xmlCriterios",
                "@Peso"
            };

            object[] vlrParam = 
            {
                est.Codigo,
                est.Descripcion,
                est.Nombre,
                est.usr,
                //est.xml,
                est.Peso
            };

            return EjecutarSentencia("uspFactoresEntornoActualizar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataset(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
