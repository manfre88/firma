﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using Microsoft.Exchange.WebServices.Data;






namespace ClinicaCES.Correo
{
    public class Correo
    {
        #region Variables
        private string strDe;
        private string strPara;
        private string strAsunto;
        private string strMensaje;
        private string[] arrAdjuntos;
        #endregion

        #region Propiedades
        public string De
        {
            set
            {
                strDe = value;
            }
        }
        public string Para
        {
            set
            {
                strPara = value;
            }
        }
        public string Asunto
        {
            set
            {
                strAsunto = value;
            }
        }
        public string Mensaje
        {
            set
            {
                strMensaje = value;
            }
        }

        public string[] Adjuntos
        {
            set
            {
                arrAdjuntos = value;
            }
        }

        #endregion

        #region Metodos

        public bool Enviar()
        {
            AlternateView plainView = AlternateView.CreateAlternateViewFromString("This is my plain text content, viewable by those clients that don't support html", null, "text/plain");
            //AlternateView htmlView = AlternateView.CreateAlternateViewFromString("Here is an embedded image.<img src=cid:companylogo>", null, "text/html");
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strMensaje, null, "text/html");
            LinkedResource logo = new LinkedResource("c:\\ImgCes.jpg");
            logo.ContentId = "companylogo";
            htmlView.LinkedResources.Add(logo);

            System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
            correo.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["mailSoporte"]);
            correo.To.Add(strPara);
            correo.Subject = strAsunto;
            correo.Body = strMensaje;
            correo.IsBodyHtml = true;


            correo.AlternateViews.Add(plainView);

            correo.AlternateViews.Add(htmlView);


            correo.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
            correo.Headers.Add("Disposition-Notification-To", ConfigurationManager.AppSettings["mailSoporte"]);
            System.Net.Mail.Attachment attachment;
            if (arrAdjuntos != null)
            {
                foreach (string item in arrAdjuntos)
                {
                    attachment = new System.Net.Mail.Attachment(item.Replace("\\\\", "\\"));
                    correo.Attachments.Add(attachment);
                }
            }
            
            
            

            //correo.IsBodyHtml = false;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = ConfigurationManager.AppSettings["smtp"];
            smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
            
            //smtp.Credentials = new System.Net.NetworkCredential("UsuariodelDominio", "contraseñadelDominio");
            try
            {
                smtp.Send(correo);
                //if (DeliveryNotificationOptions.OnSuccess == true)
                //{ }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public void correoExchange()
        {
            int dato=0;
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013);
            service.Credentials = new WebCredentials(CredentialCache.DefaultNetworkCredentials);
            service.TraceEnabled = true;
            service.TraceFlags = TraceFlags.All;
            service.AutodiscoverUrl("andresvilla@clinicaces.com", RedirectionUrlValidationCallback);
            Folder inbox = Folder.Bind(service, WellKnownFolderName.Inbox);
            List<string> parts = new List<string>();
            ItemView view = new ItemView(10);
            foreach (object obj in inbox.FindItems(view))
            {
                Item item = obj as Item;
                //MailItem item = obj as MailItem;
                if (item != null)
                {
                    //dato = item.DateTimeReceived
                    if (item.Subject.ToString() == "Retransmitido: prueba")
                    {

                    }
                    else
                    {
 
                    }
                    

                    //listView1.Items.Add("Hora :" + item.ReceivedTime + " , Remitente :" + item.SenderName + " , Asunto :" + item.Subject);
                }
            }









            //PropertySet propSet = new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject);


            //EmailMessage email = new EmailMessage(service);
            //SearchFilter sf = new SearchFilter.SearchFilterCollection(LogicalOperator.And, new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false));
            //ItemView view = new ItemView(1);

            //// Fire the query for the unread items.
            //// This method call results in a FindItem call to EWS.
            //FindItemsResults<Item> findResults = service.FindItems(WellKnownFolderName.Inbox, sf, view);
            //List<string> parts = new List<string>();
            //foreach (Item item in findResults)
            //{
            //    EmailMessage mesaage = (EmailMessage)item;
            //    parts.Add(mesaage);
            //}
            

            






            
        }


        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }
        
        #endregion
    }
}
