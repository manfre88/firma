﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;

namespace ClinicaCES.Correo
{
    public class Correo
    {
        #region Variables
        private string strDe;
        private string strPara;
        private string strAsunto;
        private string strMensaje;
        private string[] arrAdjuntos;
        #endregion

        #region Propiedades
        public string De
        {
            set
            {
                strDe = value;
            }
        }
        public string Para
        {
            set
            {
                strPara = value;
            }
        }
        public string Asunto
        {
            set
            {
                strAsunto = value;
            }
        }
        public string Mensaje
        {
            set
            {
                strMensaje = value;
            }
        }

        public string[] Adjuntos
        {
            set
            {
                arrAdjuntos = value;
            }
        }

        #endregion

        #region Metodos

        public bool Enviar()
        {
            System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
            correo.From = new System.Net.Mail.MailAddress(strDe);
            correo.To.Add(strPara);
            correo.Subject = strAsunto;
            correo.Body = strMensaje;
            System.Net.Mail.Attachment attachment;
            if (arrAdjuntos != null)
            {
                foreach (string item in arrAdjuntos)
                {
                    attachment = new System.Net.Mail.Attachment(item.Replace("\\\\", "\\"));
                    correo.Attachments.Add(attachment);
                }
            }
            
            


            correo.IsBodyHtml = false;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = ConfigurationManager.AppSettings["smtp"];
            smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
            //smtp.Credentials = new System.Net.NetworkCredential("UsuariodelDominio", "contraseñadelDominio");
            try
            {
                smtp.Send(correo);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
