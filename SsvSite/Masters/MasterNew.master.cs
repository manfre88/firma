﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Masters_MasterNew : System.Web.UI.MasterPage
{
    #region "Formulario"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //PermisosFormulario(Session["Usuario"].ToString(), "Principal.Master", menu);
            CargarMenuPPal();

        }
    }
    protected void lnkSalir_Click(object sender, EventArgs e)
    {
        HttpCookie SIDGVCookie = Request.Cookies["SIDGV1"];

        SIDGVCookie["Save1"] = "false";
        SIDGVCookie["Nombre1"] = Session["Nombre1"].ToString();
        SIDGVCookie["Perfil"] = Session["Perfil1"].ToString();
        SIDGVCookie["Nick1"] = Session["Nick1"].ToString();

        SIDGVCookie.Expires = DateTime.Now.AddDays(10d);
        Response.Cookies.Add(SIDGVCookie);
        //Response.Cookies.Add(SIDGVCookie);
        Session.Clear();
        Response.Redirect("~/General/LoginNew.aspx");
    }
    //protected void lnkDirectorio_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("~/Formularios/Principal/frmDirectorio.aspx");
    //}
    //protected void imguser_DataBinding(object sender, EventArgs e)
    //{
    //    cEmpleados_Activos objEmpleados_Activos = new cEmpleados_Activos();
    //    objEmpleados_Activos.E_MAIL = Session["EmailUsuario"].ToString();
    //    DataTable dtDatos = objEmpleados_Activos.ListarPorCorreo();

    //    Session["EMPLEADO"] = dtDatos.Rows[0]["EMPLEADO"].ToString();
    //}

    #endregion
    #region "Funciones"

    private bool CargarMenuPPal()
    {
        

        //lblProyecto.Text =Session["Codigo_Proyecto"].ToString()+"  "+ Session["Proyecto"].ToString();
        lblNombreUsuario.Text = Session["Nick1"].ToString();
        lblNombre.Text = Session["Nombre1"].ToString();
        lblNombre_Cargo.Text = Session["PerfilN1"].ToString();
        imguser.ImageUrl = "~/img/User.svg.png";
        imguserpeq.ImageUrl = "~/img/User.svg.png";
        imguserdir.ImageUrl = "~/img/User.svg.png";
        //lblFuncionario.Text = "Funcionario desde el " + Convert.ToDateTime(dtDatos.Rows[0]["F_INGRESO"]).ToLongDateString().ToString();
        //Session["EMPLEADO"] = dtDatos.Rows[0]["EMPLEADO"].ToString();

        //imguser.ImageUrl = "~/ImageHandler.ashx?EMPLEADO=" + Session["EMPLEADO"].ToString();
        //imguserdir.ImageUrl = "~/ImageHandler.ashx?EMPLEADO=" + Session["EMPLEADO"].ToString();
        //imguserpeq.ImageUrl = "~/ImageHandler.ashx?EMPLEADO=" + Session["EMPLEADO"].ToString();

        return true;

    }
    //private bool PermisosFormulario(string Usuario_windows, string Formulario_Padre, Control control)
    //{
    //    cUsuarios objUsuarios = new cUsuarios();
    //    objUsuarios.Usuario_windows = Usuario_windows;
    //    objUsuarios.Formulario_Padre = Formulario_Padre;
    //    DataTable dtPermisos = objUsuarios.ListarPermisosFormulario();

    //    foreach (DataRow row in dtPermisos.Rows)
    //    {
    //        foreach (Control lih in control.Controls)
    //        {
    //            if (lih.ID == row[9].ToString())
    //            {
    //                lih.Visible = true;

    //                foreach (Control li in lih.Controls)
    //                {
    //                    if (li.ID == row[5].ToString())
    //                    {
    //                        li.Visible = true;
    //                    }
    //                }

    //            }
    //        }
    //    }

    //    return true;
    //}

    #endregion

}
