﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InfPenAutHos.aspx.cs" Inherits="Consultas_InfPenAutHos" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="InfPenAutHos.aspx.cs" Inherits="Consultas_InfPenAutHos" Title="Informe de Autorizaciones Pendientes en Pacientes Hospitalizados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   

    <br />
    
    <table align="center" style="width: 605px">
        <tr>
            <td align="center">
    <asp:Panel ID="pnlInforme" runat="server" Visible="false">
    
    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../icons/icon_excel.gif" 
            onclick="Excel_Click" style="height: 16px" />
        <table border="1">
        </table>
        <table align="center" style="width: 590px">
            <tr>
                <td>
                    <asp:GridView AutoGenerateColumns="False" ID="gvInforme" runat="server" AllowPaging="True" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="20" 
                        onrowdatabound="gvInforme_RowDataBound"
                        onpageindexchanging="gvInforme_PageIndexChanging" Width="570px">                   
                        <FooterStyle BackColor="#F7DFB5" />
                        <RowStyle CssClass="normalrow"/>
                        <AlternatingRowStyle CssClass="alterrow" />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White"  />                
                        <Columns>      
                             <asp:BoundField DataField="ORDNOMBRE" HeaderText="ORDNOMBRE" />
                             <asp:BoundField DataField="ORDNUMERO" HeaderText="ORDNUMERO" />
                             <asp:BoundField DataField="PRECOD" HeaderText="PRECOD" />
                             <asp:BoundField DataField="PRENOM" HeaderText="PRENOM" />
                             <asp:BoundField DataField="FECHASOLIC" HeaderText="FECHASOLIC" />   
                             <asp:BoundField DataField="CODESTADO" HeaderText="CODESTADO" />
                             <asp:BoundField DataField="OBSERVACION" HeaderText="OBSERVACION" />
                             <asp:BoundField DataField="MTVCORRELATIVO" HeaderText="MTVCORRELATIVO" />
                             <asp:BoundField DataField="ID" HeaderText="ID" />
                             <asp:BoundField DataField="CAMASERVICIO" HeaderText="CAMASERVICIO" />
                             <asp:BoundField DataField="NOMBRES" HeaderText="NOMBRES" />
                             <asp:BoundField DataField="CONVENIO" HeaderText="CONVENIO" />
                                                                               
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
            </td>
        </tr>
    </table>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

