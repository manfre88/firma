﻿<%@ Page Title="Busqueda paciente para generar Anexo1" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="BusquedadAnexoNew.aspx.cs" Inherits="Anexos_BusquedadAnexoNew" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box box-primary">
                    <!--div Título-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3 align="center" runat="server" class="box-title">Consentimientos Informados</h3>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!--div campo Tipo identificación-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h5>Tipo de identificación</h5>
                                <asp:DropDownList ID="ddlTipoId" runat="server" AutoPostBack="True" class="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                    <!--div campo Identificación-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h5>Identificación</h5>
                                <asp:TextBox ID="txtId" runat="server" class="form-control" placeholder="Identificación"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                    <!--div botón Consultar-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="Click_Botones" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--div tabla-->
    <div class="box box-primary" id="divtable" runat="server" visible="false">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group"></div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <table class="table table-responsive">
                        <tr>
                            <td>
                                <asp:Panel ID="pnlInforme" runat="server" Visible="false">
                                    <table class="table table-responsive">
                                        
                                        <tr>
                                            <td >Nombre Paciente:</td>
                                            <td >
                                                <asp:Label ID="LblNombre" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Cama:</td>
                                            <td>
                                                <asp:Label ID="LblCama" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Consentiento:</td>
                                            <td>
                                                <div class="icheck-material-blue">
                                                    <label>
                                                        <asp:CheckBox ID="chkcons1" runat="server" Text="F-HC-2 Autorización de representante" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="chkcons1_CheckedChanged" />
                                                    </label> <br /> <br />
                                                    <label>
                                                        <asp:CheckBox ID="chkcons2" runat="server" Text="F-HC-13 Consulta historia clínica" CssClass="icheck-material-blue" AutoPostBack="True" />
                                                    </label> <br /> <br />
                                                    <label>
                                                        <asp:CheckBox ID="chkcons3" runat="server" Text="F-HC-7 Procedimientos de enfermería" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="chkcons3_CheckedChanged" />
                                                    </label> <br /> <br />
                                                    <label>
                                                        <asp:CheckBox ID="chkcons4" runat="server" Text="F-HC-4 Notificación acompañante permanente" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="chkcons3_CheckedChanged" />
                                                    </label> <br /> <br />
                                                </div>
                                                
                                                </td>
                                        </tr>
                                        <asp:Panel ID="pnlAcompanante" runat="server" Visible="false">
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Cédula Autorizado :</td>
                                                <td>
                                                    <asp:TextBox ID="txtIdAut" runat="server" class="form-control" placeholder="Cédula Autorizado"></asp:TextBox><br />
                                                </td>
                                                
                                            </tr>
                                            <tr></tr>
                                            <tr>
                                                <td>Nombre Autorizado :</td>
                                                <td>
                                                    <asp:TextBox ID="txtNomAut" runat="server" class="form-control" placeholder="Nombre Autorizado"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlEnfermeria" runat="server" Visible="false">
                                            <tr>
                                                <td>Consideraciones :</td>
                                                <td>
                                                    <asp:TextBox ID="txtConsideraciones" runat="server" CausesValidation="True" Height="83px" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </asp:Panel>
                                        <tr>
                                            <td>&nbsp; </td>
                                            <td>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnGeneraPDF" runat="server" CssClass="btn btn-primary" OnClick="Click_Botones" Text="Generar Consentimiento" />
                                                <asp:Button ID="btnReview" runat="server" CssClass="btn btn-primary" Text="Review" OnClick="btnReview_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <asp:Panel ID="Plnpdf" runat="server" Visible="false">
                            <tr>
                                <td>
                                   <asp:HyperLink ID="hyperlink1" ImageUrl="../img/pdf.png" Text="Descargue Archivo" Target="_new" runat="server" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td>
                                <asp:Label ID="lblDescripcion" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>

                    </table>
                    <div class="col-md-3">
                        <div class="form-group"></div>
                    </div>
                </div>
        </div>
    </div>
    </div>

    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

