﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" Title="Anexo 3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    
    <table style="width: 1003px; height: 96px;">
    <tr>
        <td rowspan="5" style="width: 63px">
            <asp:Image ID="Image1" runat="server" Height="77px" ImageUrl="~/img/escudocol.png" Width="102px" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="6"><B>MINISTERIO DE LA PROTECCION SOCIAL</B></td>
    </tr>
    <tr>
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="6"><B><font SIZE=4>SOLICITUD DE AUTORIZACION DE SERVICIOS DE SALUD</font></B></td>
    </tr>
    <tr>
        <td align="center" style="width: auto"><b>NUMERO DE SOLICITUD</b></td>
        <td align="center" style="width: auto; margin-left: 40px;">
            <asp:TextBox ID="TextBox1" runat="server" Width="173px" style="text-align:center" Font-Size="Small" MaxLength="15" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" runat="server" TargetControlID="TextBox1" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
        </td>
        <td align="center" style="width: auto"><b>Fecha:</b></td>
        <td align="center" style="width: auto">
            <asp:TextBox ID="TextBox10" runat="server" Width="135px" style="text-align:center" Font-Size="Small" MaxLength="10" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%><%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
        </td>
        <td align="center" style="width: auto"><b>Hora:</b></td>
        <td align="center" style="width: auto">
            <asp:TextBox ID="TextBox20" runat="server" Width="90px" style="text-align:center" Font-Size="Small" MaxLength="5" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
        </td>
    </tr>
</table>
   
    <table style="width: 975px">
    <tr>
        <td colspan="6"><b>INFORMACION DEL PRESTADOR (Solicitante)</b></td>
    </tr>
    <tr>
        <td style="width: auto"><b>Nombre</b></td>
        <td><b>NIT</b></td>
        <td>
            <asp:TextBox ID="TextBox25" runat="server" Width="16px" ReadOnly="True" Font-Underline="False" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">X</asp:TextBox>
        </td>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td colspan="2">
            <asp:TextBox ID="TextBox26" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox27" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">9</asp:TextBox>
            <asp:TextBox ID="TextBox28" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox29" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">9</asp:TextBox>
            <asp:TextBox ID="TextBox30" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox31" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox32" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">6</asp:TextBox>
            <asp:TextBox ID="TextBox33" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox34" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox35" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox36" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">-</asp:TextBox>
            <asp:TextBox ID="TextBox37" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBox38" runat="server" Enabled="False" ReadOnly="True" Width="550px" CssClass="form_input">CORPORACION PARA ESTUDIOS EN SALUD CLINICA CES</asp:TextBox>
        </td>
        <td><b>CC</b></td>
        <td>
            <asp:TextBox ID="TextBox39" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" ReadOnly="True" CssClass="form_input"></asp:TextBox>
            </td>
        <td>&nbsp;</td>
        <td>Numero:</td>
        <td>
            <asp:TextBox ID="TextBox41" runat="server" Width="249px" Enabled="False" ReadOnly="True" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
</table>
  
    <table style="width: 975px">
    <tr>
        <td style="width: 64px"><b>Codigo:</b></td>
        <td style="width: 378px">
            <asp:TextBox ID="TextBox42" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox43" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">5</asp:TextBox>
            <asp:TextBox ID="TextBox44" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox45" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox46" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
            <asp:TextBox ID="TextBox47" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox48" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox49" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
            <asp:TextBox ID="TextBox50" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox51" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">4</asp:TextBox>
            <asp:TextBox ID="TextBox52" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox53" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            </td>
        <td><b>Direcciòn Prestador:</b></td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td rowspan="2" style="width: 64px"><b>Teléfono:</b></td>
        <td style="width: 191px">
            <asp:TextBox ID="TextBox54" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">4</asp:TextBox>
            <asp:TextBox ID="TextBox55" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox56" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox57" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox58" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            </td>
        <td style="width: 348px">
            <asp:TextBox ID="TextBox59" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">5</asp:TextBox>
            <asp:TextBox ID="TextBox60" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox61" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">6</asp:TextBox>
            <asp:TextBox ID="TextBox62" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox63" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox64" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox65" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            </td>
        <td colspan="9">
            <asp:TextBox ID="TextBox66" runat="server" Width="514px" Enabled="False" CssClass="form_input">CALLE 58 # 50C-2 PRADO CENTRO</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 191px" align="center">Indicativo</td>
        <td style="width: 348px" align="center">Número</td>
        <td style="width: auto"><b>Departamento:</b></td>
        <td>
            <asp:TextBox ID="TextBox67" runat="server" Enabled="False" ReadOnly="True" Width="130px" CssClass="form_input">ANTIOQUIA</asp:TextBox>
        </td>
        <td bgcolor="Gray">
            &nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td><b>Municipio:</b></td>
        <td>
            <asp:TextBox ID="TextBox68" runat="server" Enabled="False" ReadOnly="True" Width="130px" CssClass="form_input">MEDELLIN</asp:TextBox>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
    </tr>
    </table>

    <table style="width: 975px">
    <tr>
        <td style="height: 26px;"><b>ENTIDAD A LA QUE SE LE SOLICITA (Pagador)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
        <td style="height: 26px">&nbsp;<b>CODIGO:</b></td>
        <td style="height: 26px">
            <asp:TextBox ID="TextBox69" runat="server" Width="99px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="TextBox69" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox2" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="TextBox2_FilteredTextBoxExtender" runat="server" TargetControlID="TextBox2" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox3" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="TextBox3_FilteredTextBoxExtender" runat="server" TargetControlID="TextBox3" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox4" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox4" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            
            <asp:TextBox ID="TextBox5" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="TextBox5" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox6" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="TextBox6" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox7" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="TextBox7" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox8" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="TextBox8" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox9" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="TextBox9" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    </table>
   
    <table style="width: 975px">
    <tr>
        <td colspan="8" align="center"><B>DATOS DEL PACIENTE</b></td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBox75" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBox76" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox77" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox78" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px"><b>Primer Apellido</b></td>
        <td style="height: 23px"><b>Segundo Apellido</b></td>
        <td style="height: 23px" colspan="3"><b>Primer Nombre</b></td>
        <td style="height: 23px" colspan="3"><b>Segundo Nombre</b></td>
    </tr>
    <tr>
        <td colspan="8"><b>Tipo Documento de Identificación</b></td>
    </tr>
    <tr>
        <td colspan="2" rowspan="4">
            <asp:RadioButtonList ID="rblTipoId" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Width="473px" CssClass="form_input" Enabled="False">
                <asp:ListItem Value="2">Registro Civil</asp:ListItem>
                <asp:ListItem Value="P">Pasaporte</asp:ListItem>
                <asp:ListItem Value="3">Tarjeta de Identidad</asp:ListItem>
                <asp:ListItem Value="A">Adulto sin Identificación</asp:ListItem>
                <asp:ListItem Value="4">Cédula de Ciudadania</asp:ListItem>
                <asp:ListItem Value="M">Menor sin Idenficaci&#243;n</asp:ListItem>
                <asp:ListItem Value="5">Cédula de Extranjería</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td>&nbsp;</td>
        <td colspan="5">
            <asp:TextBox ID="TextBox81" runat="server" Width="280px" style="text-align:center" Font-Size="Small" MaxLength="10" Height="16px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="TextBox81" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="TextBox10" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" colspan="3">Número Documento de Identificación</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><b>Fecha de Nacimiento:</b></td>
        <td colspan="4">
            <asp:TextBox ID="TextBox102" runat="server" Width="163px" style="text-align:center" Font-Size="Small" MaxLength="10" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox11" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="TextBox11" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox12" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="TextBox12" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox13" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="TextBox13" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:Label ID="Label3" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox14" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="TextBox14" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox15" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="TextBox15" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:Label ID="Label1" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox16" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="TextBox16" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox17" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="TextBox17" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td><b>Dirección de Residencia Habitual:</b></td>
        <td>
            <asp:TextBox ID="TextBox112" runat="server" Width="396px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td><b>Teléfono:</b></td>
        <td>
            <asp:TextBox ID="TextBox113" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="1" Height="16px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender54" runat="server" TargetControlID="TextBox113" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="TextBox20" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox21" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="TextBox21" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:Label ID="Label2" runat="server" Text=":" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox22" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="TextBox22" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox23" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="TextBox23" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td><b>Departamento:</b></td>
        <td>
            <asp:TextBox ID="TextBox123" runat="server" Width="240px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td><b>Municipio:</b></td>
        <td>
            <asp:TextBox ID="TextBox124" runat="server" Width="240px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
    </tr>
    </table>
   
    <table style="width: 975px">
    <tr>
        <td><b>Teléfono Celular:</b></td>
        <td>
            <asp:TextBox ID="TextBox125" runat="server" Width="186px" style="text-align:center" Font-Size="Small" MaxLength="1" Height="16px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender63" runat="server" TargetControlID="TextBox125" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <asp:TextBox ID="TextBox70" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="TextBox70" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox71" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="TextBox71" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox72" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="TextBox72" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox73" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="TextBox73" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox74" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="TextBox74" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td><b>Correo Electrónico:</b></td>
        <td>
            <asp:TextBox ID="TextBox136" runat="server" Width="434px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    </table>
   
    <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b>Cobertura en Salud</b></td>
    </tr>
    <tr>
        <td>
            <asp:RadioButtonList ID="rblCobertura" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" CssClass="form_input" Width="975px">
                <asp:ListItem Value="1">Regimen Contributivo</asp:ListItem>
                <asp:ListItem Value="2">Regimen Subsidiado - Parcial</asp:ListItem>
                <asp:ListItem Value="3">Población Pobre No Asegurada Sin SISBEN</asp:ListItem>
                <asp:ListItem Value="4">Plan Adicional de Salud</asp:ListItem>
                <asp:ListItem Value="5">Regimen Subsidiado - Total</asp:ListItem>
                <asp:ListItem Value="6">Población Pobre No Asegurada Con SISBEN</asp:ListItem>
                <asp:ListItem Value="7">Desplazado</asp:ListItem>
                <asp:ListItem Value="8">Otro</asp:ListItem>
            </asp:RadioButtonList>
            </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px" align="center" colspan="7"><B>INFORMACIÓN DE LA ATENCIÓN Y SERVICIOS SOLICITADOS</B></td>
    </tr>
    <tr>
        <td style="height: 23px"><b>Origen de la atención</b></td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px"><b>Tipo de servicios solicitados</b></td>
        <td style="height: 23px"><b>Prioridad de la atención</b></td>
    </tr>
    <tr>
        <td align="left" colspan="5">
            <asp:RadioButtonList ID="rblAtencion" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="form_input">
                <asp:ListItem Value="13">Enfermedad General</asp:ListItem>
                <asp:ListItem Value="01">Accidente de Trabajo</asp:ListItem>
                <asp:ListItem Value="06">Evento Catastrófico</asp:ListItem>
                <asp:ListItem Value="14">Enfermedad Profesional</asp:ListItem>
                <asp:ListItem Value="02">Accidente de Tránsito</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td align="left">
            <asp:RadioButtonList ID="rblServicio" runat="server" CssClass="form_input">
                <asp:ListItem Value="1">Posterior a la atención inicial de urgencias</asp:ListItem>
                <asp:ListItem Value="2">Servicios Electivos</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td align="left">
            <asp:RadioButtonList ID="rblPrioridad" runat="server" CssClass="form_input">
                <asp:ListItem Value="1">Prioritaria</asp:ListItem>
                <asp:ListItem Value="2">No Prioritaria</asp:ListItem>
            </asp:RadioButtonList>
            </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px" colspan="5"><b>Ubicación del paciente al momento de la solicitud de autorización</b></td>
    </tr>
    <tr>
        <td align="left" rowspan="2">
            <asp:RadioButtonList ID="rblUbicacion" runat="server" CssClass="form_input" RepeatColumns="2" RepeatDirection="Horizontal" Width="341px" Enabled="False">
                <asp:ListItem Value="2">Consulta Externa</asp:ListItem>
                <asp:ListItem Value="3">Hospitalización</asp:ListItem>
                <asp:ListItem Value="1">Urgencias</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td style="height: 23px">Servicio</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox164" runat="server" Width="286px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td style="height: 23px">Cama</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox157" runat="server" Width="72px" style="text-align:center" Font-Size="Small" MaxLength="4" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox82" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="TextBox82" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox83" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="TextBox83" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox84" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="TextBox84" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox85" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="TextBox85" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox86" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="TextBox86" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox87" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="TextBox87" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox88" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="TextBox88" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox89" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="TextBox89" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox90" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="TextBox90" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox91" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" TargetControlID="TextBox91" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox92" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" TargetControlID="TextBox92" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox93" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" TargetControlID="TextBox93" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox94" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" TargetControlID="TextBox94" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox95" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server" TargetControlID="TextBox95" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox96" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender43" runat="server" TargetControlID="TextBox96" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    <tr>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
    </tr>
    </table>
   
    <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b></b>Manejo integral según guía de:</b></td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox165" runat="server" Width="766px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px"><b>Código CUPS</b></td>
        <td style="height: 23px"><b>Cantidad</b></td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px"><b>Descripción</b></td>
    </tr>
    <tr>
        <td style="height: 23px">1</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox166" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%-- <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender44" runat="server" TargetControlID="TextBox102" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox103" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender45" runat="server" TargetControlID="TextBox103" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox104" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender46" runat="server" TargetControlID="TextBox104" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox105" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender47" runat="server" TargetControlID="TextBox105" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox106" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender48" runat="server" TargetControlID="TextBox106" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox107" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender49" runat="server" TargetControlID="TextBox107" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox108" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender50" runat="server" TargetControlID="TextBox108" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox109" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender51" runat="server" TargetControlID="TextBox109" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox110" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender52" runat="server" TargetControlID="TextBox110" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox111" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender53" runat="server" TargetControlID="TextBox111" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--  <asp:TextBox ID="TextBox114" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender55" runat="server" TargetControlID="TextBox114" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox115" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender56" runat="server" TargetControlID="TextBox115" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox116" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender57" runat="server" TargetControlID="TextBox116" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox117" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender58" runat="server" TargetControlID="TextBox117" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox118" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender59" runat="server" TargetControlID="TextBox118" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox119" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender60" runat="server" TargetControlID="TextBox119" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox120" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender61" runat="server" TargetControlID="TextBox120" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox121" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender62" runat="server" TargetControlID="TextBox121" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox174" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" Height="16px" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender87" runat="server" TargetControlID="TextBox174" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox126" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender64" runat="server" TargetControlID="TextBox126" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox127" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender65" runat="server" TargetControlID="TextBox127" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox128" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender66" runat="server" TargetControlID="TextBox128" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox129" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender67" runat="server" TargetControlID="TextBox129" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox130" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender68" runat="server" TargetControlID="TextBox130" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox131" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender69" runat="server" TargetControlID="TextBox131" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox132" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender70" runat="server" TargetControlID="TextBox132" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox133" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender71" runat="server" TargetControlID="TextBox133" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox134" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender72" runat="server" TargetControlID="TextBox134" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox135" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender73" runat="server" TargetControlID="TextBox135" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox173" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">2</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox177" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%-- <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender74" runat="server" TargetControlID="TextBox157" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox158" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender75" runat="server" TargetControlID="TextBox158" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox159" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender76" runat="server" TargetControlID="TextBox159" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox160" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender77" runat="server" TargetControlID="TextBox160" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox161" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender78" runat="server" TargetControlID="TextBox161" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox162" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender79" runat="server" TargetControlID="TextBox162" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender80" runat="server" TargetControlID="TextBox166" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox184" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender97" runat="server" TargetControlID="TextBox184" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <asp:TextBox ID="TextBox167" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender81" runat="server" TargetControlID="TextBox167" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox168" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender82" runat="server" TargetControlID="TextBox168" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox169" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender83" runat="server" TargetControlID="TextBox169" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox170" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender84" runat="server" TargetControlID="TextBox170" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox171" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender85" runat="server" TargetControlID="TextBox171" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox172" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender86" runat="server" TargetControlID="TextBox172" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            

            <asp:TextBox ID="TextBox450" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">3</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox188" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox175" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender88" runat="server" TargetControlID="TextBox175" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox176" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender89" runat="server" TargetControlID="TextBox176" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender90" runat="server" TargetControlID="TextBox177" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox314" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender107" runat="server" TargetControlID="TextBox314" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <asp:TextBox ID="TextBox178" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender91" runat="server" TargetControlID="TextBox178" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox179" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender92" runat="server" TargetControlID="TextBox179" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox180" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender93" runat="server" TargetControlID="TextBox180" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox181" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender94" runat="server" TargetControlID="TextBox181" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox182" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender95" runat="server" TargetControlID="TextBox182" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox183" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender96" runat="server" TargetControlID="TextBox183" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox431" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">4</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox195" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox185" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender98" runat="server" TargetControlID="TextBox185" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox186" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender99" runat="server" TargetControlID="TextBox186" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender100" runat="server" TargetControlID="TextBox188" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox317" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender117" runat="server" TargetControlID="TextBox317" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <asp:TextBox ID="TextBox189" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender101" runat="server" TargetControlID="TextBox189" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox190" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender102" runat="server" TargetControlID="TextBox190" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox191" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender103" runat="server" TargetControlID="TextBox191" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox192" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender104" runat="server" TargetControlID="TextBox192" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox193" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender105" runat="server" TargetControlID="TextBox193" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox194" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender106" runat="server" TargetControlID="TextBox194" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox432" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">5</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox202" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox315" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender108" runat="server" TargetControlID="TextBox315" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox316" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender109" runat="server" TargetControlID="TextBox316" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender110" runat="server" TargetControlID="TextBox195" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox320" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender127" runat="server" TargetControlID="TextBox320" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <asp:TextBox ID="TextBox196" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender111" runat="server" TargetControlID="TextBox196" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox197" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender112" runat="server" TargetControlID="TextBox197" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox198" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender113" runat="server" TargetControlID="TextBox198" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox199" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender114" runat="server" TargetControlID="TextBox199" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox200" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender115" runat="server" TargetControlID="TextBox200" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox201" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender116" runat="server" TargetControlID="TextBox201" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox433" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">6</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox209" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox318" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender118" runat="server" TargetControlID="TextBox318" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox319" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender119" runat="server" TargetControlID="TextBox319" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender120" runat="server" TargetControlID="TextBox202" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox323" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender137" runat="server" TargetControlID="TextBox323" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox203" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender121" runat="server" TargetControlID="TextBox203" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox204" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender122" runat="server" TargetControlID="TextBox204" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox205" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender123" runat="server" TargetControlID="TextBox205" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox206" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender124" runat="server" TargetControlID="TextBox206" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox207" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender125" runat="server" TargetControlID="TextBox207" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox208" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender126" runat="server" TargetControlID="TextBox208" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox434" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">7</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox216" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox321" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender128" runat="server" TargetControlID="TextBox321" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox322" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender129" runat="server" TargetControlID="TextBox322" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender130" runat="server" TargetControlID="TextBox209" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox326" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender147" runat="server" TargetControlID="TextBox326" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox210" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender131" runat="server" TargetControlID="TextBox210" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox211" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender132" runat="server" TargetControlID="TextBox211" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox212" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender133" runat="server" TargetControlID="TextBox212" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox213" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender134" runat="server" TargetControlID="TextBox213" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox214" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender135" runat="server" TargetControlID="TextBox214" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox215" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender136" runat="server" TargetControlID="TextBox215" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox435" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">8</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox223" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%-- <asp:TextBox ID="TextBox324" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender138" runat="server" TargetControlID="TextBox324" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox325" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender139" runat="server" TargetControlID="TextBox325" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            </td>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender140" runat="server" TargetControlID="TextBox216" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox329" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender157" runat="server" TargetControlID="TextBox329" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox217" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender141" runat="server" TargetControlID="TextBox217" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox218" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender142" runat="server" TargetControlID="TextBox218" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox219" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender143" runat="server" TargetControlID="TextBox219" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox220" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender144" runat="server" TargetControlID="TextBox220" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox221" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender145" runat="server" TargetControlID="TextBox221" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox222" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender146" runat="server" TargetControlID="TextBox222" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox436" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">9</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox230" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%-- <asp:TextBox ID="TextBox327" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender148" runat="server" TargetControlID="TextBox327" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox328" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender149" runat="server" TargetControlID="TextBox328" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender150" runat="server" TargetControlID="TextBox223" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox332" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender167" runat="server" TargetControlID="TextBox332" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox224" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender151" runat="server" TargetControlID="TextBox224" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox225" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender152" runat="server" TargetControlID="TextBox225" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox226" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender153" runat="server" TargetControlID="TextBox226" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox227" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender154" runat="server" TargetControlID="TextBox227" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox228" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender155" runat="server" TargetControlID="TextBox228" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox229" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender156" runat="server" TargetControlID="TextBox229" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox437" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">10</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox237" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox330" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender158" runat="server" TargetControlID="TextBox330" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox331" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender159" runat="server" TargetControlID="TextBox331" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender160" runat="server" TargetControlID="TextBox230" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox335" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender177" runat="server" TargetControlID="TextBox335" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox231" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender161" runat="server" TargetControlID="TextBox231" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox232" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender162" runat="server" TargetControlID="TextBox232" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox233" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender163" runat="server" TargetControlID="TextBox233" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox234" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender164" runat="server" TargetControlID="TextBox234" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox235" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender165" runat="server" TargetControlID="TextBox235" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox236" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender166" runat="server" TargetControlID="TextBox236" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox438" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">11</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox244" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox333" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender168" runat="server" TargetControlID="TextBox333" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox334" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender169" runat="server" TargetControlID="TextBox334" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender170" runat="server" TargetControlID="TextBox237" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox338" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender187" runat="server" TargetControlID="TextBox338" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox238" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender171" runat="server" TargetControlID="TextBox238" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox239" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender172" runat="server" TargetControlID="TextBox239" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox240" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender173" runat="server" TargetControlID="TextBox240" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox241" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender174" runat="server" TargetControlID="TextBox241" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox242" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender175" runat="server" TargetControlID="TextBox242" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox243" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender176" runat="server" TargetControlID="TextBox243" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox439" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">12</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox251" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox336" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender178" runat="server" TargetControlID="TextBox336" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox337" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender179" runat="server" TargetControlID="TextBox337" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender180" runat="server" TargetControlID="TextBox244" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox341" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender197" runat="server" TargetControlID="TextBox341" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox245" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender181" runat="server" TargetControlID="TextBox245" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox246" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender182" runat="server" TargetControlID="TextBox246" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox247" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender183" runat="server" TargetControlID="TextBox247" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox248" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender184" runat="server" TargetControlID="TextBox248" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox249" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender185" runat="server" TargetControlID="TextBox249" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox250" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender186" runat="server" TargetControlID="TextBox250" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox440" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">13</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox258" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox339" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender188" runat="server" TargetControlID="TextBox339" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox340" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender189" runat="server" TargetControlID="TextBox340" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender190" runat="server" TargetControlID="TextBox251" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox344" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender207" runat="server" TargetControlID="TextBox344" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox252" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender191" runat="server" TargetControlID="TextBox252" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox253" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender192" runat="server" TargetControlID="TextBox253" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox254" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender193" runat="server" TargetControlID="TextBox254" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox255" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender194" runat="server" TargetControlID="TextBox255" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox256" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender195" runat="server" TargetControlID="TextBox256" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox257" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender196" runat="server" TargetControlID="TextBox257" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox441" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">14</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox265" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox342" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender198" runat="server" TargetControlID="TextBox342" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox343" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender199" runat="server" TargetControlID="TextBox343" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender200" runat="server" TargetControlID="TextBox258" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox347" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender217" runat="server" TargetControlID="TextBox347" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox259" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender201" runat="server" TargetControlID="TextBox259" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox260" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender202" runat="server" TargetControlID="TextBox260" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox261" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender203" runat="server" TargetControlID="TextBox261" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox262" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender204" runat="server" TargetControlID="TextBox262" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox263" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender205" runat="server" TargetControlID="TextBox263" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox264" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender206" runat="server" TargetControlID="TextBox264" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox442" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">15</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox272" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%-- <asp:TextBox ID="TextBox345" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender208" runat="server" TargetControlID="TextBox345" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox346" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender209" runat="server" TargetControlID="TextBox346" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender210" runat="server" TargetControlID="TextBox265" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox350" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender227" runat="server" TargetControlID="TextBox350" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox266" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender211" runat="server" TargetControlID="TextBox266" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox267" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender212" runat="server" TargetControlID="TextBox267" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox268" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender213" runat="server" TargetControlID="TextBox268" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox269" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender214" runat="server" TargetControlID="TextBox269" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox270" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender215" runat="server" TargetControlID="TextBox270" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox271" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender216" runat="server" TargetControlID="TextBox271" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox443" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">16</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox279" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox348" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender218" runat="server" TargetControlID="TextBox348" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox349" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender219" runat="server" TargetControlID="TextBox349" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender220" runat="server" TargetControlID="TextBox272" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox353" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender237" runat="server" TargetControlID="TextBox353" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <asp:TextBox ID="TextBox273" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender221" runat="server" TargetControlID="TextBox273" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox274" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender222" runat="server" TargetControlID="TextBox274" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox275" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender223" runat="server" TargetControlID="TextBox275" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox276" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender224" runat="server" TargetControlID="TextBox276" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox277" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender225" runat="server" TargetControlID="TextBox277" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox278" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender226" runat="server" TargetControlID="TextBox278" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox444" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">17</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox286" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox351" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender228" runat="server" TargetControlID="TextBox351" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox352" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender229" runat="server" TargetControlID="TextBox352" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender230" runat="server" TargetControlID="TextBox279" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox356" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender247" runat="server" TargetControlID="TextBox356" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <asp:TextBox ID="TextBox280" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender231" runat="server" TargetControlID="TextBox280" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox281" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender232" runat="server" TargetControlID="TextBox281" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox282" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender233" runat="server" TargetControlID="TextBox282" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox283" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender234" runat="server" TargetControlID="TextBox283" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox284" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender235" runat="server" TargetControlID="TextBox284" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox285" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender236" runat="server" TargetControlID="TextBox285" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox445" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">18</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox293" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox354" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender238" runat="server" TargetControlID="TextBox354" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox355" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender239" runat="server" TargetControlID="TextBox355" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender240" runat="server" TargetControlID="TextBox286" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox359" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender257" runat="server" TargetControlID="TextBox359" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox287" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender241" runat="server" TargetControlID="TextBox287" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox288" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender242" runat="server" TargetControlID="TextBox288" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox289" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender243" runat="server" TargetControlID="TextBox289" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox290" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender244" runat="server" TargetControlID="TextBox290" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox291" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender245" runat="server" TargetControlID="TextBox291" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox292" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender246" runat="server" TargetControlID="TextBox292" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox446" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">19</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox300" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox357" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender248" runat="server" TargetControlID="TextBox357" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox358" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender249" runat="server" TargetControlID="TextBox358" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender250" runat="server" TargetControlID="TextBox293" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox362" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender267" runat="server" TargetControlID="TextBox362" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox294" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender251" runat="server" TargetControlID="TextBox294" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox295" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender252" runat="server" TargetControlID="TextBox295" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox296" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender253" runat="server" TargetControlID="TextBox296" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox297" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender254" runat="server" TargetControlID="TextBox297" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox298" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender255" runat="server" TargetControlID="TextBox298" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox299" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender256" runat="server" TargetControlID="TextBox299" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox447" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">20</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox307" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="7" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox360" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender258" runat="server" TargetControlID="TextBox360" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox361" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender259" runat="server" TargetControlID="TextBox361" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender260" runat="server" TargetControlID="TextBox300" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox365" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender277" runat="server" TargetControlID="TextBox365" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox301" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender261" runat="server" TargetControlID="TextBox301" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox302" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender262" runat="server" TargetControlID="TextBox302" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox303" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender263" runat="server" TargetControlID="TextBox303" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox304" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender264" runat="server" TargetControlID="TextBox304" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox305" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender265" runat="server" TargetControlID="TextBox305" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox306" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender266" runat="server" TargetControlID="TextBox306" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox448" runat="server" Width="674px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b>Justificación Clínica</b></td>
    </tr>
    <tr>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox449" runat="server" Height="37px" TextMode="MultiLine" Width="948px" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b>Impresión Diagnóstica</b></td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">Código CIE10</td>
        <td style="height: 23px">Descripción</td>
    </tr>
    <tr>
        <td style="height: 23px">Diagnóstico Principal</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox387" runat="server" Width="85px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%-- <asp:TextBox ID="TextBox363" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender268" runat="server" TargetControlID="TextBox363" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox364" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender269" runat="server" TargetControlID="TextBox364" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox399" runat="server" Width="672px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">Diagnóstico Relacionado 1</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox391" runat="server" Width="85px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender270" runat="server" TargetControlID="TextBox307" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox400" runat="server" Width="672px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">Diagnóstico Relacionado 2</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox395" runat="server" Width="85px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<asp:TextBox ID="TextBox308" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender271" runat="server" TargetControlID="TextBox308" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox309" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender272" runat="server" TargetControlID="TextBox309" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox310" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender273" runat="server" TargetControlID="TextBox310" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox311" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender274" runat="server" TargetControlID="TextBox311" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox312" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender275" runat="server" TargetControlID="TextBox312" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox313" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender276" runat="server" TargetControlID="TextBox313" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox401" runat="server" Width="672px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px" colspan="6" align="center"><B>INFORMACIÓN DE LA PERSONA QUE SOLICITA</B></td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2">Nombre de la persona que solicita</td>
        <td rowspan="3">Teléfono</td>
        <td style="height: 23px" align="center">
            <asp:TextBox ID="TextBox403" runat="server" Width="91px" style="text-align:center" Font-Size="Small" MaxLength="1" Height="16px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender292" runat="server" TargetControlID="TextBox403" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox366" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender278" runat="server" TargetControlID="TextBox366" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox367" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender279" runat="server" TargetControlID="TextBox367" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px" align="center">
            <asp:TextBox ID="TextBox408" runat="server" Width="126px" style="text-align:center" Font-Size="Small" MaxLength="1" Height="16px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender297" runat="server" TargetControlID="TextBox408" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%-- <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender280" runat="server" TargetControlID="TextBox387" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox388" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender281" runat="server" TargetControlID="TextBox388" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox389" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender282" runat="server" TargetControlID="TextBox389" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox390" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender283" runat="server" TargetControlID="TextBox390" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
        <td style="height: 23px" align="center">
            <asp:TextBox ID="TextBox415" runat="server" Width="85px" style="text-align:center" Font-Size="Small" MaxLength="1" Height="16px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender304" runat="server" TargetControlID="TextBox415" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender284" runat="server" TargetControlID="TextBox391" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox392" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender285" runat="server" TargetControlID="TextBox392" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox393" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender286" runat="server" TargetControlID="TextBox393" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox394" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender287" runat="server" TargetControlID="TextBox394" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2">
            <asp:TextBox ID="TextBox402" runat="server" Width="448px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td style="height: 23px" align="center">Indicativo</td>
        <td style="height: 23px" align="center">Número</td>
        <td style="height: 23px" align="center">Extensión</td>
    </tr>
    <tr>
        <td style="height: 23px">Cargo o Actvidad:</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox419" runat="server" Width="334px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td style="height: 23px" align="center">Teléfono Celular:</td>
        <td style="height: 23px" colspan="2" align="center">
            <asp:TextBox ID="TextBox420" runat="server" Width="200px" style="text-align:center" Font-Size="Small" MaxLength="10" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender308" runat="server" TargetControlID="TextBox420" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender288" runat="server" TargetControlID="TextBox395" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox396" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender289" runat="server" TargetControlID="TextBox396" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox397" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender290" runat="server" TargetControlID="TextBox397" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox398" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender291" runat="server" TargetControlID="TextBox398" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    <tr>
        <td style="height: 23px; font-size: xx-small;" colspan="6">MPS-SAS V5.0 2008-07-11</td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px; width: 159px;"><b>Observaciones Correo:</b></td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox451" runat="server" CssClass="form_input" Width="786px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2" align="center">
            <asp:Button ID="Button1" runat="server" Text="Generar PDF" OnClick="Button1_Click" />
        &nbsp;<asp:Button ID="Button2" runat="server" Text="Imprimir PDF" OnClick="Button2_Click" Enabled="False" Visible="False" />
        </td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2" align="center">
            &nbsp;</td>
    </tr>
</table>
   <asp:Literal ID="litScript" runat="server"></asp:Literal>
    
</asp:Content>