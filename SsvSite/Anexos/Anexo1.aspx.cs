﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClinicaCES.Correo;
using ClinicaCES.Entidades;
using Microsoft;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;
using System.IO;

public partial class Anexos_Anexo1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            datosIniciales();
        }
    }
    protected void datosIniciales()
    {
        //generarSolicitud()
        if (Request.QueryString["cookie1"] != null)
        {
            txtFecha.Text = DateTime.Now.ToShortDateString();
            txtHora.Text = DateTime.Now.ToShortTimeString();
            Procedimientos.LlenarCombos(dllDepartamento, new ClinicaCES.Logica.LMaestros().ListarDepartamentos(), "CODDEP", "DEP");
            Procedimientos.LlenarCombos(ddlConvenio, new ClinicaCES.Logica.LMaestros().ListarEmpresasAnexo(), "CORREO", "CONVENIO");
            TraerDatos(Procedimientos.descifrar(Request.QueryString["cookie1"]));
            Procedimientos.LlenarCombosBlanco(ddlTipoId, new ClinicaCES.Logica.LMaestros().ListaTiposIdentificacion(), "TIPOID", "ID");
            CargarSolicitante();
        }
    }
    protected void CargarSolicitante()
    {
        try
        {
            DataRow Solicitante = new ClinicaCES.Logica.LMaestros().CargarSolicitante(Session["Nick1"].ToString()).Rows[0];
            TextBox402.Text = Solicitante[9].ToString();
            txtExtSolic.Text = Solicitante[6].ToString();
            txtNumeroSolic.Text = "5767272";
            txtIndicativoSolic.Text = "4";
            DataRow Cargo = new ClinicaCES.Logica.LMaestros().TraerCargo(Session["Nick1"].ToString()).Rows[0];
            TextBox419.Text = Cargo[0].ToString();
        }
        catch
        {
            string msg = "67";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            Response.Redirect("../General/LogIn.aspx");
        }

    }
    protected void dllDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        Procedimientos.LlenarCombos(ddlMunicipio, new ClinicaCES.Logica.LMaestros().ListarMunicipios(dllDepartamento.SelectedValue), "CODMUN", "MUN");

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string msg = "";
        if (validar())
        {
            Button1.Enabled = false;
            Button2.Visible = true;
            Button2.Enabled = true;
            DataRow Cargo = new ClinicaCES.Logica.LMaestros().generarSolicitudAne1().Rows[0];
            txtNumInforme.Text = Cargo[0].ToString();
            string html = retornarHtml();
            var htmlContent = String.Format(html);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            string pApellido = "";
            string sApellido = "";
            string pNombre = "";
            string sNombre = "";
            if (TextBox75.Text != "") { pApellido = TextBox75.Text.Substring(0, TextBox75.Text.Length - TextBox75.Text.Length + 1).Trim(); }
            if (TextBox76.Text != "") { sApellido = TextBox76.Text.Substring(0, TextBox76.Text.Length - TextBox76.Text.Length + 1).Trim(); }
            if (TextBox77.Text != "") { pNombre = TextBox77.Text.Substring(0, TextBox77.Text.Length - TextBox77.Text.Length + 1).Trim(); }
            if (TextBox78.Text != "") { sNombre = TextBox78.Text.Substring(0, TextBox78.Text.Length - TextBox78.Text.Length + 1).Trim(); }
            string archivo = ViewState["tipoId"].ToString() + "-" + txtDocOrigen.Text.Trim() + "-" + pApellido + sApellido + pNombre + sNombre + "-" + txtNumInforme.Text;
            string convenio = ddlConvenio.SelectedValue;
            //string value = ddlConvenio.SelectedItem.Text;
            string Mensaje = construirCorreo();

            htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + archivo + "_Anexo1.pdf"));
            if (enviar(convenio, Mensaje, archivo + "_Anexo1.pdf - Envio No: " + 1, Server.MapPath("../AUTPDF/" + archivo + "_Anexo1.pdf")))
            {
                Anexo1 ane1 = new Anexo1();

                ane1.Numero = txtNumInforme.Text;
                ane1.Fecha = txtFecha.Text;
                ane1.Hora = txtHora.Text;
                //ane1.TipoInconsistencia = TextBox1.Text + "|" + TextBox2.Text;
                ane1.TipoInconsistencia = rblInconsistencia.Text;
                ane1.Cobertura = rblCobertura.SelectedValue;
                string Doc = rblTipoId.SelectedValue;
                ane1.InfoBD = TextBox75.Text.ToUpper() + "|" + TextBox76.Text.ToUpper() + "|" + TextBox77.Text.ToUpper() + "|" + Doc + "|" + txtDocOrigen.Text.ToUpper() + "|" + txtFechaOrigen.Text.ToUpper() + "|" + TextBox112.Text.ToUpper() + "|" + txtTelefonoOrigen.Text.ToUpper() + "|" + dllDepartamento.SelectedItem.Text.ToUpper() + "|" + ddlMunicipio.SelectedItem.Text.ToUpper();
                ane1.Correccion = txtPrimerApellidoDest.Text.ToUpper() + "|" + txtSegundoApellidoDest.Text.ToUpper() + "|" + txtPrimerNombreDest.Text.ToUpper() + "|" + txtSegundoNombreDest.Text.ToUpper() + "|" + ddlTipoId.SelectedItem.ToString().ToUpper() + "|" + txtNumDocDest.Text.ToUpper() + "|" + txtFechaNacDest.Text.ToUpper();
                ane1.Observacion = TextBox386.Text.ToUpper();
                ane1.Solicitante = txtIndicativoSolic.Text + "|" + txtNumeroSolic.Text + "|" + txtExtSolic.Text + "|" + TextBox419.Text.ToUpper() + "|" + txtCelularSolicita.Text.ToUpper();
                ane1.pdf = "../AUTPDF/" + archivo + "_Anexo1.pdf";
                ane1.Usuario = Session["Nick1"].ToString();
                ane1.Para = convenio;
                ane1.Mensaje = Mensaje;
                ane1.Mensaje2 = "";
                ane1.TipoAnexo = "1";
                ane1.Asunto = archivo + "_Anexo1.pdf";
                ane1.Ruta = Server.MapPath("../AUTPDF/" + archivo + "_Anexo1.pdf");
                ane1.Identificacion = txtDocOrigen.Text;
                if (new ClinicaCES.Logica.LAnexo1().Anexo1Crear(ane1))
                {
                    msg = "1";
                    //ddlConvenio.SelectedItem.Text = value;
                    inactivarControles();
                    ViewState["Archivo1"] = archivo + "_Anexo1.pdf";
                }
                else
                {
                    msg = "3";
                }
            }
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
        else
        {
            string value = ddlConvenio.SelectedItem.Text;
            msg = "68";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
        
    }
    protected void AgregarPrintScript(string ruta)
    {
        string msg = "";
        try
        {
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", ruta));
            Response.ContentType = "application/pdf";
            Response.WriteFile(Server.MapPath(Path.Combine("~/AUTPDF", ruta)));
            Response.End();
        }
        catch
        {
            msg = "74";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected bool validar()
    {
        bool Valido = true;

        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };

        if (string.IsNullOrEmpty(rblInconsistencia.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            //lblValidaNombres.ForeColor = color[1];
            rblInconsistencia.CssClass = css[1];
        }
        else
        {
            //lblValidaNombres.ForeColor = color[0];
            rblInconsistencia.CssClass = css[0];
        }
        //if (string.IsNullOrEmpty(TextBox75.Text.Trim()))
        //{
        //    Valido = false;
        //    //lblValidaApellido.ForeColor = color[1];
        //    TextBox75.CssClass = css[1];
        //}
        //else
        //{
        //    //lblValidaApellido.ForeColor = color[0];
        //    TextBox75.CssClass = css[0];
        //}
        ////if (string.IsNullOrEmpty(TextBox76.Text.Trim()))
        ////{
        ////    Valido = false;
        ////    //lblValidaApellido.ForeColor = color[1];
        ////    TextBox76.CssClass = css[1];
        ////}
        ////else
        ////{
        ////    //lblValidaApellido.ForeColor = color[0];
        ////    TextBox76.CssClass = css[0];
        ////}

        //if (string.IsNullOrEmpty(TextBox77.Text.Trim()))
        //{
        //    Valido = false;
        //    //lblValidaApellido.ForeColor = color[1];
        //    TextBox77.CssClass = css[1];
        //}
        //else
        //{
        //    //lblValidaApellido.ForeColor = color[0];
        //    TextBox77.CssClass = css[0];
        //}
        //if (string.IsNullOrEmpty(TextBox78.Text.Trim()))
        //{
        //    Valido = false;
        //    //lblValidaApellido.ForeColor = color[1];
        //    TextBox78.CssClass = css[1];
        //}
        //else
        //{
        //    //lblValidaApellido.ForeColor = color[0];
        //    TextBox78.CssClass = css[0];
        //}
        if (string.IsNullOrEmpty(rblTipoId.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            //lblValidaNombres.ForeColor = color[1];
            rblTipoId.CssClass = css[1];
        }
        else
        {
            //lblValidaNombres.ForeColor = color[0];
            rblTipoId.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(txtDocOrigen.Text.Trim()))
        {
            Valido = false;
            //lblValidaApellido.ForeColor = color[1];
            txtDocOrigen.CssClass = css[1];
        }
        else
        {
            //lblValidaApellido.ForeColor = color[0];
            txtDocOrigen.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(txtFechaOrigen.Text.Trim()))
        {
            Valido = false;
            //lblValidaApellido.ForeColor = color[1];
            txtFechaOrigen.CssClass = css[1];
        }
        else
        {
            //lblValidaApellido.ForeColor = color[0];
            txtFechaOrigen.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(TextBox112.Text.Trim()))
        {
            Valido = false;
            //lblValidaApellido.ForeColor = color[1];
            TextBox112.CssClass = css[1];
        }
        else
        {
            //lblValidaApellido.ForeColor = color[0];
            TextBox112.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(txtTelefonoOrigen.Text.Trim()))
        {
            Valido = false;
            //lblValidaApellido.ForeColor = color[1];
            txtTelefonoOrigen.CssClass = css[1];
        }
        else
        {
            //lblValidaApellido.ForeColor = color[0];
            txtTelefonoOrigen.CssClass = css[0];
        }
        if (dllDepartamento.SelectedIndex == 0)
        {
            Valido = false;
            dllDepartamento.ForeColor = color[1];
        }
        else
        {
            dllDepartamento.ForeColor = color[0];
        }
        if (ddlMunicipio.SelectedIndex == 0)
        {
            Valido = false;
            ddlMunicipio.ForeColor = color[1];
        }
        else
        {
            ddlMunicipio.ForeColor = color[0];
        }
        if (string.IsNullOrEmpty(rblCobertura.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            //lblValidaNombres.ForeColor = color[1];
            rblCobertura.CssClass = css[1];
        }
        else
        {
            //lblValidaNombres.ForeColor = color[0];
            rblCobertura.CssClass = css[0];
        }
        if (TextBox145.Text == "X" || TextBox145.Text == "x")
        {
            if (string.IsNullOrEmpty(txtPrimerApellidoDest.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                txtPrimerApellidoDest.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                txtPrimerApellidoDest.CssClass = css[0];
            }
        }
        if (TextBox3.Text == "X" || TextBox3.Text == "x")
        {
            if (string.IsNullOrEmpty(txtSegundoApellidoDest.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                txtSegundoApellidoDest.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                txtSegundoApellidoDest.CssClass = css[0];
            }
        }
        if (TextBox148.Text == "X" || TextBox148.Text == "x")
        {
            if (string.IsNullOrEmpty(txtPrimerNombreDest.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                txtPrimerNombreDest.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                txtPrimerNombreDest.CssClass = css[0];
            }
        }
        if (TextBox472.Text == "X" || TextBox472.Text == "x")
        {
            if (string.IsNullOrEmpty(txtSegundoNombreDest.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                txtSegundoNombreDest.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                txtSegundoNombreDest.CssClass = css[0];
            }
        }
        if (TextBox495.Text == "X" || TextBox495.Text == "x")
        {
            if (ddlTipoId.SelectedIndex == 0)
            {
                Valido = false;
                ddlTipoId.CssClass = css[1];
            }
            else
            {
                ddlTipoId.CssClass = css[0];
            }
        }
        if (TextBox518.Text == "X" || TextBox518.Text == "x")
        {
            if (string.IsNullOrEmpty(txtNumDocDest.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                txtNumDocDest.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                txtNumDocDest.CssClass = css[0];
            }
        }
        if (TextBox541.Text == "X" || TextBox541.Text == "x")
        {
            if (string.IsNullOrEmpty(txtFechaNacDest.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                txtFechaNacDest.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                try
                {
                    DateTime FechaIni = Convert.ToDateTime(txtFechaNacDest.Text);
                    txtFechaNacDest.CssClass = css[0];
                }
                catch
                {
                    txtFechaNacDest.CssClass = css[1];
                    Valido = false;
                }
                
            }
        }
        if (ddlConvenio.SelectedIndex == 0)
        {
            Valido = false;
            ddlConvenio.CssClass = css[1];
        }
        else
        {
            ddlConvenio.ForeColor = color[0];
        }
        if (txtPrimerApellidoDest.Text != "")
        {
            if (string.IsNullOrEmpty(TextBox145.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                TextBox145.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                TextBox145.CssClass = css[0];
            }
        }
        if (txtSegundoApellidoDest.Text != "")
        {
            if (string.IsNullOrEmpty(TextBox3.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                TextBox3.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                TextBox3.CssClass = css[0];
            }
        }
        if (txtPrimerNombreDest.Text != "")
        {
            if (string.IsNullOrEmpty(TextBox148.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                TextBox148.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                TextBox148.CssClass = css[0];
            }
        }
        if (txtSegundoNombreDest.Text != "")
        {
            if (string.IsNullOrEmpty(TextBox472.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                TextBox472.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                TextBox472.CssClass = css[0];
            }
        }
        if (txtSegundoNombreDest.Text != "")
        {
            if (string.IsNullOrEmpty(TextBox472.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                TextBox472.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                TextBox472.CssClass = css[0];
            }
        }
        if (ddlTipoId.SelectedValue.ToString() == "" || ddlTipoId.SelectedValue.ToString() == "-1")
        {
            ddlTipoId.CssClass = css[0];
            

            
        }
        else
        {
            if (string.IsNullOrEmpty(TextBox495.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                TextBox495.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                TextBox495.CssClass = css[0];
            }
            
        }
        if (txtNumDocDest.Text != "")
        {
            if (string.IsNullOrEmpty(TextBox518.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                TextBox518.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                TextBox518.CssClass = css[0];
            }
        }
        if (txtFechaNacDest.Text != "")
        {
            if (string.IsNullOrEmpty(TextBox541.Text.Trim()))
            {
                Valido = false;
                //lblValidaApellido.ForeColor = color[1];
                TextBox541.CssClass = css[1];
            }
            else
            {
                //lblValidaApellido.ForeColor = color[0];
                TextBox541.CssClass = css[0];
            }
        }
        
        return Valido;
    }
    protected string construirCorreo()
    {
        string path = Server.MapPath("../img/escudocol.png");
        string stringconexion = "";
        int counter = 0;
        string line;
        // Read the file and display it line by line.
        System.IO.StreamReader file =
            new System.IO.StreamReader(@"C:\inetpub\wwwroot\CENAUT\Anexos\CorreoAnexo1_1.aspx");
        while ((line = file.ReadLine()) != null)
        {
            stringconexion = stringconexion + " " + line.ToString();
            counter++;
        }

        return textoTilde(stringconexion);
    }
    protected bool enviar(string Para, string Mensaje, string Asunto, string ruta)
    {
        bool bandera = false;
        Correo correo = new Correo();
        correo.Para = Para;
        correo.Mensaje = Mensaje;
        correo.Asunto = Asunto;
        //correo.De = ConfigurationManager.AppSettings["mailSoporte"];
        //string path = Server.MapPath(@"..\img\ImgCes.jpg"); ;
        string[] result = new string[] { ruta };
        correo.Adjuntos = result;
        if (correo.Enviar())
        { bandera = true; }
        return bandera;
    }
    protected string textoTilde(string texto)
    {
        string nuevoTexto = "";
        string valor = "";
        for (int i = 0; i < texto.Length; i++)
        {
            valor = texto.Substring(i, 1);
            if (valor == "á") { valor = "&#225"; } else if (valor == "Á") { valor = "&#193"; }
            if (valor == "é") { valor = "&#233"; } else if (valor == "É") { valor = "&#201"; }
            if (valor == "í") { valor = "&#237"; } else if (valor == "Í") { valor = "&#205"; }
            if (valor == "ó") { valor = "&#243"; } else if (valor == "Ó") { valor = "&#211"; }
            if (valor == "ú") { valor = "&#250"; } else if (valor == "Ú") { valor = "&#218"; }
            if (valor == "ñ") { valor = "&#241"; } else if (valor == "Ñ") { valor = "&#209"; }
            nuevoTexto = nuevoTexto + valor;
        }
        return nuevoTexto;
    }
    protected string valorTamano()
    {
        int tam = 20;
        int maxCaracter = 100;
        if (TextBox386.Text.Length > maxCaracter)
        {
            while (TextBox386.Text.Length > maxCaracter)
            {
                maxCaracter = maxCaracter + 100;
                tam = tam + 20;
            }
        }
        return tam.ToString();
    }
    protected string retornarHtml()
    {
        string tam = valorTamano();
        string [] inconsistencia={ "", ""};
        string[] tipoId = { "", "", "", "", "", "", "" };
        string[] cobertura = { "", "", "", "", "", "", "", "" };
        if (rblInconsistencia.SelectedValue == "1") { inconsistencia[0] = "X"; inconsistencia[1] = ""; } else if (rblInconsistencia.SelectedValue == "2") { inconsistencia[0] = ""; inconsistencia[1] = "X"; }
        if (rblTipoId.SelectedValue == "2") { tipoId[0] = "X"; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "3") { tipoId[0] = ""; tipoId[1] = "X"; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "4") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = "X"; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "5") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = "X"; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "P") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = "X"; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "A") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = "X"; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "M") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = "X"; }
        if (rblCobertura.SelectedValue == "1") { cobertura[0] = "X"; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "2") { cobertura[0] = ""; cobertura[1] = "X"; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "3") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = "X"; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "4") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = "X"; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "5") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = "X"; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "6") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = "X"; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "7") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = "X"; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "8") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = "X"; }
        string path = Server.MapPath("../img/escudocol.png");
        string html = "    <table style='width: 975px; height: 96px;'>                                                                                                                                                                            " +                                                                          								
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td rowspan='5' style='width: 63px'>                                                                                                                                                                                " +
"             <img src='" + path + "' style='height:77px;width:102px;' />  " +
//"            <img id='Image1' src='../img/escudocol.png' style='height:77px;width:102px;border-width:0px;' />                                                                                                                " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td align='center' colspan='6'><B>MINISTERIO DE LA PROTECCI&#211N SOCIAL</B></td>                                                                                                                                       " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td colspan='6'>&nbsp;</td>                                                                                                                                                                                         " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td align='center' colspan='6'><B><font SIZE=4>                                                                                                                                                                     " +                                                                                                       
"            <span id='Label1' style='font-size:Medium;'>INFORME DE POSIBLES INCONSISTENCIAS EN LA BASE DE DATOS DE LA ENTIDAD RESPONSABLE DE PAGO</span></font></B></td>                                                     " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td align='center' style='width: auto'>                                                                                                                                                                             " +
"            <span id='Label2' style='font-weight:bold;'>N&#218MERO INFORME</span></td>                                                                                                                                          " +                                                                                                       
"        <td align='center' style='width: auto; margin-left: 40px;'>                                                                                                                                                         " +                                                                                                       
"            <input name='txtNumInforme' type='text' id='txtNumInforme' style='width:100px;text-align:center' value='" + txtNumInforme.Text + "'/>                                                             " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td align='center' style='width: auto'><b>Fecha:</b></td>                                                                                                                                                           " +                                                                                                       
"        <td align='center' style='width: auto'>                                                                                                                                                                             " +
"            <input name='txtfecha' type='text' id='txtfecha' style='width:125px;text-align:center' value='" + txtFecha.Text + "'/>                                                          " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td align='center' style='width: auto'><b>Hora:</b></td>                                                                                                                                                            " +                                                                                                       
"        <td align='center' style='width: auto'>                                                                                                                                                                             " +
"            <input name='txtHora' type='text'  id='txtHora' style='width:67px;text-align:center' value='" + txtHora.Text + "'/>                                                          " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"</table>                                                                                                                                                                                                                    " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td colspan='6'><b>INFORMACI&#211N DEL PRESTADOR (Solicitante)</b></td>                                                                                                                                                 " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='width: auto'><b>Nombre</b></td>                                                                                                                                                                          " +                                                                                                       
"        <td><b>NIT</b></td>                                                                                                                                                                                                 " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +                                                                                                       
"            <input name='TextBox25' type='text' value='X' readonly='readonly' id='TextBox25' disabled='disabled' style='text-decoration:none;width:16px;text-align:center'/>                                " +                                                                                                    
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td colspan='2'>                                                                                                                                                                                                    " +                                                                                                       
"            <input name='TextBox26' type='text' value='8' readonly='readonly' id='TextBox26' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox27' type='text' value='9' readonly='readonly' id='TextBox27' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox28' type='text' value='0' readonly='readonly' id='TextBox28' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox29' type='text' value='9' readonly='readonly' id='TextBox29' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox30' type='text' value='8' readonly='readonly' id='TextBox30' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox31' type='text' value='2' readonly='readonly' id='TextBox31' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox32' type='text' value='6' readonly='readonly' id='TextBox32' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox33' type='text' value='0' readonly='readonly' id='TextBox33' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox34' type='text' value='8' readonly='readonly' id='TextBox34' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox35' type='text' readonly='readonly' id='TextBox35' disabled='disabled' style='width:16px;text-align:center'/>                                                               " +                                                                                                     
"            <span id='Label6' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                         " +                                                                                                       
"            <input name='TextBox37' type='text' value='1' readonly='readonly' id='TextBox37' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox38' type='text' value='CORPORACI&#211N PARA ESTUDIOS EN SALUD CL&#205NICA CES' readonly='readonly' id='TextBox38' disabled='disabled' style='width:566px;' />                                       " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td><b>CC</b></td>                                                                                                                                                                                                  " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +                                                                                                       
"            <input name='TextBox39' type='text' maxlength='1' id='TextBox39' style='width:16px;text-align:center' value='" + textoTilde(TextBox39.Text) + "'/>                                                          " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td>Numero:</td>                                                                                                                                                                                                    " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +                                                                                                       
"            <input name='TextBox41' type='text' id='TextBox41' style='width:226px;' value='" + textoTilde(TextBox41.Text) + "'/>                                                                                                        " +                                                                                                       
"            <span id='Label7' style='font-size:Medium;font-weight:normal;'></span>                                                                                                                                        " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"</table>                                                                                                                                                                                                                    " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='width: 64px'><b>C&#243digo:</b></td>                                                                                                                                                                         " +                                                                                                       
"        <td style='width: 326px'>                                                                                                                                                                                           " +                                                                                                       
"            <input name='TextBox42' type='text' value='0' readonly='readonly' id='TextBox42' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox42.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox43' type='text' value='5' readonly='readonly' id='TextBox43' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox43.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox44' type='text' value='0' readonly='readonly' id='TextBox44' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox44.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox45' type='text' value='0' readonly='readonly' id='TextBox45' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox45.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox46' type='text' value='1' readonly='readonly' id='TextBox46' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox46.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox47' type='text' value='0' readonly='readonly' id='TextBox47' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox47.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox48' type='text' value='2' readonly='readonly' id='TextBox48' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox48.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox49' type='text' value='1' readonly='readonly' id='TextBox49' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox49.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox50' type='text' value='2' readonly='readonly' id='TextBox50' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox50.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox51' type='text' value='4' readonly='readonly' id='TextBox51' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox51.Text) + "'/>                      " +                                                                                                       
"            <input name='TextBox52' type='text' readonly='readonly' id='TextBox52' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox52.Text) + "'/>                                " +                                                                                                       
"            <input name='TextBox53' type='text' readonly='readonly' id='TextBox53' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox53.Text) + "'/>                                " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td><b>Direcci&#243n Prestador:</b></td>                                                                                                                                                                                " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td rowspan='2' style='width: 64px'><b>Tel&#233fono:</b></td>                                                                                                                                                           " +                                                                                                       
"        <td style='width: 191px'>                                                                                                                                                                                           " +                                                                                                       
"            <input name='TextBox54' type='text' value='4' readonly='readonly' id='TextBox54' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox55' type='text' readonly='readonly' id='TextBox55' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox55.Text) + "'/>                                " +                                                                                                       
"            <input name='TextBox56' type='text' readonly='readonly' id='TextBox56' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox56.Text) + "'/>                                " +                                                                                                       
"            <input name='TextBox57' type='text' readonly='readonly' id='TextBox57' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox57.Text) + "'/>                                " +                                                                                                       
"            <input name='TextBox58' type='text' readonly='readonly' id='TextBox58' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox58.Text) + "'/>                                " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td style='width: 277px'>                                                                                                                                                                                           " +                                                                                                       
"            <input name='TextBox59' type='text' value='5' readonly='readonly' id='TextBox59' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox60' type='text' value='7' readonly='readonly' id='TextBox60' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox61' type='text' value='6' readonly='readonly' id='TextBox61' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox62' type='text' value='7' readonly='readonly' id='TextBox62' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox63' type='text' value='2' readonly='readonly' id='TextBox63' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox64' type='text' value='7' readonly='readonly' id='TextBox64' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            <input name='TextBox65' type='text' value='2' readonly='readonly' id='TextBox65' disabled='disabled' style='width:16px;text-align:center' />                                                    " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td colspan='9'>                                                                                                                                                                                                    " +                                                                                                       
"            <input name='TextBox66' type='text' id='TextBox66' style='width:563px;' value='" + textoTilde(TextBox66.Text) + "'/>                                                                                                        " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='width: 191px' align='center'>Indicativo</td>                                                                                                                                                             " +
"        <td style='width: 277px' align='center'>N&#250mero</td>                                                                                                                                                                 " +                                                                                                       
"        <td style='width: auto'><b>Departamento:</b></td>                                                                                                                                                                   " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +                                                                                                       
"            <input name='TextBox67' type='text' value='ANTIOQUIA' readonly='readonly' id='TextBox67' disabled='disabled' style='width:130px;' />                                                                            " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td bgcolor='Gray'>                                                                                                                                                                                                 " +                                                                                                       
"            &nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"        <td><b>Municipio:</b></td>                                                                                                                                                                                          " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox68' type='text' value='MEDELL&#205N' readonly='readonly' id='TextBox68' disabled='disabled' style='width:130px;' />                                                                             " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 26px;'><b>ENTIDAD A LA QUE SE LE INFORMA (Pagador)</b></td>                                                                                                                                     " +
"        <td style='height: 26px'>&nbsp;<b>C&#211DIGO:</b></td>                                                                                                                                                                  " +                                                                                                       
"        <td style='height: 26px'>                                                                                                                                                                                           " +                                                                                                       
"            <input name='TextBox69' type='text' maxlength='1' id='TextBox69' style='width:99px;text-align:center' value='" + textoTilde(TextBox69.Text) + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox70' type='text' maxlength='1' id='TextBox70' style='width:16px;text-align:center' value='" + TextBox70.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox71' type='text' maxlength='1' id='TextBox71' style='width:16px;text-align:center' value='" + TextBox71.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox72' type='text' maxlength='1' id='TextBox72' style='width:16px;text-align:center' value='" + TextBox72.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox73' type='text' maxlength='1' id='TextBox73' style='width:16px;text-align:center' value='" + TextBox73.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox74' type='text' maxlength='1' id='TextBox74' style='width:16px;text-align:center' value='" + TextBox74.Text + "'/>                                                          " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td colspan='2' align='center'><span id='Label8' style='font-size:Medium;font-weight:bold;'>Tipo de inconsistencia</span></td>                                                                                      " +
"        <td align='center'><input name='TextBox1' type='text' maxlength='1' id='TextBox1' style='width:16px;text-align:center' value='" + inconsistencia[0] + "'/></td>                                         " +                                                                                                       
"        <td colspan='6' align='left'><span id='Label9'>El paciente no existe en la base de datos</span></td>                                                                                                                " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                         " +
"        <td align='center'><input name='TextBox2' type='text' maxlength='1' id='TextBox2' style='width:16px;text-align:center' value='" + inconsistencia[1] + "'/></td>                                         " +
"        <td colspan='8' align='left'><span id='Label10'>Los datos del usuario no corresponden con los del documento de identificaci&#243n presentado</span></td>                                                                " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px' colspan='12' align='center'><span id='Label11' style='font-weight:bold;'>DATOS DEL USUARIO (Como aparece en la base de datos)</span></td>                                                 " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td colspan='3'>                                                                                                                                                                                                    " +                                                                                                       
"            <input name='TextBox75' type='text' id='TextBox75' style='width:233px;' value='" + textoTilde(TextBox75.Text.ToUpper()) + "'/>                                                                                                        " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td colspan='3'>                                                                                                                                                                                                    " +
"            <input name='TextBox76' type='text' id='TextBox76' style='width:233px;' value='" + textoTilde(TextBox76.Text.ToUpper()) + "'/>                                                                                                        " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td colspan='3'>                                                                                                                                                                                                    " +
"            <input name='TextBox77' type='text' id='TextBox77' style='width:233px;' value='" + textoTilde(TextBox77.Text.ToUpper()) + "'/>                                                                                                        " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td colspan='3'>                                                                                                                                                                                                    " +
"            <input name='TextBox78' type='text' id='TextBox78' style='width:233px;' value='" + textoTilde(TextBox78.Text.ToUpper()) + "'/>                                                                                                        " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px' colspan='3' align='center'><b>Primer Apellido</b></td>                                                                                                                                     " +                                                                                                       
"        <td style='height: 23px' colspan='3' align='center'><b>Segundo Apellido</b></td>                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px' colspan='3' align='center'><b>Primer Nombre</b></td>                                                                                                                                       " +                                                                                                       
"        <td style='height: 23px' colspan='3' align='center'><b>Segundo Nombre</b></td>                                                                                                                                      " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td colspan='12'><b>Tipo Documento de Identificaci&#243n</b></td>                                                                                                                                                       " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox79' type='text' maxlength='1' id='TextBox79' style='width:16px;text-align:center' value='" + tipoId[0] + "'/>                                                          " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td style='width: 145px'>Registro Civil</td>                                                                                                                                                                        " +                                                                                                       
"        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                         " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox80' type='text' maxlength='1' id='TextBox80' style='width:16px;text-align:center' value='" + tipoId[4] + "'/>                                                          " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td>Pasaporte</td>                                                                                                                                                                                                  " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td colspan='5'>                                                                                                                                                                                                    " +
"            <input name='txtDocOrigen' type='text' id='txtDocOrigen' style='width:286px' value='" + txtDocOrigen.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox82' type='text' maxlength='1' id='TextBox82' style='width:16px;text-align:center' value='" + TextBox82.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox83' type='text' maxlength='1' id='TextBox83' style='width:16px;text-align:center' value='" + TextBox83.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox84' type='text' maxlength='1' id='TextBox84' style='width:16px;text-align:center' value='" + TextBox84.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox85' type='text' maxlength='1' id='TextBox85' style='width:16px;text-align:center' value='" + TextBox85.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox86' type='text' maxlength='1' id='TextBox86' style='width:16px;text-align:center' value='" + TextBox86.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox87' type='text' maxlength='1' id='TextBox87' style='width:16px;text-align:center' value='" + TextBox87.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox88' type='text' maxlength='1' id='TextBox88' style='width:16px;text-align:center' value='" + TextBox88.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox89' type='text' maxlength='1' id='TextBox89' style='width:16px;text-align:center' value='" + TextBox89.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox90' type='text' maxlength='1' id='TextBox90' style='width:16px;text-align:center' value='" + TextBox90.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox91' type='text' maxlength='1' id='TextBox91' style='width:16px;text-align:center' value='" + TextBox91.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox92' type='text' maxlength='1' id='TextBox92' style='width:16px;text-align:center' value='" + TextBox92.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox93' type='text' maxlength='1' id='TextBox93' style='width:16px;text-align:center' value='" + TextBox93.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox94' type='text' maxlength='1' id='TextBox94' style='width:16px;text-align:center' value='" + TextBox94.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox95' type='text' maxlength='1' id='TextBox95' style='width:16px;text-align:center' value='" + TextBox95.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox96' type='text' maxlength='1' id='TextBox96' style='width:16px;text-align:center' value='" + TextBox96.Text + "'/>                                                          " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox97' type='text' maxlength='1' id='TextBox97' style='width:16px;text-align:center' value='" + tipoId[1] + "'/>                                                          " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td style='width: 145px'>Tarjeta de Identidad</td>                                                                                                                                                                  " +                                                                                                       
"        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                         " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox98' type='text' maxlength='1' id='TextBox98' style='width:16px;text-align:center' value='" + tipoId[5] + "'/>                                                          " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td>Adulto sin Identificaci&#243n</td>                                                                                                                                                                                  " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +
"        <td align='center' colspan='3'>Numero Documento de Identificaci&#243n</td>                                                                                                                                              " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox99' type='text' maxlength='1' id='TextBox99' style='width:16px;text-align:center' value='" + tipoId[2] + "'/>                                                          " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td style='width: 145px'>C&#233dula de Ciudadania</td>                                                                                                                                                                  " +                                                                                                       
"        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                         " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox100' type='text' maxlength='1' id='TextBox100' style='width:16px;text-align:center' value='" + tipoId[6] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td>Menor sin Identificaci&#243n</td>                                                                                                                                                                                   " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                         " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox101' type='text' maxlength='1' id='TextBox101' style='width:16px;text-align:center' value='" + tipoId[3] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td style='width: 145px'>C&#233dula de Extranjer&#237a</td>                                                                                                                                                                  " +                                                                                                       
"        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                         " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td>&nbsp;</td>                                                                                                                                                                                                     " +                                                                                                       
"        <td><b>Fecha de Nacimiento:</b></td>                                                                                                                                                                                " +                                                                                                       
"        <td colspan='5'>                                                                                                                                                                                                    " +
"            <input name='txtFechaOrigen' type='text'  id='txtFechaOrigen' style='width:176px;text-align:center' value='" + textoTilde(txtFechaOrigen.Text.ToUpper().Trim()) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox103' type='text' maxlength='1' id='TextBox103' style='width:16px;text-align:center' value='" + TextBox103.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox104' type='text' maxlength='1' id='TextBox104' style='width:16px;text-align:center' value='" + TextBox104.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox105' type='text' maxlength='1' id='TextBox105' style='width:16px;text-align:center' value='" + TextBox105.Text + "'/>                                                       " +                                                                                                       
//"            <span id='Label15' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                        " +                                                                                                       
//"            <input name='TextBox106' type='text' maxlength='1' id='TextBox106' style='width:16px;text-align:center' value='" + TextBox106.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox107' type='text' maxlength='1' id='TextBox107' style='width:16px;text-align:center' value='" + TextBox107.Text + "'/>                                                       " +                                                                                                       
//"            <span id='Label24' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                        " +                                                                                                       
//"            <input name='TextBox108' type='text' maxlength='1' id='TextBox108' style='width:16px;text-align:center' value='" + TextBox108.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox109' type='text' maxlength='1' id='TextBox109' style='width:16px;text-align:center' value='" + TextBox109.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td><b>Direcci&#243n de Residencia Habitual:</b></td>                                                                                                                                                                   " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox112' type='text' id='TextBox112' style='width:419px;' value='" + textoTilde(TextBox112.Text.ToUpper()) + "'/>                                                                                                     " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +
"        <td><b>Tel&#233fono:</b></td>                                                                                                                                                                                           " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +                                                                                                       
"            <input name='TextBox113' type='text' maxlength='1' id='TextBox113' style='width:152px;text-align:center' value='" + txtTelefonoOrigen.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox114' type='text' maxlength='1' id='TextBox114' style='width:16px;text-align:center' value='" + TextBox114.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox115' type='text' maxlength='1' id='TextBox115' style='width:16px;text-align:center' value='" + TextBox115.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox116' type='text' maxlength='1' id='TextBox116' style='width:16px;text-align:center' value='" + TextBox116.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox117' type='text' maxlength='1' id='TextBox117' style='width:16px;text-align:center' value='" + TextBox117.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox118' type='text' maxlength='1' id='TextBox118' style='width:16px;text-align:center' value='" + TextBox118.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox119' type='text' maxlength='1' id='TextBox119' style='width:16px;text-align:center' value='" + TextBox119.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox120' type='text' maxlength='1' id='TextBox120' style='width:16px;text-align:center' value='" + TextBox120.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox121' type='text' maxlength='1' id='TextBox121' style='width:16px;text-align:center' value='" + TextBox121.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td><b>Departamento:</b></td>                                                                                                                                                                                       " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox123' type='text' id='TextBox123' style='width:240px;' value='" + textoTilde(dllDepartamento.SelectedItem.Text.ToUpper()) + "'/>                                                                                                     " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"        <td><b>Municipio:</b></td>                                                                                                                                                                                          " +                                                                                                       
"        <td>                                                                                                                                                                                                                " +
"            <input name='TextBox124' type='text' id='TextBox124' style='width:207px;' value='" + textoTilde(ddlMunicipio.SelectedItem.Text.ToUpper()) + "'/>                                                                                                     " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                      " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td colspan='8' style='height: 23px'><b>Cobertura en Salud</b></td>                                                                                                                                                 " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='TextBox137' type='text' maxlength='1' id='TextBox137' style='width:16px;text-align:center' value='" + cobertura[0] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td style='height: 23px'>R&#233gimen Contributivo</td>                                                                                                                                                                  " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='TextBox138' type='text' maxlength='1' id='TextBox138' style='width:16px;text-align:center' value='" + cobertura[1] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td style='height: 23px'>R&#233gimen Subsidiado - Parcial</td>                                                                                                                                                          " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='TextBox139' type='text' maxlength='1' id='TextBox139' style='width:16px;text-align:center' value='" + cobertura[2] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td style='height: 23px'>Poblaci&#243n Pobre No Asegurada Sin SISBEN</td>                                                                                                                                               " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='TextBox140' type='text' maxlength='1' id='TextBox140' style='width:16px;text-align:center' value='" + cobertura[3] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td style='height: 23px'>Plan Adicional de Salud</td>                                                                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='TextBox141' type='text' maxlength='1' id='TextBox141' style='width:16px;text-align:center' value='" + cobertura[4] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td style='height: 23px'>R&#233gimen Subsidiado - Total</td>                                                                                                                                                            " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='TextBox142' type='text' maxlength='1' id='TextBox142' style='width:16px;text-align:center' value='" + cobertura[5] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +
"        <td style='height: 23px'>Poblaci&#243n Pobre No Asegurada Con SISBEN</td>                                                                                                                                               " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='TextBox143' type='text' maxlength='1' id='TextBox143' style='width:16px;text-align:center' value='" + cobertura[6] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td style='height: 23px'>Desplazado</td>                                                                                                                                                                            " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='TextBox144' type='text' maxlength='1' id='TextBox144' style='width:16px;text-align:center' value='" + cobertura[7] + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td style='height: 23px'>Otro</td>                                                                                                                                                                                  " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px' align='center' colspan='12'>                                                                                                                                                               " +
"            <span id='Label12' style='font-weight:bold;'>INFORMACI&#211N DE LA POSIBLE INCONSISTENCIA</span></td>                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px' colspan='4'><span id='Label13' style='font-weight:bold;'>VARIABLE PRESUNTAMENTE INCORRECTA</span></td>                                                                     " +
"        <td style='height: 23px' colspan='8'><span id='Label14' style='font-weight:bold;'>DATOS SEGUN DOCUMENTO DE IDENTIFICACI&#211N (f&#237sico)</span></td>                                                       " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 10px' width='2'><input name='TextBox145' type='text' maxlength='1' id='TextBox145' style='width:16px;text-align:center' value='" + textoTilde(TextBox145.Text.ToUpper()) + "'/></td>                   " +                                                                                                       
"        <td style='height: AUTO'  colspan='1' align='left'>Primer Apellido</td>                                                                                                                                             " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='2'>Primer Apellido</td>                                                                    " +
"        <td style='height: 10px' colspan='6'>                                " +
"            <input name='txtPrimerApellidoDest' type='text' id='txtPrimerApellidoDest' style='width:450px' value='" + textoTilde(txtPrimerApellidoDest.Text.ToUpper()) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox450' type='text' maxlength='1' id='TextBox450' style='width:16px;text-align:center' value='" + TextBox450.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox451' type='text' maxlength='1' id='TextBox451' style='width:16px;text-align:center' value='" + TextBox451.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox452' type='text' maxlength='1' id='TextBox452' style='width:16px;text-align:center' value='" + TextBox452.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox453' type='text' maxlength='1' id='TextBox453' style='width:16px;text-align:center' value='" + TextBox453.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox454' type='text' maxlength='1' id='TextBox454' style='width:16px;text-align:center' value='" + TextBox454.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox455' type='text' maxlength='1' id='TextBox455' style='width:16px;text-align:center' value='" + TextBox455.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox456' type='text' maxlength='1' id='TextBox456' style='width:16px;text-align:center' value='" + TextBox456.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox457' type='text' maxlength='1' id='TextBox457' style='width:16px;text-align:center' value='" + TextBox457.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox458' type='text' maxlength='1' id='TextBox458' style='width:16px;text-align:center' value='" + TextBox458.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox459' type='text' maxlength='1' id='TextBox459' style='width:16px;text-align:center' value='" + TextBox459.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox460' type='text' maxlength='1' id='TextBox460' style='width:16px;text-align:center' value='" + TextBox460.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox461' type='text' maxlength='1' id='TextBox461' style='width:16px;text-align:center' value='" + TextBox461.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox462' type='text' maxlength='1' id='TextBox462' style='width:16px;text-align:center' value='" + TextBox462.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox463' type='text' maxlength='1' id='TextBox463' style='width:16px;text-align:center' value='" + TextBox463.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox464' type='text' maxlength='1' id='TextBox464' style='width:16px;text-align:center' value='" + TextBox464.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox465' type='text' maxlength='1' id='TextBox465' style='width:16px;text-align:center' value='" + TextBox465.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox466' type='text' maxlength='1' id='TextBox466' style='width:16px;text-align:center' value='" + TextBox466.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox467' type='text' maxlength='1' id='TextBox467' style='width:16px;text-align:center' value='" + TextBox467.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 10px'  width='2' colspan='1'><input name='TextBox3' type='text' maxlength='1' id='TextBox3' style='width:16px;text-align:center' value='" + textoTilde(TextBox3.Text.ToUpper()) + "'/></td>            " +                                                                                                       
"        <td style='height: 23px' colspan='1' align='left'>Segundo Apellido</td>                                                                                                                                             " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"                                                                                                                                                                                                                            " +
"        <td style='height: 23px' colspan='2'><span id='Label16'>Segundo Apellido</span>   </td>                                                                    " +
"        <td style='height: 10px' colspan='6'>                                " +
"            <input name='TextBox4' type='text' maxlength='1' id='TextBox4' style='width:450px' value='" + textoTilde(txtSegundoApellidoDest.Text.ToUpper()) + "'/>                                                             " +                                                                                                       
//"            <input name='TextBox5' type='text' maxlength='1' id='TextBox5' style='width:16px;text-align:center' value='" + TextBox5.Text + "'/>                                                             " +                                                                                                       
//"            <input name='TextBox18' type='text' maxlength='1' id='TextBox18' style='width:16px;text-align:center' value='" + TextBox18.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox19' type='text' maxlength='1' id='TextBox19' style='width:16px;text-align:center' value='" + TextBox19.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox22' type='text' maxlength='1' id='TextBox22' style='width:16px;text-align:center' value='" + TextBox22.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox36' type='text' maxlength='1' id='TextBox36' style='width:16px;text-align:center' value='" + TextBox36.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox40' type='text' maxlength='1' id='TextBox40' style='width:16px;text-align:center' value='" + TextBox40.Text + "'/>                                                          " +                                                                                                       
//"            <input name='TextBox122' type='text' maxlength='1' id='TextBox122' style='width:16px;text-align:center' value='" + TextBox122.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox125' type='text' maxlength='1' id='TextBox125' style='width:16px;text-align:center' value='" + TextBox125.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox126' type='text' maxlength='1' id='TextBox126' style='width:16px;text-align:center' value='" + TextBox126.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox127' type='text' maxlength='1' id='TextBox127' style='width:16px;text-align:center' value='" + TextBox127.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox128' type='text' maxlength='1' id='TextBox128' style='width:16px;text-align:center' value='" + TextBox128.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox129' type='text' maxlength='1' id='TextBox129' style='width:16px;text-align:center' value='" + TextBox129.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox130' type='text' maxlength='1' id='TextBox130' style='width:16px;text-align:center' value='" + TextBox130.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox131' type='text' maxlength='1' id='TextBox131' style='width:16px;text-align:center' value='" + TextBox131.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox132' type='text' maxlength='1' id='TextBox132' style='width:16px;text-align:center' value='" + TextBox132.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox133' type='text' maxlength='1' id='TextBox133' style='width:16px;text-align:center' value='" + TextBox133.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox134' type='text' maxlength='1' id='TextBox134' style='width:16px;text-align:center' value='" + TextBox134.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox135' type='text' maxlength='1' id='TextBox135' style='width:16px;text-align:center' value='" + TextBox135.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 10px' width='2' colspan='1'><input name='TextBox148' type='text' maxlength='1' id='TextBox148' style='width:16px;text-align:center' value='" + textoTilde(TextBox148.Text.ToUpper()) + "'/></td>       " +                                                                                                       
"        <td style='height: 23px' colspan='1' align='left'>Primer Nombre</td>                                                                                                                                                " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +
"        <td style='height: 23px' colspan='2'><span id='Label17'>Primer Nombre</span></td>                                                                    " +
"        <td style='height: 10px' colspan='6'>                                " +
"            <input name='txtPrimerNombreDest' type='text' id='txtPrimerNombreDest' style='width:450px' value='" + textoTilde(txtPrimerNombreDest.Text.ToUpper()) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox152' type='text' maxlength='1' id='TextBox152' style='width:16px;text-align:center' value='" + TextBox152.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox187' type='text' maxlength='1' id='TextBox187' style='width:16px;text-align:center' value='" + TextBox187.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox368' type='text' maxlength='1' id='TextBox368' style='width:16px;text-align:center' value='" + TextBox368.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox369' type='text' maxlength='1' id='TextBox369' style='width:16px;text-align:center' value='" + TextBox369.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox370' type='text' maxlength='1' id='TextBox370' style='width:16px;text-align:center' value='" + TextBox370.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox371' type='text' maxlength='1' id='TextBox371' style='width:16px;text-align:center' value='" + TextBox371.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox372' type='text' maxlength='1' id='TextBox372' style='width:16px;text-align:center' value='" + TextBox372.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox373' type='text' maxlength='1' id='TextBox373' style='width:16px;text-align:center' value='" + TextBox373.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox374' type='text' maxlength='1' id='TextBox374' style='width:16px;text-align:center' value='" + TextBox374.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox375' type='text' maxlength='1' id='TextBox375' style='width:16px;text-align:center' value='" + TextBox375.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox376' type='text' maxlength='1' id='TextBox376' style='width:16px;text-align:center' value='" + TextBox376.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox377' type='text' maxlength='1' id='TextBox377' style='width:16px;text-align:center' value='" + TextBox377.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox378' type='text' maxlength='1' id='TextBox378' style='width:16px;text-align:center' value='" + TextBox378.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox379' type='text' maxlength='1' id='TextBox379' style='width:16px;text-align:center' value='" + TextBox379.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox380' type='text' maxlength='1' id='TextBox380' style='width:16px;text-align:center' value='" + TextBox380.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox381' type='text' maxlength='1' id='TextBox381' style='width:16px;text-align:center' value='" + TextBox381.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox382' type='text' maxlength='1' id='TextBox382' style='width:16px;text-align:center' value='" + TextBox382.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox383' type='text' maxlength='1' id='TextBox383' style='width:16px;text-align:center' value='" + TextBox383.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox384' type='text' maxlength='1' id='TextBox384' style='width:16px;text-align:center' value='" + TextBox384.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 10px'   width='2' colspan='1'><input name='TextBox472' type='text' maxlength='1' id='TextBox472' style='width:16px;text-align:center' value='" + textoTilde(TextBox472.Text.ToUpper()) + "'/></td>     " +                                                                                                       
"        <td style='height: 23px' colspan='1' align='left'>Segundo Nombre</td>                                                                                                                                               " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +
"        <td style='height: 23px' colspan='2'><span id='Label18'>Segundo nombre</span>  </td>                                                                    " +
"        <td style='height: 10px' colspan='6'>                                " +
"            <input name='txtSegundoNombreDest' type='text'  id='txtSegundoNombreDest' style='width:450px' value='" + textoTilde(txtSegundoNombreDest.Text.ToUpper()) + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox474' type='text' maxlength='1' id='TextBox474' style='width:16px;text-align:center' value='" + TextBox474.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox475' type='text' maxlength='1' id='TextBox475' style='width:16px;text-align:center' value='" + TextBox475.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox476' type='text' maxlength='1' id='TextBox476' style='width:16px;text-align:center' value='" + TextBox476.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox477' type='text' maxlength='1' id='TextBox477' style='width:16px;text-align:center' value='" + TextBox477.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox478' type='text' maxlength='1' id='TextBox478' style='width:16px;text-align:center' value='" + TextBox478.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox479' type='text' maxlength='1' id='TextBox479' style='width:16px;text-align:center' value='" + TextBox479.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox480' type='text' maxlength='1' id='TextBox480' style='width:16px;text-align:center' value='" + TextBox480.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox481' type='text' maxlength='1' id='TextBox481' style='width:16px;text-align:center' value='" + TextBox481.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox482' type='text' maxlength='1' id='TextBox482' style='width:16px;text-align:center' value='" + TextBox482.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox483' type='text' maxlength='1' id='TextBox483' style='width:16px;text-align:center' value='" + TextBox483.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox484' type='text' maxlength='1' id='TextBox484' style='width:16px;text-align:center' value='" + TextBox484.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox485' type='text' maxlength='1' id='TextBox485' style='width:16px;text-align:center' value='" + TextBox485.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox486' type='text' maxlength='1' id='TextBox486' style='width:16px;text-align:center' value='" + TextBox486.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox487' type='text' maxlength='1' id='TextBox487' style='width:16px;text-align:center' value='" + TextBox487.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox488' type='text' maxlength='1' id='TextBox488' style='width:16px;text-align:center' value='" + TextBox488.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox489' type='text' maxlength='1' id='TextBox489' style='width:16px;text-align:center' value='" + TextBox489.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox490' type='text' maxlength='1' id='TextBox490' style='width:16px;text-align:center' value='" + TextBox490.Text + "'/>                                                       " +                                                                                                       
////"            <input name='TextBox491' type='text' maxlength='1' id='TextBox491' style='width:16px;text-align:center' value='" + TextBox491.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox492' type='text' maxlength='1' id='TextBox492' style='width:16px;text-align:center' value='" + TextBox492.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 10px'  width='2' colspan='1'><input name='TextBox495' type='text' maxlength='1' id='TextBox495' style='width:16px;text-align:center' value='" + textoTilde(TextBox495.Text.ToUpper()) + "'/></td>      " +
"        <td style='height: 23px' colspan='1' align='left'>Tipo Documento de Identificaci&#243n</td>                                                                                                                                               " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +
"        <td style='height: 23px' colspan='2'><span id='Label19'>Tipo documento de Identificaci&#243n</span> </td>                                                                    " +
"        <td style='height: 10px' colspan='6'>                                " +
"            <input name='txtTipoDocDest' type='text'id='txtTipoDocDest' style='width:400px' value='" + textoTilde(ddlTipoId.SelectedItem.ToString().ToUpper()) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox497' type='text' maxlength='1' id='TextBox497' style='width:16px;text-align:center' value='" + TextBox497.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox498' type='text' maxlength='1' id='TextBox498' style='width:16px;text-align:center' value='" + TextBox498.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox499' type='text' maxlength='1' id='TextBox499' style='width:16px;text-align:center' value='" + TextBox499.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox500' type='text' maxlength='1' id='TextBox500' style='width:16px;text-align:center' value='" + TextBox500.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox501' type='text' maxlength='1' id='TextBox501' style='width:16px;text-align:center' value='" + TextBox501.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox502' type='text' maxlength='1' id='TextBox502' style='width:16px;text-align:center' value='" + TextBox502.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox503' type='text' maxlength='1' id='TextBox503' style='width:16px;text-align:center' value='" + TextBox503.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox504' type='text' maxlength='1' id='TextBox504' style='width:16px;text-align:center' value='" + TextBox504.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox505' type='text' maxlength='1' id='TextBox505' style='width:16px;text-align:center' value='" + TextBox505.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox506' type='text' maxlength='1' id='TextBox506' style='width:16px;text-align:center' value='" + TextBox506.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox507' type='text' maxlength='1' id='TextBox507' style='width:16px;text-align:center' value='" + TextBox507.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox508' type='text' maxlength='1' id='TextBox508' style='width:16px;text-align:center' value='" + TextBox508.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox509' type='text' maxlength='1' id='TextBox509' style='width:16px;text-align:center' value='" + TextBox509.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox510' type='text' maxlength='1' id='TextBox510' style='width:16px;text-align:center' value='" + TextBox510.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox511' type='text' maxlength='1' id='TextBox511' style='width:16px;text-align:center' value='" + TextBox511.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox512' type='text' maxlength='1' id='TextBox512' style='width:16px;text-align:center' value='" + TextBox512.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox513' type='text' maxlength='1' id='TextBox513' style='width:16px;text-align:center' value='" + TextBox513.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox514' type='text' maxlength='1' id='TextBox514' style='width:16px;text-align:center' value='" + TextBox514.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox515' type='text' maxlength='1' id='TextBox515' style='width:16px;text-align:center' value='" + TextBox515.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 10px'  width='2' colspan='1'><input name='TextBox518' type='text' maxlength='1' id='TextBox518' style='width:16px;text-align:center' value='" + textoTilde(TextBox518.Text.ToUpper()) + "'/></td>      " +
"        <td style='height: 23px' colspan='1' align='left'>N&#250mero Documento de Identificaci&#243n</td>                                                                                                                                             " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +
"        <td style='height: 23px' colspan='2'><span id='Label20'>N&#250mero de documento de Identificaci&#243n</span></td>                                                                    " +
"        <td style='height: 10px' colspan='6'>                                " +
"            <input name='txtNumDocDest' type='text'  id='txtNumDocDest' style='width:400px' value='" + textoTilde(txtNumDocDest.Text.ToUpper()) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox520' type='text' maxlength='1' id='TextBox520' style='width:16px;text-align:center' value='" + TextBox520.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox521' type='text' maxlength='1' id='TextBox521' style='width:16px;text-align:center' value='" + TextBox521.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox522' type='text' maxlength='1' id='TextBox522' style='width:16px;text-align:center' value='" + TextBox522.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox523' type='text' maxlength='1' id='TextBox523' style='width:16px;text-align:center' value='" + TextBox523.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox524' type='text' maxlength='1' id='TextBox524' style='width:16px;text-align:center' value='" + TextBox524.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox525' type='text' maxlength='1' id='TextBox525' style='width:16px;text-align:center' value='" + TextBox525.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox526' type='text' maxlength='1' id='TextBox526' style='width:16px;text-align:center' value='" + TextBox526.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox527' type='text' maxlength='1' id='TextBox527' style='width:16px;text-align:center' value='" + TextBox527.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox528' type='text' maxlength='1' id='TextBox528' style='width:16px;text-align:center' value='" + TextBox528.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox529' type='text' maxlength='1' id='TextBox529' style='width:16px;text-align:center' value='" + TextBox529.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox530' type='text' maxlength='1' id='TextBox530' style='width:16px;text-align:center' value='" + TextBox530.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox531' type='text' maxlength='1' id='TextBox531' style='width:16px;text-align:center' value='" + TextBox531.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox532' type='text' maxlength='1' id='TextBox532' style='width:16px;text-align:center' value='" + TextBox532.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox533' type='text' maxlength='1' id='TextBox533' style='width:16px;text-align:center' value='" + TextBox533.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox534' type='text' maxlength='1' id='TextBox534' style='width:16px;text-align:center' value='" + TextBox534.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox535' type='text' maxlength='1' id='TextBox535' style='width:16px;text-align:center' value='" + TextBox535.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox536' type='text' maxlength='1' id='TextBox536' style='width:16px;text-align:center' value='" + TextBox536.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 10px' width='2' colspan='1'><input name='TextBox541' type='text' maxlength='1' id='TextBox541' style='width:16px;text-align:center' value='" + textoTilde(TextBox541.Text.ToUpper()) + "'/></td>       " +                                                                                                       
"        <td style='height: 23px' colspan='1' align='left'>Fecha de nacimiento</td>                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 10px' colspan='1'></td>                                                                                                                                                                          " +                                                                                                       
"        <td style='height: 23px' colspan='1'><span id='Label21'>Fecha de nacimiento</span></td>                                                                                                                             " +
"        <td style='height: 23px' colspan='2'></td>                                                                    " +
"        <td style='height: 23px' colspan='6'>                                " +
"            <input name='txtFechaNacDest' type='text' id='txtFechaNacDest' style='width:140px' value='" + textoTilde(txtFechaNacDest.Text.ToUpper()) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox543' type='text' maxlength='1' id='TextBox543' style='width:16px;text-align:center' value='" + TextBox543.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox544' type='text' maxlength='1' id='TextBox544' style='width:16px;text-align:center' value='" + TextBox544.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox545' type='text' maxlength='1' id='TextBox545' style='width:16px;text-align:center' value='" + TextBox545.Text + "'/>                                                       " +                                                                                                       
//"            <span id='Label22' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                        " +                                                                                                       
//"            <input name='TextBox546' type='text' maxlength='1' id='TextBox546' style='width:16px;text-align:center' value='" + TextBox546.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox547' type='text' maxlength='1' id='TextBox547' style='width:16px;text-align:center' value='" + TextBox547.Text + "'/>                                                       " +                                                                                                       
//"            <span id='Label23' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                        " +                                                                                                       
//"            <input name='TextBox548' type='text' maxlength='1' id='TextBox548' style='width:16px;text-align:center' value='" + TextBox548.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox549' type='text' maxlength='1' id='TextBox549' style='width:16px;text-align:center' value='" + TextBox549.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"                                                                                                                                                                                                                            " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px'><b>Observaciones</b></td>                                                                                                                                                                  " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <textarea name='TextBox386' rows='2' cols='20' id='TextBox386' style='height:" + tam + "px;width:955px;' value=''>" + textoTilde(TextBox386.Text.ToUpper()) + "</textarea>                                                                     " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    </table>                                                                                                                                                                                                                " +                                                                                                       
"    <table style='width: 975px'>                                                                                                                                                                                            " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 23px' colspan='12' align='center'><B>INFORMACI&#211N DE LA PERSONA QUE REPORTA</B></td>                                                                                                             " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px' colspan='2'>Nombre de quien reporta</td>                                                                                                                                         " +
"        <td rowspan='3'>Tel&#233fono</td>                                                                                                                                                                                       " +                                                                                                       
"        <td style='height: 23px; width: 111px;'>                                                                                                                                                                            " +
"            <input name='txtIndicativoSolic' type='text' id='txtIndicativoSolic' style='width:91px;text-align:left' value='" + textoTilde(txtIndicativoSolic.Text) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox404' type='text' maxlength='1' id='TextBox404' style='width:16px;text-align:center' value='" + TextBox404.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox405' type='text' maxlength='1' id='TextBox405' style='width:16px;text-align:center' value='" + TextBox405.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='txtNumeroSolic' type='text'  id='txtNumeroSolic' style='width:126px;text-align:left' value='" + textoTilde(txtNumeroSolic.Text) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox409' type='text' maxlength='1' id='TextBox409' style='width:16px;text-align:center' value='" + TextBox409.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox410' type='text' maxlength='1' id='TextBox410' style='width:16px;text-align:center' value='" + TextBox410.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox411' type='text' maxlength='1' id='TextBox411' style='width:16px;text-align:center' value='" + TextBox411.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox412' type='text' maxlength='1' id='TextBox412' style='width:16px;text-align:center' value='" + TextBox412.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox413' type='text' maxlength='1' id='TextBox413' style='width:16px;text-align:center' value='" + TextBox413.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox414' type='text' maxlength='1' id='TextBox414' style='width:16px;text-align:center' value='" + TextBox414.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +
"            <input name='txtExtSolic' type='text' id='txtExtSolic' style='width:85px;text-align:left' value='" + textoTilde(txtExtSolic.Text) + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox416' type='text' maxlength='1' id='TextBox416' style='width:16px;text-align:center' value='" + TextBox416.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox417' type='text' maxlength='1' id='TextBox417' style='width:16px;text-align:center' value='" + TextBox417.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox418' type='text' maxlength='1' id='TextBox418' style='width:16px;text-align:center' value='" + TextBox418.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px' colspan='2'>                                                                                                                                                                               " +                                                                                                       
"            <input name='TextBox402' type='text' id='TextBox402' style='width:476px;' value='" + textoTilde(TextBox402.Text.ToUpper().Trim()) + "'/>                                                                                                     " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +                                                                                                       
"        <td style='height: 23px; width: 111px;' align='center'>Indicativo</td>                                                                                                                                              " +
"        <td style='height: 23px' align='center'>N&#250mero</td>                                                                                                                                                                 " +
"        <td style='height: 23px' align='center'>Extensi&#243n</td>                                                                                                                                                              " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +                                                                                                       
"        <td style='height: 23px'>Cargo o Actvidad:</td>                                                                                                                                                                     " +                                                                                                       
"        <td style='height: 23px'>                                                                                                                                                                                           " +                                                                                                       
"            <input name='TextBox419' type='text' id='TextBox419' style='width:359px;' value='" + textoTilde(TextBox419.Text.ToUpper().Trim()) + "'/>                                                                                                     " +                                                                                                       
"        </td>                                                                                                                                                                                                               " +
"        <td style='height: 23px; width: 111px;' align='center'>Tel&#233fono Celular:</td>                                                                                                                                       " +                                                                                                       
"        <td style='height: 23px' colspan='2' align='center'>                                                                                                                                                                " +
"            <input name='txtCelularSolicita' type='text' id='txtCelularSolicita' style='width:200px;text-align:left' value='" + txtCelularSolicita.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox421' type='text' maxlength='1' id='TextBox421' style='width:16px;text-align:center' value='" + TextBox421.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox422' type='text' maxlength='1' id='TextBox422' style='width:16px;text-align:center' value='" + TextBox422.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox423' type='text' maxlength='1' id='TextBox423' style='width:16px;text-align:center' value='" + TextBox423.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox424' type='text' maxlength='1' id='TextBox424' style='width:16px;text-align:center' value='" + TextBox424.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox425' type='text' maxlength='1' id='TextBox425' style='width:16px;text-align:center' value='" + TextBox425.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox426' type='text' maxlength='1' id='TextBox426' style='width:16px;text-align:center' value='" + TextBox426.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox427' type='text' maxlength='1' id='TextBox427' style='width:16px;text-align:center' value='" + TextBox427.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox428' type='text' maxlength='1' id='TextBox428' style='width:16px;text-align:center' value='" + TextBox428.Text + "'/>                                                       " +                                                                                                       
//"            <input name='TextBox429' type='text' maxlength='1' id='TextBox429' style='width:16px;text-align:center' value='" + TextBox429.Text + "'/>                                                       " +                                                                                                       
"            </td>                                                                                                                                                                                                           " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"    <tr>                                                                                                                                                                                                                    " +
"        <td style='height: 23px;font-size: xx-small;' colspan='6'>MPS-SAS V5.0 2008-07-11</td>                                                                                                                                                   " +                                                                                                       
"    </tr>                                                                                                                                                                                                                   " +                                                                                                       
"</table>                                                                                                                                                                                                                    " +                                                                                                       
"                                                                                                                                                                                                                            "                                                                                                        ;
        return html;
    }
    protected void TraerDatos(string Busqueda)
    {
        string msg = "";
        try
        {
            string[] Filtros = Busqueda.Split('|');
            DataTable dtGrid = new ClinicaCES.Logica.LAnexo1().TraerPaciente(Filtros[0], Filtros[1]);
            DataRow registro = dtGrid.Rows[0];
            TextBox75.Text = registro[0].ToString();
            TextBox76.Text = registro[1].ToString();
            TextBox77.Text = registro[2].ToString();
            TextBox78.Text = registro[3].ToString();
            txtDocOrigen.Text = registro[4].ToString();
            rblTipoId.SelectedValue = registro[5].ToString();
            // TraerIndentificacion(registro[5].ToString());
            txtFechaOrigen.Text = registro[6].ToString();
            TextBox112.Text = registro[7].ToString();
            txtTelefonoOrigen.Text = registro[8].ToString();
            dllDepartamento.SelectedValue = registro[9].ToString();
            Procedimientos.LlenarCombos(ddlMunicipio, new ClinicaCES.Logica.LMaestros().ListarMunicipios(dllDepartamento.SelectedValue), "CODMUN", "MUN");
            ddlMunicipio.SelectedValue = registro[10].ToString();
            ViewState["tipoId"] = Convert.ToString(registro["TIPOIDCOD"]);
        }
        catch
        {
            msg = "69";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        AgregarPrintScript(ViewState["Archivo1"].ToString());
    }
    protected void inactivarControles()
    {
        Button1.Enabled = false;
        rblCobertura.Enabled = false;
        TextBox145.Enabled = false;
        TextBox3.Enabled = false;
        TextBox148.Enabled = false;
        TextBox472.Enabled = false;
        TextBox495.Enabled = false;
        TextBox518.Enabled = false;
        TextBox541.Enabled = false;
        txtPrimerApellidoDest.Enabled = false;
        txtSegundoApellidoDest.Enabled = false;
        txtPrimerNombreDest.Enabled = false;
        txtSegundoNombreDest.Enabled = false;
        ddlTipoId.Enabled = false;
        txtNumDocDest.Enabled = false;
        txtFechaNacDest.Enabled = false;
        TextBox386.Enabled = false;
        ddlConvenio.Enabled = false;
        rblInconsistencia.Enabled = false;
    }
}