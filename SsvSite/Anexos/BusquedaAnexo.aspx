﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusquedaAnexo.aspx.cs" Inherits="Anexos_BusquedaAnexo" %>--%>
<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="BusquedaAnexo.aspx.cs" Inherits="Anexos_BusquedaAnexo" Title="Busqueda paciente para generar Anexo1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <br />
    <table align="center" style="width: 328px">
        <tr>
            <td align="left">
                Tipo Identificacion:</td>
            <td align="left">
                <asp:DropDownList ID="ddlTipoId" runat="server" Height="20px" Width="151px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                Identificacion:</td>
            <td align="left">
                <asp:TextBox ID="txtId" runat="server" Width="143px"></asp:TextBox>
            </td>
        </tr>
        <tr>

            <td colspan="2" align="center">
                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    

               <div>

</div>

    <table align="center">
        <tr>
            <td align="center">
    <asp:Panel ID="pnlInforme" runat="server" Visible="false" Width="517px">
    
        <table border="1">
        </table>
      
        
          <table align="center">
            <tr>
                <td style="width: 136px">Nombre Paciente:</td>
                <td style="width: 354px">
                    <asp:Label ID="LblNombre" runat="server"></asp:Label>
                </td>
              </tr>
                          <tr>
                <td style="width: 136px">Cama:</td>
                <td style="width: 354px">
                    <asp:Label ID="LblCama" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 136px">&nbsp;</td>
                <td style="width: 354px">
                    &nbsp;</td>
            </tr>

         <tr>
                <td style="width: 136px">&nbsp;</td>
                <td style="width: 354px">
                    &nbsp;</td>
            </tr>
            
              
              <tr>
                <td style="width: 136px">Consentiento:</td>
                <td style="width: 354px">
                    <asp:CheckBox ID="chkcons1" runat="server" AutoPostBack="True" OnCheckedChanged="chkcons1_CheckedChanged" />
                    F-HC-2 Autorizacion de Representante<br />
                    <asp:CheckBox ID="chkcons2" runat="server" AutoPostBack="True"/>
                    F-HC-13 Consulta Historia Cíinica<br />
                    <asp:CheckBox ID="chkcons3" runat="server" AutoPostBack="True" OnCheckedChanged="chkcons3_CheckedChanged" />
                    F-HC-7 Procedimientos de Enfermeria<br />
                    <asp:CheckBox ID="chkcons4" runat="server" AutoPostBack="True" OnCheckedChanged="chkcons3_CheckedChanged" />
                    F-HC-4 Notificacion Acompañante Permanente</td>
               </tr>            
              
        <asp:Panel ID="pnlAcompanante" runat="server" Visible="false">
              <tr>
                <td style="width: 136px">&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
             <tr>
                <td style="width: 136px">Cedula Autorizado :</td>
                <td align="left">
                    <asp:TextBox ID="txtIdAut" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                <td style="width: 136px">Nombre Autorizado :</td>
                <td align="left">
                    <asp:TextBox ID="txtNomAut" runat="server" Width="252px"></asp:TextBox>
                 </td>
             </tr>

              <tr>
                <td style="width: 136px">&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
             </asp:Panel>


            <asp:Panel ID="pnlEnfermeria" runat="server" Visible ="false">
             <tr>
                <td style="width: 136px">Consideraciones :</td>
                <td align="left">
                    <asp:TextBox ID="txtConsideraciones" runat="server" CausesValidation="True" Height="83px" TextMode="MultiLine" Width="257px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                <td style="width: 136px"></td>
                <td align="left">
                    &nbsp;</td>
             </tr>


        </asp:Panel>
              
              <tr>
                <td style="width: 136px">
&nbsp;
                </td>
                <td style="width: 354px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" dir="ltr">
                    <asp:Button ID="btnGeneraPDF" runat="server" CssClass="btn" onclick="Click_Botones" onmouseout="this.className='btn'" onmouseover="this.className='btnhov'" Text="Genera Consentimiento" />
                </td>
            </tr>


        </table>




    </asp:Panel>
            </td>
        </tr>

 <asp:Panel ID="Plnpdf" runat="server" Visible="false">
                <tr>
            <td align="center">
                <asp:HyperLink id="hyperlink1" 
                  ImageUrl="../img/pdf.png"
                  Text="Descargue Archivo"
                  Target="_new"
                  runat="server"/>   
            </td>
        </tr>
     </asp:Panel>   
     
     <tr>
            <td align="center">
                <asp:Label ID="lblDescripcion" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

