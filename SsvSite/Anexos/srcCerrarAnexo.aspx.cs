﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Anexos_srcCerrarAnexo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Busqueda de Anexos Generados para cerrar el evento", this.Page);
        }
        litScript.Text = string.Empty;

    }
    protected void txtId_TextChanged(object sender, EventArgs e)
    {
        consultarConvenios(txtId.Text);
    }
    
    private void consultarConvenios(string CodAnexo)
    {
        DataTable dtOracle = new ClinicaCES.Logica.LAnexos().AnexosBuscarEstado(CodAnexo);


        DataTable dtSql = new ClinicaCES.Logica.LAnexos().AnexosBuscarEstadoSql(CodAnexo);



        DataTable dtGrid = new DataTable(); ;
        if (dtSql.Rows.Count > 0)
        {
            dtGrid.Columns.Add(new DataColumn("ANEXO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("TIPOANEXO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("IDPACIENTE", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("PACIENTE", System.Type.GetType("System.String")));
            for (int i = 0; i < dtOracle.Rows.Count; i++)
            {
                DataRow fila = dtGrid.NewRow();
                foreach (DataRow fila2 in dtSql.Rows)
                {
                    if (dtOracle.Rows[i]["ANEXO"].ToString() == fila2[0].ToString() && dtOracle.Rows[i]["TIPOANEXO"].ToString().Substring(6, 1) == fila2[2].ToString() && fila2[1].ToString() != "X")
                    {
                        fila["ANEXO"] = dtOracle.Rows[i]["ANEXO"];
                        fila["TIPOANEXO"] = dtOracle.Rows[i]["TIPOANEXO"];
                        fila["IDPACIENTE"] = dtOracle.Rows[i]["IDPACIENTE"];
                        fila["PACIENTE"] = dtOracle.Rows[i]["PACIENTE"];
                        dtGrid.Rows.Add(fila);
                        break;
                    }
                    //else
                    //{
                    //    fila["ESTADO_AUT"] = dtOracle.Rows[i]["ESTADO_AUT"];
                    //    fila["CODPRESTACION"] = dtOracle.Rows[i]["CODPRESTACION"];
                    //    fila["DESCRIPCION"] = dtOracle.Rows[i]["DESCRIPCION"];
                    //}
                }
                
            }
        }



        if (dtGrid.Rows.Count > 0)
        {
            pnlInforme.Visible = true;
            Procedimientos.LlenarGrid(dtGrid, gvAnexos);
            ViewState["dtPaginas"] = dtGrid;
            ViewState["dtPaginas2"] = dtGrid;
        }
        else
        {
            pnlInforme.Visible = false;
            Procedimientos.Script("mensajini", "Mensaje(76)", this.Page);
        }
    }
    protected DataTable llenardt(DataTable dt)
    {
        DataTable dtGrid = dt;
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
        return dtGrid;
    }
    protected void gvAnexos_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string msg = "";
        int TipoId = Convert.ToInt32(gvAnexos.Rows[e.RowIndex].Cells[1].Text.Substring(6, 1));

        if (new ClinicaCES.Logica.LAnexos().AnexosActualizarEstado(gvAnexos.Rows[e.RowIndex].Cells[0].Text, TipoId, Session["Nick1"].ToString()))
        {
            msg = "1";
        }
        else
        {
            msg = "3";
        }
        pnlInforme.Visible = false;
        txtId.Text = "";
        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }
}