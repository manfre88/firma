﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Pagina Principal", this.Page);
            datosUsuario(Session["Nick1"].ToString());
        }
    }

    private void datosUsuario(string usuario)
    {
        imgUsuario.ImageUrl = "../img/usuarios/" + usuario + ".jpg";
        imgUsuario.Width = new Unit(116);
        imgUsuario.Height = new Unit(116);

        ClinicaCES.Entidades.Usuarios usr = new ClinicaCES.Entidades.Usuarios();

        usr.Usuario = usuario;
        DataTable dtUsr = new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(usr);

        DataRow row = dtUsr.Rows[0];

        lblEmail.Text = row["EMAIL"].ToString();
        lblNick.Text = usuario;
        lblNivel.Text = row["NNIVEL"].ToString();
        lblNombre.Text = row["NOMBREC"].ToString();
    }
}
