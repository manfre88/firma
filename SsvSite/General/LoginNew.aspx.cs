﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;

public partial class General_LoginNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Form.DefaultButton = btnIniciar.UniqueID;
        if (!IsPostBack)
        {
            if (Request.Cookies["SIDGV1"] != null)
            {
                if (bool.Parse(Page.Server.HtmlEncode(Page.Request.Cookies["SIDGV1"]["Save1"])))
                    Response.Redirect("IndexNew.aspx");
                txtUsuario.Text = Page.Server.HtmlEncode(Page.Request.Cookies["SIDGV1"]["Nick1"]);
                txtPass.Focus();
            }
            else
                txtUsuario.Focus();
        }
    }

    protected void btnIniciar_Click(object sender, EventArgs e)
    {
        IniciarSession(txtUsuario.Text.Trim(), Encripcion.clsEncriptar.Encriptar(txtPass.Text));
        //IniciarSession(txtUsuario.Text.Trim(), txtPass.Text);
    }
    private void IniciarSession(string usr, string pass)
    {
        string[] nombreParam = {
            "@Usuario",
            "@Clave"
        };

        object[] vlrParam = {
            usr,
            pass
        };

        DataTable dt = new ClinicaCES.DA.DA().getDataTable("uspUsuarioValidar", nombreParam, vlrParam);

        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                string Nombre = row["Nombre"].ToString();
                string Perfil = row["NIVEL"].ToString();
                string Nick = row["USUARIO"].ToString();
                string PerfilN = row["Perfil"].ToString();
                string Tema = row["TEMA"].ToString();


                Session["Nombre1"] = Nombre;
                Session["PerfilN1"] = PerfilN;
                Session["Perfil1"] = Perfil;
                Session["Nick1"] = Nick;

                Session["Tema1"] = Tema;

                Response.Cookies.Remove("SIDGV1");
                HttpCookie SidgvCookie;
                if (Request.Cookies["SIDGV1"] == null)
                {
                    SidgvCookie = new HttpCookie("SIDGV1");
                }
                else
                {
                    SidgvCookie = Request.Cookies["SIDGV1"];
                }

                SidgvCookie["Nombre1"] = Nombre;//"&nbsp;" + Nombre + " ( <em>" + PerfilN + "</em> ) &nbsp;";
                SidgvCookie["Perfil1"] = Perfil;
                SidgvCookie["PerfilN1"] = PerfilN;
                SidgvCookie["Nick1"] = Nick;
                SidgvCookie["Save1"] = chkRecordar.Checked.ToString();

                SidgvCookie["Tema1"] = Tema;

                SidgvCookie.Expires = DateTime.Now.AddDays(10d);
                Response.Cookies.Add(SidgvCookie);

                //if (Request.QueryString["name"] != null && Request.QueryString["ver"] != null)
                //    Response.Redirect("frms/viwQuery.aspx?name=" + Request.QueryString["name"] + "&ver=" + Request.QueryString["ver"]);

                if (!string.IsNullOrEmpty(Request.QueryString["redirect"]))
                    Response.Redirect(Request.QueryString["redirect"]);

                Response.Redirect("IndexNew.aspx");
            }
            else
            {
                Interno.InnerHtml = "Usuario y/o Clave Incorrectos";
                warning.Visible = true;
                txtUsuario.Focus();
            }
        }

    }
}