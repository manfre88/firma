﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogIn.aspx.cs" Inherits="General_LogIn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>...::: BIENVENIDO :::...</title>
    <link href="../Style/cssSiDeg.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../js/mensajes.js"></script>
    <style type="text/css">
        .style1
        {
            border: 1px dashed #cccccc;
            width: 290px;
            height: 18px;
            font-weight: bold;
        }
        .style2
        {
            border: 1px dashed #cccccc;
            width: 290px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="center" >
            <tr>
                <td class="style1" align="center">
                                <img alt="" src="../img/logo.JPG" style="width: 142px; height: 96px" /></td>                
            </tr>           
            <tr>
                <td class="style1" align="center">CENTRAL<br />
                                DE AUTORIZACIONES</td>                
            </tr>           
            <tr>
                <td class="style2">
                <table align="center" class="punteadin">            
                    <tr>
                        <td colspan="2" class="cabezaT">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>Usuario:</td>
                        <td><asp:TextBox CssClass="form_input" ID="txtUsuario" runat="server"></asp:TextBox></td>
                    </tr>       
                    <tr>
                        <td>Password:</td>
                        <td><asp:TextBox CssClass="form_input" ID="txtPass" runat="server" TextMode="Password"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><asp:CheckBox ID="chkRecordar" runat="server" Text="Recordar Usuario" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"><asp:Button ID="btnIniciar" runat="server" Text="Ingresar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" OnClick="btnIniciar_Click" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="cabezaT">
                    <asp:Label ID="lblMensajeError" runat="server" Font-Bold="False" ForeColor="Red" Style="z-index: 106; left: 571px; position: absolute; top: 291px;
                    text-align: center; height: 13px; width: 281px; bottom: 283px;"></asp:Label>
                        </td>
                    </tr>
                </table>  
                </td>
            </tr>
            </table>
    </div>
    </form>
</body>
</html>
