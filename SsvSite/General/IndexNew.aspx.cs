﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class General_IndexNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Pagina Principal", this.Page);
            
        }

    }

}