﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography; 

/// <summary>
/// Summary description for Procedimientos
/// </summary>
public class Procedimientos
{
    public static string clave = "ABCDEFG54669525PQRSTUVWXYZabcdef852846opqrstuvwxyz"; // Clave de cifrado. NOTA: Puede ser cualquier combinación de carácteres.
    public Procedimientos()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void Script(string nombre, string script, Page Page)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), nombre, script, true);
    }

    public static void Script(string script, Literal litScript)
    {
        litScript.Text = "<script>" + script + "</script>";
    }

    public static void Titulo(string titulo, Page page)
    {
        Label lblTitulo = (Label)page.Master.FindControl("lblTitulo");
        lblTitulo.Text = titulo;
    }

    public static void LlenarCombos(DropDownList combo, DataTable dtOrigen, string campoValor, string campoTexto)
    {
       combo.DataSource = dtOrigen;
        combo.DataValueField = campoValor;
        combo.DataTextField = campoTexto;
        combo.DataBind();
        combo.Items.Insert(0, new ListItem("----Elegir Opción----", "-1"));
    }

    public static void LlenarCombosBlanco(DropDownList combo, DataTable dtOrigen, string campoValor, string campoTexto)
    {
        combo.DataSource = dtOrigen;
        combo.DataValueField = campoValor;
        combo.DataTextField = campoTexto;
        combo.DataBind();
        combo.Items.Insert(0, new ListItem("                  ", "-1"));
    }

    #region Llenar Grid
    public static void LlenarGrid(DataTable dtGrid, GridView gvGrid)
    {
        gvGrid.PageIndex = 0;
        gvGrid.DataSource = dtGrid;
        gvGrid.Visible = true;
        gvGrid.DataBind();
    }

    public static DataTable dtFiltrado(string sort, string Filtro, DataTable viewState)
    {
        DataRow[] dr = viewState.Select(Filtro, sort);
        DataTable dt = new DataTable();
        dt = viewState.Clone();
        for (int i = 0; i < dr.Length; i++)
        {
            dt.ImportRow(dr[i]);
        }
        /*if (dt.Rows.Count > 0)
        {
            
        }*/
        return dt;
    }

    public static void ValidarSession(Page page)
    {
        if (page.Session["Nombre1"] == null)
        {
            if (page.Request.Cookies["SIDGV1"] != null)
            {
                page.Session["Nombre1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["Nombre1"]);
                page.Session["Perfil1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["Perfil1"]);
                page.Session["PerfilN1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["PerfilN1"]);
                page.Session["Nick1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["Nick1"]);
                page.Session["Tema1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["Tema1"]);
            }
            else
                page.Response.Redirect("../General/LogIn.aspx");
        }
    }

    public static void ValidarSession(Page page, string url)
    {
        if (page.Session["Nombre"] == null)
        {
            if (page.Request.Cookies["SIDGV1"] != null)
            {
                page.Session["Nombre1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["Nombre1"]);
                page.Session["Perfil1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["Perfil1"]);
                page.Session["PerfilN1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["PerfilN1"]);
                page.Session["Nick1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["Nick1"]);
                page.Session["Tema1"] = page.Server.HtmlEncode(page.Request.Cookies["SIDGV1"]["Tema1"]);
            }
            else
                page.Response.Redirect("../General/LogIn.aspx?redirect=" + url);

        }
    }

    public static string cifrar(string cadena)
    {
        string result = string.Empty;
        byte[] encryted = System.Text.Encoding.Unicode.GetBytes(cadena);
        result = Convert.ToBase64String(encryted);
        return result;
    }
    public static string descifrar(string cadena)
    {
        string result = string.Empty;
        byte[] decryted = Convert.FromBase64String(cadena);
        //result = System.Text.Encoding.Unicode.GetString(decryted, 0, decryted.ToArray().Length);
        result = System.Text.Encoding.Unicode.GetString(decryted);
        return result;
    }

    #region Exportar Excel
    /// <summary>
    /// Exportar grid a excel
    /// </summary>
    /// <param name="grid">Grid a exportar</param>
    /// <param name="pagina">Pagina de la que se envia el grid</param>
    /// <param name="nombreArchivo">Nombre del archivo descargable</param>
    public static void ExportarExcel(GridView grid, Page pagina, string nombreArchivo)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        Page page = new Page();
        HtmlForm form = new HtmlForm();

        grid.EnableViewState = false;

        // Deshabilitar la validación de eventos, sólo asp.net 2
        page.EnableEventValidation = false;

        // Realiza las inicializaciones de la instancia de la clase Page que requieran los diseñadores RAD.
        page.DesignerInitialize();

        page.Controls.Add(form);
        form.Controls.Add(grid);

        page.RenderControl(htw);
        page.Dispose();
        page = null;

        pagina.Response.Clear();
        pagina.Response.Buffer = true;
        pagina.Response.ContentType = "application/vnd.ms-excel";
        pagina.Response.AddHeader("Content-Disposition", "attachment;filename=" + nombreArchivo + ".xls");
        pagina.Response.Charset = "UTF-8";
        pagina.Response.ContentEncoding = Encoding.Default;
        pagina.Response.Write(sb.ToString());
        pagina.Response.End();
    }



    /// <summary>
    /// Exportar grid a excel
    /// </summary>
    /// <param name="lit">literal con la tabla a exportar/param>
    /// <param name="pagina">Pagina de la que se envia el literal</param>
    /// <param name="nombreArchivo">Nombre del archivo descargable</param>

    public static void ExportarExcel(Control ctrl, Page pagina, string nombreArchivo)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        Page page = new Page();
        HtmlForm form = new HtmlForm();

        //grid.EnableViewState = false;

        // Deshabilitar la validación de eventos, sólo asp.net 2
        page.EnableEventValidation = false;

        // Realiza las inicializaciones de la instancia de la clase Page que requieran los diseñadores RAD.
        page.DesignerInitialize();

        page.Controls.Add(form);
        form.Controls.Add(ctrl);

        page.RenderControl(htw);
        page.Dispose();
        page = null;

        pagina.Response.Clear();
        pagina.Response.Buffer = true;
        pagina.Response.ContentType = "application/vnd.ms-excel";
        pagina.Response.AddHeader("Content-Disposition", "attachment;filename=" + nombreArchivo + ".xls");
        pagina.Response.Charset = "UTF-8";
        pagina.Response.ContentEncoding = Encoding.Default;
        pagina.Response.Write(sb.ToString());
        pagina.Response.End();
    }
    #endregion

    public static void comboEstadoInicial(DropDownList ddl)
    {
        ddl.Items.Clear();
        ddl.Items.Insert(0, new ListItem("----Elegir Opción----", "-1"));
    }

    public static void EstadoPaginacion(GridView gvPaginar, LinkButton lnkPaginacion, object origenDatos)
    {
        if (gvPaginar.Rows.Count == 0)
            return;

        if (gvPaginar.AllowPaging)
        {
            lnkPaginacion.Text = "Habilitar Paginación";
            gvPaginar.AllowPaging = false;
        }
        else
        {
            lnkPaginacion.Text = "Deshabilitar Paginación";
            gvPaginar.AllowPaging = true;
        }
        gvPaginar.DataSource = origenDatos;
        gvPaginar.PageIndex = 0;
        gvPaginar.DataBind();
    }

    public static void CrearDatatable(string[] Columas, string[] Vlrs, DataTable dt)
    {
        //Grid y datatable         
        DataRow dr;
        dr = dt.NewRow();

        for (int i = 0; i < Columas.Length; i++)
        {
            if (dt.Rows.Count == 0)
                dt.Columns.Add(Columas[i]);
            dr[Columas[i]] = Vlrs[i];
        }

        dt.Rows.Add(dr);
    }


    public static void ExportarDataTable(DataTable table, string name)
    {

        HttpContext context = HttpContext.Current;
        context.Response.Clear();
        foreach (DataColumn column in table.Columns)
        {
            context.Response.Write(column.ColumnName + ";");

        }
        context.Response.Write(Environment.NewLine);
        foreach (DataRow row in table.Rows)
        {
            for (int i = 0; i < table.Columns.Count; i++)
            {
                context.Response.Write(row[i].ToString().Replace(";", string.Empty).Replace(",", ".").Replace('Ñ', 'N').Replace('Á', 'A').Replace('É', 'E').Replace('Í', 'I').Replace('Ó', 'O').Replace('Ú', 'U').Replace("&quot", string.Empty).Replace("+", string.Empty).Replace("#", " ") + ";");
            }
            context.Response.Write(Environment.NewLine);
        }

        context.Response.ContentType = "text/csv";
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + name + ".csv");

        context.Response.End();


    }
    public static void ExportarDataTable(DataTable table, string name, GridView grid)
    {
        int contador = 0;
        string alineacion = "center";
        string color = "#006699";
        string colorTexto = "#ffffff";
        string fondoFooter = "#F7DFB5";


        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + name + ".xls");

        HttpContext.Current.Response.Charset = "utf-8";
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
        //sets font
        HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
        HttpContext.Current.Response.Write("<BR><BR><BR>");
        HttpContext.Current.Response.Write("<Table border='1' bgColor='#006699' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
        int columnscount = table.Columns.Count;

        for (int j = 0; j < columnscount; j++)
        {
            //Makes headers bold

            HttpContext.Current.Response.Write("<Td align=" + alineacion + "  bgcolor=" + color + ">");
            //HttpContext.Current.Response.Write("<Td CssClass=" + cabeza + " >");
            HttpContext.Current.Response.Write("<font color=" + colorTexto + ">");
            HttpContext.Current.Response.Write("<B>");

            HttpContext.Current.Response.Write(grid.Columns[j].HeaderText.ToString());
            //HttpContext.Current.Response.Write(grid.Columns[j].FooterText.ToString());
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Write("</Td>");
        }
        HttpContext.Current.Response.Write("</TR>");
        string fondoFilas = "";
        foreach (DataRow row in table.Rows)
        {
            if (contador == 0)
            {
                fondoFilas = "#dcdcdc";
                contador = 1;
            }
            else if (contador == 1)
            {
                fondoFilas = "#ffffff";
                contador = 0;
            }
            HttpContext.Current.Response.Write("<TR>");
            for (int i = 0; i < table.Columns.Count; i++)
            {

                HttpContext.Current.Response.Write("<Td bgcolor=" + fondoFilas + ">");
                HttpContext.Current.Response.Write(row[i].ToString());

                HttpContext.Current.Response.Write("</Td>");

            }
            
            
           ////prueba footer 

            HttpContext.Current.Response.Write("</TR>");
           ////
           //   //  HttpContext.Current.Response.Write(grid.Columns[8].FooterText.ToString());
           
            
           //// 
           // //fin pruega 




            
        }
        for (int i = 0; i < table.Columns.Count; i++)
        {
            //HttpContext.Current.Response.Write("<TR>");
            HttpContext.Current.Response.Write("<Td bgcolor=" + fondoFooter + ">");
            if (grid.FooterRow != null)
            {
                
                //table.Rows.Add();
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(grid.FooterRow.Cells[i].Text);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
                //HttpContext.Current.Response.Write("</TR>");
            }

        }
        HttpContext.Current.Response.Write("</Table>");
        HttpContext.Current.Response.Write("</font>");
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();

    }

    public static bool regularExpression(string text, string expression)
    {
        System.Text.RegularExpressions.Regex l_reg = new System.Text.RegularExpressions.Regex(expression);
        return (l_reg.IsMatch(text));
    }

    #region Select Distict

    public static DataTable SelectDistinct(DataTable SourceTable, params string[] FieldNames)
    {
        object[] lastValues;
        DataTable newTable;
        DataRow[] orderedRows;

        if (FieldNames == null || FieldNames.Length == 0)
            throw new ArgumentNullException("FieldNames");

        lastValues = new object[FieldNames.Length];
        newTable = new DataTable();

        foreach (string fieldName in FieldNames)
            newTable.Columns.Add(fieldName, SourceTable.Columns[fieldName].DataType);

        orderedRows = SourceTable.Select("", string.Join(", ", FieldNames));

        foreach (DataRow row in orderedRows)
        {
            if (!fieldValuesAreEqual(lastValues, row, FieldNames))
            {
                newTable.Rows.Add(createRowClone(row, newTable.NewRow(), FieldNames));

                setLastValues(lastValues, row, FieldNames);
            }
        }

        return newTable;
    }



    private static bool fieldValuesAreEqual(object[] lastValues, DataRow currentRow, string[] fieldNames)
    {
        bool areEqual = true;

        for (int i = 0; i < fieldNames.Length; i++)
        {
            if (lastValues[i] == null || !lastValues[i].Equals(currentRow[fieldNames[i]]))
            {
                areEqual = false;
                break;
            }
        }

        return areEqual;
    }

    private static DataRow createRowClone(DataRow sourceRow, DataRow newRow, string[] fieldNames)
    {
        foreach (string field in fieldNames)
            newRow[field] = sourceRow[field];

        return newRow;
    }

    private static void setLastValues(object[] lastValues, DataRow sourceRow, string[] fieldNames)
    {
        for (int i = 0; i < fieldNames.Length; i++)
            lastValues[i] = sourceRow[fieldNames[i]];
    }



    #endregion

    public static void EnviarCorreo(string Para, string Mensaje, string Asunto)
    {
        ClinicaCES.Correo.Correo objCorreo = new ClinicaCES.Correo.Correo();
        objCorreo.Asunto = Asunto;
        objCorreo.De = ConfigurationManager.AppSettings["mailSoporte"];
        objCorreo.Mensaje = Mensaje;
        objCorreo.Para = Para;
        if (!objCorreo.Enviar())
        {
            new ClinicaCES.Error.Error().EscibirError("Error enviando Email");
        }

    }

    public static void EnviarCorreo(string Para, string Mensaje, string Asunto, string[] Adjuntos)
    {
        ClinicaCES.Correo.Correo objCorreo = new ClinicaCES.Correo.Correo();
        objCorreo.Asunto = Asunto;
        objCorreo.De = ConfigurationManager.AppSettings["mailSoporte"];
        objCorreo.Mensaje = Mensaje;
        objCorreo.Para = Para;
        objCorreo.Adjuntos = Adjuntos;
        if (!objCorreo.Enviar())
        {
            new ClinicaCES.Error.Error().EscibirError("Error enviando Email");
        }

    }

    public static bool ValidaGuardar(TextBox[] txt, DropDownList[] ddl)
    {
        bool valido = true;

        if (txt != null)
        {
            foreach (TextBox item in txt)
            {
                if (string.IsNullOrEmpty(item.Text.Trim()))
                {
                    item.CssClass = "invalidtxt";
                    valido = false;
                }
                else
                    item.CssClass = "form_input";
            }
        }

        if (ddl != null)
        {
            foreach (DropDownList item in ddl)
            {
                if (item.SelectedIndex == 0)
                {
                    item.CssClass = "invalidtxt";
                    valido = false;
                }
                else
                    item.CssClass = "";
            }
        }

        return valido;
    }

    #endregion

}
