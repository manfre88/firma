﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;
using ClinicaCES.Logica;

public partial class Admin_srcCursos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Listado de Ambulatorios", this.Page);
            DataTable dtEstado = new ClinicaCES.Logica.LAmbulatorios().EstadosConsultar();
            Procedimientos.LlenarCombos(ddlEstado, dtEstado, "CODIGO", "DESCESTADO");
            ddlEstado.SelectedIndex = 0;
            Consultar();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Consultar()
    {

        Ambulatorios amb = new Ambulatorios();
        amb.Id = "";

        DataTable dtEvento = new LAmbulatorios().AmbulatoriosConsultar(amb);

        Procedimientos.LlenarGrid(dtEvento, gvUsr);
        ViewState["dtEvento"] = dtEvento;
    }

    protected void gvUsr_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onclick", "redirect('AddAmbulatorios.aspx?eve=" + e.Row.Cells[0].Text.Trim() + "')");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }

        GridViewRow row = e.Row;
        Image img = e.Row.FindControl("ImgSemaforo") as Image;

        if (row.RowIndex > -1)
        {
            string datos = row.Cells[7].Text.Trim();

            switch (row.Cells[7].Text.Trim())
            {
                case "PENDIENTE GESTION":
                    img.ImageUrl = "../img/Rojo.png";
                    break;
                case "PROCESO AUTORIZACION":
                    img.ImageUrl = "../img/Amarillo.png";
                    break;
                case "APROBADO":
                    img.ImageUrl = "../img/Verde.png";
                    break;
                case "NEGADO":
                    img.ImageUrl = "../img/Naranja.png";
                    break;
                case "CANCELADO":
                    img.ImageUrl = "../img/Gris.png";
                    break;
                case "DEVUELTO":
                    img.ImageUrl = "../img/Azul.png";
                    break;
            }
        }

    }

    private void Filtrar()
    {
        string estado = "";
        if (ddlEstado.SelectedValue != "-1")
            estado = ddlEstado.SelectedValue;

        DataTable dtEvento = Procedimientos.dtFiltrado("NOMBRE", "PRE_PRE_DESCRIPCIO LIKE '*" + txtBusquedaUsuario.Text.Trim() + "*' AND NOMBRE LIKE '*" + txtBusquedaNombre.Text.Trim() + "*' AND CON_DESCRIP LIKE '*" + txtBusquedaConvenio.Text.Trim() + "*' AND IDENTIFICACION LIKE '*" + txtCedula.Text.Trim() + "*' AND ESTADO LIKE '*" + estado + "*'", (DataTable)ViewState["dtEvento"]);
        Procedimientos.LlenarGrid(dtEvento, gvUsr);
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgCurso))
            Response.Redirect("addAmbulatorios.aspx");
    }

    private void EstadoInicial()
    {
        if (pnlNombre.Visible)
            kitarFiltro("Capacitador", "Capacitador", pnlNombre, txtBusquedaNombre);
        if (pnlUsuario.Visible)
            kitarFiltro("Curso", "Curso", pnlUsuario, txtBusquedaUsuario);
        if (pnlFecha.Visible)
            kitarFiltro("Convenio", "Convenio", pnlFecha, txtBusquedaConvenio);
        if (pnlCedula.Visible)
            kitarFiltro("Cedula", "Cedula", pnlCedula, txtCedula);
        if (pnlEstado.Visible)
            kitarFiltro2("Estado", "Estado", pnlEstado, ddlEstado);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
        txtBusquedaUsuario.Text = string.Empty;
        txtBusquedaNombre.Text = string.Empty;
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkKitarNombre))
            kitarFiltro("Capacitador", "Capacitador", pnlNombre, txtBusquedaNombre);
        else if (sender.Equals(lnkKitarUsuario))
            kitarFiltro("Curso", "Curso", pnlUsuario, txtBusquedaUsuario);
        else if (sender.Equals(lnkKitarConvenio))
            kitarFiltro("Convenio", "Convenio", pnlFecha, txtBusquedaConvenio);
        else if (sender.Equals(lnkKitarCedula))
            kitarFiltro("Cedula", "Cedula", pnlCedula, txtCedula);
        else if (sender.Equals(lnkKitarEstado))
            kitarFiltro2("Estado", "Estado", pnlEstado, ddlEstado);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "Capacitador":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            case "Curso":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlUsuario.Visible = true;
                    break;
                }
            case "Convenio":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlFecha.Visible = true;
                    break;
                }
            case "Cedula":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCedula.Visible = true;
                    break;
                }

            case "Estado":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlEstado.Visible = true;
                    break;
                }

            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    break;
                }
        }
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void kitarFiltro2(string value, string text, Panel pnl, DropDownList txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.SelectedIndex = 0;
    }

    protected void gvUsr_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsr.PageIndex = e.NewPageIndex;
        gvUsr.DataSource = ViewState["dtEvento"];
        gvUsr.DataBind();
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
    }


    private bool ValidaSemana()
    {
        bool valido = true;

        if ((txtFecha.Text != "") && (txtFechaF.Text != ""))
        {
            DateTime FechaIni = Convert.ToDateTime(txtFecha.Text);
            DateTime FechaFin = Convert.ToDateTime(txtFechaF.Text);

            if (FechaIni.CompareTo(FechaFin) == 1)
            {
                valido = false;
            }

            if (!valido)
            {
                txtFecha.Text = string.Empty;
                txtFechaF.Text = string.Empty;
            }
        }
        return valido;
    }

    protected void txtFechaF_TextChanged1(object sender, EventArgs e)
    {

        if (!ValidaSemana())
        {
            Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
        }
    }

    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(txtFechaF.Text.Trim()))
            if (!ValidaSemana())
            {
                Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
            }
    }
}
