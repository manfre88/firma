﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Logica;
using ClinicaCES.Entidades;

public partial class Formulario_AddAmbulatorio : System.Web.UI.Page
{
    string eve = "";
    bool sw = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Procedimientos.Titulo("Adicion/Actualizacion Ambulatorios", this.Page);
            txtIdentificacion.Attributes.Add("onkeypress", "return clickButton(event,'" + lnkComprobar.ClientID + "')");
            ListarCombos();
            ddlTiempo.SelectedValue = "1";


            if (Request.QueryString["eve"] != null)
            {
                eve = Request.QueryString["eve"];
                Comprobar(Request.QueryString["eve"]);
            }
            else
            {
                hdnAccion.Value = "I";
                btnAgregar.Visible = true;
                btnNuevo.Visible = false;
                ddlEstado.SelectedValue = "1";
                ddlEstado.Enabled = false;
                eve = "";
            }

            litScript.Text = string.Empty;


        }

        if (Request.QueryString["eve"] != null)
        {
            eve = Request.QueryString["eve"];
        }
        else
        {
            eve = "";
        }
    }

    private void ListarCombos()
    {

        DataTable dtEspecialidad = new ClinicaCES.Logica.LAmbulatorios().CmbEspecialidad();
        Procedimientos.LlenarCombos(ddlEspecialidad, dtEspecialidad, "CODIGO", "DESCRIPCION");
        ddlProcIndicado.SelectedIndex = 0;

        DataTable dtAsegurador = new ClinicaCES.Logica.LAmbulatorios().CmbAsegurador("");
        Procedimientos.LlenarCombos(ddlAsegurador, dtAsegurador, "CODIGO", "DESCRIPCION");
        ddlProcIndicado.SelectedIndex = 0;

        DataTable dtEstado = new ClinicaCES.Logica.LAmbulatorios().EstadosConsultar();
        Procedimientos.LlenarCombos(ddlEstado, dtEstado, "CODIGO", "DESCESTADO");
        ddlProcIndicado.SelectedIndex = 0;
    }


    private void Comprobar(string id)
    {
        Ambulatorios amb = new Ambulatorios();
        amb.Id = id;

        //string consulta = new ClinicaCES.Logica.LAmbulatorios(). AmbulatoriosConsultarConsulta(amb);
        //txtObservaciones.Text = consulta;

        DataTable dtAmbulatorios = new ClinicaCES.Logica.LAmbulatorios().AmbulatoriosConsultar(amb);

        if (dtAmbulatorios.Rows.Count > 0)
        {
            //encontro cargar
            DataRow row = dtAmbulatorios.Rows[0];

            txtIdentificacion.Text = row["IDENTIFICACION"].ToString();
            txtNombre.Text = row["NOMBRE"].ToString();
            ddlAsegurador.SelectedValue = row["CONVENIO"].ToString();
            ddlTipoSol.SelectedValue = row["TIPOSOL"].ToString().Trim();
            ddlSopEnviado.SelectedValue = row["SOPENV"].ToString().Trim();
            txtNrEvento.Text = row["EVENTO"].ToString().Trim();
            txtFecPosResp.Text = Convert.ToDateTime(row["FECRES"]).ToString("yyyy/MM/dd");
            ddlTipoSolicitud.SelectedValue = row["TIPSOLPRO"].ToString().Trim();
            ddlEspecialidad.SelectedValue = row["ESPREQU"].ToString().Trim();

            DataTable dtProcedimiento = new ClinicaCES.Logica.LAmbulatorios().CmbPrestaciones(ddlEspecialidad.SelectedValue);
            Procedimientos.LlenarCombos(ddlProcIndicado, dtProcedimiento, "CODIGO", "DESCRIPCION");
            ddlProcIndicado.SelectedIndex = 0;

            Procedimientos.LlenarCombos(ddlProcAutorizado, dtProcedimiento, "CODIGO", "DESCRIPCION");
            ddlProcAutorizado.SelectedIndex = 0;

            ddlProcIndicado.SelectedValue = row["PROCIND"].ToString().Trim();
            ddlProcAutorizado.SelectedValue = row["PROCAUT"].ToString().Trim();
            txtPrestador.Text = row["PRESTADOR"].ToString().Trim();

            string FecPro = Convert.ToDateTime(row["FECPROC"]).ToString("yyyy/MM/dd").Trim();
            string FecCir = Convert.ToDateTime(row["FECCX"]).ToString("yyyy/MM/dd").Trim();

            if (FecPro == "1900/01/01" || FecPro == "1901/01/01" || FecPro == "0001/01/01")
                txtFecProcedimiento.Text = "";
            else txtFecProcedimiento.Text = FecPro;

            if (FecCir == "1900/01/01" || FecCir == "1901/01/01" || FecCir == "0001/01/01")
                txtFechaCx.Text = "";
            else 
                txtFechaCx.Text = FecCir;
            
            txtObservaciones.Text = row["OBSERVACION"].ToString();


            if (row["ESTADO"].ToString().Trim() == "1")
            {
                Procedimientos.LlenarCombos(ddlEstado, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '1' or CODIGO='2'", new ClinicaCES.Logica.LAmbulatorios().EstadosConsultar()), "CODIGO", "DESCESTADO");
            }
            if (row["ESTADO"].ToString().Trim() == "2")
            {
                Procedimientos.LlenarCombos(ddlEstado, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '2' or CODIGO='3' or CODIGO='4' or CODIGO='5' or CODIGO='6'", new ClinicaCES.Logica.LAmbulatorios().EstadosConsultar()), "CODIGO", "DESCESTADO");
            }
            if (row["ESTADO"].ToString().Trim() == "3")
            {
                Procedimientos.LlenarCombos(ddlEstado, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '3'", new ClinicaCES.Logica.LAmbulatorios().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstado.Enabled = false;
            
            }
            if (row["ESTADO"].ToString().Trim() == "4")
            {
                Procedimientos.LlenarCombos(ddlEstado, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '4'", new ClinicaCES.Logica.LAmbulatorios().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstado.Enabled = false;
            }

            if (row["ESTADO"].ToString().Trim() == "5")
            {
                Procedimientos.LlenarCombos(ddlEstado, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '5'", new ClinicaCES.Logica.LAmbulatorios().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstado.Enabled = false;
            }
            if (row["ESTADO"].ToString().Trim() == "6")
            {
                Procedimientos.LlenarCombos(ddlEstado, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '1' or CODIGO = '6'", new ClinicaCES.Logica.LAmbulatorios().EstadosConsultar()), "CODIGO", "DESCESTADO");
            }


            ddlEstado.SelectedValue = row["ESTADO"].ToString().Trim();

            switch (row["UNDTIEMPO"].ToString().Trim())
            {

                case "1":
                    txtTiempo.Text = row["TIEMPO"].ToString();
                    break;
                case "2":
                    txtTiempo.Text = Convert.ToString((Convert.ToInt32(row["TIEMPO"].ToString()) / 60));
                    break;
                case "3":
                    txtTiempo.Text = Convert.ToString((Convert.ToInt32(row["TIEMPO"].ToString()) / 1440));
                    break;
                default:
                    break;
            }

            ddlTiempo.SelectedValue = row["UNDTIEMPO"].ToString().Trim();
            hdnAccion.Value = "A";
            btnAgregar.Visible = false;
            btnNuevo.Visible = true;

        }
        else
        {
            hdnAccion.Value = "I";
            btnAgregar.Visible = true;
            btnNuevo.Visible = false;
        }
    }

    private void ComprobarNombre(string id)
    {
        Ambulatorios amb = new Ambulatorios();
        amb.Identificacion = id.Trim();

        DataTable dtAmbulatorios = new ClinicaCES.Logica.LAmbulatorios().ConsultarNombre(amb);

        if (dtAmbulatorios.Rows.Count > 0)
        {

            DataRow row = dtAmbulatorios.Rows[0];

            txtIdentificacion.Text = row["IDENTIFICACION"].ToString();
            txtNombre.Text = row["NOMBRES"].ToString();

            DataTable dtAsegurador = new ClinicaCES.Logica.LAmbulatorios().CmbAsegurador(row["IDENTIFICACION"].ToString().Trim());
            Procedimientos.LlenarCombos(ddlAsegurador, dtAsegurador, "CODIGO", "DESCRIPCION");
            ddlProcIndicado.SelectedIndex = 0;

            ddlAsegurador.SelectedValue = row["CONVENIO"].ToString();
            txtNombre.Enabled = false;
        }
        else
        {
            txtNombre.Text = "";
            txtNombre.Enabled = true;
            ddlAsegurador.SelectedIndex = 0;

            DataTable dtAsegurador = new ClinicaCES.Logica.LAmbulatorios().CmbAsegurador("");
            Procedimientos.LlenarCombos(ddlAsegurador, dtAsegurador, "CODIGO", "DESCRIPCION");
            ddlProcIndicado.SelectedIndex = 0;

            hdnAccion.Value = "I";
            btnAgregar.Visible = true;
            btnNuevo.Visible = false;
        }

    }

    protected void Click_Botones(object sender, EventArgs e)
    {

        if (sender.Equals(btnAgregar))
        {
            Guardar(txtIdentificacion.Text
                  , txtNombre.Text.ToUpper()
                  , ddlAsegurador.SelectedValue
                  , ddlTipoSol.SelectedValue
                  , ddlSopEnviado.SelectedValue
                  , txtNrEvento.Text.ToUpper()
                  , txtFecPosResp.Text
                  , ddlTipoSolicitud.SelectedValue
                  , ddlEspecialidad.SelectedValue
                  , ddlProcIndicado.SelectedValue
                  , ddlProcAutorizado.SelectedValue
                  , txtPrestador.Text.ToUpper()
                  , txtFecProcedimiento.Text
                  , txtFechaCx.Text
                  , txtObservaciones.Text
                  , ddlEstado.SelectedValue
                  , txtTiempo.Text
                  , ddlTiempo.SelectedValue);
        }
        if (sender.Equals(btnCancelar))
        {
            Response.Redirect("srcAmbulatorios.aspx");
        }
        if (sender.Equals(btnNuevo))
        {
            Guardar(txtIdentificacion.Text
                  , txtNombre.Text.ToUpper()
                  , ddlAsegurador.SelectedValue
                  , ddlTipoSol.SelectedValue
                  , ddlSopEnviado.SelectedValue
                  , txtNrEvento.Text.ToUpper()
                  , txtFecPosResp.Text
                  , ddlTipoSolicitud.SelectedValue
                  , ddlEspecialidad.SelectedValue
                  , ddlProcIndicado.SelectedValue
                  , ddlProcAutorizado.SelectedValue
                  , txtPrestador.Text.ToUpper()
                  , txtFecProcedimiento.Text
                  , txtFechaCx.Text
                  , txtObservaciones.Text
                  , ddlEstado.SelectedValue
                  , txtTiempo.Text
                  , ddlTiempo.SelectedValue);
        }
        if (sender.Equals(lnkComprobar))
        {
            ComprobarNombre(txtIdentificacion.Text.Trim());
        }
    }

    private void Guardar(string Identificacion
                       , string Nombre
                       , string Convenio
                       , string TipoSol
                       , string SoporteEnv
                       , string NumEvento
                       , string FechaPosResp
                       , string TipoSolicitud
                       , string Especialidad
                       , string ProcIndicado
                       , string ProcedAprobado
                       , string Prestador
                       , string FechaProcedim
                       , string FechaCx
                       , string Observacion
                       , string Estado
                       , string Tiempo
                       , string UndTiempo
        )
    {
        if (!ValidaGuardar())
            return;


        Ambulatorios amb = new Ambulatorios();


        string msg = "1";

        amb.Identificacion = Identificacion;
        amb.Nombre = Nombre;
        amb.Convenio = Convenio;
        amb.TipoSolicitud = TipoSol;
        amb.SoporteEnv = SoporteEnv;
        amb.TipoProced = TipoSolicitud;
        amb.NumEvento = NumEvento;
        amb.FechaPosResp = Convert.ToDateTime(FechaPosResp);
        amb.Especialidad = Especialidad;
        amb.ProcIndicado = ProcIndicado;
        amb.ProcedAprobado = ProcedAprobado;
        amb.Prestador = Prestador;
        if (FechaProcedim != "")
        {
            amb.FechaProcedim = Convert.ToDateTime(FechaProcedim);
        }
        if (FechaCx != "")
        {
            amb.FechaCx = Convert.ToDateTime(FechaCx);
        }
        amb.Observacion = Observacion;
        amb.Estado = Estado;
        amb.UndTiempo = UndTiempo;
        amb.Usuario = Session["Nick1"].ToString();

        if (eve != null)
        {
            amb.Id = eve;
        }
        else { amb.Id = ""; }

        amb.cambioestado = sw;

        switch (UndTiempo)
        {
            case "1":
                amb.Tiempo = Tiempo;
                break;
            case "2":
                amb.Tiempo = Convert.ToString(Convert.ToInt32(Tiempo) * 60);
                break;
            case "3":
                amb.Tiempo = Convert.ToString(Convert.ToInt32(Tiempo) * 1440);
                break;
            default:
                break;
        }

        //string consulta = new ClinicaCES.Logica.LAmbulatorios().AmbulatorioActualizarconsulta(amb);

        //txtObservaciones.Text = consulta;

        if (new ClinicaCES.Logica.LAmbulatorios().AmbulatorioActualizar(amb))
        { //guardo 
            Response.Redirect("srcAmbulatorios.aspx");
        }
        else
        {
            msg = "3"; // no guardo
        }
    }

    private bool ValidaGuardar()
    {
        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };
        bool Valido = true;


        if (string.IsNullOrEmpty(txtIdentificacion.Text.Trim()))
        {
            txtIdentificacion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtIdentificacion.CssClass = "form_input";
        }


        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            txtNombre.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }

        if (ddlTipoSol.SelectedIndex == 0)
        {
            
            LblValidaTipoSolicitud.ForeColor = color[1];
            ddlTipoSol.CssClass = css[1];
            Valido = false;
        }
        else
        {
            LblValidaTipoSolicitud.ForeColor = color[0];
            ddlTipoSol.CssClass = css[0];
        }

        if (ddlEspecialidad.SelectedIndex > 0)
        {
            if (ddlProcIndicado.SelectedIndex == 0)
            {
                Valido = false;
                LblValidaTipoSolicitud.ForeColor = color[1];
                ddlProcIndicado.CssClass = css[1];
            }
            else
            {
                LblValidaTipoSolicitud.ForeColor = color[0];
                ddlProcIndicado.CssClass = css[0];
            }
        }
        else
        {
            lblValidaEspecialidad.ForeColor = color[0];
            ddlEspecialidad.CssClass = css[0];
        }

        if (string.IsNullOrEmpty(txtTiempo.Text.Trim()))
        {
            Valido = false;
            txtTiempo.CssClass = css[1];
            lblValidaAlerta.ForeColor = color[1];
        }
        else
        {
            txtTiempo.CssClass = css[0];
        }


        return Valido;
    }


    private void Consultar(string NomCurso, string Capacitador, string Duracion, string Cupo, string FechaI, string FechaF)
    {
        string msg = string.Empty;

        if (FechaI == "" | FechaF == "")
        {
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
        }

        else
        {
            Procedimientos.Script("mensajini", "Mensaje(" + msg + ")", this.Page);
            EstadoInicial();
        }

    }

    private void MostrarDatos()
    {

    }

    private void EstadoInicial()
    {
    }


    private void GuardarHorario(string Curso)
    {
    }


    protected void Excel_Click(object sender, ImageClickEventArgs e)
    {
        // Procedimientos.ExportarDataTable(dtExcel(), "Informe Registro Eventos Adversos");
    }


    public DataTable dtExcel()
    {
        string[] titulo = { "IDPACIENTE", "NOMBRES", "FECHA_EVENTO", "HORA EVENTO", "DESCRIPCION", "HUBO LESION", "LESION", "CARGO_RESPONSABLE", "SERVICIO_OCURRENCIA", "ES EVITABLE", "COMO ES EVITABLE", "ACCIONES REALIZADAS", "SERVICIO_REPORTE", "FECHA_REPORTE" };
        string[] columnas = { "IDPACIENTE", "NOMBRES", "FECEVENTO", "HORAEVENTO", "DESCRIPCION", "HUBOLESION", "LESION", "CARGORESP", "NOMSERVOCURRENCIA", "ESEVITABLE", "BARRERAENCONTRADA", "BARRERANOENCONTRADA", "NOMSERVREPORTE", "FECREPORTE" };
        string[] valores = new string[14];
        DataTable dt = new DataTable();
        DataTable dtPagina = (DataTable)ViewState["dtPaginas"];

        foreach (DataRow row in dtPagina.Rows)
        {
            for (int i = 0; i < titulo.Length; i++)
            {
                valores[i] = row[columnas[i]].ToString();
            }
            Procedimientos.CrearDatatable(titulo, valores, dt);
        }
        return dt;
    }

    protected void txtFechaF_TextChanged1(object sender, EventArgs e)
    {
    }

    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
    }


    protected void gvEventos_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkEventos = (LinkButton)e.Row.FindControl("lnkEventos");
            lnkEventos.CommandArgument = e.Row.RowIndex.ToString();
            e.Row.Attributes.Add("onclick", "postNivelGrid(" + (e.Row.RowIndex + 2) + ")");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }
    }

    protected void gvEventos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }


    protected void gvEventos_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvContac_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
    }

    protected void Gridview1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }
    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
    {
        sw = true;
        string dato = ddlEstado.SelectedValue.ToString();
    }
    protected void ddlEspecialidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtProcedimiento = new ClinicaCES.Logica.LAmbulatorios().CmbPrestaciones(ddlEspecialidad.SelectedValue);
        Procedimientos.LlenarCombos(ddlProcIndicado, dtProcedimiento, "CODIGO", "DESCRIPCION");
        ddlProcIndicado.SelectedIndex = 0;

        Procedimientos.LlenarCombos(ddlProcAutorizado, dtProcedimiento, "CODIGO", "DESCRIPCION");
        ddlProcAutorizado.SelectedIndex = 0;
    }
}
