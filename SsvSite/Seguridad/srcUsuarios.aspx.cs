﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Seguridad_srcUsuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Buscar Usuarios", this.Page);
            Consultar();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
    }

    private void Consultar()
    {
        Usuarios usr = new Usuarios();
        DataTable dtUsr = new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(usr);

        Procedimientos.LlenarGrid(dtUsr, gvUsr);
        ViewState["dtUsr"] = dtUsr;
    }

    protected void gvUsr_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onclick", "redirect('addUsuario.aspx?usr=" + e.Row.Cells[0].Text.Trim() + "')");
            //e.Row.Attributes.Add("onclick", "alert('oe')");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");


            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }
    }


    private void Filtrar()
    {
        DataTable dtUsr = Procedimientos.dtFiltrado("NOMBREC", "USUARIO LIKE '*" + txtBusquedaUsuario.Text.Trim() + "*' AND NombreC LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'", (DataTable)ViewState["dtUsr"]);
        Procedimientos.LlenarGrid(dtUsr, gvUsr);
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        //if (sender.Equals(imgRetirar))
        //    Retirar(xmlRedes(), Session["Nick"].ToString());
        if (sender.Equals(imgBuscar))
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addUsuario.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private void EstadoInicial()
    {
        if (pnlNombre.Visible)
            kitarFiltro("NombreC", "Nombre", pnlNombre, txtBusquedaNombre);
        if (pnlUsuario.Visible)
            kitarFiltro("USUARIO", "Usuario", pnlUsuario, txtBusquedaUsuario);
        Consultar();
        ddlFlitro.SelectedIndex = 0;
        txtBusquedaUsuario.Text = string.Empty;
        txtBusquedaNombre.Text = string.Empty;
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkKitarNombre))
            kitarFiltro("NombreC", "Nombre", pnlNombre, txtBusquedaNombre);
        else if (sender.Equals(lnkKitarUsuario))
            kitarFiltro("USUARIO", "Usuario", pnlUsuario, txtBusquedaUsuario);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NombreC":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlUsuario.Visible = true;
                    break;
                }
        }
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    protected void gvUsr_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsr.PageIndex = e.NewPageIndex;
        gvUsr.DataSource = ViewState["dtUsr"];
        gvUsr.DataBind();
    }
}
