﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Seguridad_addNivel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Procedimientos.ValidarSession(this.Page);

        if (!IsPostBack)
        {
            Procedimientos.Titulo("Administrar Niveles de Acceso", this.Page);
            EstadoInicial();
            txtCodigo.Attributes.Add("onkeypress", "return clickButton(event,'" + lnkComprobar.ClientID + "')");
            txtNivel.Attributes.Add("onkeypress", "return clickButton(event,'" + btnGuardar.ClientID + "')");
        }

        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        txtCodigo.Text = string.Empty;
        txtNivel.Text = string.Empty;
        EstadoControles(false);
        txtCodigo.Focus();
        Comprobar();

    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            Guardar(txtCodigo.Text.Trim(), txtNivel.Text.ToUpper().Trim());  
        }
        else if (sender.Equals(lnkComprobar))
        {
            Comprobar(txtCodigo.Text.Trim());
        }
        else if (sender.Equals(btnCancelar))
        {
            EstadoInicial();
        }
        else if (sender.Equals(btnRetirar))
        {
            Retirar(txtCodigo.Text.Trim());
        }
    }

    private void Retirar(string codigo)
    {
        Nivel nvl = new Nivel();
        nvl.Codigo = codigo;
        Procedimientos.Script("Mensaje(" + new ClinicaCES.Logica.LNivel().NivelRetirar(nvl) + ")", litScript);
        EstadoInicial();

    }

    private void Comprobar()
    {
        Nivel nvl = new Nivel();

        DataTable dtNivel = new ClinicaCES.Logica.LNivel().NivelConsultar(nvl);

        if (dtNivel.Rows.Count > 0)
        {
            Procedimientos.LlenarGrid(dtNivel, gvNivel);
        }
    }

    private void Comprobar(string Codigo)
    {
        Nivel nvl = new Nivel();
        nvl.Codigo = Codigo;

        EstadoControles(true);

        DataTable dtNivel = new ClinicaCES.Logica.LNivel().NivelConsultar(nvl);

        if (dtNivel.Rows.Count > 0)
        {
            //existe carguelo
            txtNivel.Text = dtNivel.Rows[0]["NIVEL"].ToString();
            txtCodigo.Text = dtNivel.Rows[0]["CODIGO"].ToString();
            btnRetirar.Enabled = true;
        }
        else
        { 
            //registro nuevo

        }

        Procedimientos.Script("SeleccionarText('" + txtNivel.ClientID + "')", litScript);
    }

    private void EstadoControles(bool Estado)
    {
        txtCodigo.Enabled = !Estado;
        txtNivel.Enabled = Estado;
        btnGuardar.Enabled = Estado;
        btnCancelar.Enabled = Estado;
        btnRetirar.Enabled = Estado;
    }
    

    private void Guardar(string Codigo, string nivel)
    {
        if (!ValidaGuardar())
            return;

        Nivel nvl = new Nivel();

        nvl.Codigo = Codigo;
        nvl.nivel = nivel;

        string msg = "1";

        if (new ClinicaCES.Logica.LNivel().NivelActualizar(nvl))
        {
            //guardo            
        }
        else
        { 
            // no guardo
            msg = "3";
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        EstadoInicial();
    }

    private bool ValidaGuardar()
    {
        bool Valido = true;
        if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
        {
            txtCodigo.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtCodigo.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtNivel.Text.Trim()))
        {
            txtNivel.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtNivel.CssClass = "form_input";
        }

        return Valido;
    }

    protected void gvNivel_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkNivel = (LinkButton)e.Row.FindControl("lnkNivel");
            lnkNivel.CommandArgument = e.Row.RowIndex.ToString();
            //string caca = lnkComprobar.UniqueID;
            e.Row.Attributes.Add("onclick", "postNivelGrid(" + (e.Row.RowIndex + 2 ) + ")");
            //e.Row.Attributes.Add("onclick", "alert('oe')");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");
            

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }
    }

    protected void gvNivel_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Comprobar")
        { 
            GridViewRow row = gvNivel.Rows[int.Parse(e.CommandArgument.ToString())];
            Label lblNivel = (Label)row.FindControl("lblNivel");

            Comprobar(lblNivel.Text.Trim());
        }
    }
}
