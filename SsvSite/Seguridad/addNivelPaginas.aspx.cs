﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Seguridad_addNivelPaginas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Procedimientos.ValidarSession(this.Page);
        if (!IsPostBack)
        {
            ListarNivel();
            Procedimientos.Titulo("Asignar Paginas x nivel", this.Page);
        }
        litScript.Text = string.Empty;
    }

    private void ListarNivel()
    {
        Nivel nvl = new Nivel();

        DataTable dtNivel = new ClinicaCES.Logica.LNivel().NivelConsultar(nvl);

        Procedimientos.LlenarCombos(ddlNivel, dtNivel, "CODIGO", "NIVEL");
    }

    private void ConsultarMenu(string nivel)
    {
        Nivel nvl = new Nivel();
        nvl.Codigo = nivel;

        DataTable dtMenu = new ClinicaCES.Logica.LNivel().MenuConsultar(nvl);

        Procedimientos.LlenarGrid(dtMenu, gvPaginas);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        Guardar(xmlPages(), ddlNivel.SelectedValue);
    }

    private void Guardar(string xmlPaginas, string Nivel)
    { 
        string msg = "1";
        if(new ClinicaCES.Logica.LNivel().NivelXMenuActualizar(xmlPaginas,Nivel))
        {
            //Guardo
            ConsultarMenu(ddlNivel.SelectedValue);
        }
        else
        {
            msg = "3";
        }

        Procedimientos.Script("Mensaje(" + msg + "); location.href = 'addNivelPaginas.aspx'", litScript);
    }

    protected void ddlNivel_SelectedIndexChanged(object sender, EventArgs e)
    {
        ConsultarMenu(ddlNivel.SelectedValue);
    }

    private string xmlPages()
    {
        string xmlPagi = "<Raiz>";

        for (int i = 0; i < gvPaginas.Rows.Count; i++)
        {
            GridViewRow row = gvPaginas.Rows[i];
            CheckBox chkEstado = (CheckBox)row.FindControl("chkPages");
            if (chkEstado.Checked)
                xmlPagi += "<Datos MenuID = '" + gvPaginas.DataKeys[i]["ID_MENU"].ToString() + "' />";
        }
        return xmlPagi += "</Raiz>";
    }
    protected void gvPaginas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chkEstado = (CheckBox)e.Row.FindControl("chkPages");
            chkEstado.Checked = bool.Parse(gvPaginas.DataKeys[e.Row.RowIndex]["Estado"].ToString());
        }
    }
}
