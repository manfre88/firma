﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addUsuario.aspx.cs" Inherits="Seguridad_addUsuario" Title="Usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="center">
        <tr>
            <td align="right">Usuario:</td>
            <td colspan="2"><asp:TextBox  ID="txtUsuario" runat="server" CssClass="form_input" onblur="comprobarRed(this.value,this.id)" MaxLength="15"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtUsuario" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom"></ajaxToolkit:FilteredTextBoxExtender>
            <asp:LinkButton ID="lnkComprobar" runat="server" CausesValidation="false" onclick="Click_Botones"></asp:LinkButton>
            &nbsp;</td>
        </tr>
        <tr>
            <td align="right">Nombres: 
            <asp:Label ID="lblValidaNombres" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="2"><asp:TextBox ID="txtNombre" runat="server" Width="200px" CssClass="form_input" MaxLength="50" ></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNombre" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td align="right">Primer Apellido: 
            <asp:Label ID="lblValidaApellido" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="2">
            <asp:TextBox ID="txtApellido1" runat="server" CssClass="form_input" MaxLength="25" Width="100px" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtApellido1" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td align="right">Segundo Apellido:</td>                                    
            <td colspan="2">
                <asp:TextBox  ID="txtApellido2" runat="server" CssClass="form_input" MaxLength="25" Width="100px" ></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtApellido2" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td align="right">Clave:</td>
            <td colspan="2"><asp:TextBox ID="txtClave1" runat="server" CssClass="form_input" TextMode="Password" MaxLength="20" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">Repetir Clave:</td>
            <td colspan="2">
                <asp:TextBox ID="txtClave2" runat="server" CssClass="form_input" Width="200px" TextMode="Password" MaxLength="20"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">Nivel: 
            <asp:Label ID="lblValidaNivel" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="2"><asp:DropDownList ID="ddlNivel" runat="server"></asp:DropDownList>
            </td>
        </tr>       
        <tr>
            <td align="right">Telefono: 
            <asp:Label ID="lblValidaTelefono" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="2"><asp:TextBox ID="txtTelefono" runat="server" CssClass="form_input" MaxLength="25" Width="100px" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtTelefono" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>            
            </td>
        </tr> 
        <tr>
            <td align="right">Correo Electronico: 
            <asp:Label ID="lblValidaCorreo" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="2"><asp:TextBox ID="txtEmail" runat="server" CssClass="form_input" MaxLength="50" Width="200px"></asp:TextBox>            
            </td>
        </tr>
        <tr>
            <td align="right">Dirección:</td>
            <td colspan="2"><asp:TextBox  ID="txtDireccion" runat="server" CssClass="form_input" MaxLength="50" Width="200px" ></asp:TextBox>
            </td>
        </tr>
        <td align="right">Tipo Usuario: 
            <asp:Label ID="lblValidaCliente" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
        </td>
        <td><asp:DropDownList ID="ddlCliente" runat="server"></asp:DropDownList></td>
        <tr>
            <td align="right">Foto:</td>
            <td><asp:FileUpload ID="fuFoto" runat="server" CssClass="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" /></td>
        </tr>
        <tr>
            <td align="right">Esquema de colores:</td>
            <td><asp:DropDownList ID="ddlTema" runat="server"></asp:DropDownList></td>
        </tr>        
        <tr>
            <td colspan="4">
                <table align="center">
                    <tr>
                        <td><asp:Button ID="btnGuardar" runat="server" Text="Guardar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
                        <td><asp:Button ID="btnRetirar" runat="server" Text="Retirar" OnClientClick="return Mensaje(2)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                        <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
                    </tr>
                </table>
            </td>            
        </tr>
    </table>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
    <asp:HiddenField ID="hdnAccion" runat="server" />
</asp:Content>

