﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcUsuarios.aspx.cs" Inherits="Seguridad_srcUsuarios" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
            <td><asp:ImageButton Enabled="false" runat="server" ID="imgRetirar"  ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" /></td>
            <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" 
                    ImageUrl="../icons/cancel.png" onclick="Images_Click" style="width: 16px" /></td>
            <td><asp:ImageButton runat="server" ID="imgBuscar" ToolTip="Buscar" ImageUrl="../icons/magnify.png" onclick="Images_Click" /></td>
        </tr>     
    </table>
    <hr />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table align="center">
                <tr>
                    <td>Filtro:</td>
                    <td><asp:DropDownList onkeydown="tabular(event,this)"  ID="ddlFlitro" runat="server">
                        <asp:ListItem Value="USUARIO">Usuario</asp:ListItem>
                        <asp:ListItem Value="NombreC">Nombre</asp:ListItem>
                        </asp:DropDownList></td>    
                    <td><asp:LinkButton ID="lnkAgregar" runat="server" Text="Agregar" onclick="lnkAgregar_Click"></asp:LinkButton></td> 
                </tr>    
                <tr>
                    <asp:Panel ID="pnlUsuario" runat="server" Visible="false">
                        <td>Usuario:</td>
                        <td colspan="2"><asp:TextBox CssClass="form_input" ID="txtBusquedaUsuario" runat="server"></asp:TextBox>
                        <asp:LinkButton ID="lnkKitarUsuario" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                        </td>  
                    </asp:Panel>
                </tr>
                <tr>
                    <asp:Panel ID="pnlNombre" runat="server" Visible="false">
                        <td>Nombre:</td>
                        <td colspan="2"><asp:TextBox CssClass="form_input" ID="txtBusquedaNombre" runat="server"></asp:TextBox>
                        <asp:LinkButton ID="lnkKitarNombre" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                        </td>  
                    </asp:Panel>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table align="center">
        <tr>
            <td>
                <asp:GridView AutoGenerateColumns="false" ID="gvUsr" runat="server" AllowPaging="true"
                    CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" PageSize="20"
                    onrowdatabound="gvUsr_RowDataBound" 
                    onpageindexchanging="gvUsr_PageIndexChanging" >
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle CssClass="normalrow" />
                    <AlternatingRowStyle CssClass="alterrow" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" Wrap="false"  />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" Wrap="false" />
                    <Columns>           
                         <asp:BoundField DataField="USUARIO" HeaderText="Usuario" ItemStyle-Wrap="false" />                                                               
                         <asp:BoundField DataField="NOMBRES" HeaderText="Nombres" ItemStyle-Wrap="false"  />
                         <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" ItemStyle-Wrap="false"  />                        
                         <asp:BoundField DataField="TELEFONO" HeaderText="Telefono" ItemStyle-Wrap="false"  />                         
                         <asp:BoundField DataField="EMAIL" HeaderText="Email" ItemStyle-Wrap="false"  />                         
                    </Columns>
                </asp:GridView> 
            </td>
        </tr>
    </table>
</asp:Content>

