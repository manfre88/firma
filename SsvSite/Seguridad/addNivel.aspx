﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addNivel.aspx.cs" Inherits="Seguridad_addNivel" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="center">
        <tr>
            <td>Codigo:</td>
            <td colspan="2">
                <asp:TextBox onkeypress="tabular(event,this); event.returnValue=SoloNumeros(event)" 
                    Width="20px" CssClass="form_input" onblur="comprobarRed(this.value,this.id)" 
                    ID="txtCodigo" runat="server" MaxLength="2"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCodigo" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender>                                                             
            <asp:LinkButton ID="lnkComprobar" runat="server" CausesValidation="false" onclick="Click_Botones"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>Nivel:</td>
            <td colspan="2">
                <asp:TextBox Width="200px" onkeypress="tabular(event,this)"  CssClass="form_input" ID="txtNivel" runat="server" MaxLength="30"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNivel" FilterType="UppercaseLetters, LowercaseLetters" ></ajaxToolkit:FilteredTextBoxExtender>                                                             
            </td>
        </tr>
        <tr>
            <td align="center"><asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="return Mensaje(2)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
            <td align="center"><asp:Button OnClientClick="return Mensaje(2)" ID="btnRetirar" runat="server" Text="Retirar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
            <td align="center"><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td>
                <asp:GridView AutoGenerateColumns="false" ID="gvNivel" runat="server" 
                CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                    onrowcreated="gvNivel_RowCreated" onrowcommand="gvNivel_RowCommand">
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                    <PagerStyle CssClass="cabeza" ForeColor="White"  />
                    <RowStyle CssClass="normalrow" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle CssClass="alterrow" />
                    <Columns>        
                         <asp:TemplateField>
                             <ItemTemplate>
                                 <asp:LinkButton ID="lnkNivel" runat="server" CommandName="Comprobar"></asp:LinkButton>
                                 <asp:Label ID="lblNivel" runat="server" Text='<%# Bind("CODIGO") %>'></asp:Label>
                             </ItemTemplate>
                             <HeaderTemplate>
                                 Codigo
                             </HeaderTemplate>
                             <ItemStyle HorizontalAlign="Center" />
                         </asp:TemplateField>                     
                         <asp:BoundField DataField="NIVEL" HeaderText="Nivel de Acceso" />                                                        
                    </Columns>
                </asp:GridView> 
           </td>
        </tr>
    </table>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

