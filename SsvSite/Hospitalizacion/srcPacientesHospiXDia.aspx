﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PacientesHospiXDia.aspx.cs" Inherits="Hospitalizacion_PacientesHospiXDia" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcPacientesHospiXDia.aspx.cs" Inherits="Hospitalizacion_srcPacientesHospiXDia" Title="Busqueda pacientes por Hospitalizacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <table align="center" style="width: 328px">
        <tr>
            <td>
                Tipo Identificación:</td>
            <td>
                <asp:DropDownList ID="ddlTipoId" runat="server" Height="20px" Width="151px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Identificación:</td>
            <td>
                <asp:TextBox ID="txtId" runat="server" Width="143px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />
            </td>
        </tr>
    </table>
    
    <table align="center">
        <tr>
            <td align="center">
    <asp:Panel ID="pnlInforme" runat="server" Visible="false">
    
        <table border="1">
        </table>
        <table align="center">
            <tr>
                <td>
                    <asp:GridView AutoGenerateColumns="False" ID="gvInforme" runat="server" AllowPaging="True" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="20" pa
                        onrowdatabound="gvInforme_RowDataBound"
                        onpageindexchanging="gvInforme_PageIndexChanging" Width="551px" EnableModelValidation="True">                   
                        <FooterStyle BackColor="#F7DFB5" />
                        <RowStyle CssClass="normalrow"/>
                        <AlternatingRowStyle CssClass="alterrow" />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White"  />                
                        <Columns>      
                             <asp:BoundField DataField="REQUIERE_ANEXO3" HeaderText="REQUIERE_ANEXO" />
                             <asp:BoundField DataField="IDENTIFICACION" HeaderText="IDENTIFICACION" />
                             <asp:BoundField DataField="NOMBRE" HeaderText="PACIENTE" />   
                             <asp:BoundField DataField="CONVENIO" HeaderText="CONVENIO" />
                             <asp:BoundField DataField="FECHA_INGRESO" HeaderText="FECHA_INGRESO" />
                             <asp:BoundField DataField="ESTADIA" HeaderText="ESTADIA" />     
                             <asp:BoundField DataField="CAMA" HeaderText="CAMA"  />                      
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblDescripcion" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>