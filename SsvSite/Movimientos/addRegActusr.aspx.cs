﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_addRegActusr : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Registrar Actividades", this.Page);
            DateTime hoy = DateTime.Now;


            Semana(hoy.ToShortDateString());

            //SemanaAntNext(hoy.AddDays(-7).ToShortDateString(), hoy.AddDays(7).ToShortDateString());

            Procedimientos.ValidarSession(this.Page);
            Buscar(lblSemana.Text,Session["Nick"].ToString());
        }

        litScript.Text = string.Empty;
    }

    public int getWeek(DateTime date)
    {
        System.Globalization.CultureInfo cult_info = System.Globalization.CultureInfo.CreateSpecificCulture("no");
        System.Globalization.Calendar cal = cult_info.Calendar;
        int weekCount = cal.GetWeekOfYear(date, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek);
        return weekCount;
    }

    private void Semana(string fecha)
    {
        DateTime Fecha = Convert.ToDateTime(fecha);
        string year = Fecha.Year.ToString();
        lblSemana.Text = year + "-" + getWeek(Fecha).ToString();
    }

    //private void SemanaAntNext(string fechaAnt,string fechaNext)
    //{
    //    DateTime FechaAnt = Convert.ToDateTime(fechaAnt);
    //    DateTime FechaNext = Convert.ToDateTime(fechaNext);
    //    string yearAnt = FechaAnt.Year.ToString();
    //    string yearNext = FechaNext.Year.ToString();

    //    string script = "redirect('addRegActusr.aspx?week=" + yearAnt + "-" + getWeek(FechaAnt).ToString() + "')";
    //    string url = "addRegActusr.aspx?week=" + yearAnt + "-" + getWeek(FechaAnt).ToString() + "&fecha=" + fechaAnt;
    //    //string script = "redirect('addRegActusr.aspx?week=" + yearAnt + "-" + getWeek(FechaAnt).ToString() + "')";
    //    litAnterior.Text = "<a href=" + url + "><<- Anterior </a> ";
    //}

    private void Buscar(string semana, string usr)
    { 
        ActividadReg act = new ActividadReg();
        act.Semana = semana;
        act.Responsable = usr;

        DataTable dtActi = new ClinicaCES.Logica.LActividades().AtividadBuscar(act);

        if (dtActi.Rows.Count > 0)
        {
            Procedimientos.LlenarGrid(dtActi, gvRegistro);
            btnGuardar.Visible = true;
        }
        else
        {
            btnGuardar.Visible = false;
            gvRegistro.Visible = false;
        }

    }

    public string xmlActividades(ref bool valido)
    {
        string xml = "<Raiz>";
        double tinvertido; 
        double porcentaje;
        DateTime fechafin;
        string codigo;
        bool? finalizado;

        string tinver;
        string porc;
        string fecha;

        for (int i = 0; i < gvRegistro.Rows.Count; i++)
        {
            GridViewRow row = gvRegistro.Rows[i];
            TextBox txtTiempoInvertido = (TextBox)row.FindControl("txtTiempoInvertido");
            TextBox txtPercent = (TextBox)row.FindControl("txtPercent");
            TextBox txtFechaFin = (TextBox)row.FindControl("txtFechaFin");
            CheckBox chkFinalzida = (CheckBox)row.FindControl("chkFinalzida");

            if (!double.TryParse(txtTiempoInvertido.Text.Replace('.',','), out tinvertido) && !string.IsNullOrEmpty(txtTiempoInvertido.Text))
            {
                valido = false;
                txtTiempoInvertido.CssClass = "invalidtxt";
                break;
            }
            else
            {
                txtTiempoInvertido.Text = tinvertido.ToString();
                txtTiempoInvertido.CssClass = "form_input";
            }
                


            if (!double.TryParse(txtPercent.Text, out porcentaje) && !string.IsNullOrEmpty(txtPercent.Text))
            {
                valido = false;
                txtPercent.CssClass = "invalidtxt";
                break;
            }
            else
                txtPercent.CssClass = "form_input";

            if (!DateTime.TryParse(txtFechaFin.Text, out fechafin) && !string.IsNullOrEmpty(txtFechaFin.Text))
            {
                valido = false;
                txtFechaFin.CssClass = "invalidtxt";
                break;
            }
            else
                txtFechaFin.CssClass = "form_input";

            codigo = gvRegistro.DataKeys[i]["CODIGO"].ToString();
            finalizado = chkFinalzida.Checked;

            tinver = tinvertido.ToString();
            porc = porcentaje.ToString();
            fecha = fechafin.ToShortDateString();

            if (tinver == "0")
                tinver = string.Empty;

            if (porc == "0")
                porc = string.Empty;

            if (fecha == "01/01/0001")
                fecha = string.Empty;

            xml += "<Datos tinvertido='" + tinver + "' finalizado='" + finalizado + "' porcentaje='" + porc + "' fechafin='" + fecha + "' codigo='" + codigo + "' />";


        }

        return xml + "</Raiz>";
    }

    private void Guardar()
    {
        if (!ValidaGuardar())
            return;

        string msg = "3";
        bool valido = true;
        string xmlAct = xmlActividades(ref valido);

        if (valido)
        {
            if (new ClinicaCES.Logica.LActividades().ActividadActualizarUsr(xmlAct))            
                msg = "1";           
        }
        else
            msg = "20";

        Procedimientos.Script("msjini","Mensaje(" + msg + ")", this.Page);

        
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar();
    }
    protected void lnkSemana_Click(object sender, EventArgs e)
    {
        Semana(txtSemana.Text);
        Buscar(lblSemana.Text, Session["Nick"].ToString());
    }

    protected void gvRegistro_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='overrownopoint'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkFinalzida = (CheckBox)e.Row.FindControl("chkFinalzida");
            TextBox txtPercent = (TextBox)e.Row.FindControl("txtPercent");
            
            chkFinalzida.Attributes.Add("onclick", "checkActividad('" + chkFinalzida.ClientID + "','" + txtPercent.ClientID + "')");
            txtPercent.Attributes.Add("onkeypress", "return txtActividad('" + chkFinalzida.ClientID + "','" + txtPercent.ClientID + "')");
            txtPercent.Attributes.Add("onblur", "return txtActividad('" + chkFinalzida.ClientID + "','" + txtPercent.ClientID + "')");

        }
    }

    private bool ValidaGuardar()
    {
        bool valido = true;
        foreach (GridViewRow row in gvRegistro.Rows)
        {
            TextBox txtFechaFin = (TextBox)row.FindControl("txtFechaFin");
            txtFechaFin.CssClass = "form_input";
            string FechaFinalizacion = txtFechaFin.Text.Trim();
            DateTime FechaF;

            if (!string.IsNullOrEmpty(FechaFinalizacion))
            {
                if (DateTime.TryParse(FechaFinalizacion, out FechaF))
                {
                    valido = ValidaSemana(FechaF);                    
                }
                else
                {
                    valido = false;                    
                }                    
            }

            if (!valido)
            {
                txtFechaFin.CssClass = "invalidtxt";
                Procedimientos.Script("scripsininin","Mensaje(23)", this.Page);
            }
        }

        return valido;
    }

    private bool ValidaSemana(DateTime Fecha)
    {
        bool valido = true;

        string[] semanaPrimera = lblSemana.Text.Split('-');
        int anio = int.Parse(semanaPrimera[0]);
        int semana = int.Parse(semanaPrimera[1]);
        //DateTime Fecha = Convert.ToDateTime(txtFecha.Text.Trim());
        string year = Fecha.Year.ToString();
        if ((Fecha.Year < anio))
        {
            //la semana sleccionada en fecha es menor
            valido = false;
        }
        if ((Fecha.Year < anio) && (getWeek(Fecha) < semana))
        {
            //la semana sleccionada en fecha es menor
            valido = false;
        }
        else if ((Fecha.Year == anio) && (getWeek(Fecha) < semana))
        {
            //la semana sleccionada en fecha es menor
            valido = false;
        }

        return valido;
    }
}
