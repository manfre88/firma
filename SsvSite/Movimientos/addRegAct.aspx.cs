﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_addRegAct : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Asignar Actividades", this.Page);
            txtSemana.Text = DateTime.Now.ToShortDateString();
            Semana(txtSemana.Text);
            //txtSemana.Visible = false;
            ListarCombos();
            string script = "if(trim(document.getElementById('" + txtAsunto.ClientID + "').value) != '' ) {" +
                "__doPostBack('" + lnkAsunto.UniqueID + "','') }";

            string script2 = "if(trim(document.getElementById('" + txtFecha.ClientID + "').value) != '' ) {" +
                "__doPostBack('" + lnkValidaSemana.UniqueID + "','') }";

            txtAsunto.Attributes.Add("onblur", script);
            txtFecha.Attributes.Add("onblur", script2);

        }
        litScript.Text = string.Empty;
    }

    public int getWeek(DateTime date)
    {
        System.Globalization.CultureInfo cult_info = System.Globalization.CultureInfo.CreateSpecificCulture("no");
        System.Globalization.Calendar cal = cult_info.Calendar;
        int weekCount = cal.GetWeekOfYear(date, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek);
        return weekCount;
    }

    private void ListarCombos()
    {
        Procedimientos.LlenarCombos(ddlCliente, new ClinicaCES.Logica.LClientes().ClienteConsultar(), "NIT", "NOMCOMCIAL");
        Procedimientos.LlenarCombos(ddlLinea, new ClinicaCES.Logica.LActividades().LineaListar(), "LINEAID", "NOMBRE_LINEA");
        //Procedimientos.LlenarCombos(ddlAplicacion, new ClinicaCES.Logica.LAplicacion().AplicacionConsultar(), "CODIGO", "NOMBRE");
        Procedimientos.comboEstadoInicial(ddlAplicacion);
        Procedimientos.LlenarCombos(ddlResponsable, new ClinicaCES.Logica.LUsuarios().UsuarioVSConsultar(), "USUARIO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlActividad, new ClinicaCES.Logica.LActividades().ActividadListar(), "ACTIVIDADID", "ACTIVIDAD_NOMBRE");
    }

    protected void lnkSemana_Click(object sender, EventArgs e)
    {
        Semana(txtSemana.Text);
        Buscar(lblSemana.Text, ddlResponsable.SelectedValue);
        if (!string.IsNullOrEmpty(txtFecha.Text.Trim()))
            ValidaSemana();
        ValSem();
    }

    private void Semana(string fecha)
    {
        DateTime Fecha = Convert.ToDateTime(fecha);
        string year = Fecha.Year.ToString();
        lblSemana.Text = year + "-" + getWeek(Fecha).ToString();
        if (lblSemAct.Text == "")
        {
            lblSemAct.Text = lblSemana.Text;
        }
    }

    private void Guardar
    (
        string Semana,
        string Responsable,
        string Cliente,
        string Negocio,
        string Linea,
        string Aplicacion,
        string Programa,
        string CNA,
        string Asunto,
        string Actividad,
        bool? Planeada,
        string Fecha,
        string Tiempo, 
        string codigo,
        string Requerimiento
    )
    { 
        ActividadReg act = new ActividadReg();

        act.Semana = Semana;
        act.Responsable = Responsable;
        act.Cliente = Cliente;
        act.Negocio = Negocio;
        act.Linea = Linea;
        act.Aplicacion = Aplicacion;
        act.Programa = Programa;
        act.CNA = CNA;
        act.Asunto = Asunto;
        act.Actividad = Actividad;
        act.Planeada = Planeada;
        act.Fecha = Fecha;
        act.Tiempo = Tiempo;
        act.requerimiento = Requerimiento;  
        if (!string.IsNullOrEmpty(hdnCodigo.Value))
            act.Codigo = codigo;

        string msg = "3";

        if (new ClinicaCES.Logica.LActividades().ActividadActualizar(act))
        {
            msg = "1";
            Buscar(Semana, Responsable);
            Nuevo();
            if (string.IsNullOrEmpty(hdnCodigo.Value))
            {
                string email = new ClinicaCES.Logica.LUsuarios().Email(Responsable);
                if (Procedimientos.regularExpression(email, "^(([^<;>;()[\\]\\\\.,;:\\s@\\\"]+" + "(\\.[^<;>;()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@" + "((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}" + "\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+" + "[a-zA-Z]{2,}))$"))
                    EnviarCorreo(email, "Nueva actividad asignada: " + Programa, "Nueva actividad asignada");
                else
                    msg = "34";
            }
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            string msg = "17";
            if (ValidarGuardar(ref msg))
            {
                string asunto = null, req = null;
                if (ddlCNA.SelectedValue == "R")
                {
                    req = txtAsunto.Text.Trim();
                    asunto = null;
                }
                else if (ddlCNA.SelectedValue == "C")
                {
                    req = null;
                    asunto = txtAsunto.Text.Trim();
                }
                else
                {
                    req = null;
                    asunto = null;
                }

                string[] Cliente = ddlCliente.SelectedValue.Split('|');
                Guardar(lblSemana.Text, ddlResponsable.SelectedValue, Cliente[0], Cliente[1], ddlLinea.SelectedValue,
                    ddlAplicacion.SelectedValue, txtPrograma.Text.ToUpper().Trim(), ddlCNA.SelectedValue, asunto,
                    ddlActividad.SelectedValue, chkPLaneado.Checked, txtFecha.Text.Trim(), txtHoras.Text.Trim(), hdnCodigo.Value,req);
            }
            else
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
        else if (sender.Equals(btnNuevo))
        {
            Nuevo();
        }
        else if (sender.Equals(btnRetirar))
        {
            Retirar(hdnCodigo.Value);
        }
        else if (sender.Equals(btnCancelar))
            Response.Redirect("addRegAct.aspx");
    }

    private void Nuevo()
    {
        ddlCliente.SelectedIndex = 0;
        Procedimientos.comboEstadoInicial(ddlAplicacion);
        ddlLinea.SelectedIndex = 0;
        txtPrograma.Text = string.Empty;
        ddlCNA.SelectedIndex = 0;
        ddlActividad.SelectedIndex = 0;
        chkPLaneado.Checked = false;
        txtFecha.Text = string.Empty;
        txtHoras.Text = string.Empty;
        btnRetirar.Enabled = false;
        hdnCodigo.Value = string.Empty;
        pnlAsunto.Visible = false;
        txtAsunto.Text = string.Empty;
        ddlCNA.SelectedIndex = 0;
        lblSemAct.Text = string.Empty;
    }

    private void Buscar(string semana, string usuario)
    {
        ActividadReg act = new ActividadReg();

        act.Semana = semana;
        act.Responsable = usuario;

        DataTable dtReg = new ClinicaCES.Logica.LActividades().AtividadConsultar(act);

        if (dtReg.Rows.Count > 0)
            Procedimientos.LlenarGrid(dtReg, gvRegistro);        
        else
            gvRegistro.Visible = false;
    }


    protected void ddlCNA_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtAsunto.Text = string.Empty;
        if (ddlCNA.SelectedIndex != 0)
        {
            pnlAsunto.Visible = true;
            if (ddlCNA.SelectedValue == "C")
                lblTipo.Text = "Caso:";
            else
                lblTipo.Text = "Requerimiento:";
        }            
        else
            pnlAsunto.Visible = false;
    }


    private bool ValidarGuardar(ref string msg)
    {
        bool valido = true;

        System.Drawing.Color azul = System.Drawing.Color.Blue;
        System.Drawing.Color rojo = System.Drawing.Color.Red;

        if (ddlResponsable.SelectedIndex == 0)
        {
            valido = false;
            lblValidaRespnsable.ForeColor = rojo;
            lblValidaRespnsable.Font.Bold = true;
        }
        else
        {
            lblValidaRespnsable.Font.Bold = false;
            lblValidaRespnsable.ForeColor = azul;
        }
            

        if (ddlLinea.SelectedIndex == 0)
        {
            valido = false;
            lblValidaLinea.ForeColor = rojo;
            lblValidaLinea.Font.Bold = true;
        }
        else
        {
            lblValidaLinea.Font.Bold = false;
            lblValidaLinea.ForeColor = azul;
        }

        if (ddlActividad.SelectedIndex == 0)
        {
            valido = false;
            lblValidaActividad.ForeColor = rojo;
            lblValidaActividad.Font.Bold = true;
        }
        else
        {
            lblValidaActividad.Font.Bold = false;
            lblValidaActividad.ForeColor = azul;
        }

        if (ddlCliente.SelectedIndex == 0)
        {
            valido = false;
            lblValidaCliente.ForeColor = rojo;
            lblValidaCliente.Font.Bold = true;
        }
        else
        {
            lblValidaCliente.Font.Bold = false;
            lblValidaCliente.ForeColor = azul;
        }

        if (chkPLaneado.Checked && string.IsNullOrEmpty(txtHoras.Text.Trim()))
        {
            valido = false;
            lblValidaHoras.Font.Bold = true;
            lblValidaHoras.ForeColor = rojo;
        }
        else
        {
            lblValidaHoras.Font.Bold = false;
            lblValidaHoras.ForeColor = azul;
        }

        if (pnlAsunto.Visible && string.IsNullOrEmpty(txtAsunto.Text.Trim()))
        {
            valido = false;
            lblValidaAsunto.Font.Bold = true;
            lblValidaAsunto.ForeColor = rojo;
        }
        else
        {
            lblValidaAsunto.Font.Bold = false;
            lblValidaAsunto.ForeColor = azul;
        }

        double horas;
        if (!double.TryParse(txtHoras.Text.Replace('.', ','), out horas) && chkPLaneado.Checked)
        {
            valido = false;
            lblValidaHoras.Visible = true;
            lblValidaHoras.Font.Bold = true;
            lblValidaHoras.ForeColor = rojo;
            msg = "20";
        }
        else
        {
            if(!chkPLaneado.Checked)
                lblValidaHoras.Visible = false;
            else
                txtHoras.Text = horas.ToString();

            lblValidaHoras.Font.Bold = false;
            lblValidaHoras.ForeColor = azul;
            
        }

        return valido;
    }


    protected void chkPLaneado_CheckedChanged(object sender, EventArgs e)
    {
        Checkini();
    }

    private void Checkini()
    {
        if (chkPLaneado.Checked)
        {
            lblValidaHoras.Visible = true;
            txtHoras.ReadOnly = false;
        }
        else
        {
            lblValidaHoras.Visible = false;
            txtHoras.Text = string.Empty;
            txtHoras.ReadOnly = true;
        }
    }

    protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCliente.SelectedIndex != 0)
        {
            string[] cliente = ddlCliente.SelectedValue.Split('|');
            AppCliente(cliente[0], cliente[1]);
        }
        else
        {
            Procedimientos.comboEstadoInicial(ddlAplicacion);
        }
        
    }

    private void AppCliente(string nit, string neg)
    {
        AppXCli axc = new AppXCli();
        axc.Nit = nit;
        axc.Negocio = neg;

        DataTable dtApp = new ClinicaCES.Logica.LAppXCli().AppXCliBuscar(axc);

        Procedimientos.LlenarCombos(ddlAplicacion, dtApp, "IDAPLICACION", "IDAPLICACION");
    }

    //private bool ValidarAsunto(string codigo,string tipo)
    //{
    //    //return new ClinicaCES.Logica.LAsunto().AsuntoValidar(codigo,tipo);

    //}

    protected void lnkAsunto_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtAsunto.Text.Trim()))
        {
            DataSet dsAsu = new ClinicaCES.Logica.LAsunto().AsuntoValidar(txtAsunto.Text.Trim(), ddlCNA.SelectedValue);
            DataTable dtAsu = dsAsu.Tables[0];     
            
            //if (!ValidarAsunto(txtAsunto.Text.Trim(),ddlCNA.SelectedValue))
            if (dtAsu.Rows.Count < 1)
            {
                string msg = "18";
                if (ddlCNA.SelectedValue == "R")
                    msg = "61";

                Procedimientos.Script("Msg", "Mensaje(" + msg + ")", this.Page);
                txtAsunto.Text = string.Empty;
                lblValidaAsunto.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                DataRow row = dtAsu.Rows[0];
                lblValidaAsunto.ForeColor = System.Drawing.Color.Blue;
                ddlCliente.SelectedValue = row["IDCLIENTE"].ToString();
            }
        }

    }

    protected void lnkValidaSemana_Click(object sender, EventArgs e)
    {
        if (!ValidaSemana())
            Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
    }

    private bool ValidaSemana()
    {
        bool valido = true;        

        DateTime Fecha;
        if (DateTime.TryParse(txtFecha.Text.Trim(), out Fecha))
        {
            string[] semanaPrimera = lblSemana.Text.Split('-');
            int anio = int.Parse(semanaPrimera[0]);
            int semana = int.Parse(semanaPrimera[1]);
            //DateTime Fecha = Convert.ToDateTime(txtFecha.Text.Trim());
            string year = Fecha.Year.ToString();
            if ((Fecha.Year < anio))
            {
                //la semana seleccionada en fecha es menor
                valido = false;
            }
            if ((Fecha.Year < anio) && (getWeek(Fecha) < semana))
            {
                //la semana seleccionada en fecha es menor
                valido = false;
            }
            else if ((Fecha.Year == anio) && (getWeek(Fecha) < semana))
            {
                //la semana seleccionada en fecha es menor
                valido = false;
            }
        }
        else
        {
            //formato fecha incorrecto
            valido = false;            
        }

        if (!valido)
            txtFecha.Text = string.Empty;
        
        return valido;
    }

    protected void ddlResponsable_SelectedIndexChanged(object sender, EventArgs e)
    {
        Buscar(lblSemana.Text, ddlResponsable.SelectedValue);
        Nuevo();
    }

    protected void gvRegistro_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton imgEditar = (ImageButton)e.Row.FindControl("imgEditar");
            imgEditar.CommandArgument = e.Row.RowIndex.ToString();

            ImageButton imgEliminar = (ImageButton)e.Row.FindControl("imgEliminar");
            imgEliminar.CommandArgument = e.Row.RowIndex.ToString();
        }
    }


    protected void gvRegistro_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "editar")
        {
            GridViewRow row = gvRegistro.Rows[int.Parse(e.CommandArgument.ToString())];

            ddlResponsable.SelectedValue = gvRegistro.DataKeys[row.RowIndex]["RESPONSABLE"].ToString();
            lblSemana.Text = row.Cells[1].Text;
            ddlCliente.SelectedValue = gvRegistro.DataKeys[row.RowIndex]["CLIENTE"].ToString();
            string[] cliente = ddlCliente.SelectedValue.Split('|');
            AppCliente(cliente[0], cliente[1]);
            ddlLinea.SelectedValue = gvRegistro.DataKeys[row.RowIndex]["LINEA"].ToString();
            ddlAplicacion.SelectedValue = gvRegistro.DataKeys[row.RowIndex]["IDAPP"].ToString();
            txtPrograma.Text = row.Cells[5].Text.Replace("&nbsp;","");
            ddlCNA.SelectedValue = gvRegistro.DataKeys[row.RowIndex]["CNA"].ToString().Replace(" ","");
            ddlActividad.SelectedValue = gvRegistro.DataKeys[row.RowIndex]["ACTIVIDAD"].ToString();
            chkPLaneado.Checked = bool.Parse(gvRegistro.DataKeys[row.RowIndex]["PLANEADA"].ToString());
            Checkini();
            txtFecha.Text = row.Cells[6].Text.Replace("&nbsp;", "");
            txtHoras.Text = row.Cells[7].Text.Replace("&nbsp;", "");
            hdnCodigo.Value = gvRegistro.DataKeys[row.RowIndex]["CODIGO"].ToString();
            btnRetirar.Enabled = true;
            if (ddlCNA.SelectedValue == "C")
            {
                pnlAsunto.Visible = true;
                txtAsunto.Text = gvRegistro.DataKeys[row.RowIndex]["ASUNTO"].ToString();
            }
            else if (ddlCNA.SelectedValue == "R")
            {
                pnlAsunto.Visible = true;
                txtAsunto.Text = gvRegistro.DataKeys[row.RowIndex]["REQUERIMIENTO"].ToString();
            }
        }
        else if (e.CommandName == "eliminar")
        {
            GridViewRow row = gvRegistro.Rows[int.Parse(e.CommandArgument.ToString())];
            Retirar(gvRegistro.DataKeys[row.RowIndex]["CODIGO"].ToString());
        }
    }

    private void Retirar(string codigo)
    {
        string msg = "1";
        if (!new ClinicaCES.Logica.LActividades().RegActividadesRetirar(codigo))
        {
            msg = "3";
        }

        Procedimientos.Script("msef","Mensaje(" + msg + ")", this.Page);
        Buscar(lblSemana.Text, ddlResponsable.SelectedValue);
        Nuevo();
    }

    private void EnviarCorreo(string Para, string Mensaje, string Asunto)
    { 
        ClinicaCES.Correo.Correo objCorreo = new ClinicaCES.Correo.Correo();
        objCorreo.Asunto = Asunto;
        objCorreo.De = ConfigurationManager.AppSettings["mailSoporte"];
        objCorreo.Mensaje = Mensaje;
        objCorreo.Para = Para;
        if (!objCorreo.Enviar())
        {
            new ClinicaCES.Error.Error().EscibirError("Error enviando Email");
        }
            
    }

    private bool ValSem()
    {
        bool valido = true;

        DateTime Fecha;
        if (DateTime.TryParse(lblSemana.Text.Trim(), out Fecha))
        {
            string[] semanaPrimera = lblSemAct.Text.Split('-');
            int anio = int.Parse(semanaPrimera[0]);
            int semana = int.Parse(semanaPrimera[1]);
            string[] semanaSeleccionada = lblSemana.Text.Split('-');
            int ani = int.Parse(semanaSeleccionada[0]);
            int sem = int.Parse(semanaSeleccionada[1]);
            if ((ani < anio))
            {
                //la semana seleccionada en fecha es menor
                valido = false;
            }
            if ((ani < anio) && (sem < semana))
            {
                //la semana seleccionada en fecha es menor
                valido = false;
            }
            else if ((ani == anio) && (sem < semana))
            {
                //la semana seleccionada en fecha es menor
                valido = false;
            }
        }

        if (!valido)
        {
            Procedimientos.Script("mensajini", "Mensaje(62)", this.Page);
            lblSemana.Text = lblSemAct.Text;
        }

        return valido;
    }

}
