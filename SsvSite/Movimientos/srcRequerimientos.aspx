﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcRequerimientos.aspx.cs" Inherits="Movimientos_srcRequerimientos" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table>
    <tr>
        <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
        <td><asp:ImageButton runat="server" ID="imgBuscar" ToolTip="Buscar" 
                ImageUrl="../icons/magnify.png" onclick="Images_Click" Enabled="True" /></td>
        <td><asp:ImageButton runat="server" ID="imgActualizar" ToolTip="Actualizar" 
                ImageUrl="~/img/actualizar.gif" onclick="Images_Click" /></td>
        <td><asp:ImageButton runat="server" ID="imgRetirar"  
                ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" 
                onclientclick="return Mensaje(11)" Visible="False" /></td>
        <td style="width: 82px">&nbsp;</td>
        <td style="text-align: right; width: 77px">Registros:</td>
        <td>
            <asp:TextBox ID="txtNumReg" runat="server" Enabled="False" Width="42px"></asp:TextBox>
        </td>
    </tr>     
</table>
<hr />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <table align="center">
        <tr>
            <td>
                Filtro:</td>
            <td>
                <asp:DropDownList ID="ddlFlitro" runat="server" onkeydown="tabular(event,this)">
                    <asp:ListItem Value="IDREQUERIMIENTO">Requerimiento</asp:ListItem>
                    <asp:ListItem Value="NOMCOMCIAL">Comercializador</asp:ListItem>
                    <asp:ListItem Value="IDESTADO">Estado</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:LinkButton ID="lnkAgregar" runat="server" onclick="lnkAgregar_Click" 
                    Text="Agregar"></asp:LinkButton>
            </td>
        </tr>    
        <tr>
            <asp:Panel ID="pnlNombre" runat="server" Visible="false">
                <td>Comercializador:</td>
                <td colspan="2"><asp:TextBox onkeydown="tabular(event,this)"  CssClass="form_input" ID="txtBusquedaNombre" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkNombreKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlCodigo" runat="server" Visible="false">
                <td>Caso:</td>
                <td colspan="2"><asp:TextBox onkeydown="tabular(event,this)"  CssClass="form_input" ID="txtBusquedaCod" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkCodigoKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlEstado" runat="server" Visible="false">
                <td>Estado:</td>
                <td colspan="2"><asp:DropDownList ID="ddlBusquedaEstado" runat="server"></asp:DropDownList>
                <asp:LinkButton ID="lnkEstadoKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>

<table align="center">
    <tr>
        <td><asp:ImageButton ID="imgExcel" runat="server" ImageUrl="../icons/icon_excel.gif" onclick="Excel_Click" /></td>
    </tr>
    <tr>
        <td align="center">                
            <asp:GridView AutoGenerateColumns="False" ID="gvReq" runat="server" AllowPaging="True" 
            CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="15" pa
                onrowdatabound="gvReq_RowDataBound" DataKeyNames="DESCESTADO"
                onpageindexchanging="gvReq_PageIndexChanging" Width="900px">                   
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <RowStyle CssClass="normalrow" />
                <AlternatingRowStyle CssClass="alterrow" />
                <PagerStyle CssClass="cabeza" ForeColor="White"  />
                <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                <Columns>     
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkEliminar" runat="server" />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <input type="checkbox" onclick="CheckAllCheckBoxes(this.checked); HabilitarBotonBorrarCheck('<%= imgRetirar.ClientID %>',this.checked)" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>   
                     <asp:BoundField DataField="Orden" HeaderText="#" />
                     <asp:BoundField DataField="FECHA" HeaderText="Fecha" />
                     <asp:BoundField DataField="IDAPLICACION" HeaderText="Aplicacion" />
                     <asp:BoundField DataField="NOMCOMCIAL" HeaderText="Comercializador" />                     
                     <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" ><ItemStyle CssClass="wrapword" /></asp:BoundField>                  
                    <asp:BoundField DataField="DESCESTADO" HeaderText="Estado" />
                    <asp:BoundField DataField="ENCARGADO" HeaderText="Responsable" />
                    <asp:BoundField DataField="IDASIGNADO" HeaderText="Asignado" />
                </Columns>
            </asp:GridView> 
            
            
            &nbsp;</td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

