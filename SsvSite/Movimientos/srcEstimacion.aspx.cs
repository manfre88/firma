﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_srcEstimacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Estimación de proyectos", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Consultar()
    {
        Estimacion_Proyectos are = new Estimacion_Proyectos();
        DataTable dtProyectos = new ClinicaCES.Logica.LEstimacion_Proyectos().ProyectoConsultar(are).Tables[0];
        ViewState["dtProyectos"] = dtProyectos;
        ViewState["dtPaginas"] = dtProyectos;
        Procedimientos.LlenarGrid(dtProyectos, gvProyectos);
    }

    protected void gvProyectos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvProyectos.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addEstimacion.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");                
            }

            e.Row.Cells[5].Text = e.Row.Cells[5].Text.Replace("True", "Activo").Replace("False", "Inactivo");

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvProyectos.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        //string msg = "3";
        //if (new ClinicaCES.Logica.LEstimacion_Proyectos().ProyectosRetirar(xmlPais, Usuario))
        //{
        //    msg = "7";

        //    Proyectos are = new Proyectos();
        //    DataTable dtProyectos = new ClinicaCES.Logica.LProyectos().ProyectosConsultar(are).Tables[0];
        //    ViewState["dtProyectos"] = dtProyectos;
        //    Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        //}

        //Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlArea()
    {
        string cod;
        string xmlArea = "<Raiz>";

        for (int i = 0; i < gvProyectos.Rows.Count; i++)
        {
            GridViewRow row = gvProyectos.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                cod = row.Cells[1].Text.Trim();
                xmlArea += "<Datos COMPLE='" + cod + "' />";
            }
        }

        return xmlArea += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND PROYECTO LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtProyectos = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtProyectos"]);
        Procedimientos.LlenarGrid(dtProyectos, gvProyectos);

        ViewState["dtPaginas"] = dtProyectos;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    protected void gvProyectos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProyectos.PageIndex = e.NewPageIndex;
        gvProyectos.DataSource = ViewState["dtPaginas"];
        gvProyectos.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlArea(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addEstimacion.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCRIPCION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
