﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addRegActusr.aspx.cs" Inherits="Movimientos_addRegActusr" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table align="center">
        <tr>            
            <td style="white-space:nowrap">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>   
                        Semana:                  
                        <asp:Label ID="lblSemana" runat="server"></asp:Label>
                        <asp:TextBox Width="1" style="visibility:hidden" Height="1"  ID="txtSemana" runat="server" CssClass="form_input"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy" ID="clnFecha"
                            runat="server" TargetControlID="txtSemana" PopupButtonID="imgCalendar" 
                            OnClientDateSelectionChanged="Semana"></ajaxToolkit:CalendarExtender>
                            <img style="cursor:pointer" src="../icons/calendar.png" runat="server" id="imgCalendar" />  
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="lnkSemana" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>                                    
                <asp:LinkButton ID="lnkSemana" runat="server" onclick="lnkSemana_Click"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upnlGrid" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <asp:GridView AutoGenerateColumns="False" ID="gvRegistro" runat="server"
                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="750px" 
                            DataKeyNames="CODIGO" onrowdatabound="gvRegistro_RowDataBound">                   
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <RowStyle CssClass="normalrow" />
                            <AlternatingRowStyle CssClass="alterrow" />
                            <PagerStyle CssClass="cabeza" ForeColor="White"  />
                            <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                            <Columns>     
                                 <asp:BoundField DataField="APLICACION" HeaderText="Aplicación" />
                                 <asp:BoundField DataField="PROGRAMA" HeaderText="Programa" />
                                 <asp:BoundField DataField="ACTIVIDAD" HeaderText="Actividad" />
                                 <asp:BoundField DataField="FECHA" HeaderText="Fecha" />
                                 <asp:BoundField DataField="TIEMPO" HeaderText="Tiempo" />
                                 
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                                        Tiempo Invertido
                                    </HeaderTemplate>
                                    <HeaderStyle Wrap="false" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtTiempoInvertido" Text='<%# Bind("TINVERTIDO") %>' Width="20px" runat="server" CssClass="form_input"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtTiempoInvertido" FilterType="Custom, Numbers" ValidChars="-,."></ajaxToolkit:FilteredTextBoxExtender>                                         
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                 </asp:TemplateField>
                                                         
                                <asp:TemplateField>                            
                                    <HeaderTemplate>
                                        Finalizada
                                    </HeaderTemplate>
                                    <HeaderStyle Wrap="false" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkFinalzida" Checked='<%# Bind("FINALIZADO") %>' runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>  
                                 
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        %
                                    </HeaderTemplate>
                                    <HeaderStyle Wrap="false" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtPercent" Text='<%# Bind("PORCENTAJE") %>' Width="20px" runat="server" CssClass="form_input"></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                 </asp:TemplateField>
                                
                                
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Fecha Fin
                                    </HeaderTemplate>
                                    <HeaderStyle Wrap="false" />
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox onkeyup="mascara(this,'/',true)" ID="txtFechaFin" Text='<%# Bind("FECHA_FIN") %>'  Width="60px" runat="server" CssClass="form_input"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFechaFin" FilterType="Custom, Numbers" ValidChars="/" />        
                                                    <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy" ID="CalendarExtender1" runat="server" TargetControlID="txtFechaFin" PopupButtonID="img1"></ajaxToolkit:CalendarExtender>                                               
                                                </td>
                                                <td>                                                    
                                                    <img style="cursor:pointer" src="../icons/calendar.png" runat="server" id="img1" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                 </asp:TemplateField>                        
                            </Columns>
                        </asp:GridView> 
                    </ContentTemplate>
                    <Triggers>                        
                        <asp:AsyncPostBackTrigger ControlID="lnkSemana" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnGuardar" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
                
            </td>
        </tr>
    </table>
    <table align="center">
    <tr>
        <td>
            <table align="center">
                <tr>                                           
                    <td>
                         <asp:UpdatePanel ID="UpdatePan" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" />
                            </ContentTemplate>
                            <Triggers>                        
                                <asp:AsyncPostBackTrigger ControlID="lnkSemana" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>                                                                                                   
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

