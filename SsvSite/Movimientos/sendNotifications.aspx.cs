﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Movimientos_sendNotifications : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            CargarSemana();
    }

    private void CargarSemana()
    {
        DataTable dtCorreosCargados = new ClinicaCES.DA.DA().getDataTable("uspRegistroActividadesCargarSemanaAnterior");

        if (dtCorreosCargados != null)
        {
            if (dtCorreosCargados.Rows.Count >0)
            { 
                //enviar notificacion a cada usuario, informando q sus tareas fueron asigandas en la semana actual
                foreach (DataRow item in dtCorreosCargados.Rows)
                {
                    Procedimientos.EnviarCorreo(item["EMAIL"].ToString(), "Sus actividades pendientes de la semana anterior fueron cargadas con exito a la semana actual", "Actividades Cargadas");
                }
            }
        }
        else
        { 
            //error cargando las semanas enviar correo informando
            //Procedimientos.EnviarCorreo(, "Sus actividades pendientes de la semana anterior fueron cargadas con exito a la semana actual", "Actividades Cargadas");
        }

        Response.Write("<script>window.close()</script>");
    }
}
