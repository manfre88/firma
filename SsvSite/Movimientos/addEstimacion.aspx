﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addEstimacion.aspx.cs" Inherits="Movimientos_addEstimacion" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<table align="center">
    <tr>
        <td align="right">Codigo: </td>
        <td><asp:TextBox ID="txtCodigo" runat="server" Width="50px" CssClass="form_input"></asp:TextBox>
        <asp:LinkButton ID="lnkComprobar" runat="server"
                onclick="lnkComprobar_Click"></asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td align="right">Proyecto: <asp:Label ID="lblValidaProyecto" runat="server" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td><asp:TextBox ID="txtProyecto" runat="server" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="boxProyect" runat="server" TargetControlID="txtProyecto" FilterType="UppercaseLetters, LowercaseLetters, Custom" ValidChars=" " ></ajaxToolkit:FilteredTextBoxExtender>        
        </td>
    </tr>
    <tr>
        <td align="right">Cliente: <asp:Label ID="lblValidaCliente" runat="server" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td><asp:DropDownList ID="ddlCliente" runat="server"></asp:DropDownList></td>        
    </tr>
    <tr>
        <td align="right">Gerente: <asp:Label ID="lblValidaGerente" runat="server" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td><asp:DropDownList ID="ddlGerente" runat="server"></asp:DropDownList></td>
    </tr>
</table>
<asp:Panel ID="pnlContainer" runat="server" Visible="false">
<ajaxToolkit:TabContainer ID="tbc" runat="server" ActiveTabIndex="1">
    <ajaxToolkit:TabPanel ID="tpnCUA" HeaderText="Casos de Uso Ajustados" runat="server" TabIndex="0">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:ImageButton ID="imgExcelUsos" runat="server" ImageUrl="../icons/icon_excel.gif" onclick="Excel_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView AutoGenerateColumns="False" ID="gvCasoUso" runat="server" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                            onrowdatabound="gvCasoUso_RowDataBound" DataKeyNames="TIPOID,CODIGO" 
                            onrowcreated="gvCasoUso_RowCreated" onrowcommand="gvCasoUso_RowCommand"  >                   
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle CssClass="alterrow" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>                                                    
                                                    <asp:ImageButton OnClientClick="return Mensaje(11)" CommandName="eliminar" ToolTip="Eliminar Caso de Uso"  Visible="false" ImageUrl="../icons/eliminar.gif" runat="server" ID="imgEliminarCaso" />
                                                </td>
                                                <td>                                                
                                                    <asp:ImageButton CommandName="editar" ToolTip="Editar Caso de Uso" ImageUrl="../icons/pencil.png"  Visible="false" runat="server" ID="imgEditarCaso" />
                                                </td>
                                                <td>                                                
                                                    <asp:ImageButton CommandName="guardar" ToolTip="Actualizar Caso de uso" Visible="false" ImageUrl="../icons/16_save.png" runat="server" ID="imgGuardarCaso" />
                                                </td>
                                                <td>                                                
                                                    <asp:ImageButton OnClientClick="return Mensaje(25)" CommandName="cancelar" ToolTip="Cancelar" Visible="false" ImageUrl="../icons/cancel.png" runat="server" ID="imgCancelarUso" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                 </asp:TemplateField>  
                                <asp:BoundField HeaderText="#" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="lblValidaTema" runat="server" ForeColor="Blue" Text="(*)" 
                                            Visible="false"></asp:Label>
                                        <asp:TextBox ID="txtTema" runat="server" CssClass="form_input" Visible="false"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="boxTema" runat="server" TargetControlID="txtTema" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:Label ID="lblTema" runat="server" Text='<%# Bind("TEMA") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Tema
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtSolicitud" runat="server" CssClass="form_input" Visible="false"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="boxSolicitud" runat="server" TargetControlID="txtSolicitud" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:Label ID="lblSolicitud" runat="server" Text='<%# Bind("SOLICITUD") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Solicitud
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="lblValidaFuncionalidad" runat="server" ForeColor="Blue" Text="(*)" Visible="false"></asp:Label>
                                        <asp:TextBox ID="txtFuncionalidad" runat="server" CssClass="form_input" Visible="false"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="boxFuncionalidad" runat="server" TargetControlID="txtFuncionalidad" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:Label ID="lblFuncionalidad" runat="server" Text='<%# Bind("FUNCIONALIDAD") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Funcionalidad
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlTipo" runat="server" Visible="false">
                                            <asp:ListItem Text="SIMPLE" Value="S"></asp:ListItem>
                                            <asp:ListItem Text="MEDIO" Value="M"></asp:ListItem>
                                            <asp:ListItem Text="COMPLEJO" Value="C"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("TIPO") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Tipo
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                            <PagerStyle CssClass="cabeza" ForeColor="White"  />
                            <RowStyle CssClass="normalrow" />
            </asp:GridView> 
            <asp:HiddenField ID="hdnCodigo" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td><asp:Button ID="btnAgregarCU" runat="server" Text="Agregar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" />
                    </td>                                        
                </tr>
            </table>
        </ContentTemplate>
    </ajaxToolkit:TabPanel>
    
    <ajaxToolkit:TabPanel ID="tpnActores" HeaderText="Actores" runat="server" TabIndex="1">
        <ContentTemplate>
            <table align="center">
                 <tr>
                    <td>
                        <asp:ImageButton ID="imgExportarActores" runat="server" ImageUrl="../icons/icon_excel.gif" onclick="Excel_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView AutoGenerateColumns="False" ID="gvActores" runat="server" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                            DataKeyNames="CODIGO" >                                               
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle CssClass="alterrow" />
                            <Columns>
                                <asp:BoundField DataField="NOMBRE" HeaderText="Tipo" />
                                <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción">
                                    <ItemStyle CssClass="wrapword" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValor" runat="server" CssClass="form_input" MaxLength="1" 
                                            Text='<%# Bind("Valor") %>' Width="20px"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="boxValor" runat="server" 
                                            FilterType="Numbers" TargetControlID="txtValor">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Valor
                                    </HeaderTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                            <PagerStyle CssClass="cabeza" ForeColor="White"  />
                            <RowStyle CssClass="normalrow" />
                        </asp:GridView> 
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </ajaxToolkit:TabPanel>
    
    <ajaxToolkit:TabPanel ID="TabPanel1" HeaderText="F. Complejidad Técnica" runat="server" TabIndex="2">
        <ContentTemplate>
            <table align="center">
             <tr>
                    <td>
                        <asp:ImageButton ID="imgExportarComplejidad" runat="server" ImageUrl="../icons/icon_excel.gif" onclick="Excel_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView AutoGenerateColumns="False" ID="gvComplejidadTecnica" runat="server" AllowPaging="false" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">                   
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle CssClass="alterrow" />
                            <Columns>     
                                <asp:BoundField HeaderText = "Codigo" DataField="CODIGO" />
                                <asp:BoundField HeaderText = "Nombre" DataField="NOMBRE"  ><ItemStyle CssClass="wrapword" /></asp:BoundField>
                                <asp:BoundField HeaderText = "Descripción" DataField="DESCRIPCION" ItemStyle-Wrap="true" ><ItemStyle CssClass="wrapword" /></asp:BoundField>                               
                                <asp:BoundField HeaderText = "Peso" DataField="PESO"/>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValor" MaxLength="1" Text='<%# Bind("Valor") %>' Width="20px" runat="server" CssClass ="form_input"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="boxValor2" runat="server" TargetControlID="txtValor" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender>                                         
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Valor
                                    </HeaderTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                            <PagerStyle CssClass="cabeza" ForeColor="White"  />
                            <RowStyle CssClass="normalrow" />
                        </asp:GridView> 
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </ajaxToolkit:TabPanel>
    
    <ajaxToolkit:TabPanel ID="TabPanel2" HeaderText="Factores de Entorno" runat="server" TabIndex="3">
        <HeaderTemplate>
            Factores de Entorno
        </HeaderTemplate>
        <ContentTemplate>
            <table align="center">
                <tr>
                    <td>
                        <asp:ImageButton ID="imgExportarFactores" runat="server" ImageUrl="../icons/icon_excel.gif" onclick="Excel_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView AutoGenerateColumns="False" ID="gvFactores" runat="server" 
                        CellPadding="4" ForeColor="#333333" GridLines="None">
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle CssClass="alterrow" />
                            <Columns>     
                                <asp:BoundField HeaderText = "Codigo" DataField="CODIGO" />
                                <asp:BoundField HeaderText = "Nombre" DataField="NOMBRE" ><ItemStyle CssClass="wrapword" /></asp:BoundField>
                                <asp:BoundField HeaderText = "Descripción" DataField="DESCRIPCION" ><ItemStyle CssClass="wrapword" /></asp:BoundField>
                                <asp:BoundField HeaderText = "Peso" DataField="PESO"/>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValor" MaxLength="1" Text='<%# Bind("Valor") %>' Width="20px" runat="server" CssClass ="form_input"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="boxValor3" runat="server" TargetControlID="txtValor" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender>                                         
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Valor
                                    </HeaderTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                            <PagerStyle CssClass="cabeza" ForeColor="White"  />
                            <RowStyle CssClass="normalrow" />                            
                        </asp:GridView> 
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </ajaxToolkit:TabPanel>
    <ajaxToolkit:TabPanel ID="tbEstimacion" runat="server" HeaderText="Estimación">
        <ContentTemplate>
            <table align="center">
                <tr>
                    <td>
                        <asp:ImageButton ID="imgExportarEstimacion" runat="server" ImageUrl="../icons/icon_excel.gif" onclick="Excel_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:GridView AutoGenerateColumns="False" ID="gvEstimacion" runat="server" AllowPaging="false" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" 
                        DataKeyNames="CODIGO" onrowdatabound="gvEstimacion_RowDataBound">
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle CssClass="alterrow" />
                            <Columns>     
                                <asp:BoundField HeaderText = "Actividad" DataField="DESCRIPCION" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText = "Porcentaje" DataField="PORCENTAJE" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText = "Horas - Hombre" DataField="HORAS" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText = "Vlr Hora" DataField="VALOR" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="boxCant" runat="server" TargetControlID="txtRecursos" FilterType="Numbers" ></ajaxToolkit:FilteredTextBoxExtender> 
                                        <asp:TextBox Text='<%# Bind("CANTIDAD") %>' ID="txtRecursos" Width="20px" runat="server" CssClass="form_input" MaxLength="2"></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Nro de Recursos
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText = "Costo" DataField="TOTAL" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                            <PagerStyle CssClass="cabeza" ForeColor="White"  />
                            <RowStyle CssClass="normalrow" />                            
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            
            <table align="center" style="width: 358px">
                <td align="center" style="font-weight: 700">TOTAL ESFUERZO Y COSTO </td>
            </tr>
            </table>
            <table align="center">
                <tr>
                    <td align="right">Esfuerzo: </td>
                    <td><asp:TextBox ID="txtEsfuerzo" runat="server" Width="78px" Enabled="False" CssClass="form_input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">Costo: </td>
                    <td><asp:TextBox ID="txtCostos" runat="server" Width="78px" Enabled="False" CssClass="form_input"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        
    </ajaxToolkit:TabPanel>
    
</ajaxToolkit:TabContainer>
</asp:Panel>
        
<table align="center">
    <tr>                       
        <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" 
                onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                CssClass="btn" onclick="Click_Botones" /></td>
        <td><asp:Button Visible="false" ID="btnReactivar" runat="server" Text="Reactivar" 
                onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                CssClass="btn" onclick="Click_Botones"   /></td>
        <td><asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" 
                OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" 
                onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
        <td><asp:Button ID="btnGuardar" runat="server" Text="Guardar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>                                                                                
        <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" 
                onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                CssClass="btn" onclick="Click_Botones" TabIndex="12"  /></td>                     
        <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcEstimacion.aspx')" /> </td>                     
    </tr>                    
</table>
  
<asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

