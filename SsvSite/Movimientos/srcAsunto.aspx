﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcAsunto.aspx.cs" Inherits="Movimientos_srcAsunto" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table>
    <tr>
        <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
        <td><asp:ImageButton runat="server" ID="imgBuscar" ToolTip="Buscar" 
                ImageUrl="../icons/magnify.png" onclick="Images_Click" Enabled="True" /></td>
        <td><asp:ImageButton runat="server" ID="imgActualizar" ToolTip="Actualizar" 
                ImageUrl="~/img/actualizar.gif" onclick="Images_Click" /></td>
        <td><asp:ImageButton runat="server" ID="imgRetirar"  
                ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" 
                onclientclick="return Mensaje(11)" Visible="False" /></td>
        <td style="width: 82px">&nbsp;</td>
        <td style="text-align: right; width: 77px">Registros:</td>
        <td>
            <asp:TextBox ID="txtNumReg" runat="server" Enabled="False" Width="42px"></asp:TextBox>
        </td>
    </tr>     
    
</table>
<hr />
<asp:Panel ID="pnlBusqda" runat="server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <table align="center">
        <tr>        
            <td>Filtro:</td>
            <td><asp:DropDownList onkeydown="tabular(event,this)"  ID="ddlFlitro" runat="server">
                <asp:ListItem Value="IDASUNTO">Caso</asp:ListItem>
                <asp:ListItem Value="NOMCOMCIAL">Comercializador</asp:ListItem>
                <asp:ListItem Value="IDESTADO">Estado</asp:ListItem>
                <asp:ListItem Value="IDPRIORIDAD">Prioridad</asp:ListItem>                
                <asp:ListItem Value="IDASIGNADO">Asignado</asp:ListItem>                
                </asp:DropDownList></td>    
            <td><asp:LinkButton ID="lnkAgregar" runat="server" Text="Agregar" onclick="lnkAgregar_Click"></asp:LinkButton></td> 
        </tr>    
        <tr>
            <asp:Panel ID="pnlNombre" runat="server" Visible="false">
                <td>Comercializador:</td>
                <td colspan="2"><asp:TextBox  CssClass="form_input" ID="txtBusquedaNombre" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkNombreKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlCodigo" runat="server" Visible="false">
                <td>Caso:</td>
                <td colspan="2"><asp:TextBox  CssClass="form_input" ID="txtBusquedaCod" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkCodigoKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlEstado" runat="server" Visible="false">
                <td>Estado:</td>
                <td colspan="2"><asp:DropDownList ID="ddlBusquedaEstado" runat="server"></asp:DropDownList>
                <asp:LinkButton ID="lnkEstadoKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlPrioridad" runat="server" Visible="false">
                <td>Prioridad:</td>
                <td colspan="2"><asp:DropDownList ID="ddlBusquedaPrioridad" runat="server"></asp:DropDownList>
                <asp:LinkButton ID="lnkPrioridadKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlAsignado" runat="server" Visible="false">
                <td>Asignado:</td>
                <td colspan="2"><asp:DropDownList ID="ddlBusquedaAsignado" runat="server"></asp:DropDownList>
                <asp:LinkButton ID="lnkAsignadoKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>

<table align="center">
    <tr>
        <td><asp:ImageButton ID="imgExcel" runat="server" 
                ImageUrl="../icons/icon_excel.gif" onclick="Excel_Click" style="width: 16px" /></td>
    </tr>
    <tr>
        <td align="center">                
            <asp:GridView AutoGenerateColumns="False" ID="gvAsunto" runat="server" AllowPaging="True" 
            CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="15" pa
                onrowdatabound="gvAsunto_RowDataBound" DataKeyNames="DESCESTADO"
                onpageindexchanging="gvAsunto_PageIndexChanging" Width="900px">                   
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <RowStyle CssClass="normalrow" />
                <AlternatingRowStyle CssClass="alterrow" />
                <PagerStyle CssClass="cabeza" ForeColor="White"  />
                <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                <Columns>     
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkEliminar" runat="server" />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <input type="checkbox" onclick="CheckAllCheckBoxes(this.checked); HabilitarBotonBorrarCheck('<%= imgRetirar.ClientID %>',this.checked)" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>   
                     <asp:BoundField DataField="Orden" HeaderText="#" />
                     <asp:BoundField DataField="FECHA" HeaderText="Fecha" />
                     <asp:BoundField DataField="IDAPLICACION" HeaderText="Aplicacion" />
                     <asp:BoundField DataField="NOMCOMCIAL" HeaderText="Comercializador" ><ItemStyle CssClass="wrapword" /></asp:BoundField>                    
                     <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" ><ItemStyle CssClass="wrapword" /></asp:BoundField>                  
                    <asp:BoundField DataField="DESCESTADO" HeaderText="Estado" />
                    <asp:BoundField DataField="ENCARGADO" HeaderText="Responsable" />
                    <asp:BoundField DataField="ASIGNADO" HeaderText="Asignado" ><ItemStyle CssClass="wrapword" /></asp:BoundField>
                </Columns>
            </asp:GridView> 
            
            
            &nbsp;</td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

