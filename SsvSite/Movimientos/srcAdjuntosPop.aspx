﻿<%@ Page Language="C#" MasterPageFile="~/Masters/MasterPop.master" AutoEventWireup="true" CodeFile="srcAdjuntosPop.aspx.cs" Inherits="Movimientos_srcAdjuntosPop" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table align="center">
    <tr>
        <td>
            <asp:GridView AutoGenerateColumns="False" ID="gvAdjuntos" runat="server"
            CellPadding="4" ForeColor="#333333" GridLines="None"
                PageSize="15" onrowdatabound="gvAdjuntos_RowDataBound" >                   
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <RowStyle CssClass="normalrow" />
                <AlternatingRowStyle CssClass="alterrow" />
                <PagerStyle CssClass="cabeza" ForeColor="White"  />
                <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                <Columns>                                                            
                    <asp:BoundField DataField="ARCHIVO" HeaderText="Archivos Adjuntos"  />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Content>

