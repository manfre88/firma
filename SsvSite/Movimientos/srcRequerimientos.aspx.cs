﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_srcRequerimientos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Requerimientos", this.Page);
            Pendientes();
            Estados est = new Estados();
            Procedimientos.LlenarCombos(ddlBusquedaEstado, new ClinicaCES.Logica.LEstados().EstadoConsultar(est), "CODIGO", "DESCESTADO");
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Pendientes()
    {
        DataSet dsReq = new ClinicaCES.Logica.LRequerimientos().ConsultarPendientes(null, null);
        DataTable dtReq = dsReq.Tables[0];
        DataTable dtPagina = Procedimientos.dtFiltrado("Orden", "IDESTADO = 'PE'", dtReq);
        ViewState["dtReq"] = dtReq;
        ViewState["dtPaginas"] = dtPagina;

        Procedimientos.LlenarGrid(dtPagina, gvReq);
        txtNumReg.Text = Convert.ToString(dtPagina.Rows.Count);
    }

    private void Consultar()
    {
        DataSet dsReq = new ClinicaCES.Logica.LRequerimientos().ReqConsultar(null);
        DataTable dtReq = dsReq.Tables[0];
        ViewState["dtReq"] = dtReq;
        ViewState["dtPaginas"] = dtReq;
        Procedimientos.LlenarGrid(dtReq, gvReq);
        txtNumReg.Text = Convert.ToString(dtReq.Rows.Count);
    }

    protected void gvReq_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvReq.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addRequerimientos.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }
    }

    private string xmlcli()
    {
        string cli;
        string tipo;
        string xmlPais = "<Raiz>";

        for (int i = 0; i < gvReq.Rows.Count; i++)
        {
            GridViewRow row = gvReq.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                cli = row.Cells[1].Text.Trim();
                tipo = gvReq.DataKeys[i]["TIPO"].ToString();
                xmlPais += "<Datos CLIENTE='" + cli + "' TIPO='" + tipo + "' />";
            }
        }

        return xmlPais += "</Raiz>";
    }

    private void Filtrar()
    {
        string filtro = "IDREQUERIMIENTO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND NOMCOMCIAL LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        if (pnlEstado.Visible && ddlBusquedaEstado.SelectedIndex != 0)
            filtro += " AND IDESTADO = '" + ddlBusquedaEstado.SelectedValue + "'";
        DataTable dtReq = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtReq"]);
        Procedimientos.LlenarGrid(dtReq, gvReq);

        ViewState["dtPaginas"] = dtReq;

        if (pnlCodigo.Visible)
            kitarFiltro("IDREQUERIMIENTO", "Requerimiento", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMCOMCIAL", "Comercializador", pnlNombre, txtBusquedaNombre);
        if (pnlEstado.Visible)
            kitarFiltro("IDESTADO", "Estado", pnlEstado, ddlBusquedaEstado);

        txtNumReg.Text = Convert.ToString(dtReq.Rows.Count);
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void kitarFiltro(string value, string text, Panel pnl, DropDownList ddl)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        ddl.SelectedIndex = 0;
    }

    protected void gvReq_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvReq.PageIndex = e.NewPageIndex;
        gvReq.DataSource = ViewState["dtPaginas"];
        gvReq.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addRequerimientos.aspx");
        else if (sender.Equals(imgActualizar))
        {
            Response.Redirect("addRequerimientos.aspx?id=A");
        }
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("IDREQUERIMIENTO", "Requerimiento", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMCOMCIAL", "Comercializador", pnlNombre, txtBusquedaNombre);
        if (pnlEstado.Visible)
            kitarFiltro("IDESTADO", "Estado", pnlEstado, ddlBusquedaEstado);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
        {
            if (ddlFlitro.Items.Count > 0)
                AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        }

        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("IDREQUERIMIENTO", "Requerimiento", pnlCodigo, txtBusquedaCod);
        else if (sender.Equals(lnkNombreKitar))
            kitarFiltro("NOMCOMCIAL", "Comercializador", pnlNombre, txtBusquedaNombre);
        else if (sender.Equals(lnkEstadoKitar))
            kitarFiltro("IDESTADO", "Estado", pnlEstado, ddlBusquedaEstado);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMCOMCIAL":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            case "IDESTADO":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlEstado.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }

    public DataTable dtExcel()
    {
        string[] titulo = { "#", "Fecha", "Aplicacion", "Comercializador", "Descripcion", "Estado", "Responsable" };
        string[] columnas = { "Orden", "FECHA", "IDAPLICACION", "NOMCOMCIAL", "DESCRIPCION", "DESCESTADO", "ENCARGADO" };
        string[] valores = new string[7];
        DataTable dt = new DataTable();
        DataTable dtPagina = (DataTable)ViewState["dtPaginas"];

        foreach (DataRow row in dtPagina.Rows)
        {
            for (int i = 0; i < titulo.Length; i++)
            {
                valores[i] = row[columnas[i]].ToString();
            }

            Procedimientos.CrearDatatable(titulo, valores, dt);

        }

        return dt;
    }

    protected void Excel_Click(object sender, ImageClickEventArgs e)
    {
        Procedimientos.ExportarDataTable(dtExcel(), "Requerimientos");
    }
}
