﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_addEstimacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Estimacion Proyectos", this.Page);
            ViewState["dtCasoUso"] = null;
            AgregarFilaCasoUso(string.Empty, string.Empty, string.Empty, string.Empty,string.Empty);
            ClienteListar();
            EstadoControles(false);

            string script = "if(trim(document.getElementById('" + txtCodigo.ClientID + "').value) != '') { " +
                " __doPostBack('" + lnkComprobar.UniqueID + "','') };";

            txtCodigo.Attributes.Add("onblur", script);           
            txtCodigo.Focus();

            if (Request.QueryString["id"] != null)
            {
                Consultar(Request.QueryString["id"]);
            }
        }
        litScript.Text = string.Empty;
    }

    private void EstadoControles(bool Estado)
    {
        txtCodigo.Enabled = !Estado;
        txtProyecto.Enabled = Estado;
        ddlCliente.Enabled = Estado;
        ddlGerente.Enabled = Estado;
        pnlContainer.Visible = Estado;
    }

    private void Consultar(string Codigo)
    {
        Estimacion_Proyectos est = new Estimacion_Proyectos();
        est.Codigo = Codigo;
        DataSet ds = new ClinicaCES.Logica.LEstimacion_Proyectos().ProyectoConsultar(est);
        DataTable dtProyecto = ds.Tables[0];

        EstadoControles(true);

        if (dtProyecto.Rows.Count > 0)
        { 
            //existe ya el proyecto cargarlo
            DataRow row = dtProyecto.Rows[0];
            txtProyecto.Text = row["PROYECTO"].ToString();
            txtCodigo.Text = row["CODIGO"].ToString();
            ddlGerente.SelectedValue = row["GERENTE"].ToString();
            string Cliente = row["NIT"].ToString() + "|" + row["NEGOCIO"].ToString();
            ddlCliente.SelectedValue = Cliente;

            if (ds.Tables[1].Rows.Count > 0)
            {
                AgregarFilaCasoUso(ds.Tables[1]);
            }
            
        }

        ActoresListar(Codigo);
        ConsultarComplejidad(Codigo);
        ConsultarFactor(Codigo);
        ConsultarEstimacion(Codigo);
        ConsultarEsfCosto(Codigo);
    }

    private void ConsultarEstimacion(string Proyecto)
    {
        Estimacion_Proyectos est = new Estimacion_Proyectos();
        est.Proyecto = Proyecto;
        DataTable dtEstimacion = new ClinicaCES.Logica.LEstimacion_Proyectos().Estimacion_ProyectosConsultar(est);
        Procedimientos.LlenarGrid(dtEstimacion, gvEstimacion);
    }

    private void ConsultarComplejidad(string Proyecto)
    {
        ComplejidadTecnica tec = new ComplejidadTecnica();
        tec.Proyecto = Proyecto;

        DataTable dtComplejidad = new ClinicaCES.Logica.LComplejidadTecnica().ComplejidadTecnicaConsultar(tec);

        Procedimientos.LlenarGrid(dtComplejidad, gvComplejidadTecnica);
    }

    private void ConsultarFactor(string Proyecto)
    {
        FactoresEntorno tec = new FactoresEntorno();
        tec.Proyecto = Proyecto;

        DataTable dtFactor = new ClinicaCES.Logica.LFactoresEntorno().FactoresEntornoConsultar(tec).Tables[0];

        Procedimientos.LlenarGrid(dtFactor, gvFactores);
    }

    private void ConsultarEsfCosto(string Codigo)
    {
        Estimacion_Proyectos est = new Estimacion_Proyectos();
        est.Codigo = Codigo;
        DataTable dt = new ClinicaCES.Logica.LEstimacion_Proyectos().ConsultarEsfCosto(est);

        if (dt.Rows.Count > 0)
        {
            //existe ya el proyecto cargarlo
            DataRow row = dt.Rows[0];
            txtEsfuerzo.Text = row["HORAS"].ToString();
            txtCostos.Text = row["TOTAL"].ToString();
        }
    }


    private void Guardar
        (string Codigo, string Proyecto, string Nit, string Negocio, string Gerente, 
        string Usr, string xmluso,string Simple,string Medio,string Complejo, 
        string xmlComple, string xmlFactor, string Horas,string xmlEstima) 
    {
        Estimacion_Proyectos est = new Estimacion_Proyectos();
        est.Codigo = Codigo;
        est.Descripcion = Proyecto;
        est.Nit = Nit;
        est.Negocio = Negocio;
        est.Gerente = Gerente;
        est.usr = Usr;
        est.xmlUsos = xmluso;
        est.simple = Simple;
        est.medio = Medio;
        est.complejo = Complejo;
        est.xmlComplejidad = xmlComple;
        est.xmlFactor = xmlFactor;
        est.Horas = Horas;
        est.xmlEstimacion = xmlEstima;

        string Msg = "3";

        if (new ClinicaCES.Logica.LEstimacion_Proyectos().ProyectosActualizar(est))
        {
            Msg = "1";
            ConsultarEstimacion(Codigo);
            ConsultarEsfCosto(Codigo);
        }

        Procedimientos.Script("Mensaje(" + Msg + ")", litScript);
    }


    private string xmlUsos()
    { 
        string xmlUso = "<Raiz>";
        string Consecutivo,Tema,Solicitud,Funcionalidad,Tipo;

        for (int i = 1; i < gvCasoUso.Rows.Count; i++)
        {
            GridViewRow row = gvCasoUso.Rows[i];
            Label txtTema = (Label)row.FindControl("lblTema");
            Label lblSolicitud = (Label)row.FindControl("lblSolicitud");
            Label lblFuncionalidad = (Label)row.FindControl("lblFuncionalidad");
            Consecutivo = row.Cells[1].Text;
            Tema = txtTema.Text.Trim().ToUpper();
            Solicitud = lblSolicitud.Text.Trim().ToUpper();
            Funcionalidad = lblFuncionalidad.Text.Trim().ToUpper();
            Tipo  = gvCasoUso.DataKeys[i]["TIPOID"].ToString();

            xmlUso += "<Datos CONSECUTIVO = '" + Consecutivo + "' TEMA = '" + Tema + "' " +
                "SOLICITUD = '" + Solicitud + "' FUNCIONALIDAD = '" + Funcionalidad + "' TIPO = '" + Tipo +"' />";
        }

        return xmlUso + "</Raiz>";
    }

    private void ClienteListar()
    {
        DataTable dt = new ClinicaCES.Logica.LClientes().ClienteConsultar();
        Procedimientos.LlenarCombos(ddlCliente, dt, "NIT", "NOMCOMCIAL");
        Procedimientos.LlenarCombos(ddlGerente, new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(), "USUARIO", "NOMBRE");

    }

    private void ActoresListar(string codigo)
    {
        Estimacion_Proyectos est = new Estimacion_Proyectos();
        est.Codigo = codigo;
        DataTable dtActores = new ClinicaCES.Logica.LEstimacion_Proyectos().ActoresConsultar(est);

        Procedimientos.LlenarGrid(dtActores, gvActores);
    }

    private void AgregarFilaCasoUso(string Tema, string Solicitud, string Funcionalidad, string Tipo,string TipoID)
    {
        //Grid y datatable
        DataTable dtCasoUso = new DataTable();
        string codigo = "0";
        if (ViewState["dtCasoUso"] != null)
        {
            dtCasoUso = (DataTable)ViewState["dtCasoUso"];
            GridViewRow row;
            if (gvCasoUso.Rows.Count > 1)
            {
                row = gvCasoUso.Rows[gvCasoUso.Rows.Count - 1];
                codigo = row.Cells[1].Text;
            }
        }
        else
        {
            dtCasoUso.Columns.Add("TEMA");
            dtCasoUso.Columns.Add("SOLICITUD");
            dtCasoUso.Columns.Add("FUNCIONALIDAD");
            dtCasoUso.Columns.Add("TIPO");
            dtCasoUso.Columns.Add("TIPOID");
            dtCasoUso.Columns.Add("CODIGO");
            
        }

        DataRow dr;

        dr = dtCasoUso.NewRow();
        dr["TEMA"] = Tema;
        dr["SOLICITUD"] = Solicitud;
        dr["FUNCIONALIDAD"] = Funcionalidad;
        dr["TIPO"] = Tipo;
        dr["TIPOID"] = TipoID;
        dr["CODIGO"] = int.Parse(codigo) + 1;


        dtCasoUso.Rows.Add(dr);

        gvCasoUso.DataSource = dtCasoUso;
        gvCasoUso.DataBind();
        ViewState["dtCasoUso"] = dtCasoUso;
    }

    private void AgregarFilaCasoUso(DataTable dtCasoUsoAux)
    {
        //Grid y datatable
        DataTable dtCasoUso = new DataTable();

        dtCasoUso = (DataTable)ViewState["dtCasoUso"];

        foreach (DataRow Row in dtCasoUsoAux.Rows)
        {
            DataRow dr;

            dr = dtCasoUso.NewRow();
            dr["TEMA"] = Row["TEMA"].ToString();
            dr["SOLICITUD"] = Row["SOLICITUD"].ToString();
            dr["FUNCIONALIDAD"] = Row["FUNCIONALIDAD"].ToString();
            dr["TIPO"] = Row["TIPO"].ToString();
            dr["TIPOID"] = Row["TIPOID"].ToString();
            dr["CODIGO"] = Row["CODIGO"].ToString();
            dtCasoUso.Rows.Add(dr);
        }

        

        gvCasoUso.DataSource = dtCasoUso;
        gvCasoUso.DataBind();
        ViewState["dtCasoUso"] = dtCasoUso;
    }

    protected void gvCasoUso_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (e.Row.RowIndex == 0)
            {
                //Nivel nvl = new Nivel();
                //DataTable dt = new ClinicaCES.Logica.LNivel().MenuConsultar(nvl);

                TextBox txtTema = (TextBox)e.Row.FindControl("txtTema");
                DropDownList ddlTipo = (DropDownList)e.Row.FindControl("ddlTipo");
                TextBox txtSolicitud = (TextBox)e.Row.FindControl("txtSolicitud");
                TextBox txtFuncionalidad = (TextBox)e.Row.FindControl("txtFuncionalidad");
                Label lblValidaTema = (Label)e.Row.FindControl("lblValidaTema");
                Label lblValidaFuncionalidad = (Label)e.Row.FindControl("lblValidaFuncionalidad");


                txtTema.Visible = true;
                ddlTipo.Visible = true;
                txtSolicitud.Visible = true;
                txtFuncionalidad.Visible = true;
                lblValidaTema.Visible = true;
                lblValidaFuncionalidad.Visible = true;
                
                
            }
            else
            {
                e.Row.Cells[1].Text = Convert.ToString(e.Row.RowIndex);
                ImageButton imgEliminarCaso = (ImageButton)e.Row.FindControl("imgEliminarCaso");
                ImageButton imgEditarCaso = (ImageButton)e.Row.FindControl("imgEditarCaso");
                imgEditarCaso.Visible = true;
                imgEliminarCaso.Visible = true;
            }
        }
    }

    private void AgregarFilaCasoUso(GridViewRow row)
    {
        TextBox txtTema = (TextBox)row.FindControl("txtTema");
        DropDownList ddlTipo = (DropDownList)row.FindControl("ddlTipo");
        TextBox txtSolicitud = (TextBox)row.FindControl("txtSolicitud");
        TextBox txtFuncionalidad = (TextBox)row.FindControl("txtFuncionalidad");

        AgregarFilaCasoUso(txtTema.Text.Trim().ToUpper(), txtSolicitud.Text.Trim().ToUpper(), txtFuncionalidad.Text.Trim().ToUpper(), ddlTipo.SelectedItem.Text,ddlTipo.SelectedValue);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnAgregarCU))
        {
            if(ValidaAgregar(0))
                AgregarFilaCasoUso(gvCasoUso.Rows[0]);
        }
        else if (sender.Equals(btnGuardar))
        {
            if (ValidarGuardar())
            {
                string[] Cliente = ddlCliente.SelectedValue.Split('|');
                string[] Valores = new string[3];
                foreach (GridViewRow row in gvActores.Rows)
                {
                    TextBox txtValor = (TextBox)row.FindControl("txtValor");
                    Valores[row.RowIndex] = txtValor.Text;
                }
                Guardar(txtCodigo.Text.Trim().ToUpper(), txtProyecto.Text.Trim().ToUpper(),
                    Cliente[0], Cliente[1], ddlGerente.SelectedValue, Session["Nick"].ToString(),
                    xmlUsos(), Valores[0], Valores[1], Valores[2], xmlComplejidad(), xmlfACTOR(),
                    CalcularEstimacion(),xmlEstimacion());

            }
        }
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
        {
            Response.Redirect("addEstimacion.aspx");
        }
    }

    public DataTable dtUsosExcel()
    {
        string[] columnas = { "Numero", "Tema", "Solicitud", "Funcionalidad", "Tipo" };
        string[] valores = new string[5];
        DataTable dt = new DataTable();
        foreach (GridViewRow row in gvCasoUso.Rows)
        {
            if (row.RowIndex > 0)
            {
    			valores[0] = row.Cells[1].Text;

                Label lblTema = (Label)row.FindControl("lblTema");
                valores[1] = lblTema.Text;

                Label lblSolicitud = (Label)row.FindControl("lblSolicitud");
                valores[2] = lblSolicitud.Text;

                Label lblFuncionalidad = (Label)row.FindControl("lblFuncionalidad");
                valores[3] = lblFuncionalidad.Text;

                Label lblTipo = (Label)row.FindControl("lblTipo");
                valores[4] = lblTipo.Text;

                Procedimientos.CrearDatatable(columnas, valores, dt);
            }
        }

        return dt;
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(txtCodigo.Text.Trim());
    }

    private string xmlComplejidad()
    { 
        string xmlCom = "<Raiz>";
        string Codigo;
        string Valor;

        foreach (GridViewRow row in gvComplejidadTecnica.Rows)
	    {
            TextBox txtValor = (TextBox)row.FindControl("txtValor");
    		Codigo = row.Cells[0].Text;
            Valor = txtValor.Text;

            xmlCom += "<Datos CODIGO = '" + Codigo + "' VALOR='" + Valor + "' />";
	    }

        return xmlCom + "</Raiz>";
    }

    private string xmlfACTOR()
    {
        string xmlfACT = "<Raiz>";
        string Codigo;
        string Valor;

        foreach (GridViewRow row in gvFactores.Rows)
        {
            TextBox txtValor = (TextBox)row.FindControl("txtValor");
            Codigo = row.Cells[0].Text;
            Valor = txtValor.Text;

            xmlfACT += "<Datos CODIGO = '" + Codigo + "' VALOR='" + Valor + "' />";
        }

        return xmlfACT + "</Raiz>";
    }

    private bool ValidaAgregar(int rowindex)
    {
        bool valido = true;
        GridViewRow row = gvCasoUso.Rows[rowindex];

        System.Drawing.Color azul = System.Drawing.Color.Blue;
        System.Drawing.Color red = System.Drawing.Color.Red;

        TextBox txtTema = (TextBox)row.FindControl("txtTema");
        Label lblValidaTema = (Label)row.FindControl("lblValidaTema");

        Label lblValidaFuncionalidad = (Label)row.FindControl("lblValidaFuncionalidad");
        TextBox txtFuncionalidad = (TextBox)row.FindControl("txtFuncionalidad");

        if (string.IsNullOrEmpty(txtTema.Text.Trim()))
        {
            lblValidaTema.ForeColor = red;
            txtTema.CssClass = "invalidtxt";
            valido = false;
        }
        else
        {
            lblValidaTema.ForeColor = azul;
            txtTema.CssClass = "form_input";
        }
            

        if (string.IsNullOrEmpty(txtFuncionalidad.Text.Trim()))
        {
            lblValidaFuncionalidad.ForeColor = red;
            txtFuncionalidad.CssClass = "invalidtxt";
            valido = false;
        }
        else
        {
            txtFuncionalidad.CssClass = "form_input";
            lblValidaFuncionalidad.ForeColor = azul;
        }

        return valido;

    }

    private bool ValidarGuardar()
    {
        bool valido = true;
        System.Drawing.Color azul = System.Drawing.Color.Blue;
        System.Drawing.Color rojo = System.Drawing.Color.Red;

        if (ddlGerente.SelectedIndex == 0)
        {
            lblValidaGerente.ForeColor = rojo;
            valido = false;
        }
        else
            lblValidaGerente.ForeColor = azul;

        if (ddlCliente.SelectedIndex == 0)
        {
            lblValidaCliente.ForeColor = rojo;
            valido = false;
        }
        else
            lblValidaCliente.ForeColor = azul;

        if (string.IsNullOrEmpty(txtProyecto.Text.Trim()))
        {
            lblValidaProyecto.ForeColor = rojo;
            valido = false;
        }
        else
            lblValidaProyecto.ForeColor = azul;

        if (!ValidarValores(gvFactores))
        {
            valido = false;
            tbc.ActiveTabIndex = 3;
        }

        if (!ValidarValores(gvComplejidadTecnica))
        {
            valido = false;
            tbc.ActiveTabIndex = 2;
        }

        if (!ValidarValores(gvActores))
        {
            valido = false;
            tbc.ActiveTabIndex = 1;
        }

        if (gvCasoUso.Rows.Count == 1)
        {
            valido = false;
            tbc.ActiveTabIndex = 0;
            Procedimientos.Script("Mensaje(22)", litScript);
        }

        return valido;
    }

    private bool ValidarValores(GridView gv)
    {
        bool Valido = true;

        foreach (GridViewRow row in gv.Rows)
        {
            TextBox txtValor = (TextBox)row.FindControl("txtValor");

            if (string.IsNullOrEmpty(txtValor.Text.Trim()))
            {
                Valido = false;
                txtValor.CssClass = "invalidtxt";
            }
            else
                txtValor.CssClass = "form_input";
        }

        return Valido;
    }

    private string CalcularEstimacion()
    {
        double UUCP = 0;
        double UAW = 0;
        double UUCW = 0;
        double TCF = 0;
        double EF = 0;
        double UCP = 0;
        double CF = 0;
        double E = 0;

        double[] vlrActores = { 1, 2, 3 };
        double[] vlrCasos = { 5, 10, 15 };

        /*CALCULANDO LA VARIABLE UAW ACTORES*/
        foreach (GridViewRow row in gvActores.Rows)
        {
            TextBox txtValor = (TextBox)row.FindControl("txtValor");
            double temp;
            if (double.TryParse(txtValor.Text, out temp))
                UAW += temp * vlrActores[row.RowIndex];
        }

        /*CALCULANDO UUCW CASOS DE USO*/
        //CONTANDO SIMPLES,medios y complejos
        double simplesCasos = 0;
        double medioCasos = 0;
        double complejoCasos = 0;
        for (int i = 1; i < gvCasoUso.Rows.Count; i++)
        {
            string tipo = gvCasoUso.DataKeys[i]["TIPOID"].ToString();

            if (tipo == "S")
                simplesCasos ++;
            else if (tipo == "M")
                medioCasos ++;
            else
                complejoCasos ++;
        }

        simplesCasos = simplesCasos * vlrCasos[0];
        medioCasos = medioCasos * vlrCasos[1];
        complejoCasos = complejoCasos * vlrCasos[2];

        UUCW = simplesCasos + medioCasos + complejoCasos;

        UUCP = UUCW + UAW;

        /*TCF COMPLEJIDAD TECNICA*/
        double PesoComplejidad = 0;
        double VlrComplejidad = 0;
        double totalComplejidad = 0;
        foreach (GridViewRow item in gvComplejidadTecnica.Rows)
        {
            TextBox txtValor = (TextBox)item.FindControl("txtValor");
            PesoComplejidad = double.Parse(item.Cells[3].Text);
            VlrComplejidad = double.Parse(txtValor.Text.Trim());
            totalComplejidad += (VlrComplejidad * PesoComplejidad); 
        }

        TCF = 0.6 + (0.01 * totalComplejidad);


        /*EF FACTOR DE AMBIENTE*/
        double PesoAmbiente = 0;
        double VlrAmbiente = 0;
        double totalAmbiente = 0;
        int contador = 0;

        foreach (GridViewRow item in gvFactores.Rows)
        {
            TextBox txtValor = (TextBox)item.FindControl("txtValor");
            VlrAmbiente = double.Parse(txtValor.Text.Trim());
            PesoAmbiente = double.Parse(item.Cells[3].Text);
            totalAmbiente += (VlrAmbiente * PesoAmbiente);

            if (item.RowIndex < 6)
            {
                if ((VlrAmbiente * PesoAmbiente) < 3)
                    contador++;
            }
            else
            {
                if (Math.Abs(VlrAmbiente * PesoAmbiente) > 3)
                    contador++;
            }
        }

        EF = 1.4 + (-0.03 * totalAmbiente);

        UCP = TCF *  EF * UUCP ;

        if(contador <= 2)
            CF = 20;
        else if(contador <= 4)
            CF = 28;
        else 
            CF = 36;

        E = (UCP * CF);

        return E.ToString();
    }

    private string xmlEstimacion()
    {
        string xml = "<Raiz>";
        string Estimacion;
        string Valor;
        foreach (GridViewRow item in gvEstimacion.Rows)
	    {
            TextBox txtRecursos = (TextBox)item.FindControl("txtRecursos");
            int cant;
            if(int.TryParse(txtRecursos.Text.Trim(),out cant))
            {
                Estimacion = gvEstimacion.DataKeys[item.RowIndex]["CODIGO"].ToString();
                Valor = item.Cells[3].Text;

                xml += "<Datos ESTIMACION='" + Estimacion + "' VALOR = '" + Valor + "' CANTIDAD='" + cant.ToString() + "' " +
                "HORAS = '" + item.Cells[2].Text.Replace(',','.') + "' />";

            }
	    }
        return xml + "</Raiz>";
    }

    protected void gvEstimacion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        { 
            double total;
            if (double.TryParse(e.Row.Cells[5].Text, out total))
            {
                e.Row.Cells[5].Text = total.ToString("C0");
            }
        }
    }

    protected void Excel_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgExcelUsos))
        {
            Procedimientos.ExportarDataTable(dtUsosExcel(), "Estimacion_Usos");
        }
        else if (sender.Equals(imgExportarActores))
        {
            Procedimientos.ExportarDataTable(dtActoresExcel(), "Estimacion_Actores");
        }
        else if (sender.Equals(imgExportarComplejidad))
        {
            Procedimientos.ExportarDataTable(dtComplejidadTecnicaExcel(), "Estimacion_Complejidad_Tecnica");
        }
        else if (sender.Equals(imgExportarFactores))
        {
            Procedimientos.ExportarDataTable(dtFactoresEntornoExcel(), "Estimacion_Factores_Entorno");
        }
        else if (sender.Equals(imgExportarEstimacion))
        {
            Procedimientos.ExportarDataTable(dtEstimacionExcel(), "Estimacion");
        }
        

        
    }

    public DataTable dtActoresExcel()
    {
        string[] columnas = { "Tipo", "Descripcion", "Valor" };
        string[] valores = new string[3];
        DataTable dt = new DataTable();
        foreach (GridViewRow row in gvActores.Rows)
        {
            if (row.RowIndex > 0)
            {
                valores[0] = row.Cells[0].Text;
                valores[1] = row.Cells[1].Text;

                TextBox txtValor = (TextBox)row.FindControl("txtValor");
                valores[2] = txtValor.Text;

                Procedimientos.CrearDatatable(columnas, valores, dt);
            }
        }

        return dt;
    }

    public DataTable dtComplejidadTecnicaExcel()
    {
        string[] columnas = { "Codigo", "Nombre", "Descripcion","Peso","Valor" };
        string[] valores = new string[5];
        DataTable dt = new DataTable();
        foreach (GridViewRow row in gvComplejidadTecnica.Rows)
        {
            if (row.RowIndex > 0)
            {
                valores[0] = row.Cells[0].Text;
                valores[1] = row.Cells[1].Text;
                valores[2] = row.Cells[2].Text;
                valores[3] = row.Cells[3].Text;

                TextBox txtValor = (TextBox)row.FindControl("txtValor");
                valores[4] = txtValor.Text;

                Procedimientos.CrearDatatable(columnas, valores, dt);
            }
        }

        return dt;
    }

    public DataTable dtFactoresEntornoExcel()
    {
        string[] columnas = { "Codigo", "Nombre", "Descripcion", "Peso", "Valor" };
        string[] valores = new string[5];
        DataTable dt = new DataTable();
        foreach (GridViewRow row in gvFactores.Rows)
        {
            if (row.RowIndex > 0)
            {
                valores[0] = row.Cells[0].Text;
                valores[1] = row.Cells[1].Text;
                valores[2] = row.Cells[2].Text;
                valores[3] = row.Cells[3].Text;

                TextBox txtValor = (TextBox)row.FindControl("txtValor");
                valores[4] = txtValor.Text;

                Procedimientos.CrearDatatable(columnas, valores, dt);
            }
        }

        return dt;
    }

    public DataTable dtEstimacionExcel()
    {
        string[] columnas = { "Actividad","Porcentaje","Horas - Hombre","Vlr Hora","Nro de Recursos","Costo" };
        string[] valores = new string[6];
        DataTable dt = new DataTable();
        foreach (GridViewRow row in gvEstimacion.Rows)
        {
            if (row.RowIndex > 0)
            {
                valores[0] = row.Cells[0].Text;
                valores[1] = row.Cells[1].Text;
                valores[2] = row.Cells[2].Text;
                valores[3] = row.Cells[3].Text;
                valores[5] = row.Cells[5].Text;

                TextBox txtValor = (TextBox)row.FindControl("txtRecursos");
                valores[4] = txtValor.Text;

                Procedimientos.CrearDatatable(columnas, valores, dt);
            }
        }

        return dt;
    }

    private void EliminarFila(string codigo)
    {
        DataTable dtCasoUso = (DataTable)ViewState["dtCasoUso"];
        foreach (DataRow row in dtCasoUso.Select("CODIGO = '" + codigo + "'"))
        {            
            dtCasoUso.Rows.Remove(row);
        }

        ViewState["dtCasoUso"] = dtCasoUso;

        gvCasoUso.DataSource = dtCasoUso;
        gvCasoUso.DataBind();

    }

    //Actividad	Porcentaje	Horas - Hombre	Vlr Hora	 Nro de Recursos 	Costo
    protected void gvCasoUso_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton imgEliminarCaso = (ImageButton)e.Row.FindControl("imgEliminarCaso");
            imgEliminarCaso.CommandArgument = e.Row.RowIndex.ToString();

            ImageButton imgEditarCaso = (ImageButton)e.Row.FindControl("imgEditarCaso");
            imgEditarCaso.CommandArgument = e.Row.RowIndex.ToString();

            ImageButton imgGuardarCaso = (ImageButton)e.Row.FindControl("imgGuardarCaso");
            imgGuardarCaso.CommandArgument = e.Row.RowIndex.ToString();

            ImageButton imgCancelarUso = (ImageButton)e.Row.FindControl("imgCancelarUso");
            imgCancelarUso.CommandArgument = e.Row.RowIndex.ToString();
            
        }
    }

    protected void gvCasoUso_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = gvCasoUso.Rows[int.Parse(e.CommandArgument.ToString())];
        TextBox txtTema = (TextBox)row.FindControl("txtTema");
        DropDownList ddlTipo = (DropDownList)row.FindControl("ddlTipo");
        TextBox txtSolicitud = (TextBox)row.FindControl("txtSolicitud");
        TextBox txtFuncionalidad = (TextBox)row.FindControl("txtFuncionalidad");
        Label lblValidaTema = (Label)row.FindControl("lblValidaTema");
        Label lblValidaFuncionalidad = (Label)row.FindControl("lblValidaFuncionalidad");

        Label lblTema = (Label)row.FindControl("lblTema");
        Label lblSolicitud = (Label)row.FindControl("lblSolicitud");
        Label lblFuncionalidad = (Label)row.FindControl("lblFuncionalidad");
        Label lblTipo = (Label)row.FindControl("lblTipo");
        ImageButton imgEditarCaso = (ImageButton)row.FindControl("imgEditarCaso");
        ImageButton imgGuardarCaso = (ImageButton)row.FindControl("imgGuardarCaso");
        ImageButton imgCancelarUso = (ImageButton)row.FindControl("imgCancelarUso");

        if (e.CommandName == "eliminar")
        {
            string codigo = gvCasoUso.DataKeys[row.RowIndex]["CODIGO"].ToString();
            EliminarFila(codigo);
        }
        else if (e.CommandName == "editar")
        {
            imgGuardarCaso.Visible = true;
            imgCancelarUso.Visible = true;
            imgEditarCaso.Visible = false;
            row.CssClass = "editrow";

            lblTema.Visible = false;
            lblSolicitud.Visible = false;
            lblFuncionalidad.Visible = false;
            lblTipo.Visible = false;

            txtTema.Visible = true;
            ddlTipo.Visible = true;
            txtSolicitud.Visible = true;
            txtFuncionalidad.Visible = true;
            lblValidaTema.Visible = true;
            lblValidaFuncionalidad.Visible = true;

            string tipoid = gvCasoUso.DataKeys[row.RowIndex]["TIPOID"].ToString();

            txtTema.Text = lblTema.Text;
            txtSolicitud.Text = lblSolicitud.Text;
            txtFuncionalidad.Text = lblFuncionalidad.Text;
            ddlTipo.SelectedValue = tipoid;
        }
        else if (e.CommandName == "guardar")
        {
            if (ValidaAgregar(row.RowIndex))
            {
                imgGuardarCaso.Visible = false;
                imgCancelarUso.Visible = false;
                imgEditarCaso.Visible = true;
                if(row.RowState == DataControlRowState.Alternate)
                    row.CssClass = "alterrow";
                else
                    row.CssClass = "normalrow";


                lblTema.Visible = true;
                lblSolicitud.Visible = true;
                lblFuncionalidad.Visible = true;
                lblTipo.Visible = true;

                txtTema.Visible = false;
                ddlTipo.Visible = false;
                txtSolicitud.Visible = false;
                txtFuncionalidad.Visible = false;
                lblValidaTema.Visible = false;
                lblValidaFuncionalidad.Visible = false;
                string tipoid = gvCasoUso.DataKeys[row.RowIndex]["TIPOID"].ToString();
                lblTema.Text = txtTema.Text.Trim().ToUpper();
                lblSolicitud.Text = txtSolicitud.Text.Trim().ToUpper();
                lblFuncionalidad.Text = txtFuncionalidad.Text.Trim().ToUpper();
                lblTipo.Text = ddlTipo.SelectedItem.Text.Trim().ToUpper();

                ActualizarCasoUso(lblTema.Text, lblSolicitud.Text, lblFuncionalidad.Text, lblTipo.Text, tipoid, gvCasoUso.DataKeys[row.RowIndex]["CODIGO"].ToString());
            }
        }
        else if (e.CommandName == "cancelar")
        {
            imgGuardarCaso.Visible = false;
            imgCancelarUso.Visible = false;
            imgEditarCaso.Visible = true;
            if (row.RowState == DataControlRowState.Alternate)
                row.CssClass = "alterrow";
            else
                row.CssClass = "normalrow";


            lblTema.Visible = true;
            lblSolicitud.Visible = true;
            lblFuncionalidad.Visible = true;
            lblTipo.Visible = true;

            txtTema.Visible = false;
            ddlTipo.Visible = false;
            txtSolicitud.Visible = false;
            txtFuncionalidad.Visible = false;
            lblValidaTema.Visible = false;
            lblValidaFuncionalidad.Visible = false;
        }
    }

    private void ActualizarCasoUso(string Tema, string Solicitud, string Funcionalidad, string Tipo, string TipoID, string Codigo)
    {
        DataTable dtCasoUso = (DataTable)ViewState["dtCasoUso"];
        DataRow dr = null;

        foreach (DataRow row in dtCasoUso.Select("CODIGO = '" + Codigo + "'"))
        {
            //dtCasoUso.Rows.Remove(row);
            dr = row;
        }
        
        dr["TEMA"] = Tema;
        dr["SOLICITUD"] = Solicitud;
        dr["FUNCIONALIDAD"] = Funcionalidad;
        dr["TIPO"] = Tipo;
        dr["TIPOID"] = TipoID;

        //gvCasoUso.DataSource = dtCasoUso;
        //gvCasoUso.DataBind();
        ViewState["dtCasoUso"] = dtCasoUso;
    }
}
