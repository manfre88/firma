﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_srcRegActividades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Consultar Actividades", this.Page);
            DateTime hoy = DateTime.Now;


            Semana(hoy.ToShortDateString());

            //SemanaAntNext(hoy.AddDays(-7).ToShortDateString(), hoy.AddDays(7).ToShortDateString());

            Procedimientos.ValidarSession(this.Page);
            Buscar(lblSemana.Text, Session["Nick"].ToString());
            Procedimientos.LlenarCombos(ddlResponsable, new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(), "USUARIO", "NOMBRE");
            ddlResponsable.SelectedValue = Session["Nick"].ToString();
        }

        litScript.Text = string.Empty;
    }

    public int getWeek(DateTime date)
    {
        System.Globalization.CultureInfo cult_info = System.Globalization.CultureInfo.CreateSpecificCulture("no");
        System.Globalization.Calendar cal = cult_info.Calendar;
        int weekCount = cal.GetWeekOfYear(date, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek);
        return weekCount;
    }

    private void Semana(string fecha)
    {
        DateTime Fecha = Convert.ToDateTime(fecha);
        string year = Fecha.Year.ToString();
        lblSemana.Text = year + "-" + getWeek(Fecha).ToString();
    }

    private void Buscar(string semana, string usr)
    {
        ActividadReg act = new ActividadReg();
        act.Semana = semana;
        act.Responsable = usr;

        DataTable dtActi = new ClinicaCES.Logica.LActividades().AtividadBuscar(act);

        if (dtActi.Rows.Count > 0)
        {
            Procedimientos.LlenarGrid(dtActi, gvRegistro);
        }
        else
        {
            gvRegistro.Visible = false;
        }

    }

    
    protected void lnkSemana_Click(object sender, EventArgs e)
    {
        Semana(txtSemana.Text);
        Buscar(lblSemana.Text, ddlResponsable.SelectedValue);
    }

    protected void ddlResponsable_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Semana(txtSemana.Text);
        Buscar(lblSemana.Text, ddlResponsable.SelectedValue);
    }
    protected void gvRegistro_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='overrownopoint'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }
    }
}
