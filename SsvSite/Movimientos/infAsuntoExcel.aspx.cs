﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Movimientos_infAsuntoExcel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            litImage.Text = "<IMG id=img1 src=" + ConfigurationManager.AppSettings["logo1"] + " >";
            Consultar(Request.QueryString["id"]);
            
        }
    }

    private void Consultar(string idAsun)
    {
        DataSet dsAsunto = new ClinicaCES.Logica.LAsunto().AsuntoConsultarExcel(idAsun);

        DataTable dtAsunto = dsAsunto.Tables[0];
        DataTable dtDetalle = dsAsunto.Tables[1];

        if (dtAsunto != null && dtAsunto.Rows.Count > 0)
        { 
            DataRow row = dtAsunto.Rows[0];
            lblComercializador.Text = row["NOMCOMCIAL"].ToString();
            lblAsunto.Text = row["ASUNTO"].ToString();
            lblClasificacion.Text = row["CLASIFICACION"].ToString();
            lblCaso.Text = idAsun;
            lblFecha.Text = DateTime.Now.ToShortDateString();

            if (dtDetalle != null)
                Procedimientos.LlenarGrid(dtDetalle, gvInforme);

            cabezaArriba.Style.Value = "color:Black;font-weight:bold;";
            Procedimientos.ExportarExcel(pnlExcelini, this.Page, "Seguimiento_caso_" + idAsun + "_hasta_el_" + lblFecha.Text);
        }
    }
}
