<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addRegAct.aspx.cs" Inherits="Movimientos_addRegAct" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<table align="center">
    <tr>
        <td align="right">Responsable: <asp:Label ID="lblValidaRespnsable" runat="server" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="2">
            <asp:DropDownList id="ddlResponsable" runat="server" AutoPostBack="true"
                onselectedindexchanged="ddlResponsable_SelectedIndexChanged"></asp:DropDownList>            
        </td>
        <td align="right">Semana:</td>
        <td align="right">&nbsp;</td>
        <td style="white-space:nowrap">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>                    
                    <asp:Label ID="lblSemana" runat="server"></asp:Label>
                    <asp:TextBox Width="1" style="visibility:hidden" Height="1"  ID="txtSemana" runat="server" CssClass="form_input"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy" ID="clnFecha"
                        runat="server" TargetControlID="txtSemana" PopupButtonID="imgCalendar" 
                        OnClientDateSelectionChanged="Semana"></ajaxToolkit:CalendarExtender>
                        <img style="cursor:pointer" src="../icons/calendar.png" runat="server" id="imgCalendar" />  
                    <asp:Label ID="lblSemAct" runat="server" Visible="False"></asp:Label>
                </ContentTemplate>
                <Triggers>                
                    <asp:AsyncPostBackTrigger ControlID="lnkSemana" EventName="click" />
                </Triggers>
            </asp:UpdatePanel>                                    
        </td>
        
    </tr>
    <tr>
        <td align="right" style="white-space:nowrap">
            Tipo Actividad:</td>
        <td style="width: 171px">
        
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlCNA" runat="server" AutoPostBack="true"
                                    onselectedindexchanged="ddlCNA_SelectedIndexChanged">
                                    <asp:ListItem Text="NA" Value="NA"></asp:ListItem>
                                    <asp:ListItem Text="CASO" Value="C"></asp:ListItem>
                                    <asp:ListItem Text="REQUERIMIENTO" Value="R"></asp:ListItem>
                                </asp:DropDownList>    
                            </ContentTemplate>
                            <Triggers>                                
                                <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>          
                        
                        
                        
        </td>
        <td style="width: 207px">
        
                        <asp:UpdatePanel runat="server" ID="upnlAsunto" UpdateMode="Conditional">                            
                            <ContentTemplate>
                                <asp:Panel ID="pnlAsunto" runat="server" Visible="false">
                                    <table>
                                        <tr>
                                            <td><asp:Label ID="lblTipo" runat="server"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblValidaAsunto" runat="server" Text="(*)" ForeColor="Blue"></asp:Label>                                        
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAsunto" runat="server" CssClass="form_input" MaxLength="5" 
                                                    Width="54px"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtAsunto" FilterType="Numbers" />                                                        
                                            </td>                                            
                                        </tr>
                                    </table> 
                                </asp:Panel>                                
                            </ContentTemplate>   
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCNA" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="lnkAsunto" EventName="click" />
                                <asp:AsyncPostBackTrigger ControlID="ddlCliente" EventName="SelectedIndexChanged" />
                            </Triggers>                                                                                                                                       
                        </asp:UpdatePanel>
                                        
        </td>
                                                                    <td>
            <asp:LinkButton ID="lnkSemana" runat="server" onclick="lnkSemana_Click"></asp:LinkButton>
                                                                    </td>
    </tr>   
    <tr>
        <td align="right" style="white-space:nowrap">
            Cliente: <asp:Label ID="lblValidaCliente" runat="server" Text="(*)" ForeColor="Blue"></asp:Label>
        </td>
        <td colspan="2">
        
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlCliente" runat="server" AutoPostBack="true" onselectedindexchanged="ddlCliente_SelectedIndexChanged"></asp:DropDownList>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>  
                                        
        </td>
    </tr>   
    <tr>
        <td align="right">Aplicación:</td>
        <td colspan="5">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlAplicacion" runat="server"></asp:DropDownList>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlCliente" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel> 
             
        
        
        </td>
    </tr>
    <tr>
        <td align="right">Linea: <asp:Label ID="lblValidaLinea" runat="server" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="2">
        
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlLinea" runat="server"></asp:DropDownList>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>  
                      
        </td>

    </tr>
    <tr>
        <td align="right">Programa:</td>
        <td colspan="5">
            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:TextBox Width="450px" MaxLength="500" ID="txtPrograma" runat="server" CssClass="form_input"></asp:TextBox>                    
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel> 
        </td>                
    </tr>
    <tr>
        <td align="right">Actividad: <asp:Label ID="lblValidaActividad" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
        <td colspan="2">
        
            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlActividad" runat="server"></asp:DropDownList>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel> 
                                
        </td>
        <td align="right" colspan="2">Planeada:</td>
        <td>
        
            <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:CheckBox TextAlign="Left" ID="chkPLaneado" runat="server" AutoPostBack="true" oncheckedchanged="chkPLaneado_CheckedChanged" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel> 
        
        </td>        
    </tr>
    <tr>
        <td align="right" style="white-space:nowrap">Fecha Compromiso:</td>
        <td colspan="2">
            <asp:UpdatePanel ID="upnlSemaniviris" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:TextBox ID="txtFecha" runat="server" onkeyup="mascara(this,'/',true)" CssClass="form_input" Width="60px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFecha" FilterType="Custom, Numbers" ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy" ID="CalendarExtender1" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFecha" PopupButtonID="img1"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" 
                        runat="server" id="img1" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="lnkValidaSemana" EventName="click" />
                    <asp:AsyncPostBackTrigger ControlID="lnkSemana" EventName="click" />
                </Triggers>
            </asp:UpdatePanel>                                                             
        </td>
        <td align="right" style="white-space:nowrap" colspan="2">
        <asp:UpdatePanel ID="upnlCheck" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                Tiempo Planeado: <asp:Label ID="lblValidaHoras" Visible="false" runat="server" Text="(*)" ForeColor="Blue"></asp:Label></td>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="chkPLaneado" EventName="CheckedChanged" />
            </Triggers>
        </asp:UpdatePanel>
        <td>
            <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:TextBox ReadOnly="true" ID="txtHoras" runat="server" CssClass="form_input" MaxLength="5" Width="20px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtHoras" FilterType="Custom, Numbers" ValidChars="-,."></ajaxToolkit:FilteredTextBoxExtender> 
                </ContentTemplate>
                <Triggers>                    
                    <asp:AsyncPostBackTrigger ControlID="chkPLaneado" EventName="CheckedChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>                         
            
        </td>
    </tr>
</table>
<asp:LinkButton ID="lnkAsunto" runat="server" onclick="lnkAsunto_Click" AutoPostBack="true"></asp:LinkButton><asp:LinkButton ID="lnkValidaSemana" runat="server" onclick="lnkValidaSemana_Click"></asp:LinkButton>
<table align="center">
    <tr>                       
        <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnRetirar" />
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>                   
        </td>
        <td><asp:Button ID="btnGuardar" runat="server" Text="Guardar"  onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>                                                                                
        <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>                                          
    </tr>                    
</table>


<table>
    <tr>
        <td>
            <asp:UpdatePanel ID="upnlGrid" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView AutoGenerateColumns="False" ID="gvRegistro" runat="server" AllowPaging="True" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                        PageSize="15" DataKeyNames="CODIGO,RESPONSABLE,CLIENTE,LINEA,IDAPP,CNA,ACTIVIDAD,PLANEADA,ASUNTO,REQUERIMIENTO" onrowcommand="gvRegistro_RowCommand" 
                        onrowcreated="gvRegistro_RowCreated">                   
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <RowStyle CssClass="normalrow" />
                        <AlternatingRowStyle CssClass="alterrow" />
                        <PagerStyle CssClass="cabeza" ForeColor="White"  />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                        <Columns>   
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ToolTip="Editar" ID="imgEditar" runat="server" ImageUrl="../icons/pencil.png" CommandName="editar" />
                                            </td>
                                            <td>
                                                <asp:ImageButton OnClientClick="return Mensaje(11)" ToolTip="Eliminar" ID="imgEliminar" runat="server" ImageUrl="../icons/eliminar.gif" CommandName="eliminar" />                                            
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>  
                             <asp:BoundField DataField="SEMANA" HeaderText="Semana" />
                             <asp:BoundField DataField="NOMCOMCIAL" HeaderText="Cliente" />
                             <asp:BoundField DataField="NOMBRE_LINEA" HeaderText="Linea" />                     
                            <asp:BoundField DataField="APLICACION" HeaderText="Aplicación" />
                            <asp:BoundField DataField="PROGRAMA" HeaderText="Programa" />
                            <asp:BoundField DataField="FECHA" HeaderText="Fecha" />
                            <asp:BoundField DataField="TIEMPO" HeaderText="Horas" />
                        </Columns>
                    </asp:GridView> 
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="lnkSemana" EventName="click" />
                    <asp:PostBackTrigger ControlID="gvRegistro" />
                </Triggers>
            </asp:UpdatePanel>

        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnCodigo" runat="server" />
</asp:Content>

