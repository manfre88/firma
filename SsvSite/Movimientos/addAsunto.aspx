<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addAsunto.aspx.cs" Inherits="Movimientos_addAsunto" Title=".::Casos::." %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">   
        <tr>
            <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" 
                    ImageUrl="../icons/nuevo.png" onclick="Images_Click" TabIndex="1" /></td>          
            <td><asp:ImageButton runat="server" ID="imgGuardar" ToolTip="Guardar" 
                    OnClientClick="return Mensaje(13)" ImageUrl="../icons/16_save.png" 
                    onclick="Images_Click" TabIndex="4" /></td>                
            <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" 
                    ImageUrl="../icons/cancel.png" onclick="Images_Click" TabIndex="5" /></td>
            <td><img onclick="redirect('srcAsunto.aspx')" src="../icons/magnify.png" id="imgBuscar" runat="server" style="cursor:pointer" /></td>
            <td><asp:ImageButton runat="server" ID="imgActualizar" ToolTip="Actualizar" 
                ImageUrl="~/img/actualizar.gif" onclick="Images_Click" /></td>
            <td><img runat="server" id="imgExcel" style="cursor:pointer" src="../icons/icon_excel.gif" /></td>
        </tr>
    </table>
    <br />
    <br />
    <table align="center" width="100%">
    <tr>
        <td align="right">Codigo:</td>
        <td>
            <asp:TextBox runat="server" 
                ID="txtCodigo" CssClass="form_input" AutoPostBack="true" ontextchanged="txtCodigo_TextChanged" 
                MaxLength="6" TabIndex="1" Enabled="False" Width="74px"></asp:TextBox></td>
        <td align="right">Cliente: <asp:Label runat="server" ID="lblValidaCliente" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td>
            <asp:DropDownList ID="ddlCliente" runat="server" AutoPostBack="true"
                onselectedindexchanged="ddlCliente_SelectedIndexChanged" TabIndex="2"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right">Reporto: <asp:Label runat="server" ID="lblValidaReporto" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td>
            <asp:DropDownList ID="ddlReporto" runat="server" AutoPostBack="true"
                onselectedindexchanged="ddlReporto_SelectedIndexChanged" TabIndex="3"></asp:DropDownList></td>
        <td align="right">Telefono: <asp:Label runat="server" ID="lblValidaTelefono" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td><asp:TextBox runat="server" ID="txtTelefono" CssClass="form_input" MaxLength="25" TabIndex="4"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right">E-mail: <asp:Label runat="server" ID="lblValidaEmail" Text="(*)" ForeColor="Blue"></asp:Label></td>        
        <td>
            <asp:TextBox Width="180px" runat="server" ID="txtEmail" 
                CssClass="form_input" MaxLength="50" TabIndex="5"></asp:TextBox></td>
        <td align="right">Aplicación: <asp:Label runat="server" ID="lblValidaAplicacion" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td><asp:DropDownList ID="ddlAplicacion" runat="server" AutoPostBack="true"
                onselectedindexchanged="ddlAplicacion_SelectedIndexChanged" TabIndex="6"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right">Menu:</td>
        <td style="width: 56px">
            <asp:DropDownList ID="ddlMenu" runat="server" AutoPostBack="true"
                onselectedindexchanged="ddlMenu_SelectedIndexChanged" TabIndex="7"></asp:DropDownList></td>
        <td align="right">Modulo:</td>
        <td><asp:DropDownList ID="ddlModulo" runat="server" AutoPostBack="true"
                onselectedindexchanged="ddlModulo_SelectedIndexChanged" TabIndex="8"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right">Sub Modulo:</td>        
        <td style="width: 56px">
            <asp:DropDownList ID="ddlSubModulo" runat="server" 
                TabIndex="9"></asp:DropDownList></td>
        <td align="right">Prioridad: <asp:Label runat="server" ID="lblPrioridad" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td><asp:DropDownList ID="ddlPrioridad" runat="server" TabIndex="10"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right">Asunto: <asp:Label runat="server" ID="lblValidaAsunto" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="3"><asp:DropDownList ID="ddlAsunto" runat="server" TabIndex="11"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right">Clasificación: <asp:Label runat="server" ID="lblValidaClasificacion" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="3">
            <asp:DropDownList ID="ddlClasificacion" runat="server" 
                TabIndex="12"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right"  valign="top">Descripcion: <asp:Label runat="server" ID="lblValidaDescripcion" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="3">
            <asp:TextBox Height="50px" Width="96%"  ID="txtDescripcion" 
                runat="server" CssClass="form_input" TextMode="MultiLine" MaxLength="4000" 
                TabIndex="13"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" valign="top" style="white-space:nowrap">En manos de: <asp:Label runat="server" ID="lblValidaManosDe" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td valign="top" style="width: 56px">
            <asp:DropDownList ID="ddlManosde" runat="server" TabIndex="14">
            </asp:DropDownList>
        </td>
        <td align="right" valign="top">Solicitamos:</td>
        <td><asp:TextBox Width="91%"  ID="txtSolicitamos" runat="server" 
                CssClass="form_input" TextMode="MultiLine" MaxLength="4000" TabIndex="15"></asp:TextBox></td>        
    </tr>
</table>
<asp:Panel ID="pnlDetalle" Visible="false" runat="server">
    <table  align="center" width="100%">
        <tr>
            <td align="right" style="width: 76px">Fecha:</td>
            <td colspan="2">
                <asp:Label ID="lblFecha" runat="server"></asp:Label>
            </td>
            <td colspan="2" style="white-space:nowrap">
                Hora:
                <asp:Label ID="lblHora" runat="server"></asp:Label>
            </td>
            <td align="right">
                Estado: <asp:Label ID="lblvalidaEstado" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td>
                <asp:DropDownList ID="ddlEstado" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="7">
                <hr />
            </td>
        </tr>
        <tr>
            <td align="right">Consulto:</td>
            <td colspan="4">
                <asp:DropDownList ID="ddlConsulto" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                Asignar:
            </td>
            <td>
                <asp:DropDownList ID="ddlAsignar" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">Información: <asp:Label ID="lblValidaInformacion" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="6">
                <asp:TextBox ID="txtInformacion" runat="server" CssClass="form_input" 
                    Height="50px" TextMode="MultiLine" Width="96%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <hr />
            </td>
        </tr>
        <tr>
            <td style="width: 76px">
                &nbsp;</td>
            <td align="right" style="white-space:nowrap; width: 86px;">
                <asp:CheckBox ID="chkRecibido" runat="server" Text="Recibieron" 
                    TextAlign="Left" />
            </td>
            <td align="right" colspan="2" style="white-space:nowrap">
                Fecha Recibo:</td>
            <td>
                <asp:TextBox ID="txtFecha" runat="server" CssClass="form_input" Width="50px"></asp:TextBox>
            </td>
            <td align="right">
                Recibido:</td>
            <td>
                <asp:DropDownList ID="ddlRecibio" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <hr />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlUpload" runat="server">
            <table>
                <tr>
                    <td>Archivo:</td>
                    <td><asp:FileUpload ID="fuArchivo" runat="server" CssClass="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" /></td>
                    <td><asp:Button ID="btnAdjuntar" runat="server" Text="Adjuntar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>                    
                </tr>
                <tr>
                    <asp:GridView AutoGenerateColumns="False" ID="gvAdjuntos" runat="server"
                    CellPadding="4" ForeColor="#333333" GridLines="None"  DataKeyNames="ruta"
                        PageSize="15" onrowdatabound="gvAdjuntos_RowDataBound" 
                        onrowcommand="gvAdjuntos_RowCommand" onrowcreated="gvAdjuntos_RowCreated">                   
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <RowStyle CssClass="normalrow" />
                        <AlternatingRowStyle CssClass="alterrow" />
                        <PagerStyle CssClass="cabeza" ForeColor="White"  />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                        <Columns>                                                            
                            <asp:BoundField DataField="Archivo" HeaderText="Archivos Adjuntos"  />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ToolTip="Eliminar Archivo" OnClientClick="Mensaje(26)" runat="server" ID="imgEliminarAdjunto" ImageUrl="../icons/eliminar.gif" CommandName="eliminar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </tr>
            </table>
    </asp:Panel>

    
    <table align="center">
        <tr>
            <td>
                <asp:GridView AutoGenerateColumns="False" ID="gvDetalle" runat="server"
                CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                    PageSize="15" onrowdatabound="gvDetalle_RowDataBound" DataKeyNames="hasFile" >                   
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle CssClass="normalrow" />
                    <AlternatingRowStyle CssClass="alterrow" />
                    <PagerStyle CssClass="cabeza" ForeColor="White"  />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                    <Columns>     
                        <asp:BoundField DataField="CONSECUTIVO" HeaderText="Cons" />
                        <asp:BoundField DataField="FECHA" HeaderText="Fecha" />
                        <asp:BoundField DataField="HORAFIN" HeaderText="Hora" ItemStyle-Wrap="false" >                     
                            <ItemStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="INFORMACION" HeaderText="Informacion"><ItemStyle CssClass="wrapword" /></asp:BoundField> 
                        <asp:BoundField DataField="ASIGNADO" HeaderText="Asignado" />
                        <asp:BoundField DataField="IDESTADO" HeaderText="Estado" />
                        <asp:BoundField DataField="IDRESPONSABLE" HeaderText="Encargado" />
                        <asp:BoundField DataField="" HeaderText="" />                        
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
<table align="center">
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                    <td><asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
                    <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
                    <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcAsunto.aspx')" /></td>                                                                                
                    <td><asp:Button ID="btnActualizar" runat="server" Text="Actualizar" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" style="margin-left: 0px" 
                            Width="64px"   /></td>                     
                    <td>&nbsp;<asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" 
                            OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" 
                            onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" 
                            Visible="False"  /> </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="hdnIdAsunto" runat="server" />
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnHoraInicialDetalle" runat="server" />
<asp:HiddenField ID="hdnHoraInicial" runat="server" />
</asp:Content>

