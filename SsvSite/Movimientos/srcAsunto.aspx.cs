﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_srcAsunto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Casos", this.Page);
            Pendientes();
            Estados est = new Estados();
            Procedimientos.LlenarCombos(ddlBusquedaEstado, new ClinicaCES.Logica.LEstados().EstadoConsultar(est), "CODIGO", "DESCESTADO");
            string script = "if(event.keyCode==13){ __doPostBack('" + imgBuscar.UniqueID + "','') }";
            pnlBusqda.Attributes.Add("onkeypress", script);
            Procedimientos.LlenarCombos(ddlBusquedaAsignado, new ClinicaCES.Logica.LUsuarios().UsuarioVSConsultar(), "USUARIO", "NOMBRE");
            Procedimientos.LlenarCombos(ddlBusquedaPrioridad, new ClinicaCES.Logica.LAsunto().PrioridadListar(), "CODIGO", "PRIORIDAD");
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Pendientes()
    {
        DataSet dsAsunto = new ClinicaCES.Logica.LAsunto().ConsultarPendientes(null,null);
        DataTable dtAsunto = dsAsunto.Tables[0];
        DataTable dtPagina = Procedimientos.dtFiltrado("Orden","IDESTADO = 'PE'",dtAsunto);
        ViewState["dtAsunto"] = dtAsunto;
        ViewState["dtPaginas"] = dtPagina;

        Procedimientos.LlenarGrid(dtPagina, gvAsunto);
        txtNumReg.Text = Convert.ToString(dtPagina.Rows.Count);
    }


    private void Consultar()
    {
        DataSet dsAsunto = new ClinicaCES.Logica.LAsunto().AsuntoConsultar(null);
        DataTable dtAsunto = dsAsunto.Tables[0];
        ViewState["dtAsunto"] = dtAsunto;
        ViewState["dtPaginas"] = dtAsunto;
        Procedimientos.LlenarGrid(dtAsunto, gvAsunto);
        txtNumReg.Text = Convert.ToString(dtAsunto.Rows.Count);
    }

    protected void gvAsunto_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvAsunto.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addAsunto.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
                //e.Row.Cells[4].Text = e.Row.Cells[4].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            }

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            //CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            //bool activo = bool.Parse(gvAsunto.DataKeys[e.Row.RowIndex]["ACTIVO"].ToString());
            //if (!activo)
            //    chkPais.Enabled = false;

            //chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlAsunto, string Usuario)
    {
        //string msg = "3";
        //if (new ClinicaCES.Logica.LAsunto().AsuntoRetirar(xmlAsunto, Usuario))
        //{
        //    msg = "7";

        //    Asunto cli = new Asunto();
        //    DataTable dtAsunto = new ClinicaCES.Logica.LAsunto().ClienteConsultar(cli);
        //    ViewState["dtAsunto"] = dtAsunto;
        //    Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        //}

        //Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlcli()
    {
        string cli;
        string tipo;
        string xmlPais = "<Raiz>";

        for (int i = 0; i < gvAsunto.Rows.Count; i++)
        {
            GridViewRow row = gvAsunto.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                cli = row.Cells[1].Text.Trim();
                tipo = gvAsunto.DataKeys[i]["TIPO"].ToString();
                xmlPais += "<Datos CLIENTE='" + cli + "' TIPO='" + tipo + "' />";
            }
        }

        return xmlPais += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "IDASUNTO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND NOMCOMCIAL LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        if (pnlEstado.Visible && ddlBusquedaEstado.SelectedIndex != 0)
            filtro += " AND IDESTADO = '" + ddlBusquedaEstado.SelectedValue + "'";
        if (pnlPrioridad.Visible && ddlBusquedaPrioridad.SelectedIndex != 0)
            filtro += " AND IDPRIORIDAD = '" + ddlBusquedaPrioridad.SelectedValue + "'";
        if (pnlAsignado.Visible && ddlBusquedaAsignado.SelectedIndex != 0)
        {
            string[] usr = ddlBusquedaAsignado.SelectedValue.Split('|');
            filtro += " AND IDASIGNADO = '" + usr[0] + "'";
        }
            

        DataTable dtAsunto = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtAsunto"]);
        Procedimientos.LlenarGrid(dtAsunto, gvAsunto);

        ViewState["dtPaginas"] = dtAsunto;

        if (pnlCodigo.Visible)
            kitarFiltro("IDASUNTO", "Caso", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMCOMCIAL", "Comercializador", pnlNombre, txtBusquedaNombre);
        if (pnlEstado.Visible)
            kitarFiltro("IDESTADO", "Estado", pnlEstado, ddlBusquedaEstado);
        if (pnlPrioridad.Visible)
            kitarFiltro("IDPRIORIDAD", "Prioridad", pnlPrioridad, ddlBusquedaPrioridad);
        if (pnlAsignado.Visible)
            kitarFiltro("IDASIGNADO", "Asignado", pnlAsignado, ddlBusquedaAsignado);

        txtNumReg.Text = Convert.ToString(dtAsunto.Rows.Count);
    }


    protected void gvAsunto_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAsunto.PageIndex = e.NewPageIndex;
        gvAsunto.DataSource = ViewState["dtPaginas"];
        gvAsunto.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addAsunto.aspx");
        else if (sender.Equals(imgActualizar))
        {
            Response.Redirect("addAsunto.aspx?id=A");
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("IDASUNTO", "Caso", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMCOMCIAL", "Comercializador", pnlNombre, txtBusquedaNombre);
        if (pnlEstado.Visible)
            kitarFiltro("IDESTADO", "Estado", pnlEstado, ddlBusquedaEstado);
        if (pnlEstado.Visible)
            kitarFiltro("IDPRIORIDAD", "Prioridad", pnlPrioridad, ddlBusquedaPrioridad);
        if (pnlAsignado.Visible)
            kitarFiltro("IDASIGNADO", "Asignado", pnlAsignado, ddlBusquedaAsignado);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
        {
            if (ddlFlitro.Items.Count > 0)
                AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        }

        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("IDASUNTO", "Caso", pnlCodigo, txtBusquedaCod);
        else if (sender.Equals(lnkNombreKitar))
            kitarFiltro("NOMCOMCIAL", "Comercializador", pnlNombre, txtBusquedaNombre);
        else if(sender.Equals(lnkEstadoKitar))
            kitarFiltro("IDESTADO", "Estado", pnlEstado, ddlBusquedaEstado);
        else if (sender.Equals(lnkPrioridadKitar))
            kitarFiltro("IDPRIORIDAD", "Prioridad", pnlPrioridad, ddlBusquedaPrioridad);
        else if (sender.Equals(lnkAsignadoKitar))
            kitarFiltro("IDASIGNADO", "Asignado", pnlAsignado, ddlBusquedaAsignado);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void kitarFiltro(string value, string text, Panel pnl, DropDownList ddl)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        //txt.Text = string.Empty;
        ddl.SelectedIndex = 0;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMCOMCIAL":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            case "IDESTADO":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlEstado.Visible = true;
                    break;
                }
            case "IDPRIORIDAD":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlPrioridad.Visible = true;
                    break;
                }
            case "IDASIGNADO":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlAsignado.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }

    public DataTable dtExcel()
    {
        string[] titulo = { "#", "Fecha", "Aplicacion", "Comercializador", "Descripcion", "Estado", "Responsable","Asignado" };
        string[] columnas = { "Orden", "FECHA", "IDAPLICACION", "NOMCOMCIAL", "DESCRIPCION", "DESCESTADO", "ENCARGADO","ASIGNADO" };
        string[] valores = new string[8];
        DataTable dt = new DataTable();
        DataTable dtPagina = (DataTable)ViewState["dtPaginas"];
                
        foreach (DataRow row in dtPagina.Rows)
        {
            for (int i = 0; i < titulo.Length; i++)
            {
                valores[i] = row[columnas[i]].ToString();                
            }

            Procedimientos.CrearDatatable(titulo, valores, dt);
                
        }

        return dt;
    }
    protected void Excel_Click(object sender, ImageClickEventArgs e)
    {
        Procedimientos.ExportarDataTable(dtExcel(), "Casos");
    }
}
