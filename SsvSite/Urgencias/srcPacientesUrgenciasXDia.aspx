﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusquedaPacUrg.aspx.cs" Inherits="Urgencias_BusquedaPacUrg" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcPacientesUrgenciasXDia.aspx.cs" Inherits="Urgencias_srcPacientesUrgenciasXDia" Title="Busqueda pacientes por urgencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <table align="center" style="width: 328px">
        <tr>
            <td style="width: 43px" align="center">
                Desde:</td>
            <td style="width: 346px" align="center">
                    <asp:TextBox ID="txtFecha" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px" ontextchanged="txtFecha_TextChanged"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                        runat="server" TargetControlID="txtFecha" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" ID="CalendarExtender1" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFecha" PopupButtonID="img1"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img1" __designer:mapid="766" />
            </td>
            <td style="width: 53px" align="center">
                Hasta:</td>
            <td style="width: 370px" align="center">
                    <asp:TextBox ID="txtFechaF" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px" ontextchanged="txtFechaF_TextChanged1"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="txtFechaF_FilteredTextBoxExtender" 
                        runat="server" TargetControlID="txtFechaF" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" 
                    ID="txtFechaF_CalendarExtender" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFechaF" PopupButtonID="img2"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img2" __designer:mapid="766" /></td>
        </tr>
        <tr>
            <td colspan="2">
                Tipo Identificacion</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlTipoId" runat="server" Height="20px" Width="151px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Identificacion</td>
            <td colspan="2">
                <asp:TextBox ID="txtId" runat="server" Width="143px" CssClass="form_input"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />
            </td>
        </tr>
    </table>
    
    <table align="center">
        <tr>
            <td align="center">
    <asp:Panel ID="pnlInforme" runat="server" Visible="false">
    
        <table border="1">
        </table>
        <table align="center">
            <tr>
                <td>
                    <asp:GridView AutoGenerateColumns="False" ID="gvInforme" runat="server" AllowPaging="True" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="20" pa
                        onrowdatabound="gvInforme_RowDataBound"
                        onpageindexchanging="gvInforme_PageIndexChanging" Width="551px" EnableModelValidation="True">                   
                        <FooterStyle BackColor="#F7DFB5" />
                        <RowStyle CssClass="normalrow"/>
                        <AlternatingRowStyle CssClass="alterrow" />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White"  />                
                        <Columns>      
                             <asp:BoundField DataField="REQUIERE_ANEXO2" HeaderText="REQUIERE_ANEXO" />
                             <asp:BoundField DataField="IDENTIFICACION" HeaderText="IDENTIFICACION" />
                             <asp:BoundField DataField="PACIENTE" HeaderText="PACIENTE" />   
                             <asp:BoundField DataField="CONVENIO" HeaderText="CONVENIO" />
                             <asp:BoundField DataField="FECHA_INGRESO" HeaderText="FECHA_INGRESO" />
                             <%--<asp:BoundField DataField="SOLICITUD" HeaderText="SOLICITUD" />     --%>
                             <asp:BoundField DataField="DIASHOSP" HeaderText="DIASHOSP" Visible="False" />     
                             <asp:BoundField DataField="ATE_PRE_NUMERFORMU" HeaderText="ATE_PRE_NUMERFORMU" />     
                             <asp:BoundField DataField="ATE_PRE_TIPOFORMU" HeaderText="ATE_PRE_TIPOFORMU" Visible="False" />       
                             <asp:BoundField DataField="PAC_PAC_NUMERO" HeaderText="PAC_PAC_NUMERO" Visible="False" />            
                                            
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblDescripcion" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>