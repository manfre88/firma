﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcEtapas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Etapas", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("ETAPAID", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "Etapa", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void Consultar()
    {
        Etapas etapa = new Etapas();
        DataTable dtEta = new ClinicaCES.Logica.LEtapas().EtapasConsultar(etapa);
        ViewState["dtEta"] = dtEta;
        ViewState["dtPaginas"] = dtEta;
        Procedimientos.LlenarGrid(dtEta, gvEtapas);
    }

    protected void gvEtapas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvEtapas.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addEtapas.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkClasi = (CheckBox)e.Row.FindControl("chkEliminar");

            chkClasi.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlEta, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LEtapas().EtapasRetirar(xmlEta, Usuario))
        {
            msg = "7";

            Etapas etapa = new Etapas();
            DataTable dtEta = new ClinicaCES.Logica.LEtapas().EtapasConsultar(etapa);
            ViewState["dtEta"] = dtEta;
            Filtrar();
        }
        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "ETAPAID LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND NOMBRE LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtEta = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtEta"]);
        Procedimientos.LlenarGrid(dtEta, gvEtapas);

        ViewState["dtPaginas"] = dtEta;

        if (pnlCodigo.Visible)
            kitarFiltro("ETAPAID", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "Etapa", pnlNombre, txtBusquedaNombre);
    }

    protected void gvEtapas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEtapas.PageIndex = e.NewPageIndex;
        gvEtapas.DataSource = ViewState["dtPaginas"];
        gvEtapas.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlEta(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addEtapas.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private string xmlEta()
    {
        string etapa;
        string xmlActi = "<Raiz>";

        for (int i = 0; i < gvEtapas.Rows.Count; i++)
        {
            GridViewRow row = gvEtapas.Rows[i];
            CheckBox chkActi = (CheckBox)row.FindControl("chkEliminar");
            if (chkActi.Checked)
            {
                etapa = row.Cells[1].Text.Trim();
                xmlActi += "<Datos ETAPAID='" + etapa + "' />";
            }
        }

        return xmlActi += "</Raiz>";
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("ETAPAID", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("NOMBRE", "Etapa", pnlNombre, txtBusquedaNombre);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMBRE":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }

}
