﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcActividades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Actividades", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("ACTIVIDADID", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("ACTIVIDAD_NOMBRE", "Actividad", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void Consultar()
    {
        Actividades acti = new Actividades();
        DataTable dtActi = new ClinicaCES.Logica.LMActividades().ActividadesConsultar(acti);
        ViewState["dtActi"] = dtActi;
        ViewState["dtPaginas"] = dtActi;
        Procedimientos.LlenarGrid(dtActi, gvActividades);
    }

    protected void gvActividades_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvActividades.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addActividades.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkClasi = (CheckBox)e.Row.FindControl("chkEliminar");
            //bool activo = bool.Parse(gvActividades.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            //if (!activo)
            //    chkClasi.Enabled = false;

            chkClasi.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlActi, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LMActividades().ActividadesRetirar(xmlActi, Usuario))
        {
            msg = "7";

            Actividades acti = new Actividades();
            DataTable dtActi = new ClinicaCES.Logica.LMActividades().ActividadesConsultar(acti);
            ViewState["dtActi"] = dtActi;
            Filtrar();
        }
        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "ACTIVIDADID LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND ACTIVIDAD_NOMBRE LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtActi = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtActi"]);
        Procedimientos.LlenarGrid(dtActi, gvActividades);

        ViewState["dtPaginas"] = dtActi;

        if (pnlCodigo.Visible)
            kitarFiltro("ACTIVIDADID", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("ACTIVIDAD_NOMBRE", "Actividad", pnlNombre, txtBusquedaNombre);
    }

    protected void gvActividades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvActividades.PageIndex = e.NewPageIndex;
        gvActividades.DataSource = ViewState["dtPaginas"];
        gvActividades.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlActi(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addActividades.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private string xmlActi()
    {
        string acti;
        string xmlActi = "<Raiz>";

        for (int i = 0; i < gvActividades.Rows.Count; i++)
        {
            GridViewRow row = gvActividades.Rows[i];
            CheckBox chkActi = (CheckBox)row.FindControl("chkEliminar");
            if (chkActi.Checked)
            {
                acti = row.Cells[1].Text.Trim();
                xmlActi += "<Datos ACTIVIDADID='" + acti + "' />";
            }
        }

        return xmlActi += "</Raiz>";
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("ACTIVIDADID", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("ACTIVIDAD_NOMBRE", "Actividades", pnlNombre, txtBusquedaNombre);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "ACTIVIDAD_NOMBRE":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
