﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcMenu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        litScript.Text = string.Empty;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Menu", this.Page);
            EstadoInicial();
            string script = "document.getElementById('" + imgRetirar.ClientID + "').disabled = true;";
            Procedimientos.Script(script, litScript);
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        
    }

    private void Consultar()
    {
        MMenu app = new MMenu();
        DataTable dtMenu = new ClinicaCES.Logica.LMenu().MenuConsultar(app);
        ViewState["dtMenu"] = dtMenu;
        ViewState["dtPaginas"] = dtMenu;
        Procedimientos.LlenarGrid(dtMenu, gvMenu);
    }

    protected void gvMenu_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvMenu.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addMenu.aspx?id=" + e.Row.Cells[1].Text.Trim() + "&app=" + e.Row.Cells[3].Text.Trim() + "')");
                e.Row.Cells[4].Text = e.Row.Cells[4].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            }

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvMenu.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LMenu().MenuRetirar(xmlPais,Usuario))
        {
            msg = "7";

            MMenu MEN = new MMenu();
            DataTable dtMenu = new ClinicaCES.Logica.LMenu().MenuConsultar(MEN);
            ViewState["dtMenu"] = dtMenu;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlMenu()
    {
        string app;
        string menu;
        string xmlPais = "<Raiz>";

        for (int i = 0; i < gvMenu.Rows.Count; i++)
        {
            GridViewRow row = gvMenu.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                menu = row.Cells[1].Text.Trim();
                app = row.Cells[3].Text.Trim();
                xmlPais += "<Datos APP='" + app + "' MENU='" + menu + "' />";
            }
        }

        return xmlPais += "</Raiz>";
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND MENU LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtMenu = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtMenu"]);
        Procedimientos.LlenarGrid(dtMenu, gvMenu);

        ViewState["dtPaginas"] = dtMenu;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "Pais", pnlNombre, txtBusquedaNombre);
    }

    protected void gvMenu_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMenu.PageIndex = e.NewPageIndex;
        gvMenu.DataSource = ViewState["dtPaginas"];
        gvMenu.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlMenu(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addMenu.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "Pais", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("NOMBRE", "Pais", pnlNombre, txtBusquedaNombre);
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMBRE":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
