﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addAsigCamas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Administración de asignación de camas ", this.Page);

            ConsultarAsigCamas();
          
        }
        litScript.Text = string.Empty;
    }
    private void ConsultarAsigCamas()
    {
        DataTable dtGrid = llenardt();
        Procedimientos.LlenarGrid(dtGrid, gvAsigCamas);
        //ViewState["dtPaginas"] = dtGrid;
        //ViewState["dtPaginas2"] = dtGrid;
    }
    protected DataTable llenardt()
    {
        DataTable dtGrid = new ClinicaCES.Logica.LMaestros().ConsultarAsigCamaServicios(); //tipo 2 son las prestaciones no vigentes en oracle
        //ViewState["dtPaginas"] = dtGrid;
        //ViewState["dtPaginas2"] = dtGrid;
        return dtGrid;
    }
    protected void gvAsigCamas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string customerId = gvAsigCamas.DataKeys[e.Row.RowIndex].Value.ToString();
            GridView gvOrders = e.Row.FindControl("gvOrders") as GridView;
            DataTable dtGrid = ConsultarAsigCama(customerId, e.Row.RowIndex);
            
            ViewState["dtPaginas"] = dtGrid;
            ViewState["dtPaginas2"] = dtGrid;
            gvOrders.DataSource = dtGrid;
            gvOrders.DataBind();
            //cargarGrid(dtGrid, gvOrders);
        }
    }
    protected void cargarGrid(DataTable dtGrid, GridView gvOrders)
    {
        foreach (GridViewRow row in gvOrders.Rows)
        {
            DropDownList ddlResponsable = row.FindControl("ddlResponsable") as DropDownList;
            Procedimientos.LlenarCombos(ddlResponsable, new ClinicaCES.Logica.LUsuarios().ResponsableConsultar(), "ID", "NOMBRE");
            int v = row.RowIndex;
            DataRow valor = dtGrid.Rows[v];
            //if (gvFormu.DataKeys[e.Row.RowIndex]["IDRECIBE"].ToString() != "")
            //{
            //    ddlResponsable.SelectedValue = valor["RESPONSABLE"].ToString();
            //}
        }
    }
    protected void gvOrders_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView gvOrders = (GridView)sender;
            LinkButton Editar = e.Row.FindControl("Editar") as LinkButton;

            if (gvOrders.DataKeys[e.Row.RowIndex]["ID"].ToString() != "")
            {
                Editar.Visible = true;
            }
            else
            {
                Editar.Visible = false;
            }
        }
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
    }
    protected void gvOrders_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string msg = "";
        GridView gvOrders = (GridView)sender;
        
        if (gvOrders.Rows[e.RowIndex].Cells[1].Text != "&nbsp;")
        {
            //DropDownList ddlProfesionales = (DropDownList)gvOrders.Rows[e.RowIndex].Cells[4].FindControl("ddlResponsable");
            lblCama.Text = gvOrders.Rows[e.RowIndex].Cells[0].Text;
            DataRow dr = new ClinicaCES.Logica.LMaestros().ConsultarCamaServicio(lblCama.Text).Rows[0];
            Label2.Text = dr[0].ToString();
            //string valor = gvOrders.Rows[e.RowIndex].Cells[1].Text;
            int Suma = 0;
            DataTable dtGrid = new ClinicaCES.Logica.LUsuarios().ResponsableConsultar();
            Procedimientos.LlenarGrid(dtGrid, gvInforme);
            for (int i = 0; i <= gvInforme.Rows.Count - 1; i++)
            {
                LinkButton Editar = (LinkButton)gvInforme.Rows[i].Cells[2].FindControl("Editar");
                if (gvOrders.Rows[e.RowIndex].Cells[4].Text != "&nbsp;")
                {
                    lblResActual.Visible = true;
                    lblResponsable.Visible = true;
                    lblResponsable.Text = gvOrders.Rows[e.RowIndex].Cells[4].Text;
                    Editar.Text = "Actualizar";

                }
                else
                {
                    lblResActual.Visible = false;
                    lblResponsable.Visible = false;
                    lblResponsable.Text = gvOrders.Rows[e.RowIndex].Cells[4].Text;
                    Editar.Text = "Seleccionar";

                }
                Suma = Suma + 22;
            }
            Panel1.Width = 565;
            Panel1.Height = 190 + Suma;
            ModalPopupExtender1.Show();
        }
        else
        {
            msg = "79";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
        //if (ddlProfesionales.SelectedIndex != 0)
        //{
        //    string IdResponsable = ddlProfesionales.SelectedValue.ToString();
        //    if (new ClinicaCES.Logica.LMaestros().ResponsableActualizar(gvOrders.Rows[e.RowIndex].Cells[0].Text, IdResponsable, Session["Nick"].ToString()))
        //    {
        //        msg = "1";
        //    }
        //    else
        //    {
        //        msg = "3";
        //    }
            
        //    Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        //}
        //else
        //{
        //    
        //}
    }
    protected DataTable ConsultarAsigCama(string customerId, int campo)
    {
        DataTable dtOracle = new ClinicaCES.Logica.LMaestros().ConsultarAsigCamaObjetos(customerId);
        //DataTable dtSql = new ClinicaCES.Logica.LMaestros().ConsultarAsigCamaObjetosSql();
        //DataTable dtGrid = new DataTable();
        //if (dtSql.Rows.Count > 0)
        //{
        //    dtGrid.Columns.Add(new DataColumn("CODIGO", System.Type.GetType("System.String")));
        //    dtGrid.Columns.Add(new DataColumn("OBJETO", System.Type.GetType("System.String")));
        //    dtGrid.Columns.Add(new DataColumn("ID", System.Type.GetType("System.String")));
        //    dtGrid.Columns.Add(new DataColumn("PACIENTE", System.Type.GetType("System.String")));
        //    dtGrid.Columns.Add(new DataColumn("SER_OBJ_ESTADO", System.Type.GetType("System.String")));
        //    dtGrid.Columns.Add(new DataColumn("CONVENIO", System.Type.GetType("System.String")));
        //    dtGrid.Columns.Add(new DataColumn("RESPONSABLE", System.Type.GetType("System.String")));
        //    dtGrid.Columns.Add(new DataColumn("SERVICIO", System.Type.GetType("System.String")));
            
        //    for (int i = 0; i < dtOracle.Rows.Count; i++)
        //    {
        //        DataRow fila = dtGrid.NewRow();
        //        foreach (DataRow fila2 in dtSql.Rows)
        //        {
        //            if (dtOracle.Rows[i]["CODIGO"].ToString().Trim() == fila2[1].ToString().Trim())
        //            {
                        
        //                fila["CODIGO"] = dtOracle.Rows[i]["CODIGO"];
        //                fila["OBJETO"] = dtOracle.Rows[i]["OBJETO"];
        //                fila["ID"] = dtOracle.Rows[i]["ID"];
        //                fila["PACIENTE"] = dtOracle.Rows[i]["PACIENTE"];
        //                fila["SER_OBJ_ESTADO"] = dtOracle.Rows[i]["SER_OBJ_ESTADO"];
        //                fila["CONVENIO"] = dtOracle.Rows[i]["CONVENIO"];
        //                fila["RESPONSABLE"] = fila2[2].ToString();
        //                fila["SERVICIO"] = dtOracle.Rows[i]["SERVICIO"];
        //                break;
        //            }
        //            else
        //            {
                        
        //                fila["CODIGO"] = dtOracle.Rows[i]["CODIGO"];
        //                fila["OBJETO"] = dtOracle.Rows[i]["OBJETO"];
        //                fila["ID"] = dtOracle.Rows[i]["ID"];
        //                fila["PACIENTE"] = dtOracle.Rows[i]["PACIENTE"];
        //                fila["SER_OBJ_ESTADO"] = dtOracle.Rows[i]["SER_OBJ_ESTADO"];
        //                fila["CONVENIO"] = dtOracle.Rows[i]["CONVENIO"];
        //                fila["RESPONSABLE"] = dtOracle.Rows[i]["RESPONSABLE"];
        //                fila["SERVICIO"] = dtOracle.Rows[i]["SERVICIO"];
                        
        //            }
        //        }
        //        dtGrid.Rows.Add(fila);
        //    }
        //}
        //else
        //{
          //  dtGrid = dtOracle;
        //}
            ViewState["dtPaginas"] = dtOracle;
            ViewState["dtPaginas2"] = dtOracle;
            return dtOracle;
    }
    //protected void gvOrders_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GridView gvOrders = gvAsigCamas.Rows[0].FindControl("gvOrders") as GridView;
    //    gvOrders.PageIndex = e.NewPageIndex;
    //    gvOrders.DataSource = ViewState["dtPaginas"];
    //    gvOrders.DataBind();
    //}
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        ConsultarAsigCamas();
    }
    //protected void ddlResponsable_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DropDownList ddl = (DropDownList)sender;
    //    Session["ValorDDL"] = ddl.SelectedValue;
    //}
   
    protected void gvInforme_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string msg = "";
        string IdResponsable = gvInforme.Rows[e.RowIndex].Cells[0].Text;
        bool bandera = chkServicio.Checked;
        if (bandera == true)
        {
            DataTable dt = new ClinicaCES.Logica.LMaestros().ConsultarServicioCama(Label2.Text);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (new ClinicaCES.Logica.LMaestros().ResponsableActualizarC(dr[0].ToString(), IdResponsable, Session["Nick1"].ToString()))
                    {
                        msg = "1";
                    }
                    else
                    {
                        msg = "3";
                    }
                }
            }
        }
        else
        {
            if (new ClinicaCES.Logica.LMaestros().ResponsableActualizarC(lblCama.Text, IdResponsable, Session["Nick1"].ToString()))
            {
                msg = "1";
            }
            else
            {
                msg = "3";
            }
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        ConsultarAsigCamas();
    }
}