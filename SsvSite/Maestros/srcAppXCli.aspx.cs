﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcAppXCli : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Aplicación X Cliente", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Consultar()
    {
        AppXCli cli = new AppXCli();
        DataTable dtAppXCli = new ClinicaCES.Logica.LAppXCli().AppXCliBuscar(cli);
        ViewState["dtAppXCli"] = dtAppXCli;
        ViewState["dtPaginas"] = dtAppXCli;
        Procedimientos.LlenarGrid(dtAppXCli, gvAppXCli);
    }

    protected void gvAppXCli_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvAppXCli.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addAppXCli.aspx?nit=" + e.Row.Cells[1].Text.Trim() + "&neg=" + gvAppXCli.DataKeys[e.Row.RowIndex]["NEGOCIO"].ToString() + "&app=" + gvAppXCli.DataKeys[e.Row.RowIndex]["IDAPLICACION"].ToString() + "')");
            }

            e.Row.Cells[4].Text = e.Row.Cells[4].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            //bool activo = bool.Parse(gvAppXCli.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            //if (!activo)
            //    chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlAppXCli, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LAppXCli().AppXCliRetirar(xmlAppXCli, Usuario))
        {
            msg = "7";

            AppXCli cli = new AppXCli();
            DataTable dtAppXCli = new ClinicaCES.Logica.LAppXCli().AppXCliBuscar(cli);
            ViewState["dtAppXCli"] = dtAppXCli;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlcli()
    {
        string neg;
        string nit;
        string app;
        string xmlPais = "<Raiz>";

        for (int i = 0; i < gvAppXCli.Rows.Count; i++)
        {
            GridViewRow row = gvAppXCli.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                nit = row.Cells[1].Text.Trim();
                neg = gvAppXCli.DataKeys[i]["NEGOCIO"].ToString();
                app = gvAppXCli.DataKeys[i]["IDAPLICACION"].ToString();
                xmlPais += "<Datos NIT='" + nit + "' NEGOCIO='" + neg + "' APLICACION='" + app + "' />";
            }
        }

        return xmlPais += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "NOMBRE LIKE '*" + txtBusquedaApp.Text.Trim() + "*'  AND NOMCOMCIAL LIKE '*" + txtBusquedaCliente.Text.Trim() + "*'";
        //string filtro = "NOMBRE LIKE '*" + txtBusquedaApp.Text.Trim() + "*'  AND IDAPLICACION LIKE '*" + txtBusquedaApp.Text.Trim() + "*'  AND NIT LIKE '*" + txtBusquedaCliente.Text.Trim() + "*' AND NOMCOMCIAL LIKE '*" + txtBusquedaCliente.Text.Trim() + "*'";
        DataTable dtAppXCli = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtAppXCli"]);
        Procedimientos.LlenarGrid(dtAppXCli, gvAppXCli);

        ViewState["dtPaginas"] = dtAppXCli;

        if (pnlApp.Visible)
            kitarFiltro("NOMBRE", "Aplicación", pnlApp, txtBusquedaApp);
        if (pnlCliente.Visible)
            kitarFiltro("NOMCOMCIAL", "Cliente", pnlCliente, txtBusquedaCliente);
    }


    protected void gvAppXCli_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAppXCli.PageIndex = e.NewPageIndex;
        gvAppXCli.DataSource = ViewState["dtPaginas"];
        gvAppXCli.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlcli(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addAppXCli.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlApp.Visible)
            kitarFiltro("NOMBRE", "Aplicación", pnlApp, txtBusquedaApp);
        if (pnlCliente.Visible)
            kitarFiltro("NOMCOMCIAL", "Cliente", pnlCliente, txtBusquedaCliente);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
        {
            if (ddlFlitro.Items.Count > 0)
                AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        }

        else if (sender.Equals(lnkAppKitar))
            kitarFiltro("NOMBRE", "Aplicación", pnlApp, txtBusquedaApp);
        if (sender.Equals(lnkClienteKitar))
            kitarFiltro("NOMCOMCIAL", "Cliente", pnlCliente, txtBusquedaCliente);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMCOMCIAL":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCliente.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlApp.Visible = true;
                    break;
                }
        }
    }
}
