﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addAgruparActividades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListarGrupo();
            Procedimientos.Titulo("Asignar Actividades al Grupo", this.Page);
        }
        litScript.Text = string.Empty;
    }

    private void ListarGrupo()
    {
        Etapas eta = new Etapas();

        DataTable dtEtapa = new ClinicaCES.Logica.LEtapas().EtapasConsultar(eta);

        Procedimientos.LlenarCombos(ddlEtapas, dtEtapa, "ETAPAID", "NOMBRE");
    }

    private void ConsultarAct(string etapa)
    {
        Etapas eta = new Etapas();
        eta.Codigo = etapa;

        DataTable dtEtapa = new ClinicaCES.Logica.LEtapas().ActConsultar(eta);

        Procedimientos.LlenarGrid(dtEtapa, gvActividades);
    }


    protected void Click_Botones(object sender, EventArgs e)
    {
        Guardar(xmlPages(), ddlEtapas.SelectedValue);
    }


    private string xmlPages()
    {
        string xmlPagi = "<Raiz>";

        for (int i = 0; i < gvActividades.Rows.Count; i++)
        {
            GridViewRow row = gvActividades.Rows[i];
            CheckBox chkEstado = (CheckBox)row.FindControl("chkPages");
            if (chkEstado.Checked)
                xmlPagi += "<Datos CODACT = '" + gvActividades.DataKeys[i]["ACTIVIDADID"].ToString() + "' />";
        }
        return xmlPagi += "</Raiz>";
    }

    private void Guardar(string xmlPaginas, string Etapa)
    {
        string msg = "1";
        if (new ClinicaCES.Logica.LEtapas().ActXGrupoActualizar(xmlPaginas, Etapa))
        {
            //Guardo
            ConsultarAct(ddlEtapas.SelectedValue);
        }
        else
        {
            msg = "3";
        }

        Procedimientos.Script("Mensaje(" + msg + "); location.href = 'addAgruparActividades.aspx'", litScript);
    }

    protected void ddlEtapas_SelectedIndexChanged(object sender, EventArgs e)
    {
        ConsultarAct(ddlEtapas.SelectedValue);
    }

    protected void gvActividades_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chkEstado = (CheckBox)e.Row.FindControl("chkPages");
            //chkEstado.Checked = bool.Parse(gvActividades.DataKeys[e.Row.RowIndex]["Estado"].ToString());
        }
    }


}
