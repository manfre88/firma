﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcAppXCli.aspx.cs" Inherits="Maestros_srcAppXCli" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table>
    <tr>
        <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
        <td><asp:ImageButton runat="server" ID="imgRetirar" Enabled="false"  
                ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" 
                onclientclick="return Mensaje(11)" /></td>
        <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" ImageUrl="../icons/cancel.png" onclick="Images_Click" /></td>
        <td><asp:ImageButton runat="server" ID="imgBuscar" ToolTip="Buscar" ImageUrl="../icons/magnify.png" onclick="Images_Click" /></td>
    </tr>     
</table>
<hr />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <table align="center">
        <tr>
            <td>Filtro:</td>
            <td><asp:DropDownList onkeydown="tabular(event,this)"  ID="ddlFlitro" runat="server">
                <asp:ListItem Value="NOMCOMCIAL">Cliente</asp:ListItem>
                <asp:ListItem Value="NOMBRE">Aplicación</asp:ListItem>
                </asp:DropDownList></td>    
            <td><asp:LinkButton ID="lnkAgregar" runat="server" Text="Agregar" onclick="lnkAgregar_Click"></asp:LinkButton></td> 
        </tr>    
        <tr>
            <asp:Panel ID="pnlCliente" runat="server" Visible="false">
                <td>Cliente:</td>
                <td colspan="2"><asp:TextBox onkeydown="tabular(event,this)"  CssClass="form_input" ID="txtBusquedaCliente" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkClienteKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlApp" runat="server" Visible="false">
                <td>Aplicación:</td>
                <td colspan="2"><asp:TextBox onkeydown="tabular(event,this)"  CssClass="form_input" ID="txtBusquedaApp" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkAppKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>

<table align="center">
    <tr>
        <td colspan="3" align="center">                
            <asp:GridView AutoGenerateColumns="False" ID="gvAppXCli" runat="server" AllowPaging="True" 
            CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" PageSize="15" 
                onrowdatabound="gvAppXCli_RowDataBound" DataKeyNames="ESTADO,TIPO,NEGOCIO,IDAPLICACION"
                onpageindexchanging="gvAppXCli_PageIndexChanging">                   
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <RowStyle CssClass="normalrow" />
                <AlternatingRowStyle CssClass="alterrow" />
                <PagerStyle CssClass="cabeza" ForeColor="White"  />
                <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                <Columns>     
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkEliminar" runat="server"  />
                            <%--Enabled='<%# Bind("vlrCheck") %>'--%>
                        </ItemTemplate>
                        <HeaderTemplate>
                            <input type="checkbox" onclick="CheckAllCheckBoxesMaestros(this.checked,'<%= imgRetirar.ClientID %>',this)" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>   
                     <asp:BoundField DataField="NIT" HeaderText="Nit" />
                     <asp:BoundField DataField="NOMCOMCIAL" HeaderText="Cliente" />
                     <asp:BoundField DataField="NOMBRE" HeaderText="Aplicacion" />                     
                    <asp:BoundField DataField="ESTADO" HeaderText="Estado" />
                </Columns>
            </asp:GridView> 
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

