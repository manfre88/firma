﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addCargos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Cargos", this.Page);
            ListaroModulo();
            if (Request.QueryString["id"] != null)
                Consultar(Request.QueryString["area"], Request.QueryString["id"]);

            string script =
                "if(document.getElementById('" + ddlArea.ClientID + "').value != '-1' && trim(document.getElementById('" + txtCodigo.ClientID + "').value) != ''){" +
                    "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            ddlArea.Attributes.Add("onblur", script);
            txtCodigo.Attributes.Add("onblur", script);
        }
        litScript.Text = string.Empty;
    }

    private void Guardar(string Area, string Codigo, string Desc, string Usr,string vlrHora)
    {
        if (!ValidarGuardar())
            return;

        Cargos car = new Cargos();

        car.Codigo = Codigo;
        car.Area = Area;
        car.Nombre = Desc;
        car.Usuario = Usr;
        car.VlrHora = vlrHora;

        string msg = "3";

        if (new ClinicaCES.Logica.LCargos().CargoActualizar(car))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addCargos.aspx?id=" + Codigo + "&area=" + Area + "')", litScript);

    }

    private void CambiarEstado(string Codigo, string Area, bool? Estado)
    {
        Cargos car = new Cargos();

        car.Estado = Estado;
        car.Area = Area;
        car.Codigo = Codigo;

        string msg = "3";

        if (new ClinicaCES.Logica.LCargos().CargoEstado(car))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addCargos.aspx?id=" + Codigo + "&area=" + Area + "')", litScript);
    }

    private void Consultar(string Area, string codigo)
    {
        Cargos car = new Cargos();
        car.Area = Area;
        car.Codigo = codigo;

        DataTable dt = new ClinicaCES.Logica.LCargos().CargoConsultar(car);

        txtCodigo.Text = codigo;
        ddlArea.SelectedValue = Area;

        btnGuardar.Enabled = true;
        btnRetirar.Enabled = false;
        btnRetirar.Visible = true;
        btnReactivar.Visible = false;
        txtNombre.Enabled = true;
        txtVlrHora.Enabled = true;
        txtCodigo.Enabled = false;
        ddlArea.Enabled = false;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";



        if (dt.Rows.Count > 0)
        {
            txtNombre.Text = dt.Rows[0]["DESCRIPCION"].ToString();
            txtVlrHora.Text = dt.Rows[0]["VLRHORA"].ToString();
            bool Estado = bool.Parse(dt.Rows[0]["ESTADO"].ToString());

            btnGuardar.Enabled = Estado;
            btnRetirar.Enabled = Estado;
            btnRetirar.Visible = Estado;
            btnReactivar.Visible = !Estado;
            imgReactivar.Visible = !Estado;
            imgRetirar.Visible = Estado;
            imgGuardar.Enabled = Estado;

            if (!Estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }

            txtNombre.Focus();
        }
    }

    private void ListaroModulo()
    {
        Procedimientos.LlenarCombos(ddlArea, new ClinicaCES.Logica.LCargos().AreaListar(), "CODIGO", "DESCRIPCION");        
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(ddlArea.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(),txtVlrHora.Text.Trim());
        else if (sender.Equals(imgNuevo) || sender.Equals(imgCancelar))
            Response.Redirect("addCargos.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlArea.SelectedValue, false);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlArea.SelectedValue, true);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(ddlArea.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(),txtVlrHora.Text.Trim());
        else if (sender.Equals(btnNuevo) || sender.Equals(btnCancelar))
            Response.Redirect("addCargos.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlArea.SelectedValue, false);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlArea.SelectedValue, true);
    }

    private bool ValidarGuardar()
    {
        bool valido = true;

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            valido = false;
            txtNombre.CssClass = "invalidtxt";
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }

        //double vlr;

        //if (!double.TryParse(txtVlrHora.Text.Trim(), out vlr))
        //{
        //    valido = false;
        //    txtVlrHora.CssClass = "invalidtxt";
        //}
        //else
        //{
        //    txtVlrHora.CssClass = "form_input";
        //}

        return valido;
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(ddlArea.SelectedValue, txtCodigo.Text.Trim());
    }
}
