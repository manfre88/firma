﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcPaises.aspx.cs" Inherits="Maestros_srcPaises" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table>
    <tr>
        <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
        <td><asp:ImageButton OnClientClick="return Mensaje(45)" runat="server" ID="imgRetirar" ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" /></td>
        <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" ImageUrl="../icons/cancel.png" onclick="Images_Click" /></td>
        <td><asp:ImageButton runat="server" ID="imgBuscar" ToolTip="Buscar" ImageUrl="../icons/magnify.png" onclick="Images_Click" /></td>
    </tr>     
</table>
<hr />
    <table align="center">
        <tr>
            <td>Filtro:</td>
            <td><asp:DropDownList onkeydown="tabular(event,this)"  ID="ddlFlitro" runat="server">
                <asp:ListItem Value="DESCRIPCION">Pais</asp:ListItem>
                <asp:ListItem Value="CODIGO">Codigo</asp:ListItem>
                <asp:ListItem Value="INDICATIVO">Indicativo</asp:ListItem>
                </asp:DropDownList></td>    
            <td><asp:LinkButton ID="lnkAgregar" runat="server" Text="Agregar" onclick="lnkAgregar_Click"></asp:LinkButton></td> 
        </tr>    
        <tr>
            <asp:Panel ID="pnlPais" runat="server" Visible="false">
                <td>Pais:</td>
                <td colspan="2"><asp:TextBox onkeydown="tabular(event,this)"  CssClass="form_input" ID="txtBusquedaPais" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkPaiskitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlCodigo" runat="server" Visible="false">
                <td>Codigo:</td>
                <td colspan="2"><asp:TextBox onkeydown="tabular(event,this)"  CssClass="form_input" ID="txtBusquedaCod" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkCodigoKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel ID="pnlIndicativo" runat="server" Visible="false">
                <td>Indicativo:</td>
                <td colspan="2"><asp:TextBox onkeydown="tabular(event,this)"  CssClass="form_input" ID="txtBusquedaIndicativo" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkIndicativoKitar" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                </td>  
            </asp:Panel>
        </tr>
    </table>

<table align="center">
    <tr>
        <td colspan="3" align="center">                
            <asp:GridView AutoGenerateColumns="False" ID="gvPais" runat="server" AllowPaging="True" 
            CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" PageSize="15" 
                onrowdatabound="gvPais_RowDataBound" DataKeyNames="ESTADO"
                onpageindexchanging="gvPais_PageIndexChanging">                   
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <RowStyle CssClass="normalrow" />
                <AlternatingRowStyle CssClass="alterrow" />
                <PagerStyle CssClass="cabeza" ForeColor="White"  />
                <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                <Columns>     
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkEliminar" runat="server" />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <input type="checkbox" onclick="CheckAllCheckBoxesMaestros(this.checked,'<%= imgRetirar.ClientID %>',this)" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                     </asp:TemplateField>   
                     <asp:BoundField DataField="CODIGO" HeaderText="Codigo" />
                     <asp:BoundField DataField="DESCRIPCION" HeaderText="País" />
                     <asp:BoundField DataField="INDICATIVO" HeaderText="Indicativo" />
                    <asp:BoundField DataField="ESTADO" HeaderText="Estado" />
                </Columns>
            </asp:GridView> 
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

