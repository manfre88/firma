﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcComplejidadTecnica : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Complejidad Tecnica", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Consultar()
    {
        ComplejidadTecnica are = new ComplejidadTecnica();
        DataTable dtComplejidadTecnica = new ClinicaCES.Logica.LComplejidadTecnica().ComplejidadTecnicaConsultar(are);
        ViewState["dtComplejidadTecnica"] = dtComplejidadTecnica;
        ViewState["dtPaginas"] = dtComplejidadTecnica;
        Procedimientos.LlenarGrid(dtComplejidadTecnica, gvComplejidadTecnica);
    }

    protected void gvComplejidadTecnica_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvComplejidadTecnica.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addComplejidadTecnica.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
                e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            }

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvComplejidadTecnica.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LComplejidadTecnica().ComplejidadTecnicaRetirar(xmlPais, Usuario))
        {
            msg = "7";

            ComplejidadTecnica are = new ComplejidadTecnica();
            DataTable dtComplejidadTecnica = new ClinicaCES.Logica.LComplejidadTecnica().ComplejidadTecnicaConsultar(are);
            ViewState["dtComplejidadTecnica"] = dtComplejidadTecnica;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlArea()
    {
        string cod;
        string xmlArea = "<Raiz>";

        for (int i = 0; i < gvComplejidadTecnica.Rows.Count; i++)
        {
            GridViewRow row = gvComplejidadTecnica.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                cod = row.Cells[1].Text.Trim();
                xmlArea += "<Datos COMPLE='" + cod + "' />";
            }
        }

        return xmlArea += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND NOMBRE LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtComplejidadTecnica = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtComplejidadTecnica"]);
        Procedimientos.LlenarGrid(dtComplejidadTecnica, gvComplejidadTecnica);

        ViewState["dtPaginas"] = dtComplejidadTecnica;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    protected void gvComplejidadTecnica_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvComplejidadTecnica.PageIndex = e.NewPageIndex;
        gvComplejidadTecnica.DataSource = ViewState["dtPaginas"];
        gvComplejidadTecnica.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlArea(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addComplejidadTecnica.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCRIPCION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
