﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addAplicacion : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Aplicaciones", this.Page);
            txtCodigo.Focus();
            
            if (Request.QueryString["id"] != null)
                Buscar(Request.QueryString["id"]);
            else
                Procedimientos.Script("SeleccionarText('" + txtCodigo.ClientID + "')", litScript);
        }
        litScript.Text = string.Empty;
    }
    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addAplicacion.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addAplicacion.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true);
    }

    private void CambiarEstado(string Codigo, bool? Estado)
    {
        Aplicacion app = new Aplicacion();
        app.Codigo = Codigo;
        app.Estado = Estado;
        string msg = "1";

        if (!new ClinicaCES.Logica.LAplicacion().AplicacionEstado(app))
            msg = "3";

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addAplicacion.aspx?id=" + Codigo + "')", litScript);


        
    }

    private void Buscar(string Codigo)
    {
        Aplicacion app = new Aplicacion();
        app.Codigo = Codigo;

        DataTable dt = new ClinicaCES.Logica.LAplicacion().AplicacionConsultar(app);

        txtCodigo.Enabled = false;
        txtNombre.Enabled = true;
        btnGuardar.Enabled = true;
        btnRetirar.Enabled = false;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";
       
        if(Request.QueryString["id"] != null)
            txtCodigo.Text = Request.QueryString["id"];

        if (dt.Rows.Count > 0)
        { 
            DataRow row = dt.Rows[0];
            
            txtNombre.Text = row["NOMBRE"].ToString();
            txtCodigo.Text = row["CODIGO"].ToString();

            bool estado = bool.Parse(row["ESTADO"].ToString());

            btnGuardar.Enabled = estado;
            txtNombre.Enabled = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            imgRetirar.Enabled = estado;

            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }
        }

        Procedimientos.Script("SeleccionarText('" + txtNombre.ClientID + "')", litScript);
        txtNombre.Focus();
    }

    private bool ValidaGuardar()
    {
        bool Valido = true;
        if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
        {
            txtCodigo.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtCodigo.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            txtNombre.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }

        return Valido;

    }

    private void Guardar(string Codigo, string Nombre, string Usr)
    {
        if (!ValidaGuardar())
            return;

        Aplicacion app = new Aplicacion();
        app.Usuario = Usr;
        app.Codigo = Codigo;
        app.Nombre = Nombre;

        string msg = "3";

        if (new ClinicaCES.Logica.LAplicacion().AplicacionActualizar(app))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addAplicacion.aspx?id=" + Codigo + "')", litScript);
    }
}
