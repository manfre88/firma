﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addEstimacion_Proyecto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Etapas de Proyectos", this.Page);

            string script = "if(trim(document.getElementById('" + txtCodigo.ClientID + "').value)!= '') { " +
                " __doPostBack('" + lnkComprobar.UniqueID + "','') };";

            txtCodigo.Attributes.Add("onblur", script);

            if (Request.QueryString["id"] != null)
                Buscar(Request.QueryString["id"]);
            else
                Procedimientos.Script("SeleccionarText('" + txtCodigo.ClientID + "')", litScript);
        }
        litScript.Text = string.Empty;
    }
    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(),txtPorcentaje.Text.Trim(),txtValor.Text.Trim());
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addEstimacion_Proyecto.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false, Session["Nick1"].ToString());
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true, Session["Nick1"].ToString());
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(), txtPorcentaje.Text.Trim(), txtValor.Text.Trim());
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addEstimacion_Proyecto.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false, Session["Nick1"].ToString());
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true, Session["Nick1"].ToString());
    }

    private void CambiarEstado(string Codigo, bool? Estado, string usr)
    {
        Estimacion_Proyectos est = new Estimacion_Proyectos();
        est.Codigo = Codigo;
        est.Estado = Estado;
        est.usr = usr;
        string msg = "3";

        if (new ClinicaCES.Logica.LEstimacion_Proyectos().Estimacion_ProyectosEstado(est))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addEstimacion_Proyecto.aspx?id=" + Codigo + "')", litScript);
    }

    private void Buscar(string Codigo)
    {
        Estimacion_Proyectos est = new Estimacion_Proyectos();
        est.Codigo = Codigo;

        DataTable dt = new ClinicaCES.Logica.LEstimacion_Proyectos().Estimacion_ProyectosConsultar(est);

        txtCodigo.Enabled = false;
        txtNombre.Enabled = true;
        txtPorcentaje.Enabled = true;
        txtValor.Enabled = true;
        btnGuardar.Enabled = true;
        btnRetirar.Enabled = false;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";

        if (Request.QueryString["id"] != null)
            txtCodigo.Text = Request.QueryString["id"];

        if (dt.Rows.Count > 0)
        {
            DataRow row = dt.Rows[0];

            txtNombre.Text = row["DESCRIPCION"].ToString();
            txtPorcentaje.Text = row["PORCENTAJE"].ToString();
            txtValor.Text = row["VALOR"].ToString();
            txtCodigo.Text = row["CODIGO"].ToString();

            bool estado = bool.Parse(row["ESTADO"].ToString());

            btnGuardar.Enabled = estado;
            imgGuardar.Enabled = estado;
            txtNombre.Enabled = estado;
            txtPorcentaje.Enabled = estado;
            txtValor.Enabled = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            imgRetirar.Enabled = estado;

            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }
        }

        Procedimientos.Script("SeleccionarText('" + txtNombre.ClientID + "')", litScript);
    }

    private bool ValidaGuardar()
    {
        bool Valido = true;
        if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
        {
            txtCodigo.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtCodigo.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            txtNombre.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }

        double porcentaje;
        double vlr;

        if (!double.TryParse(txtPorcentaje.Text.Trim(),out porcentaje))
        {
            txtPorcentaje.CssClass = "invalidtxt";            
            Valido = false;
        }
        else
        {
            txtPorcentaje.CssClass = "form_input";
            txtPorcentaje.Text = porcentaje.ToString();
        }

        if (!double.TryParse(txtValor.Text.Trim(), out vlr))
        {
            txtValor.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtValor.CssClass = "form_input";
            txtValor.Text = vlr.ToString();
        }

        return Valido;

    }

    private void Guardar(string Codigo, string Nombre, string Usr, string Porcentaje, string Valor)
    {
        if (!ValidaGuardar())
            return;

        Estimacion_Proyectos est = new Estimacion_Proyectos();
        est.Codigo = Codigo;
        est.Descripcion = Nombre;
        est.Porcentaje = Porcentaje;
        est.usr = Usr;
        est.Vlr = Valor;

        string msg = "3";

        if (new ClinicaCES.Logica.LEstimacion_Proyectos().Estimacion_ProyectosActualizar(est))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addEstimacion_Proyecto.aspx?id=" + Codigo + "')", litScript);
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Buscar(txtCodigo.Text.Trim());
    }
}