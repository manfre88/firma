﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addPrioridades.aspx.cs" Inherits="Maestros_addPrioridades" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<table width="100%">
    <tr>
        <td>
            <table align="left">
           
                <tr>
                    <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" 
                            ImageUrl="../icons/nuevo.png" onclick="Images_Click" TabIndex="1" /></td>
                    <td><asp:ImageButton runat="server" ID="imgRetirar"  ToolTip="Retirar" 
                            ImageUrl="../icons/eliminar_d.gif" Enabled="false" onclick="Images_Click" 
                            OnClientClick="return Mensaje(11)" TabIndex="2" /></td>
                    <td><asp:ImageButton runat="server" Visible="false" ID="imgReactivar" 
                            ToolTip="Reactivar" ImageUrl="../icons/refresh.png" onclick="Images_Click" 
                            TabIndex="3"  /></td>                
                    <td><asp:ImageButton runat="server" ID="imgGuardar" ToolTip="Guardar" 
                            OnClientClick="return Mensaje(13)" ImageUrl="../icons/16_save_d.png" 
                            Enabled="false" onclick="Images_Click" TabIndex="4" /></td>                
                    <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" 
                            ImageUrl="../icons/cancel.png" onclick="Images_Click" TabIndex="5" /></td>
                    <td><img onclick="redirect('srcPrioridades.aspx')" src="../icons/magnify.png" id="imgBuscar" runat="server" style="cursor:pointer" /></td>
                </tr>     
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center" style="width: 358px">
                <tr>
                    <td align="right">Codigo:</td>                        
                    <td>
                        <asp:TextBox onblur="if(trim(this.value) != ''){redirect('addPrioridades.aspx?id=' + this.value)}" CssClass="form_input" ID="txtCodigo" runat="server" Width="77px" MaxLength="10" TabIndex="6"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCodigo" FilterType="UppercaseLetters, LowercaseLetters, Numbers"  ></ajaxToolkit:FilteredTextBoxExtender>                                            
                        </td>
                </tr>
                <tr>
                    <td align="right">Prioridad:</td>
                    <td>
                        <asp:TextBox  Enabled="false" CssClass="form_input" ID="txtNombre" runat="server" MaxLength="20" Width="130px" TabIndex="7"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNombre" FilterType="UppercaseLetters, LowercaseLetters"  ></ajaxToolkit:FilteredTextBoxExtender>                                            
                        </td>                    
                </tr>
                <tr>
                    <td align="right">Tiempo de Respuesta (Horas):</td>
                    <td>
                    <asp:TextBox CssClass="form_input" ID="txtHoras" runat="server" Width="40px" MaxLength="2" TabIndex="6" AutoPostBack="True"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtHoras" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender>                     
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="8"   /></td>
                    <td><asp:Button Visible="false" ID="btnReactivar" runat="server" Text="Reactivar" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="9"   /></td>
                    <td><asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" 
                            OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" 
                            onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" 
                            TabIndex="10"  /></td>
                    <td><asp:Button ID="btnGuardar" Enabled="false" runat="server" Text="Guardar" 
                            OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" 
                            onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" 
                            TabIndex="11" /></td>                                                                                
                    <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="12"  /></td>                     
                    <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcPrioridades.aspx')" /> </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<br />
</asp:Content>

