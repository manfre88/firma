﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addMenu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Procedimientos.Titulo("Menus", this.Page);
            ListarAplicacion();
            if (Request.QueryString["id"] != null)
                Consultar(Request.QueryString["app"], Request.QueryString["id"]);

            string script =
    "if(document.getElementById('" + ddlAplicacion.ClientID + "').value != '-1' && trim(document.getElementById('" + txtCodigo.ClientID + "').value) != ''){" +
        "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            ddlAplicacion.Attributes.Add("onblur", script);
            txtCodigo.Attributes.Add("onblur", script);
        }
        litScript.Text = string.Empty;
    }

    private void Guardar(string Aplicacion,string Codigo,string Desc, string Usr)
    {
        if (!Validaguardar())
            return;

        MMenu men = new MMenu();

        men.Aplicacion = Aplicacion;
        men.Codigo = Codigo;
        men.Menu = Desc;
        men.Usr = Usr;

        string msg = "3";

        if (new ClinicaCES.Logica.LMenu().MenuActualizar(men))
        { 
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addMenu.aspx?id=" + Codigo + "&app=" + Aplicacion + "')", litScript);

    }

    private void CambiarEstado(string Codigo, string App, bool? Estado)
    {
        MMenu men = new MMenu();

        men.Estado = Estado;
        men.Aplicacion = App;
        men.Codigo = Codigo;

        string msg = "1";

        if (new ClinicaCES.Logica.LMenu().MenuEstado(men))
            msg = "3";
       
        Procedimientos.Script("Mensaje(" + msg + ");redirect('addMenu.aspx?id=" + Codigo + "&app=" + App + "')", litScript);

    }

    private void Consultar(string aplicacion, string codigo)
    {
        MMenu men = new MMenu();
        men.Aplicacion = aplicacion;
        men.Codigo = codigo;

        DataTable dt = new ClinicaCES.Logica.LMenu().MenuConsultar(men);

        txtCodigo.Text = codigo;
        ddlAplicacion.SelectedValue = aplicacion;

        btnGuardar.Enabled = true;
        btnRetirar.Enabled = false;
        btnRetirar.Visible = true;
        btnReactivar.Visible = false;
        txtNombre.Enabled = true;
        txtCodigo.Enabled = false;
        ddlAplicacion.Enabled = false;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";
        btnRetirar.Enabled = false;


        if (dt.Rows.Count > 0)
        {
            txtNombre.Text = dt.Rows[0]["MENU"].ToString();
            bool estado = bool.Parse(dt.Rows[0]["ESTADO"].ToString());

            btnGuardar.Enabled = estado;
            txtNombre.Enabled = estado;
            //txtIndicativo.Enabled = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            imgRetirar.Enabled = estado;

            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }
        }
    }

    private void ListarAplicacion()
    {
        Procedimientos.LlenarCombos(ddlAplicacion, new ClinicaCES.Logica.LMenu().AplicacionListar(), "CODIGO", "NOMBRE");
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(ddlAplicacion.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(imgNuevo) || sender.Equals(imgCancelar))
            Response.Redirect("addMenu.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, false);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, true);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(ddlAplicacion.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(btnNuevo) || sender.Equals(btnCancelar))
            Response.Redirect("addMenu.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, false);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, true);
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(ddlAplicacion.SelectedValue, txtCodigo.Text.Trim());
    }

    private bool Validaguardar()
    {
        bool valido = true;

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            txtNombre.CssClass = "invalidtxt";
            valido = false;
        }
        else
            txtNombre.CssClass = "form_input";

        return valido;
    }
}
