﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcModulo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        litScript.Text = string.Empty;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Modulos", this.Page);
            EstadoInicial();
            string script = "document.getElementById('" + imgRetirar.ClientID + "').disabled = true;";
            Procedimientos.Script(script, litScript);
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
    }

    private void Consultar()
    {
        Modulo app = new Modulo();
        DataTable dtModulo = new ClinicaCES.Logica.LModulo().ModuloConsultar(app);
        ViewState["dtModulo"] = dtModulo;
        ViewState["dtPaginas"] = dtModulo;
        Procedimientos.LlenarGrid(dtModulo, gvModulo);
    }

    protected void gvModulo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvModulo.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addModulo.aspx?id=" + e.Row.Cells[1].Text.Trim() + "&app=" + e.Row.Cells[4].Text.Trim() + "&men=" + gvModulo.DataKeys[e.Row.RowIndex]["MENU"].ToString() + "')");
            }

            e.Row.Cells[5].Text = e.Row.Cells[5].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvModulo.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LModulo().ModuloRetirar(xmlPais, Usuario))
        {
            msg = "7";

            Modulo MEN = new Modulo();
            DataTable dtModulo = new ClinicaCES.Logica.LModulo().ModuloConsultar(MEN);
            ViewState["dtModulo"] = dtModulo;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlModulo()
    {
        string app;
        string Modulo;
        string Menu;
        string xmlPais = "<Raiz>";

        for (int i = 0; i < gvModulo.Rows.Count; i++)
        {
            GridViewRow row = gvModulo.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                Modulo = row.Cells[1].Text.Trim();
                app = row.Cells[4].Text.Trim();
                Menu = gvModulo.DataKeys[i]["MENU"].ToString();
                xmlPais += "<Datos APP='" + app + "' Modulo='" + Modulo + "' Menu = '" + Menu + "' />";
            }
        }

        return xmlPais += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND Modulo LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtModulo = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtModulo"]);
        Procedimientos.LlenarGrid(dtModulo, gvModulo);

        ViewState["dtPaginas"] = dtModulo;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "Modulo", pnlNombre, txtBusquedaNombre);
    }


    protected void gvModulo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModulo.PageIndex = e.NewPageIndex;
        gvModulo.DataSource = ViewState["dtPaginas"];
        gvModulo.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlModulo(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addModulo.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "Modulo", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("NOMBRE", "Modulo", pnlNombre, txtBusquedaNombre);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMBRE":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
