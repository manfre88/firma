﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addDepartamentos.aspx.cs" Inherits="Maestros_addDepartamentos" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="100%">
    <tr>
        <td>
            <table align="left">
                <tr>
                    <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
                    <td><asp:ImageButton runat="server" ID="imgRetirar"  ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" OnClientClick="return Mensaje(11)" /></td>
                    <td><asp:ImageButton runat="server" Visible="false" ID="imgReactivar" ToolTip="Reactivar" ImageUrl="../icons/refresh.png" onclick="Images_Click"  /></td>                
                    <td><asp:ImageButton runat="server" ID="imgGuardar" ToolTip="Guardar" 
                            OnClientClick="return Mensaje(46)" ImageUrl="../icons/16_save_d.png" 
                            onclick="Images_Click" Enabled="False" /></td>                
                    <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" ImageUrl="../icons/cancel.png" onclick="Images_Click" /></td>
                    <td><img onclick="redirect('srcDepartamentos.aspx')" src="../icons/magnify.png" id="imgBuscar" runat="server" style="cursor:pointer" /></td>
                </tr>     
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>
                    <td style="text-align: right">Pais:</td>
                    <td><asp:DropDownList ID="ddlPais" runat="server"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">Codigo:</td>                        
                    <td><asp:TextBox  CssClass="form_input" ID="txtCodigo" runat="server" Width="49px" MaxLength="2"></asp:TextBox>
                    <asp:LinkButton ID="lnkComprobar" runat="server" onclick="lnkComprobar_Click"></asp:LinkButton>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCodigo" FilterType="UppercaseLetters, LowercaseLetters, Numbers"  ></ajaxToolkit:FilteredTextBoxExtender>                                            
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">Nombre:</td>
                    <td><asp:TextBox Enabled="false" CssClass="form_input" ID="txtNombre" runat="server" MaxLength="40"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNombre" FilterType="UppercaseLetters, LowercaseLetters, Custom" ValidChars=" ,ñ,Ñ"  ></ajaxToolkit:FilteredTextBoxExtender>                                            
                    </td>                    
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                    <td><asp:Button Visible="false" ID="btnReactivar" runat="server" Text="Reactivar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                    <td><asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" OnClientClick="return Mensaje(46)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
                    <td><asp:Button ID="btnGuardar" Enabled="false" runat="server" Text="Guardar" OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>                                                                                
                    <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>                     
                    <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcDepartamentos.aspx')" /> </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<br />
</asp:Content>

