﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addPrioridades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Prioridades", this.Page);
            txtCodigo.Focus();

            if (Request.QueryString["id"] != null)
                Buscar(Request.QueryString["id"]);
            else
                Procedimientos.Script("SeleccionarText('" + txtCodigo.ClientID + "')", litScript);
        }
        litScript.Text = string.Empty;
    }

    private void Buscar(string Codigo)
    {
        Prioridades prioridad = new Prioridades();
        prioridad.Codigo = Codigo;

        DataTable dt = new ClinicaCES.Logica.LPrioridades().PrioridadConsultar(prioridad);

        txtCodigo.Enabled = false;
        txtNombre.Enabled = true;
        btnGuardar.Enabled = true;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";
        btnRetirar.Enabled = false;

        if (Request.QueryString["id"] != null)
            txtCodigo.Text = Request.QueryString["id"];

        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                txtNombre.Text = row["PRIORIDAD"].ToString();
                txtCodigo.Text = row["CODIGO"].ToString();
                txtHoras.Text = row["HORAS"].ToString();

                bool estado = bool.Parse(row["ESTADO"].ToString());

                btnGuardar.Enabled = estado;
                txtNombre.Enabled = estado;
                txtHoras.Enabled = estado;
                imgGuardar.Enabled = estado;
                btnRetirar.Enabled = estado;
                btnReactivar.Visible = !estado;
                btnRetirar.Visible = estado;
                imgReactivar.Visible = !estado;
                imgRetirar.Visible = estado;
                imgRetirar.Enabled = estado;

                if (!estado)
                {
                    imgGuardar.ImageUrl = "../icons/16_save_d.png";
                    imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
                }
                else
                {
                    imgGuardar.ImageUrl = "../icons/16_save.png";
                    imgRetirar.ImageUrl = "../icons/eliminar.gif";
                }
            }
        }
        txtNombre.Focus();
        Procedimientos.Script("SeleccionarText('" + txtNombre.ClientID + "')", litScript);
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
        {
            if (ValidaGuardar())
                Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(), txtHoras.Text);
        }    
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addPrioridades.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            if (ValidaGuardar())
                Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(), txtHoras.Text);
        }
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addPrioridades.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true);
    }

    private void Guardar(string Codigo, string Nombre, string Usr, string horas)
    {

        Prioridades prioridad = new Prioridades();
        prioridad.Usuario = Usr;
        prioridad.Codigo = Codigo;
        prioridad.Nombre = Nombre;
        prioridad.Horas = horas;

        string msg = "3";

        if (new ClinicaCES.Logica.LPrioridades().PrioridadGuardar(prioridad))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addPrioridades.aspx?id=" + Codigo + "')", litScript);
    }

    private bool ValidaGuardar()
    {
        bool Valido = true;
        if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
        {
            txtCodigo.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtCodigo.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            txtNombre.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }

        double horas;

        if (double.TryParse(txtHoras.Text.Trim().Replace(',', '.'), out horas))
        {
            txtHoras.Text = horas.ToString();
            txtHoras.CssClass = "form_input";
        }
        else
        {
            txtHoras.CssClass = "invalidtxt";            
        }

        return Valido;
    }

    private void CambiarEstado(string Codigo, bool? Estado)
    {
        Prioridades prioridad = new Prioridades();
        prioridad.Codigo = Codigo;
        prioridad.Estado = Estado;
        string msg = "1";

        if (!new ClinicaCES.Logica.LPrioridades().PrioridadEstado(prioridad))
            msg = "3";

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addPrioridades.aspx?id=" + Codigo + "')", litScript);
    }
}
