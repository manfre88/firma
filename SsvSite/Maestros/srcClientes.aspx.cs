﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcClientes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Clientes", this.Page);
            EstadoInicial();
            Companias cia = new Companias();
            Procedimientos.LlenarCombos(ddlCia, new ClinicaCES.Logica.LCompanias().CiaConsultar(cia), "CODIGO", "COMPANIA");
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Consultar()
    {
        Clientes cli = new Clientes();
        DataTable dtClientes = new ClinicaCES.Logica.LClientes().ClienteConsultar(cli).Tables[0];
        ViewState["dtClientes"] = dtClientes;
        ViewState["dtPaginas"] = dtClientes;
        Procedimientos.LlenarGrid(dtClientes, gvClientes);
    }

    protected void gvClientes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvClientes.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addClientes.aspx?nit=" + e.Row.Cells[1].Text.Trim() + "&tip=" + gvClientes.DataKeys[e.Row.RowIndex]["TIPO"].ToString() + "&neg=" + gvClientes.DataKeys[e.Row.RowIndex]["NEGOCIO"].ToString() + "')");
            }

            e.Row.Cells[6].Text = e.Row.Cells[6].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvClientes.DataKeys[e.Row.RowIndex]["ACTIVO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlClientes, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LClientes().ClientesRetirar(xmlClientes, Usuario))
        {
            msg = "7";

            Clientes cli = new Clientes();
            DataTable dtClientes = new ClinicaCES.Logica.LClientes().ClienteConsultar(cli).Tables[0];
            ViewState["dtClientes"] = dtClientes;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlcli()
    {
        string cli;
        string tipo;
        string xmlPais = "<Raiz>";

        for (int i = 0; i < gvClientes.Rows.Count; i++)
        {
            GridViewRow row = gvClientes.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                cli = row.Cells[1].Text.Trim();
                tipo = gvClientes.DataKeys[i]["TIPO"].ToString();
                xmlPais += "<Datos CLIENTE='" + cli + "' TIPO='" + tipo + "' />";
            }
        }
        return xmlPais += "</Raiz>";
    }

    private void Filtrar()
    {
        DataTable dtClientes = null;
        if (pnlCia.Visible)
        {
            Clientes cli = new Clientes();
            cli.nit = txtBusquedaCod.Text.Trim();
            cli.nomcomcial = txtBusquedaNombre.Text.Trim();
            if (ddlCia.SelectedIndex != 0)
                cli.Cia = ddlCia.SelectedValue;

            dtClientes = new ClinicaCES.Logica.LClientes().ClienteConsultarCia(cli);
        }
        else
        {
            string filtro = "NIT LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND NOMCOMCIAL LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
            dtClientes = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtClientes"]);
                        
        }

        Procedimientos.LlenarGrid(dtClientes, gvClientes);
        ViewState["dtPaginas"] = dtClientes;        

        if (pnlCodigo.Visible)
            kitarFiltro("NIT", "Nit", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMCOMCIAL", "Nombre", pnlNombre, txtBusquedaNombre);
        if (pnlCia.Visible)
            kitarFiltro("COMPANIA", "Compañia", pnlCia, ddlCia);
    }

    protected void gvClientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvClientes.PageIndex = e.NewPageIndex;
        gvClientes.DataSource = ViewState["dtPaginas"];
        gvClientes.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlcli(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addClientes.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("NIT", "Nit", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMCOMCIAL", "Nombre", pnlNombre, txtBusquedaNombre);
        if (pnlCia.Visible)
            kitarFiltro("COMPANIA", "Compañia", pnlCia, ddlCia);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
        {
            if(ddlFlitro.Items.Count> 0)
                AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        }
            
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("NIT", "Nit", pnlCodigo, txtBusquedaCod);
        else if (sender.Equals(lnkNombreKitar))
            kitarFiltro("NOMCOMCIAL", "Nombre", pnlNombre, txtBusquedaNombre);
        else if (sender.Equals(lnkCiaKitar))
            kitarFiltro("COMPANIA", "Compañia", pnlCia, ddlCia);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void kitarFiltro(string value, string text, Panel pnl, DropDownList txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.SelectedIndex = 0;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMCOMCIAL":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            case "COMPANIA":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCia.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }


    public DataTable dtExcel()
    {
        string[] titulo = { "Nit", "Nombre", "Tipo", "Direccion", "Telefono", "Estado" };
        string[] columnas = { "NIT", "NOMCOMCIAL", "TIPO", "DIRECCION", "TELEFONO", "ACTIVO" };
        string[] valores = new string[6];
        DataTable dt = new DataTable();
        DataTable dtPagina = (DataTable)ViewState["dtPaginas"];

        foreach (DataRow row in dtPagina.Rows)
        {
            for (int i = 0; i < titulo.Length; i++)
            {
                valores[i] = row[columnas[i]].ToString();
            }

            Procedimientos.CrearDatatable(titulo, valores, dt);

        }

        return dt;
    }
    protected void Excel_Click(object sender, ImageClickEventArgs e)
    {
        Procedimientos.ExportarDataTable(dtExcel(), "Clientes");
    }
}
