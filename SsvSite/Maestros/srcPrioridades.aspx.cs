﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcPrioridades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Prioridades", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("PRIORIDAD", "Prioridad", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void Consultar()
    {
        Prioridades prioridad = new Prioridades();
        DataTable dtPrioridad = new ClinicaCES.Logica.LPrioridades().PrioridadConsultar(prioridad);
        ViewState["dtPrioridad"] = dtPrioridad;
        ViewState["dtPaginas"] = dtPrioridad;
        Procedimientos.LlenarGrid(dtPrioridad, gvPrioridad);
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    protected void gvPrioridad_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvPrioridad.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addPrioridades.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Cells[4].Text = e.Row.Cells[4].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPrioridad = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvPrioridad.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPrioridad.Enabled = false;

            chkPrioridad.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPrioridad, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LPrioridades().PrioridadesRetirar(xmlPrioridad, Usuario))
        {
            msg = "7";

            Prioridades prioridad = new Prioridades();
            DataTable dtPrioridad = new ClinicaCES.Logica.LPrioridades().PrioridadConsultar(prioridad);
            ViewState["dtPrioridad"] = dtPrioridad;
            Filtrar();
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND PRIORIDAD LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtPrioridad = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtPrioridad"]);
        Procedimientos.LlenarGrid(dtPrioridad, gvPrioridad);

        ViewState["dtPaginas"] = dtPrioridad;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("PRIORIDAD", "Prioridad", pnlNombre, txtBusquedaNombre);
    }

    protected void gvPrioridad_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPrioridad.PageIndex = e.NewPageIndex;
        gvPrioridad.DataSource = ViewState["dtPaginas"];
        gvPrioridad.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlPrioridad(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addPrioridades.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private string xmlPrioridad()
    {
        string prioridad;
        string xmlPrioridad = "<Raiz>";

        for (int i = 0; i < gvPrioridad.Rows.Count; i++)
        {
            GridViewRow row = gvPrioridad.Rows[i];
            CheckBox chkPrioridad = (CheckBox)row.FindControl("chkEliminar");
            if (chkPrioridad.Checked)
            {
                prioridad = row.Cells[1].Text.Trim();
                xmlPrioridad += "<Datos CODIGO='" + prioridad + "' />";
            }
        }

        return xmlPrioridad += "</Raiz>";
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("PRIORIDAD", "Prioridad", pnlNombre, txtBusquedaNombre);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "PRIORIDAD":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
