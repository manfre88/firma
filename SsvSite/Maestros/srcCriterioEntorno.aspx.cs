﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcCriterioEntorno : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Creiterios de Entorno", this.Page);
            EstadoInicial();
            if (Request.QueryString["pop"] != null)
            {
                pnlIconos.Visible = false;
            }
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.QueryString["pop"] != null)
        {
            this.MasterPageFile = "../Masters/MasterPop.master";
        }
    }

    private void Consultar()
    {
        CriterioEntorno are = new CriterioEntorno();
        DataTable dtCriterioEntorno = new ClinicaCES.Logica.LCriterioEntorno().CriterioEntornoConsultar(are);
        ViewState["dtCriterioEntorno"] = dtCriterioEntorno;
        ViewState["dtPaginas"] = dtCriterioEntorno;
        Procedimientos.LlenarGrid(dtCriterioEntorno, gvCriterioEntorno);
    }

    protected void gvCriterioEntorno_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string script;
            if (Request.QueryString["pop"] != null)
            {
                script = "window.opener.document.getElementById('" + Request.QueryString["pop"] + "').value = " + e.Row.Cells[1].Text.Trim() + "; " +
                    //"window.opener.document.getElementById('" + Request.QueryString["nom"] + "').value = " + e.Row.Cells[2].Text.Trim() + "; "+
                    "window.opener.focus(); window.close();";
                gvCriterioEntorno.Columns[0].Visible = false;
            }                
            else
                script = "redirect('addCriteriosEntorno.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')";

            for (int i = 1; i < gvCriterioEntorno.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", script);                
            }

            e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvCriterioEntorno.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LCriterioEntorno().CriterioEntornoRetirar(xmlPais, Usuario))
        {
            msg = "7";

            CriterioEntorno are = new CriterioEntorno();
            DataTable dtCriterioEntorno = new ClinicaCES.Logica.LCriterioEntorno().CriterioEntornoConsultar(are);
            ViewState["dtCriterioEntorno"] = dtCriterioEntorno;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlArea()
    {
        string cod;
        string xmlArea = "<Raiz>";

        for (int i = 0; i < gvCriterioEntorno.Rows.Count; i++)
        {
            GridViewRow row = gvCriterioEntorno.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                cod = row.Cells[1].Text.Trim();
                xmlArea += "<Datos COD='" + cod + "' />";
            }
        }

        return xmlArea += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND NOMBRE LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtCriterioEntorno = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtCriterioEntorno"]);
        Procedimientos.LlenarGrid(dtCriterioEntorno, gvCriterioEntorno);

        ViewState["dtPaginas"] = dtCriterioEntorno;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    protected void gvCriterioEntorno_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCriterioEntorno.PageIndex = e.NewPageIndex;
        gvCriterioEntorno.DataSource = ViewState["dtPaginas"];
        gvCriterioEntorno.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlArea(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addCriteriosEntorno.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCRIPCION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
