﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcMunicipios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        litScript.Text = string.Empty;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Municipios", this.Page);
            EstadoInicial();
            string script = "document.getElementById('" + imgRetirar.ClientID + "').disabled = true;";
            Procedimientos.Script(script, litScript);
            ListarPais();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        
    }

    private void ListarPais()
    {
        Procedimientos.LlenarCombos(ddlPais, new ClinicaCES.Logica.LDepartamentos().PaisListar(), "CODIGO", "DESCRIPCION");
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Municipio", pnlNombre, txtBusquedaNombre);
        if (pnlPais.Visible)
            kitarFiltro("COD_PAIS", "Pais", pnlPais, ddlPais);
        if (pnlDpto.Visible)
            kitarFiltro("NOMDEP", "Departamento", pnlDpto, txtBusquedaDpto);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void kitarFiltro(string value, string text, Panel pnl, DropDownList txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.SelectedIndex = 0;
    }

    private void Consultar()
    {
        Municipios mun = new Municipios();
        DataTable dtMunicipio = new ClinicaCES.Logica.LMunicipios().MunConsultar(mun);
        ViewState["dtMunicipio"] = dtMunicipio;
        ViewState["dtPaginas"] = dtMunicipio;
        Procedimientos.LlenarGrid(dtMunicipio, gvMunicipio);
    }

    protected void gvMunicipio_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvMunicipio.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addMunicipios.aspx?id=" + e.Row.Cells[1].Text.Trim() + "&pais=" + e.Row.Cells[5].Text.Trim() + "&dep=" + e.Row.Cells[3].Text.Trim() + "')");
            }

            e.Row.Cells[6].Text = e.Row.Cells[6].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvMunicipio.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlMunicipio, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LMunicipios().MunRetirar(xmlMunicipio, Usuario))
        {
            msg = "7";

            Municipios mun = new Municipios();
            DataTable dtMunicipio = new ClinicaCES.Logica.LMunicipios().MunConsultar(mun);
            ViewState["dtMunicipio"] = dtMunicipio;
            Filtrar();
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND DESCRIPCION LIKE '*" + txtBusquedaNombre.Text.Trim() + "*' AND NOMDEP LIKE '*" + txtBusquedaDpto.Text.Trim() + "*'";
        if (ddlPais.SelectedIndex != 0)
        {
            filtro += " AND COD_PAIS = '" + ddlPais.SelectedValue + "'";
        }
        DataTable dtMunicipio = Procedimientos.dtFiltrado("DESCRIPCION", filtro, (DataTable)ViewState["dtMunicipio"]);
        Procedimientos.LlenarGrid(dtMunicipio, gvMunicipio);

        ViewState["dtPaginas"] = dtMunicipio;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Municipio", pnlNombre, txtBusquedaNombre);
        if (pnlPais.Visible)
            kitarFiltro("COD_PAIS", "Pais", pnlPais, ddlPais);
        if (pnlDpto.Visible)
            kitarFiltro("NOMDEP", "Departamento", pnlDpto, txtBusquedaDpto);
    }

    private string xmlMunicipio()
    {
        string pais;
        string dep;
        string municipio;
        string xmlMunicipio = "<Raiz>";

        for (int i = 0; i < gvMunicipio.Rows.Count; i++)
        {
            GridViewRow row = gvMunicipio.Rows[i];
            CheckBox chkMunicipio = (CheckBox)row.FindControl("chkEliminar");
            if (chkMunicipio.Checked)
            {
                municipio = row.Cells[1].Text.Trim();
                pais = row.Cells[5].Text.Trim();
                dep = row.Cells[3].Text.Trim();
                xmlMunicipio += "<Datos pais='" + pais + "' dep='" + dep + "' id='" + municipio + "' />";
            }
        }

        return xmlMunicipio += "</Raiz>";
    }

    protected void gvMunicipio_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMunicipio.PageIndex = e.NewPageIndex;
        gvMunicipio.DataSource = ViewState["dtPaginas"];
        gvMunicipio.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlMunicipio(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addMunicipios.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        else if (sender.Equals(lnkNombreKitar))
            kitarFiltro("DESCRIPCION", "Municipio", pnlNombre, txtBusquedaNombre);
        else if (sender.Equals(lnkPaisKitar))
            kitarFiltro("COD_PAIS", "Pais", pnlPais, ddlPais);
        else if (sender.Equals(lnkDptoKitar))
            kitarFiltro("NOMDEP", "Departamento", pnlDpto, txtBusquedaDpto);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCRIPCION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            case "COD_PAIS":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlPais.Visible = true;
                    break;
                }
            case "NOMDEP":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlDpto.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
