﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addSubmodulo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("SubModulo", this.Page);
            Procedimientos.comboEstadoInicial(ddlMenu);
            Procedimientos.comboEstadoInicial(ddlModulo);
            ListarAplicacion();

            if (Request.QueryString["id"] != null)
                Consultar(Request.QueryString["app"], Request.QueryString["id"], Request.QueryString["men"], Request.QueryString["mod"]);

            string script =
                "if(document.getElementById('" + ddlAplicacion.ClientID + "').value != '-1' && trim(document.getElementById('" + txtCodigo.ClientID + "').value) != ''){" +
                    "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            ddlModulo.Attributes.Add("onblur", script);
            txtCodigo.Attributes.Add("onblur", script);
        }
        litScript.Text = string.Empty;
    }

    private void Guardar(string Modulo, string Codigo, string Desc, string Usr, string Menu, string Aplicacion)
    {
        if (!ValidaGuardar())
            return;

        SubModulo men = new SubModulo();

        men.Mod = Modulo;
        men.Codigo = Codigo;
        men.SubMod = Desc;
        men.Usr = Usr;
        men.Menu = Menu;
        men.Aplicacion = Aplicacion;

        string msg = "3";

        if (new ClinicaCES.Logica.LSubModulo().SubModuloActualizar(men))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addSubModulo.aspx?id=" + Codigo + "&mod=" + Modulo + "&app=" + Aplicacion + "&men=" + Menu + "')", litScript);

    }

    private void CambiarEstado(string Codigo, string Aplicacion, string Menu, string Modulo, bool? Estado)
    {
        SubModulo men = new SubModulo();

        men.Aplicacion = Aplicacion;
        men.Menu = Menu;
        men.Estado = Estado;
        men.Mod = Modulo;
        men.Codigo = Codigo;

        string msg = "1";

        if (!new ClinicaCES.Logica.LSubModulo().SubModuloEstado(men))
        {
            msg = "3";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addSubModulo.aspx?id=" + Codigo + "&mod=" + Modulo + "&app=" + Aplicacion + "&men=" + Menu + "')", litScript);
    }

    private void Consultar(string aplicacion, string codigo, string Menu, string Modulo)
    {
        SubModulo men = new SubModulo();
        men.Aplicacion = aplicacion;
        men.Codigo = codigo;
        men.Mod = Modulo;
        men.Menu = Menu;

        DataTable dt = new ClinicaCES.Logica.LSubModulo().SubModuloConsultar(men);

        txtCodigo.Text = codigo;
        ddlAplicacion.SelectedValue = aplicacion;
        ListarMenu(ddlAplicacion.SelectedValue);
        ddlMenu.SelectedValue = Menu;
        ListarModulo(Menu, aplicacion);
        ddlModulo.SelectedValue = Modulo;

        btnGuardar.Enabled = true;
        btnRetirar.Enabled = false;
        btnRetirar.Visible = true;
        btnReactivar.Visible = false;
        txtNombre.Enabled = true;
        txtCodigo.Enabled = false;
        ddlModulo.Enabled = false;
        ddlAplicacion.Enabled = false;
        ddlMenu.Enabled = false;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";

        if (dt.Rows.Count > 0)
        {
            txtNombre.Text = dt.Rows[0]["SubModulo"].ToString();
            bool Estado = bool.Parse(dt.Rows[0]["ESTADO"].ToString());
            ddlMenu.SelectedValue = dt.Rows[0]["MENU"].ToString();
            ListarModulos(ddlMenu.SelectedValue);
            ddlModulo.SelectedValue = dt.Rows[0]["MODULO"].ToString();

            btnGuardar.Enabled = Estado;
            btnRetirar.Enabled = Estado;
            btnRetirar.Visible = Estado;
            btnReactivar.Visible = !Estado;
            imgReactivar.Visible = !Estado;
            imgRetirar.Visible = Estado;
            txtNombre.Enabled = Estado;
            imgGuardar.Enabled = Estado;

            if (!Estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }

            txtNombre.Focus();
        }
    }

    private void ListarModulo()
    {
        Procedimientos.LlenarCombos(ddlModulo, new ClinicaCES.Logica.LSubModulo().ModuloListar(), "CODIGO", "NOMBRE");
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(ddlModulo.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick"].ToString(),ddlMenu.SelectedValue,ddlAplicacion.SelectedValue);
        else if (sender.Equals(imgNuevo) || sender.Equals(imgCancelar))
            Response.Redirect("addSubModulo.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, ddlMenu.SelectedValue, ddlModulo.SelectedValue,false);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, ddlMenu.SelectedValue, ddlModulo.SelectedValue, true);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(ddlModulo.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick"].ToString(), ddlMenu.SelectedValue, ddlAplicacion.SelectedValue);
        else if (sender.Equals(btnNuevo) || sender.Equals(btnCancelar))
            Response.Redirect("addSubModulo.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, ddlMenu.SelectedValue, ddlModulo.SelectedValue, false);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, ddlMenu.SelectedValue, ddlModulo.SelectedValue, true);
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(ddlAplicacion.SelectedValue, txtCodigo.Text.Trim(),ddlMenu.SelectedValue,ddlModulo.SelectedValue);
    }

    private void ListarAplicacion()
    {
        Procedimientos.LlenarCombos(ddlAplicacion, new ClinicaCES.Logica.LModulo().AplicacionListar(), "CODIGO", "CODIGO");
    }

    private void ListarMenu(string App)
    {
        MMenu men = new MMenu();
        men.Aplicacion = App;
        Procedimientos.LlenarCombos(ddlMenu, Procedimientos.dtFiltrado("MENU","ESTADO = 1", new ClinicaCES.Logica.LMenu().MenuConsultar(men)), "CODIGO", "MENU");
    }

    private void ListarModulos(string Men)
    {
        Modulo mod = new Modulo();
        mod.Menu = Men;
        Procedimientos.LlenarCombos(ddlModulo, Procedimientos.dtFiltrado("MODULO","ESTADO = 1", new ClinicaCES.Logica.LModulo().ModuloConsultar(mod)), "CODIGO", "MODULO");
    }

    protected void ddlAplicacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAplicacion.SelectedIndex != 0)
        {
            ListarMenu(ddlAplicacion.SelectedValue);
        }
    }

    protected void ddlMenu_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMenu.SelectedIndex != 0)
        {
            ListarModulo(ddlMenu.SelectedValue, ddlAplicacion.SelectedValue);
        }
    }


    private void ListarModulo(string Men, string App)
    {
        Modulo mod = new Modulo();
        mod.Menu = Men;
        mod.Aplicacion = App;
        Procedimientos.LlenarCombos(ddlModulo, Procedimientos.dtFiltrado("MODULO", "ESTADO = 1", new ClinicaCES.Logica.LModulo().ModuloConsultar(mod)), "CODIGO", "MODULO");
        
    }

    private bool ValidaGuardar()
    { 
        TextBox[] txt = {txtCodigo,txtNombre};
        DropDownList[] ddl = {ddlAplicacion,ddlMenu,ddlModulo};
        return Procedimientos.ValidaGuardar(txt, ddl);
    }


}
