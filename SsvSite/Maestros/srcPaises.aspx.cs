﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcPaises : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        litScript.Text = string.Empty;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Paises", this.Page);
            EstadoInicial();
            string script = "document.getElementById('" + imgRetirar.ClientID + "').disabled = true;";
            Procedimientos.Script(script, litScript);
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        
    }

    private void Consultar()
    {
        Paises pais = new Paises();
        DataTable dtPais = new ClinicaCES.Logica.LPaises().PaisesConsultar(pais);
        ViewState["dtPais"] = dtPais;
        ViewState["dtPaginas"] = dtPais;
        Procedimientos.LlenarGrid(dtPais, gvPais);
    }

    protected void gvPais_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvPais.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addPaises.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Cells[4].Text = e.Row.Cells[4].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvPais.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LPaises().PaisesRetirar(xmlPais, Usuario)) 
        {
            msg = "7";

            Paises pais = new Paises();
            DataTable dtPais = new ClinicaCES.Logica.LPaises().PaisesConsultar(pais);
            ViewState["dtPais"] = dtPais;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlPais()
    {
        string pais;
        string xmlPais = "<Raiz>";

        for (int i = 0; i < gvPais.Rows.Count; i++)
        {
            GridViewRow row = gvPais.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                pais = row.Cells[1].Text.Trim();
                xmlPais += "<Datos PAIS='" + pais + "' />";
            }
        }

        return xmlPais += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND DESCRIPCION LIKE '*" + txtBusquedaPais.Text.Trim() + "*' AND INDICATIVO LIKE '*" + txtBusquedaIndicativo.Text.Trim() + "*'";
        DataTable dtPais = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtPais"]);
        Procedimientos.LlenarGrid(dtPais, gvPais);

        ViewState["dtPaginas"] = dtPais;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);    
        if (pnlPais.Visible)
            kitarFiltro("DESCRIPCION", "Pais", pnlPais, txtBusquedaPais);
        if (pnlIndicativo.Visible)
            kitarFiltro("INDICATIVO", "Indicativo", pnlIndicativo, txtBusquedaIndicativo);
    }


    protected void gvPais_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPais.PageIndex = e.NewPageIndex;
        gvPais.DataSource = ViewState["dtPaginas"];
        gvPais.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlPais(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addPaises.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlPais.Visible)
            kitarFiltro("DESCRIPCION", "Pais", pnlPais, txtBusquedaPais);
        if (pnlIndicativo.Visible)
            kitarFiltro("INDICATIVO", "Indicativo", pnlIndicativo, txtBusquedaIndicativo);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        else if (sender.Equals(lnkPaiskitar))
            kitarFiltro("DESCRIPCION", "Pais", pnlPais, txtBusquedaPais);
        else
            kitarFiltro("INDICATIVO", "Indicativo", pnlIndicativo, txtBusquedaIndicativo);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCRIPCION":
            {
                ddlFlitro.Items.RemoveAt(index);
                pnlPais.Visible = true;
                break;
            }
            case "INDICATIVO":
            {
                ddlFlitro.Items.RemoveAt(index);
                pnlIndicativo.Visible = true;
                break;
            }
            default:
            {
                ddlFlitro.Items.RemoveAt(index);
                pnlCodigo.Visible = true;
                break;
            }
        }
    }
}
