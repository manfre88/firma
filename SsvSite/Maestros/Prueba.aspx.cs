﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_Prueba : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Procedimientos.Titulo("Administración de asignación de camas ", this.Page);
            ConsultarAsigCamas();
        }
    }
    private void ConsultarAsigCamas()
    {
        //DataTable dtGrid = llenardt();
        //Procedimientos.LlenarGrid(dtGrid, gvCustomers);
        //ViewState["dtPaginas"] = dtGrid;
        //ViewState["dtPaginas2"] = dtGrid;
    }
    protected DataTable llenardt()
    {
        DataTable dtGrid = new ClinicaCES.Logica.LMaestros().ConsultarAsigCamaServicios(); //tipo 2 son las prestaciones no vigentes en oracle
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
        return dtGrid;
    }

    protected void gvCustomers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //string customerId = gvCustomers.DataKeys[e.Row.RowIndex].Value.ToString();
            //GridView gvOrders = e.Row.FindControl("gvOrders") as GridView;
            //gvOrders.DataSource = new ClinicaCES.Logica.LMaestros().ConsultarAsigCamaObjetos(customerId);
            //gvOrders.DataBind();
        }
    }
    protected void gvInforme_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInforme_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
}