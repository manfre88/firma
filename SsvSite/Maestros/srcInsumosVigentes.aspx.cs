﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcInsumosVigentes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Insumos vigentes con o sin autorización ", this.Page);
            ConsultarInsumos();
        }
        litScript.Text = string.Empty;
    }
    protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
    {
        AgregarFiltro(ddlFiltro.SelectedValue, ddlFiltro.SelectedIndex);
    }
    private void ConsultarInsumos()
    {
        DataTable dtGrid = llenardt();
        Procedimientos.LlenarGrid(dtGrid, gvInsumos);
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
    }
    protected DataTable llenardt()
    {
        DataTable dtGrid = new ClinicaCES.Logica.LMaestros().ConsultarInsumosOracle(1); //tipo 2 son las prestaciones no vigentes en oracle
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
        return dtGrid;
    }
    protected void gvInsumos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chkEstado = (CheckBox)e.Row.FindControl("chkPages");
            chkEstado.Checked = bool.Parse(gvInsumos.DataKeys[e.Row.RowIndex]["ESTADO_AUT"].ToString());
        }
    }
    protected void gvInsumos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInsumos.PageIndex = e.NewPageIndex;
        gvInsumos.DataSource = ViewState["dtPaginas"];
        gvInsumos.DataBind();
    }
    protected void txtFiltro_TextChanged(object sender, EventArgs e)
    {
        DataTable dtNew = Procedimientos.dtFiltrado("DESCRIPCION", "CODINSUMO LIKE '*" + txtFiltro.Text.Trim() + "*' OR DESCRIPCION LIKE '*" + txtFiltro.Text.Trim() + "*'", (DataTable)ViewState["dtPaginas2"]);
        Procedimientos.LlenarGrid(dtNew, gvInsumos);
    }
    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "1":
                {
                    pnlBusqueda.Visible = true;
                    break;
                }
            case "2":
                {
                    pnlBusqueda.Visible = false;
                    txtFiltro.Text = "";
                    DataTable dtNew = Procedimientos.dtFiltrado("ESTADO_AUT", "ESTADO_AUT LIKE '*true*'", (DataTable)ViewState["dtPaginas2"]);
                    Procedimientos.LlenarGrid(dtNew, gvInsumos);
                    ViewState["dtPaginas"] = dtNew;
                    break;
                }
            case "3":
                {
                    pnlBusqueda.Visible = false;
                    txtFiltro.Text = "";
                    DataTable dtNew = Procedimientos.dtFiltrado("ESTADO_AUT", "ESTADO_AUT LIKE '*false*'", (DataTable)ViewState["dtPaginas2"]);
                    Procedimientos.LlenarGrid(dtNew, gvInsumos);
                    ViewState["dtPaginas"] = dtNew;
                    break;
                }
        }
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        //string msg = "";
        //msg = gvPrestaciones.PageIndex.ToString();
        //Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        Prestaciones prestacion = new Prestaciones();
        string msg = "";
        for (int i = 0; i < gvInsumos.Rows.Count; i++)
        {
            CheckBox check = (CheckBox)gvInsumos.Rows[i].FindControl("chkPages");
            //if (check.Checked == true)
            //{
           
            if (new ClinicaCES.Logica.LMaestros().InsumosActualizar(gvInsumos.Rows[i].Cells[1].Text, check.Checked, Session["Nick1"].ToString()))
            {
                msg = "1";
            }
            else
            {
                msg = "3";
            }
            //}
        }
        if (msg == "1")
        {
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            gvInsumos.PageIndex = gvInsumos.PageIndex;
            gvInsumos.DataSource = llenardt();
            gvInsumos.DataBind();
        }
        else
        {
            if (msg == "3")
            { Procedimientos.Script("Mensaje(" + msg + ")", litScript); }
        }
    }
}