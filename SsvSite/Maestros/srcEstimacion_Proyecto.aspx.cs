﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcEstimacion_Proyecto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Etapas de Proyectos", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void Consultar()
    {
        Estimacion_Proyectos are = new Estimacion_Proyectos();
        DataTable dtEstimacion_Proyectos = new ClinicaCES.Logica.LEstimacion_Proyectos().Estimacion_ProyectosConsultar(are);
        ViewState["dtEstimacion_Proyectos"] = dtEstimacion_Proyectos;
        ViewState["dtPaginas"] = dtEstimacion_Proyectos;
        Procedimientos.LlenarGrid(dtEstimacion_Proyectos, gvEstimacion_Proyectos);
    }

    protected void gvEstimacion_Proyectos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvEstimacion_Proyectos.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addEstimacion_Proyecto.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
                e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            }

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvEstimacion_Proyectos.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LEstimacion_Proyectos().Estimacion_ProyectosRetirar(xmlPais, Usuario))
        {
            msg = "7";

            Estimacion_Proyectos are = new Estimacion_Proyectos();
            DataTable dtEstimacion_Proyectos = new ClinicaCES.Logica.LEstimacion_Proyectos().Estimacion_ProyectosConsultar(are);
            ViewState["dtEstimacion_Proyectos"] = dtEstimacion_Proyectos;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlArea()
    {
        string cod;
        string xmlArea = "<Raiz>";

        for (int i = 0; i < gvEstimacion_Proyectos.Rows.Count; i++)
        {
            GridViewRow row = gvEstimacion_Proyectos.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                cod = row.Cells[1].Text.Trim();
                xmlArea += "<Datos COD='" + cod + "' />";
            }
        }

        return xmlArea += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND DESCRIPCION LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtEstimacion_Proyectos = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtEstimacion_Proyectos"]);
        Procedimientos.LlenarGrid(dtEstimacion_Proyectos, gvEstimacion_Proyectos);

        ViewState["dtPaginas"] = dtEstimacion_Proyectos;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    protected void gvEstimacion_Proyectos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEstimacion_Proyectos.PageIndex = e.NewPageIndex;
        gvEstimacion_Proyectos.DataSource = ViewState["dtPaginas"];
        gvEstimacion_Proyectos.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlArea(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addEstimacion_Proyecto.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCRIPCION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
