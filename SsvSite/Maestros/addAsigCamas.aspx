﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="addAsigCamas.aspx.cs" Inherits="Maestros_addAsigCamas" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addAsigCamas.aspx.cs" Inherits="Maestros_addAsigCamas" Title="Asignación Camas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <head>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("[src*=plus]").live("click", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '777'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "../img/minus.png");
        });
        $("[src*=minus]").live("click", function () {
            $(this).attr("src", "../img/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
    </head>
<table style="width: auto">
    <tr>
        <td>
            <table align="left" style="width: 997px">
                <tr>
                    <td>
                <asp:GridView AutoGenerateColumns="False" ID="gvAsigCamas" runat="server" DataKeyNames="CODIGO" CssClass="Grid"
                CellPadding="1" ForeColor="#333333" GridLines="None"  PageSize="20" Width="90%" EnableModelValidation="True" EnableTheming="True" OnRowDataBound="gvAsigCamas_RowDataBound"
                     >
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#dcdcdc" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <PagerStyle  ForeColor="White" HorizontalAlign="Center" CssClass="cabeza" />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <AlternatingRowStyle BackColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <Columns>        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <img alt = "" style="cursor: pointer" src="../img/plus.png" />
                                <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                    <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="false" CssClass = "ChildGrid" DataKeyNames="ID" OnRowDataBound="gvOrders_RowDataBound" OnRowUpdating="gvOrders_RowUpdating"
                                        CellPadding="1" ForeColor="#333333" GridLines="None"  PageSize="40" Width="100%" EnableModelValidation="True" AllowPaging="True" EnableTheming="True" 
                                        >
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <RowStyle BackColor="#dcdcdc" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle  ForeColor="White" HorizontalAlign="Center" CssClass="cabeza" />
                                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <%--<asp:BoundField ItemStyle-Width="150px" DataField="CODIGO" HeaderText="CODIGO" />--%>
                                        <asp:BoundField ItemStyle-Width="150px" DataField="CODIGO" HeaderText="CODIGO" />
                                       
                                        <asp:BoundField ItemStyle-Width="150px" DataField="ID" HeaderText="ID" />
                                       
                                        <asp:BoundField ItemStyle-Width="150px" DataField="PACIENTE" HeaderText="PACIENTE" />
                                        <asp:BoundField ItemStyle-Width="150px" DataField="CONVENIO" HeaderText="CONVENIO" />
                                        
                                        <asp:BoundField ItemStyle-Width="150px" DataField="RESPONSABLE" HeaderText="RESPONSABLE" />
                                        <asp:BoundField ItemStyle-Width="70px" DataField="SERVICIO" HeaderText="SERVICIO" Visible="false"/>
                                        <%--<asp:TemplateField>
                                             <ItemTemplate>
                                                 
                                                 <asp:DropDownList ID="ddlResponsable" runat="server" DataTextField="RESPONSABLE" Width="155px" AutoPostBack="True" OnSelectedIndexChanged="ddlResponsable_SelectedIndexChanged" ></asp:DropDownList>
                                                 
                                             </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" />
                             
                                             <HeaderTemplate>
                                                RESPONSABLE
                                             </HeaderTemplate>
                                         </asp:TemplateField>  --%>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                            <asp:LinkButton ID="Editar" Runat="server" CommandName="Update">Seleccionar</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                    </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:TemplateField>  
                                                                          
                         <%--<asp:BoundField DataField="CODIGO" HeaderText="CODIGO"  />--%>
                    
                         <asp:BoundField DataField="SERVICIO" HeaderText="SERVICIO" >
                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                         <%--<asp:BoundField DataField="ESTADO" HeaderText="Estado" Visible="false" />--%>
                       
                       
                                                                          
                    </Columns>
                </asp:GridView>
               

                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>                                                                                
                    <td>&nbsp;</td>                     
                    <td>
                        <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick">
                        </asp:Timer>
                    </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
            <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" 
	 okcontrolid="btnOkay" 
	targetcontrolid="lblCama" popupcontrolid="Panel1" 
	popupdraghandlecontrolid="PopupHeader" drag="true" DropShadow="true"
	backgroundcssclass="modalBackground">
</ajaxToolkit:ModalPopupExtender>

<asp:panel id="Panel1" runat="server" CssClass="modalPopup" Style="display: none;
                text-align: left;" Width="518px" Height="453px" BackColor="Silver" ForeColor="Black" BorderStyle="Solid">
	<div class="HellowWorldPopup">
               <%-- <div class="PopupHeader" id="PopupHeader"><b>Formulario de diligenciamiento de clientes</b></div>
                <div class="PopupBody">--%>
                 <table style="width: 435px"  >
            <tr>
                <td  align="center"  colspan="6" >
                    <div class="TipoInformacion">
                        <B>Listado de Responsables</B></div>
                </td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="Label1" runat="server" Text="Cama:" Font-Bold="True" Font-Size="Small"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="lblCama" runat="server" Text="Label" Font-Size="Small"></asp:Label>
                </td>
                <td align="left" class="auto-style1">
                    &nbsp;</td>
                <td align="left" class="auto-style1">
                    &nbsp;</td>
                <td  align="left">
                    &nbsp;</td>
                <td  >
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblResActual" runat="server" Text="Responsable Actual:" Font-Bold="True" Font-Size="Small" Visible="False"></asp:Label>
                </td>
                <td align="left" >
                    <asp:Label ID="lblResponsable" runat="server" Text="Label" Font-Size="Small" Visible="False"></asp:Label>
                </td>
                <td align="leff" >
                    &nbsp;</td>
                <td align="leff" >
                    &nbsp;</td>
                <td align="right">
                    &nbsp;</td>
                <td  >
                    &nbsp;</td>
            </tr>
                     <tr>
                <td align="left" >
                    <asp:Label ID="Label3" runat="server" Text="Asignar todo el servicio:" Font-Bold="True" Font-Size="Small" Visible="True"></asp:Label>
                </td>
                <td align="left" >
                    
                    <asp:CheckBox ID="chkServicio" runat="server" />
                </td>
                <td align="leff" >
                    &nbsp;</td>
                <td align="leff" >
                    &nbsp;</td>
                <td align="right">
                    &nbsp;</td>
                <td  >
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center"  colspan="6">
                    <asp:GridView ID="gvInforme" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" ForeColor="#333333" GridLines="None" 
                        PageSize="20" Width="551px"  OnRowUpdating="gvInforme_RowUpdating">
                        <FooterStyle BackColor="#F7DFB5" />
                        <PagerSettings Visible="False" />
                        <RowStyle CssClass="normalrow" />
                        <AlternatingRowStyle CssClass="alterrow" />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="USUARIO" />
                            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" />
                             <asp:TemplateField>    
                                 <ItemTemplate>
                                 <asp:LinkButton ID="Editar" Runat="server" OnClientClick="return confirm('¿Desea actualizar el registro?');" CommandName="Update">Actualizar</asp:LinkButton>
                                 </ItemTemplate>
                             </asp:TemplateField> 
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="Label2" runat="server" Text="Cama:" Font-Bold="True" Font-Size="Small" Visible="false"></asp:Label>
                </td>
            </tr>
                     <tr>
                <td align="center"  colspan="6">
                    <div class="Controls" >
                    <asp:Button ID="btnOkay" runat="server" OnClick="btnRetornar_Click" Text="Retornar" />
		</div></td>
            </tr>
        </table>
                   

                </div>
                
        </div>
</asp:panel>

</asp:Content>




