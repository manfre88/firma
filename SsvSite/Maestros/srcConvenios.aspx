﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="srcConvenios.aspx.cs" Inherits="Maestros_srcConvenios" %>--%>



<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcConvenios.aspx.cs" Inherits="Maestros_srcConvenios" Title="Convenios que requieren anexos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    &nbsp;<table align="center">
        <tr>
            <td>Filtro:</td>
            <td><asp:DropDownList ID="ddlFiltro" AutoPostBack="true" runat="server" 
                    onselectedindexchanged="ddlFiltro_SelectedIndexChanged">
                <asp:ListItem>--- Seleccione ---</asp:ListItem>
                <asp:ListItem Value="1">Codigo o Descripción</asp:ListItem>
                <asp:ListItem Value="2">Requiere Autorización</asp:ListItem>
                <asp:ListItem Value="3">No Requiere Autorización</asp:ListItem>
                </asp:DropDownList></td>
            <td>
                <asp:Panel ID="pnlBusqueda" runat="server" Visible="False">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <br />
            </td>
        </tr>
        </table>
    <table align="center">
        <asp:Panel ID="Panel1" runat="server" Visible="False">
        <tr>
            <td>
                
                    <asp:GridView AutoGenerateColumns="False" ID="gvConvenios" runat="server" DataKeyNames="ESTADO"
                CellPadding="4" ForeColor="#333333" GridLines="None"  PageSize="20" Width="100%" EnableModelValidation="True" OnRowDataBound="gvConvenios_RowDataBound" AllowPaging="True" EnableTheming="True" OnPageIndexChanging="gvConvenios_PageIndexChanging" 
                     >
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#dcdcdc" />
                        <EditRowStyle BackColor="#2461BF" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <PagerStyle  ForeColor="White" HorizontalAlign="Center" CssClass="cabeza" />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%--<asp:CheckBox ID="chkPages" runat="server" Text='<%# Bind("ESTADO_AUT") %>' />--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkPages" runat="server" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderTemplate>
                                    REQUIERE / NO REQUIERE
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CODCONVENIO" HeaderText="CODCONVENIO"  />
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" />
                            <%--<asp:BoundField DataField="ESTADO" HeaderText="Estado" Visible="false" />--%>
                        </Columns>
                    </asp:GridView>
                
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="return Mensaje(2)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" OnClick="btnGuardar_Click" />
                </td>
        </tr>
        </asp:Panel>
    </table>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>
