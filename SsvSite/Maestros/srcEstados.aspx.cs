﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcEstados : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Estados", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCESTADO", "Nombre", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void Consultar()
    {
        Estados esta = new Estados();
        DataTable dtEstado = new ClinicaCES.Logica.LEstados().EstadoConsultar(esta);
        ViewState["dtEstado"] = dtEstado;
        ViewState["dtPaginas"] = dtEstado;
        Procedimientos.LlenarGrid(dtEstado, gvEstados);
    }

    protected void gvEstados_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvEstados.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addEstados.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkEstado = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvEstados.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkEstado.Enabled = false;

            chkEstado.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlEstado, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LEstados().EstadosRetirar(xmlEstado, Usuario))
        {
            msg = "7";

            Estados estado = new Estados();
            DataTable dtEstado = new ClinicaCES.Logica.LEstados().EstadoConsultar(estado);
            ViewState["dtEstado"] = dtEstado;
            Filtrar();
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND DESCESTADO LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtEstado = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtEstado"]);
        Procedimientos.LlenarGrid(dtEstado, gvEstados);

        ViewState["dtPaginas"] = dtEstado;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCESTADO", "Nombre", pnlNombre, txtBusquedaNombre);
    }

    protected void gvEstados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEstados.PageIndex = e.NewPageIndex;
        gvEstados.DataSource = ViewState["dtPaginas"];
        gvEstados.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlEstado(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addEstados.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private string xmlEstado()
    {
        string estado;
        string xmlEstado = "<Raiz>";

        for (int i = 0; i < gvEstados.Rows.Count; i++)
        {
            GridViewRow row = gvEstados.Rows[i];
            CheckBox chkEstado = (CheckBox)row.FindControl("chkEliminar");
            if (chkEstado.Checked)
            {
                estado = row.Cells[1].Text.Trim();
                xmlEstado += "<Datos CODIGO='" + estado + "' />";
            }
        }

        return xmlEstado += "</Raiz>";
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("DESCESTADO", "Nombre", pnlNombre, txtBusquedaNombre);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCESTADO":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
