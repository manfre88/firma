﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addDepartamentos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Departamentos", this.Page);
            ListarPais();
            if (Request.QueryString["dep"] != null)
                Consultar(Request.QueryString["pais"],Request.QueryString["dep"] );

            string script =
        "if(document.getElementById('" + ddlPais.ClientID + "').value != '-1' && trim(document.getElementById('" + txtCodigo.ClientID + "').value) != ''){" +
        "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            ddlPais.Attributes.Add("onblur", script);
            txtCodigo.Attributes.Add("onblur", script);
        }
        litScript.Text = string.Empty;
    }

    private void ListarPais()
    {
        Procedimientos.LlenarCombos(ddlPais, new ClinicaCES.Logica.LDepartamentos().PaisListar(), "CODIGO", "DESCRIPCION");
    }

    private void Consultar(string pais, string codigo)
    {
        Departamentos dep = new Departamentos();
        dep.Pais = pais;
        dep.CodDepto = codigo;

        DataTable dt = new ClinicaCES.Logica.LDepartamentos().DeparConsultar(dep);

        txtCodigo.Text = codigo;
        ddlPais.SelectedValue = pais;

        btnGuardar.Enabled = true;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";
        btnRetirar.Enabled = false;
        btnRetirar.Visible = true;
        btnReactivar.Visible = false;
        txtNombre.Enabled = true;
        txtCodigo.Enabled = false;
        ddlPais.Enabled = false;

        if (dt.Rows.Count > 0)
        {
            txtNombre.Text = dt.Rows[0]["DESCRIPCION"].ToString();
            bool Estado = bool.Parse(dt.Rows[0]["ESTADO"].ToString());

            btnGuardar.Enabled = Estado;
            btnRetirar.Enabled = Estado;
            btnRetirar.Visible = Estado;
            btnReactivar.Visible = !Estado;
            imgReactivar.Visible = !Estado;
            imgRetirar.Visible = Estado;
            txtNombre.Enabled = Estado;

            if (!Estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }
        }
    }

    private void Guardar(string Pais, string Codigo, string Depar, string Usr)
    {
        if (!ValidaGuardar())
            return;

        Departamentos dep = new Departamentos();

        dep.Pais = Pais;
        dep.CodDepto = Codigo;
        dep.Departamento = Depar;
        dep.Usuario = Usr;

        string msg = "3";

        if (new ClinicaCES.Logica.LDepartamentos().DepActualizar(dep))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addDepartamentos.aspx?dep=" + Codigo + "&pais=" + Pais + "')", litScript);

    }

    private void CambiarEstado(string Codigo, string Pais, bool? Estado)
    {
        Departamentos dep = new Departamentos();

        dep.Estado = Estado;
        dep.Pais = Pais;
        dep.CodDepto = Codigo;

        string msg = "1";

        if (!new ClinicaCES.Logica.LDepartamentos().DepEstado(dep))
            msg = "3";

        Procedimientos.Script("Mensaje(37,'" + msg + "');redirect('addDepartamentos.aspx?dep=" + Codigo + "&pais=" + Pais + "')", litScript);


    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(ddlPais.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(imgNuevo) || sender.Equals(imgCancelar))
            Response.Redirect("addDepartamentos.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(),ddlPais.SelectedValue, false);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(),ddlPais.SelectedValue, true);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(ddlPais.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(btnNuevo) || sender.Equals(btnCancelar))
            Response.Redirect("addDepartamentos.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(),ddlPais.SelectedValue, false);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(),ddlPais.SelectedValue, true);
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(ddlPais.SelectedValue, txtCodigo.Text.Trim());
    }

    private bool ValidaGuardar()
    {        
        TextBox[] txt = {txtCodigo,txtNombre};
        DropDownList[] ddl = {ddlPais};
        return Procedimientos.ValidaGuardar(txt, ddl);        
    }
}
