﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Maestros_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            llenarInfo();
        }

    }
    protected void llenarInfo()
    {
        Procedimientos.LlenarCombos(ddlEspecialidad, new ClinicaCES.Logica.LMaestros().ConsultarEspecialidades(), "ORDTIPO", "ORDNOMBRE");
    }

    protected void ddlEspecialidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        Procedimientos.LlenarCombos(ddlPrestacion, new ClinicaCES.Logica.LMaestros().ConsultarPrestaciones(ddlEspecialidad.SelectedValue.ToString()), "CODIGO", "PRESTACION");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}

    