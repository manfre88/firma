﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addDiasHabiles.aspx.cs" Inherits="Maestros_addDiasHabiles" Title="Untitled Page" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script language="javascript">
    function checkXml()
    {
        var xml = '<Raiz>';
        
        cont = document.aspnetForm.elements.length;
        for(k=0;k<cont;k++)
            if(document.aspnetForm.elements[k].type == 'checkbox')
                if(document.aspnetForm.elements[k].checked)
                    xml = xml + '<Festivos Dia="' + document.aspnetForm.elements[k].value + '" />';
        xml = xml + "</Raiz>";
        //alert(xml)
        document.getElementById('ctl00_ContentPlaceHolder1_hdnXML').value = xml;

    }
</script>
    <asp:ScriptManager id="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
            <asp:Calendar ID="Calendar1" runat="server" BackColor="White"
                DayNameFormat="Full" FirstDayOfWeek="Sunday" Font-Names="Verdana"
                Font-Size="8pt" ForeColor="Black" Height="460px" BorderColor="Silver" BorderWidth="1px" ShowGridLines="True"
                Width="548px" OnDayRender="Calendar1_DayRender">
                <SelectedDayStyle BackColor="red" Font-Bold="True" ForeColor="White" />
                <TodayDayStyle ForeColor="Black" />
                <SelectorStyle BackColor="Yellow" />
                <OtherMonthDayStyle ForeColor="Maroon" />
                <NextPrevStyle VerticalAlign="Bottom" />
                <DayHeaderStyle CssClass="headCalendar" Font-Bold="True" ForeColor="White" Font-Size="7pt" Wrap="False" />
                <TitleStyle CssClass="headCalendar" BackColor="Black" Height="10" Font-Bold="True" ForeColor="White" />
                <DayStyle BackColor="White" />
            </asp:Calendar> 
            <asp:HiddenField id="hdnMes" runat="server"></asp:HiddenField>
            <input id="hdnXML" runat="server" type="hidden" />
            <asp:TextBox id="txtXml" runat="server" Visible="false"></asp:TextBox>
            <asp:Button onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" ID="btnGuardar" runat="server" OnClientClick="checkXml()" Text="Guardar" OnClick="btnGuardar_Click" />  
        </contenttemplate>
    </asp:UpdatePanel> 
</asp:Content>

