﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addComplejidadTecnica : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Complejidad Tecnica", this.Page);

            string script = "if(trim(document.getElementById('" + txtCodigo.ClientID + "').value)!= '') { " +
                " __doPostBack('" + lnkComprobar.UniqueID + "','') };";

            txtCodigo.Attributes.Add("onblur", script);

            if (Request.QueryString["id"] != null)
                Buscar(Request.QueryString["id"]);
            else
                Procedimientos.Script("SeleccionarText('" + txtCodigo.ClientID + "')", litScript);
        }
        litScript.Text = string.Empty;
    }
    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
        {
            if (ValidaGuardar())
                Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(), txtDescripcion.Text.Trim(), txtPeso.Text);
        }            
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addComplejidadTecnica.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false, Session["Nick1"].ToString());
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true, Session["Nick1"].ToString());
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            if (ValidaGuardar())
                Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(), txtDescripcion.Text.Trim(), txtPeso.Text);   
        }            
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addComplejidadTecnica.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false, Session["Nick1"].ToString());
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true, Session["Nick1"].ToString());
    }

    private void CambiarEstado(string Codigo, bool? Estado, string usr)
    {
        ComplejidadTecnica est = new ComplejidadTecnica();
        est.Codigo = Codigo;
        est.Estado = Estado;
        est.usr = usr;
        string msg = "3";

        if (new ClinicaCES.Logica.LComplejidadTecnica().ComplejidadTecnicaEstado(est))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addComplejidadTecnica.aspx?id=" + Codigo + "')", litScript);
    }

    private void Buscar(string Codigo)
    {
        ComplejidadTecnica est = new ComplejidadTecnica();
        est.Codigo = Codigo;

        DataTable dt = new ClinicaCES.Logica.LComplejidadTecnica().ComplejidadTecnicaConsultar(est);

        txtCodigo.Enabled = false;
        txtNombre.Enabled = true;
        txtPeso.Enabled = true;
        txtDescripcion.Enabled = true;
        btnGuardar.Enabled = true;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";
        btnRetirar.Enabled = false;

        if (Request.QueryString["id"] != null)
            txtCodigo.Text = Request.QueryString["id"];

        if (dt.Rows.Count > 0)
        {
            DataRow row = dt.Rows[0];

            txtNombre.Text = row["NOMBRE"].ToString();
            txtPeso.Text = row["PESO"].ToString();            
            txtDescripcion.Text = row["DESCRIPCION"].ToString();
            txtCodigo.Text = row["CODIGO"].ToString();

            bool estado = bool.Parse(row["ESTADO"].ToString());

            btnGuardar.Enabled = estado;
            imgGuardar.Enabled = estado;
            txtNombre.Enabled = estado;
            txtPeso.Enabled = estado;
            txtDescripcion.Enabled = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            imgRetirar.Enabled = estado;

            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }
        }

        Procedimientos.Script("SeleccionarText('" + txtNombre.ClientID + "')", litScript);
    }

    private bool ValidaGuardar()
    {
        bool Valido = true;
        if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
        {
            txtCodigo.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtCodigo.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            txtNombre.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }


        if (string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
        {
            txtDescripcion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtDescripcion.CssClass = "form_input";
        }
        double Peso;
        if (double.TryParse(txtPeso.Text.Trim().Replace('.', ','), out Peso))
        {
            txtPeso.CssClass = "form_input";
            txtPeso.Text = Peso.ToString();
        }
        else
        {
            Valido = false;
            txtPeso.CssClass = "invalidtxt";
        }

        return Valido;

    }

    private void Guardar(string Codigo, string Nombre, string Usr, string Descripcion,string Peso)
    {
        
        ComplejidadTecnica est = new ComplejidadTecnica();
        est.Codigo = Codigo;
        est.Nombre = Nombre;
        est.Descripcion = Descripcion;
        est.usr = Usr;
        est.Peso = Peso;

        string msg = "3";

        if (new ClinicaCES.Logica.LComplejidadTecnica().ComplejidadTecnicaActualizar(est))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addComplejidadTecnica.aspx?id=" + Codigo + "')", litScript);
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Buscar(txtCodigo.Text.Trim());
    }
}
