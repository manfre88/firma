﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addEspecialidad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Procedimientos.ValidarSession(this.Page);

        if (!IsPostBack)
        {
            Procedimientos.Titulo("Administrar Especialidades", this.Page);
            EstadoInicial();
            txtEspecialidad.Attributes.Add("onkeypress", "return clickButton(event,'" + lnkComprobar.ClientID + "')");
            txtEspecialidad.Attributes.Add("onkeypress", "return clickButton(event,'" + btnGuardar.ClientID + "')");
        }

        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        txtCodigo.Text = string.Empty;
        txtEspecialidad.Text = string.Empty;
        EstadoControles(false);
        txtCodigo.Focus();
        Comprobar();

    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            Guardar(txtCodigo.Text.Trim(), txtEspecialidad.Text.ToUpper().Trim());  
        }
        else if (sender.Equals(lnkComprobar))
        {
            Comprobar(txtCodigo.Text.Trim());
        }
        else if (sender.Equals(btnCancelar))
        {
            EstadoInicial();
        }
        else if (sender.Equals(btnRetirar))
        {
            Retirar(txtCodigo.Text.Trim());
        }
    }

    private void Retirar(string codigo)
    {
        Especialidad esp = new Especialidad();
        esp.Codigo = codigo;
        Procedimientos.Script("Mensaje(" + new ClinicaCES.Logica.LEspecialidad().EspecialidaRetirar(esp) + ")", litScript);
        EstadoInicial();

    }

    private void Comprobar()
    {
        Especialidad esp = new Especialidad();

        DataTable dtEspecialidad = new ClinicaCES.Logica.LEspecialidad().EspecialidadConsultar(esp);

        if (dtEspecialidad.Rows.Count > 0)
        {
            Procedimientos.LlenarGrid(dtEspecialidad, gvEspecialidad);
        }
    }

    private void Comprobar(string Codigo)
    {
        Especialidad esp = new Especialidad();
        esp.Codigo = Codigo;

        EstadoControles(true);

        DataTable dtEspecialidad = new ClinicaCES.Logica.LEspecialidad().EspecialidadConsultar(esp);

        if (dtEspecialidad.Rows.Count > 0)
        {
            //existe carguelo
            txtEspecialidad.Text = dtEspecialidad.Rows[0]["ESPECIALIDAD"].ToString();
            txtCodigo.Text = dtEspecialidad.Rows[0]["CODIGO"].ToString();
            btnRetirar.Enabled = true;
        }
        else
        { 
            //registro nuevo
        }

        Procedimientos.Script("SeleccionarText('" + txtEspecialidad.ClientID + "')", litScript);
    }

    private void EstadoControles(bool Estado)
    {
        txtCodigo.Enabled = !Estado;
        txtEspecialidad.Enabled = Estado;
        btnGuardar.Enabled = Estado;
        btnCancelar.Enabled = Estado;
        btnRetirar.Enabled = Estado;
    }
    

    private void Guardar(string Codigo, string especialidad)
    {
        if (!ValidaGuardar())
            return;

        Especialidad esp = new Especialidad();


        esp.Codigo = Codigo;
        esp.especialidad = especialidad;

        string msg = "1";

        if (new ClinicaCES.Logica.LEspecialidad().EspecialidadActualizar(esp))
        {
            //guardo            
        }
        else
        { 
            // no guardo
            msg = "3";
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        EstadoInicial();
    }

    private bool ValidaGuardar()
    {
        bool Valido = true;
        if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
        {
            txtCodigo.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtCodigo.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtEspecialidad.Text.Trim()))
        {
            txtEspecialidad.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtEspecialidad.CssClass = "form_input";
        }

        return Valido;
    }

    protected void gvEspecialidad_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkEspecialidad = (LinkButton)e.Row.FindControl("lnkEspecialidad");
            lnkEspecialidad.CommandArgument = e.Row.RowIndex.ToString();
            //string caca = lnkComprobar.UniqueID;
            e.Row.Attributes.Add("onclick", "postEspecialidadGrid(" + (e.Row.RowIndex + 2 ) + ")");
            //e.Row.Attributes.Add("onclick", "alert('oe')");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");
            

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }
    }

    protected void gvEspecialidad_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Comprobar")
        { 
            GridViewRow row = gvEspecialidad.Rows[int.Parse(e.CommandArgument.ToString())];
            Label lblEspecialidad = (Label)row.FindControl("lblNivel");

            Comprobar(lblEspecialidad.Text.Trim());
        }
    }
}
