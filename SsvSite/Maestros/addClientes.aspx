﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addClientes.aspx.cs" Inherits="Maestros_addClientes" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<table>
    <tr>
        <td>
            <table align="left">
                <tr>
                    <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
                    <td><asp:ImageButton runat="server" ID="imgRetirar"  ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" OnClientClick="return Mensaje(11)" /></td>
                    <td><asp:ImageButton runat="server" Visible="false" ID="imgReactivar" ToolTip="Reactivar" ImageUrl="../icons/refresh.png" onclick="Images_Click"  /></td>                
                    <td><asp:ImageButton runat="server" Enabled="false" ID="imgGuardar" ToolTip="Guardar" OnClientClick="return Mensaje(13)" ImageUrl="../icons/16_save_d.png" onclick="Images_Click" /></td>                
                    <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" ImageUrl="../icons/cancel.png" onclick="Images_Click" /></td>
                    <td><img onclick="redirect('srcClientes.aspx')" src="../icons/magnify.png" id="imgBuscar" runat="server" style="cursor:pointer" /></td>
                </tr>     
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center" width="100%">
                <tr>
                    <td align="right">Nit:</td>
                    <td><asp:TextBox ID="txtNit" MaxLength="13" runat="server" CssClass="form_input"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNit" FilterType="Numbers, Custom" ValidChars="-"  ></ajaxToolkit:FilteredTextBoxExtender>                    
                    </td>
                    <td align="right">Tipo:</td>
                    <td colspan="3"><asp:DropDownList ID="ddlTipo" runat="server"></asp:DropDownList>
                    <asp:LinkButton ID="lnkComprobar" runat="server" onclick="lnkComprobar_Click"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td align="right">Razon Social:</td>
                    <td><asp:TextBox MaxLength="60" Width="150px" ID="txtRazonSocial" runat="server" CssClass="form_input"></asp:TextBox>                                        
                    </td>            
                    <td align="right">Nombre Comercial:</td>
                    <td colspan="3"><asp:TextBox MaxLength="60" Width="150px" ID="txtNomComercial" runat="server" CssClass="form_input"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtNomComercial" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom" ValidChars=" ,ñ,Ñ"  ></ajaxToolkit:FilteredTextBoxExtender>                    
                    </td>
                </tr>
                <tr>   
                    <td align="right">Dirección:</td>
                    <td><asp:TextBox ID="txtDireccion" MaxLength="50" runat="server" CssClass="form_input"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtDireccion" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom" ValidChars=" ,ñ,Ñ,-,#"  ></ajaxToolkit:FilteredTextBoxExtender>                    
                    </td>
                    <td align="right">Telefono:</td>
                    <td colspan="3"><asp:TextBox MaxLength="25" ID="txtTelefono" runat="server" CssClass="form_input"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtTelefono" FilterType="Numbers, Custom" ValidChars=" ,(,),-"  ></ajaxToolkit:FilteredTextBoxExtender>                    
                    </td>
                </tr>
                <tr>
                    <td align="right">Celular:</td>
                    <td><asp:TextBox ID="txtCelular" MaxLength="25" runat="server" CssClass="form_input"></asp:TextBox>                    
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtCelular" FilterType="Numbers" ></ajaxToolkit:FilteredTextBoxExtender>                    
                    </td>
                    <td align="right">Fax:</td>
                    <td colspan="2"><asp:TextBox MaxLength="20" ID="txtFax" runat="server" CssClass="form_input"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtFax" FilterType="Numbers"  ></ajaxToolkit:FilteredTextBoxExtender>                    
                    </td>
                </tr>
                <tr>
                    <td align="right">Email</td>
                    <td colspan="2"><asp:TextBox MaxLength="50" ID="txtEmail" runat="server" CssClass="form_input"></asp:TextBox></td>
                    <td align="center"><asp:CheckBox ID="chkInternet" runat="server" Text="Internet" /></td>
                    <td align="center"><asp:CheckBox ID="chkRemoto" runat="server" Text="Remoto" /></td>
                </tr>
                <tr>
                    <td colspan="5">
                        <table width="100%" align="center">
                            <tr> 
                                <td align="right">Pais:</td>
                                <td><asp:DropDownList ID="ddlPais" runat="server" AutoPostBack="true" onselectedindexchanged="SelectedIndexChanged"></asp:DropDownList></td>
                                <td align="right">Dpto:</td>
                                <td><asp:DropDownList runat="server" ID="ddlDpto"  AutoPostBack="true" onselectedindexchanged="SelectedIndexChanged"></asp:DropDownList></td>
                                <td align="right">Municipio:</td>
                                <td><asp:DropDownList runat="server" ID="ddlMpio"></asp:DropDownList></td>            
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center">
                        <asp:GridView AutoGenerateColumns="False" ID="gvCias" runat="server" AllowPaging="False" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" DataKeyNames="CODIGO,REGIONAL" 
                            onrowdatabound="gvCias_RowDataBound">                   
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <RowStyle CssClass="normalrow" />
                            <AlternatingRowStyle CssClass="alterrow" />
                            <PagerStyle CssClass="cabeza" ForeColor="White"  />
                            <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                            <Columns>     
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCia" runat="server" Checked='<%# Bind("CliXCia") %>' />
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <input type="checkbox" onclick="ChecksGrid('chkCia','<%= gvCias.ClientID %>',<%= hdnFilas.Value %>,this.checked)" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                 </asp:TemplateField>   
                                 <asp:BoundField DataField="NOMBRE" HeaderText="Compañia" />
                                 <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlRegional" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Regional
                                    </HeaderTemplate>
                                 </asp:TemplateField>
                            </Columns>
                        </asp:GridView> 
                        <asp:HiddenField ID="hdnFilas" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    
    <table align="center">
        <tr>
            <td>
                <table align="center">
                    <tr>                       
                        <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                        <td><asp:Button Visible="false" ID="btnReactivar" runat="server" Text="Reactivar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                        <td><asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
                        <td><asp:Button ID="btnGuardar" Enabled="false" runat="server" Text="Guardar" OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>                                                                                
                        <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>                     
                        <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcClientes.aspx')" /> </td>                     
                    </tr>                    
                </table>
            </td>
        </tr>
    </table>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
    <asp:HiddenField ID="hdnNegocio" runat="server" />

            <asp:Panel ID="pnlSeleccion" runat="server">
                <table>
                    <tr>
                        <td>
                            <asp:GridView AutoGenerateColumns="False" ID="gvNegocios" runat="server" AllowPaging="False" 
                            CellPadding="4" ForeColor="#333333" GridLines="None" DataKeyNames="NEGOCIO" 
                                onrowdatabound="gvNegocios_RowDataBound">                   
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle CssClass="normalrow" />
                                <AlternatingRowStyle CssClass="alterrow" />
                                <PagerStyle CssClass="cabeza" ForeColor="White"  />
                                <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                                <Columns>      
                                     <asp:BoundField DataField="NIT" HeaderText="Nit" />
                                     <asp:BoundField DataField="RAZSOCIAL" HeaderText="Razon Social" />
                                     <asp:BoundField DataField="NOMCOMCIAL" HeaderText="Nombre Comercial" />
                                     <asp:BoundField DataField="TIPO" HeaderText="TIPO" />
                                </Columns>
                            </asp:GridView> 
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="btnNuevoNegocio" runat="server" Text="Nuevo" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>                
                    </tr>
                </table>

        </asp:Panel>

        <ajaxToolkit:ModalPopupExtender ID="MPE" runat="server"
        TargetControlID="lnkComprobar"
        PopupControlID="pnlSeleccion"
        BackgroundCssClass="modalBackground" 
        DropShadow="true" 
          />
</asp:Content>

