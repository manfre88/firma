﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addModulo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Modulo", this.Page);
            Procedimientos.comboEstadoInicial(ddlMenu);
            ListarAplicacion();
            if (Request.QueryString["id"] != null)
                Consultar(Request.QueryString["app"], Request.QueryString["id"], Request.QueryString["men"]);

            string script =
                "if(document.getElementById('" + ddlAplicacion.ClientID + "').value != '-1' && document.getElementById('" + ddlMenu.ClientID + "').value != '-1' && trim(document.getElementById('" + txtCodigo.ClientID + "').value) != ''){" +
                    "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            ddlAplicacion.Attributes.Add("onblur", script);
            ddlMenu.Attributes.Add("onblur", script);
            txtCodigo.Attributes.Add("onblur", script);
        }
        litScript.Text = string.Empty;
    }

    private void Guardar(string Aplicacion, string Codigo, string Desc, string Usr, string Menu)
    {
        if (!ValidaGuardar())
            return;

        Modulo men = new Modulo();

        men.Aplicacion = Aplicacion;
        men.Codigo = Codigo;
        men.Mod = Desc;
        men.Usr = Usr;
        men.Menu = Menu;

        string msg = "3";

        if (new ClinicaCES.Logica.LModulo().ModuloActualizar(men))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addModulo.aspx?id=" + Codigo + "&app=" + Aplicacion + "&men=" + Menu + "')", litScript);

    }

    private void CambiarEstado(string Codigo, string App, bool? Estado, string Menu)
    {
        Modulo men = new Modulo();

        men.Estado = Estado;
        men.Aplicacion = App;
        men.Menu = Menu;
        men.Codigo = Codigo;

        string msg = "1";

        if (!new ClinicaCES.Logica.LModulo().ModuloEstado(men))
            msg = "3";
        
        Procedimientos.Script("Mensaje(" + msg + ");redirect('addModulo.aspx?id=" + Codigo + "&app=" + App + "&men=" + Menu + "')", litScript);

    }

    private void Consultar(string aplicacion, string codigo, string Menu)
    {
        Modulo mon = new Modulo();
        mon.Aplicacion = aplicacion;
        mon.Codigo = codigo;
        mon.Menu = Menu;
        DataTable dt = new ClinicaCES.Logica.LModulo().ModuloConsultar(mon);

        txtCodigo.Text = codigo;
        ddlAplicacion.SelectedValue = aplicacion;
        ListarMenu(ddlAplicacion.SelectedValue);
        ddlMenu.SelectedValue = Menu;

        
        btnGuardar.Enabled = true;
        btnRetirar.Enabled = false;
        btnRetirar.Visible = true;
        btnReactivar.Visible = false;
        txtNombre.Enabled = true;
        txtCodigo.Enabled = false;
        ddlAplicacion.Enabled = false;
        ddlMenu.Enabled = false;        

        if (dt.Rows.Count > 0)
        {
            txtNombre.Text = dt.Rows[0]["Modulo"].ToString();
            bool Estado = bool.Parse(dt.Rows[0]["ESTADO"].ToString());
            ddlMenu.SelectedValue = dt.Rows[0]["MENU"].ToString();

            btnGuardar.Enabled = Estado;
            btnRetirar.Enabled = Estado;
            btnRetirar.Visible = Estado;
            btnReactivar.Visible = !Estado;
            imgReactivar.Visible = !Estado;
            imgRetirar.Visible = Estado;
            imgGuardar.Enabled = Estado;
            txtNombre.Enabled = Estado;
            imgGuardar.Enabled = Estado;

            if (!Estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }

            txtNombre.Focus();
        }
    }

    private void ListarAplicacion()
    {
        Procedimientos.LlenarCombos(ddlAplicacion, new ClinicaCES.Logica.LModulo().AplicacionListar(), "CODIGO", "CODIGO");
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(ddlAplicacion.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(),ddlMenu.SelectedValue);
        else if (sender.Equals(imgNuevo) || sender.Equals(imgCancelar))
            Response.Redirect("addModulo.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, false,ddlMenu.SelectedValue);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, true,ddlMenu.SelectedValue);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(ddlAplicacion.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(),ddlMenu.SelectedValue);
        else if (sender.Equals(btnNuevo) || sender.Equals(btnCancelar))
            Response.Redirect("addModulo.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, false, ddlMenu.SelectedValue);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlAplicacion.SelectedValue, true, ddlMenu.SelectedValue);
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(ddlAplicacion.SelectedValue, txtCodigo.Text.Trim(), ddlMenu.SelectedValue);
    }

    private void ListarMenu(string App)
    {
        MMenu men = new MMenu();
        men.Aplicacion = App;
        Procedimientos.LlenarCombos(ddlMenu, Procedimientos.dtFiltrado("MENU","ESTADO = 1", new ClinicaCES.Logica.LMenu().MenuConsultar(men)), "CODIGO", "MENU");
    }

    protected void ddlAplicacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAplicacion.SelectedIndex != 0)
        {
            ListarMenu(ddlAplicacion.SelectedValue);
        }
    }

    private bool ValidaGuardar()
    {
        TextBox[] txt = 
        {
            txtCodigo, txtNombre
        };

        DropDownList[] ddl = 
        {
            ddlAplicacion,ddlMenu
        };

        return Procedimientos.ValidaGuardar(txt, ddl);
    }
}
