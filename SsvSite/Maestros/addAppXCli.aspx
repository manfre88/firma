﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addAppXCli.aspx.cs" Inherits="Maestros_addAppXCli" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="100%">
    <tr>
        <td>
            <table align="left">
                <tr>
                    <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
                    <td><asp:ImageButton Enabled="false" runat="server" ID="imgRetirar"  ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" OnClientClick="return Mensaje(11)" /></td>
                    <td><asp:ImageButton runat="server" Visible="false" ID="imgReactivar" ToolTip="Reactivar" ImageUrl="../icons/refresh.png" onclick="Images_Click"  /></td>                
                    <td><asp:ImageButton Enabled="false" runat="server" ID="imgGuardar" ToolTip="Guardar" OnClientClick="return Mensaje(13)" ImageUrl="../icons/16_save_d.png" onclick="Images_Click" /></td>                
                    <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" ImageUrl="../icons/cancel.png" onclick="Images_Click" /></td>
                    <td><img onclick="redirect('srcAppXCli.aspx')" src="../icons/magnify.png" id="imgBuscar" runat="server" style="cursor:pointer" /></td>
                </tr>     
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>
                    <td align="right">Cliente:</td>
                    <td><asp:DropDownList ID="ddlCliente" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="right">Aplicación:</td>
                    <td><asp:DropDownList ID="ddlAplicacion" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="right">Instalo:</td>
                    <td><asp:DropDownList ID="ddlUsrs" runat="server"></asp:DropDownList>
                    <asp:LinkButton ID="lnkComprobar" runat="server"
                            onclick="lnkComprobar_Click"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td align="right">Fecha:</td>
                    <td><asp:TextBox MaxLength="10" onkeyup="mascara(this,'/',true)" Width="70px" ID="txtFecha" runat="server" CssClass="form_input"></asp:TextBox>  
                    <img style="cursor:pointer" runat="server" id="imgCalendar" src="../img/calendar.gif" />
                    <ajaxToolkit:CalendarExtender ID="clnFecha" runat="server" PopupButtonID="imgCalendar" TargetControlID="txtFecha" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



<table align="center">
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                    <td><asp:Button Visible="false" ID="btnReactivar" runat="server" Text="Reactivar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                    <td><asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
                    <td><asp:Button ID="btnGuardar" Enabled="false" runat="server" Text="Guardar" OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>                                                                                
                    <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>                     
                    <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcAppXCli.aspx')" /> </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnNegocio" runat="server" />
</asp:Content>

