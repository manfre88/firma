﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcRegionales : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Regionales", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("REGIONAL", "Regional", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void Consultar()
    {
        Regionales reg = new Regionales();
        DataTable dtReg = new ClinicaCES.Logica.LRegionales().RegionalesConsultar(reg);
        ViewState["dtReg"] = dtReg;
        ViewState["dtPaginas"] = dtReg;
        Procedimientos.LlenarGrid(dtReg, gvRegionales);
    }

    protected void gvRegionales_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvRegionales.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addRegionales.aspx?reg=" + e.Row.Cells[1].Text.Trim() + "&comp=" + gvRegionales.DataKeys[e.Row.RowIndex]["COMPANIA"].ToString() + "')");
                e.Row.Cells[4].Text = e.Row.Cells[4].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            }

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkReg = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvRegionales.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkReg.Enabled = false;

            chkReg.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlRegional, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LRegionales().RegionalRetirar(xmlRegional, Usuario))
        {
            msg = "7";

            Regionales reg = new Regionales();
            DataTable dtReg = new ClinicaCES.Logica.LRegionales().RegionalesConsultar(reg);
            ViewState["dtReg"] = dtReg;
            Filtrar();
        }
        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND REGIONAL LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtReg = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtReg"]);
        Procedimientos.LlenarGrid(dtReg, gvRegionales);

        ViewState["dtPaginas"] = dtReg;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("REGIONAL", "Regional", pnlNombre, txtBusquedaNombre);
    }

    private string xmlRegional()
    {
        string reg;
        string comp;
        string xmlRegional = "<Raiz>";

        for (int i = 0; i < gvRegionales.Rows.Count; i++)
        {
            GridViewRow row = gvRegionales.Rows[i];
            CheckBox chkReg = (CheckBox)row.FindControl("chkEliminar");
            if (chkReg.Checked)
            {
                reg = row.Cells[1].Text.Trim();
                comp = row.Cells[3].Text.Trim();
                xmlRegional += "<Datos CODIGO='" + reg + "' COMPANIA='" + comp + "' />";
            }
        }

        return xmlRegional += "</Raiz>";
    }

    protected void gvRegionales_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegionales.PageIndex = e.NewPageIndex;
        gvRegionales.DataSource = ViewState["dtPaginas"];
        gvRegionales.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlRegional(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addRegionales.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("REGIONAL", "Regional", pnlNombre, txtBusquedaNombre);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "REGIONAL":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
