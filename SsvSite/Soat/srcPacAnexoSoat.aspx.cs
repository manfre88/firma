﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
public partial class Soat_srcPacAnexoSoat : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblDescripcion.Text = "Busqueda de pacientes de SOAT";
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Busqueda de pacientes de SOAT", this.Page);
            Procedimientos.LlenarCombos(ddlTipoId, new ClinicaCES.Logica.LMaestros().ListaTiposIdentificacion(), "TIPOID", "ID");
        }
        this.Form.DefaultButton = btnConsultar.UniqueID;
        litScript.Text = string.Empty;
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        Consultar(txtFecha.Text, txtFechaF.Text, ddlTipoId.SelectedValue.ToString(), txtId.Text);
    }

    private void Consultar(string FechaI, string FechaF, string TipoId, string Id)
    {

        if (FechaI == "" | FechaF == "" | TipoId == "" | Id == "")
        {
            pnlInforme.Visible = false;
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
        }

        else
        {
            DataSet ds = new ClinicaCES.Logica.LBusquedaPacientes().BusquedaSoat(FechaI, FechaF, TipoId, Id.Trim());
            DataTable dtInforme = ds.Tables[0];
            //DataTable dtPagina = Procedimientos.dtFiltrado("IDENTIFICACION", "", dtInforme);
            string[] campo = { "IDENTIFICACION" };

            if (dtInforme.Rows.Count > 0)
            {
                ViewState["dtInformes"] = dtInforme;
                //ViewState["dtPaginas"] = dtPagina;
                Procedimientos.LlenarGrid(dtInforme, gvInforme);
                pnlInforme.Visible = true;
                string[] campos = { "NOMCOMCIAL" };
            }
            else
            {
                pnlInforme.Visible = false;
                Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
            }
        }

    }





    private bool ValidaSemana()
    {
        bool valido = true;

        if ((txtFecha.Text != "") && (txtFechaF.Text != ""))
        {
            DateTime FechaIni = Convert.ToDateTime(txtFecha.Text);
            DateTime FechaFin = Convert.ToDateTime(txtFechaF.Text);

            if (FechaIni.CompareTo(FechaFin) == 1)
            {
                valido = false;
            }


            if (!valido)
            {
                txtFecha.Text = string.Empty;
                txtFechaF.Text = string.Empty;

            }

        }

        return valido;
    }


    protected void txtFechaF_TextChanged1(object sender, EventArgs e)
    {
        if (!ValidaSemana())
        {
            Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
            pnlInforme.Visible = false;
        }
    }


    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtFechaF.Text.Trim()))
            if (!ValidaSemana())
            {
                Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
                pnlInforme.Visible = false;
            }
    }


    protected void gvInforme_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInforme.PageIndex = e.NewPageIndex;
        gvInforme.DataSource = ViewState["dtInformes"];
        gvInforme.DataBind();
        
    }


    protected void gvInforme_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
            if (e.Row.Cells[0].Text.Trim() == "SI")
            {
                DataTable dt = (DataTable)ViewState["dtInformes"];
                string cookie = ddlTipoId.SelectedValue.ToString() + ";" + e.Row.Cells[1].Text.Trim() + "|" + dt.Rows[e.Row.RowIndex][4].ToString() + "|" + dt.Rows[e.Row.RowIndex][5].ToString();
                //string cookie = e.Row.Cells[1].Text.Trim();
                e.Row.Attributes.Add("onclick", "redirect('../Anexos/AnexoSoat.aspx?cookieSoat=" + Procedimientos.cifrar(cookie) + "')");
            }


        }
    }
}